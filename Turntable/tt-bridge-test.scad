/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=360;
include <../lib/shapes.scad>

L=110;
W=12;

BRIDGE=0;
BOTTOM=1;

if( BOTTOM ) {
  translate([0,L/2,0]) {
    // top ring
    difference() {
      translate([L/2,L/2,11]) cylinder(1, L/2+3, L/2+3);
      translate([L/2,L/2,11]) cylinder(1, L/2+1, L/2+1);
      for( b =[0:47] ) {
        translate([L/2,L/2,12-.2]) rotate( (360/48) * b ) cube([.4, L/2+3, .2]);
      }
    }
    difference() {
      translate([L/2,L/2,0]) cylinder(12, L/2+2, L/2+2);
      translate([L/2,L/2,1.4]) cylinder(11, L/2-1, L/2-1);
      translate([L/2,L/2,10]) cylinder(11, L/2+1, L/2+1);
      // draaipunt
      translate([L/2,L/2,0]) cylinder(8, 5, 5);
      // motor bevestiging
      translate([L/2-8,L/2-35/2,0]) cylinder(1.4, 1.6, 3.2);
      translate([L/2-8,L/2-35/2+35,0]) cylinder(1.4, 1.6, 3.2);
  
      // HALL opening
      translate([W/2+5,L/2,0]) cylinder(6, 2.5, 2.5);
      translate([3,L/2-2.5,.6]) cube([8, 5, 1]);
  
      // tracks
      for( b =[0:47] ) {
        translate([L/2,L/2,12-.2]) rotate( (360/48) * b ) cube([.4, L/2+3, .2]);
      }
    }  
    
    
    
  }
}

if( BRIDGE ) {
  translate([0,0,0]) {
    // brug dek
    difference() {
      translate([0,-2,0]) cube([L, W+4, 1]);
      translate([L/2,W/2,0]) difference() {
        translate([0,0,0]) cylinder(2, L/2+4, L/2+4);
        translate([0,0,0]) cylinder(2, L/2, L/2);
      }
      translate([0,W/2-.3,0]) cube([L, .6, .4]);
      translate([W/2,W/2,0]) cylinder(6, 1.6, 1.6);
    }    
    
    difference() {
      translate([0,0,0]) cube([L, W, 2]);
      translate([L/2,W/2,0]) difference() {
        translate([0,0,0]) cylinder(2, L/2+4, L/2+4);
        translate([0,0,0]) cylinder(2, L/2, L/2);
      }
      translate([0,W/2-.3,0]) cube([L, .6, .4]);
      translate([W/2,W/2,0]) cylinder(6, 1.6, 1.6);
    }
  
    // draaipunt
    difference() {
      translate([L/2,W/2,0]) cylinder(7.6, W/2, W/2);
      translate([L/2-2.6,W/2-1.6,2]) cube([5.2, 3.2, 7]);
    }
    
    // magneet houder
    difference() {
      translate([W/2,W/2,0]) cylinder(6, 3, 3);
      translate([W/2,W/2,0]) cylinder(6, 1.6, 1.6);
    }


    // onder constructie
    translate([L/2-10,0,2]) cube([20, 1, 5]);
    translate([L/2-10,W-1,2]) cube([20, 1, 5]);

    translate([L-4,0,2]) rotate([0,0,90]) prism(1, L/2-14, 5);
    translate([4,1,2]) rotate([180,180,90]) prism(1, L/2-14, 5);


    translate([L-4,W-1,2]) rotate([0,0,90]) prism(1, L/2-14, 5);
    translate([4,W,2]) rotate([180,180,90]) prism(1, L/2-14, 5);
  }
}
