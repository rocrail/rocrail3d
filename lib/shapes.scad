/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/

module ledLens(d) {
  difference() {
    translate([0,0,0]) cylinder(1.8,d/2,d/2);
    translate([0,0,.4]) cylinder(1.8,(d-2)/2,(d-2)/2);
  }
}

module bogieSpring(hight,radius, cnt) {
  module line3D(p1, p2, thickness, fn = 24) {
      $fn = fn;

      hull() {
          translate(p1) sphere(thickness / 2);
          translate(p2) sphere(thickness / 2);
      }
  }

  module polyline3D(points, thickness, fn) {
      module polyline3D_inner(points, index) {
          if(index < len(points)) {
              line3D(points[index - 1], points[index], thickness, fn);
              polyline3D_inner(points, index + 1);
          }
      }

      polyline3D_inner(points, 1);
  }

  r = radius;
  h = 2.2;
  fa = 5;
  circles = cnt;

  points = [
      for(a = [0:fa:360 * circles]) 
          [r * cos(a), r * sin(a), h / (360 / fa) * (a / fa)]
  ];
  polyline3D(points, 2, 3);
}



module tube(R,r) {
  fn = 128;
  outer_radius = R;
  bending_radius = r;
  wall_thickness = 2;
  height = 3.1415 * bending_radius / 2;

  translate([outer_radius, 0, 0])
  rotate([90, 0, 180])
  intersection()
  {
    rotate_extrude($fn = fn, convexity = 4)
    translate([bending_radius, 0, 0])
    difference()
    {
      circle(r = outer_radius, $fn = fn);
      circle(r = outer_radius-wall_thickness, $fn = fn);
    }
    translate([0,0,-outer_radius-1])
    cube([outer_radius + bending_radius + 1, 
    outer_radius + bending_radius + 1, 
    outer_radius * 2 + 2]);
  }      
}



module windowCut(width, height, z, r) {
  translate([0,r,0]) cube([width, height-(2*r), z]);
  translate([r,0,0]) cube([width-(2*r), height, z]);

  translate([r,height-r,0]) cylinder(z, r, r);
  translate([width-r,height-r,0]) cylinder(z, r, r);

  translate([r,r,0]) cylinder(z, r, r);
  translate([width-r,r,0]) cylinder(z, r, r);
}



module prism(l, w, h){
  polyhedron(
    points=[[0,0,0], [l,0,0], [l,w,0], [0,w,0], [0,w,h], [l,w,h]],
    faces=[[0,1,2,3],[5,4,3,2],[0,4,5,1],[0,3,4],[5,2,1]]
  );
}

module spring(x,y,h,t,c){
  translate([x,y,0]) {
    difference() {
      translate([2,h+1,0]) cylinder(t, 2, 2);    
      translate([2,h+1,0]) cylinder(t, 1, 1);    
      translate([0,h-1,0]) cube([4, 2, t]);
    }
    difference() {
      translate([0,3,0]) cube([4, h-2, t]);
      translate([1,3,0]) cube([2, h-2, t]);
    }
  }
  // Oposite circle
  if( c ) {
    translate([x+3,2,0]) {
      difference() {
        translate([2,1,0]) cylinder(t, 2, 2);    
        translate([2,1,0]) cylinder(t, 1, 1);    
        translate([0,1,0]) cube([4, 2, t]);
      }
    }
  }
}


module rcube(L,W,H,r) {
  translate([r,r,0]) rotate([0,0,0]) cylinder(L,r,r);
  translate([r,H-r,0]) rotate([0,0,0]) cylinder(L,r,r);
  translate([W-r,r,0]) rotate([0,0,0]) cylinder(L,r,r);
  translate([W-r,H-r,0]) rotate([0,0,0]) cylinder(L,r,r);
  translate([0,r,0]) cube([W,H-r-r,L]);
  translate([r,0,0]) cube([W-r-r,H,L]);
}


module roundedcube(x, y, z, r){
  translate([r,r,0]) cylinder(z, r, r);
  translate([x-r,r,0]) cylinder(z, r, r);
  translate([r,y-r,0]) cylinder(z, r, r);
  translate([x-r,y-r,0]) cylinder(z, r, r);

  translate([r,0,0]) cube([x-2*r, y, z]);
  translate([0,r,0]) cube([x, y-2*r, z]);
}

module holder(h=8) {
  difference() {
    translate([0,0,0]) cube([6, 5, h]);
    translate([1,1,0]) cube([4, 4, h]);
  }  
}

module ellipse(h, r, w){
  linear_extrude(h) resize([r*2,w*2])circle(d=r*2);
}


module triangle(h, base, height){
  linear_extrude(h) polygon(points=[[0,0],[base,0],[base/2,height]]);
}


module sector(h, d, a1, a2) {
    if (a2 - a1 > 180) {
        difference() {
            cylinder(h=h, d=d);
            translate([0,0,-0.5]) sector(h+1, d+1, a2-360, a1); 
        }
    } else {
        difference() {
            cylinder(h=h, d=d);
            rotate([0,0,a1]) translate([-d/2, -d/2, -0.5])
                cube([d, d/2, h+1]);
            rotate([0,0,a2]) translate([-d/2, 0, -0.5])
                cube([d, d/2, h+1]);
        }
    }
}    


//The top point will be centred
module pyramid(w, l, h) {
	mw = w/2;
	ml = l/2;
	polyhedron(points = [
		[0,  0,  0],
		[w,  0,  0],
		[0,  l,  0],
		[w,  l,  0],
		[mw, ml, h]
	], triangles = [
		[4, 1, 0],
		[4, 3, 1],
		[4, 2, 3],
		[4, 0, 2],
		//base
		[0, 1, 2],
		[2, 1, 3]
	]);
}



module RoundedPolygon(P,r,fn)
{
    v = rounded_polygon_v(P,r,fn);
    polygon(v);
}

function rounded_polygon_v(P,r,fn) =
let
(
    step = 360 / fn,
    n = len(P),
    o_v = [ for(i=[0:n-1]) atan2(P[(i+1)%n][1] - P[i][1], P[(i+1)%n][0] - P[i][0]) + 90 + 360 ]
)
[ 
        for(i=[0:n-1])
            let 
            (
                n1 = i, 
                n2 = (i+1)%n,
                w1 = o_v[n1],
                w2 = (o_v[n2] < w1) ? o_v[n2] : o_v[n2] - 360
            )
            for (w=[w1:-step:w2]) 
                [ cos(w)*r+P[n2][0], sin(w)*r+P[n2][1] ] 
] ;





module LEDHOLES(x,y) {
  translate([x,y-2.5/2,0]) cylinder(10,.75,.75);
  translate([x,y+2.5/2,0]) cylinder(10,.75,.75);
}

module LEDCUP(x,y,cut) {
  difference() {
    translate([x,y,0]) cylinder(3.5,4,4);
    if( !cut ) {
      translate([x,y,2]) cylinder(5,3,3);
      translate([x,y-2.5/2,0]) cylinder(4,.75,.75);
      translate([x,y+2.5/2,0]) cylinder(4,.75,.75);
    }
  }
}
