fn=128;

linear_extrude(height=2)
    RoundedPolygon([[0,0],[0,70],[50,0]],2,fn);

module RoundedPolygon(P,r,fn)
{
    v = rounded_polygon_v(P,r,fn);
    polygon(v);
}

function rounded_polygon_v(P,r,fn) =
let
(
    step = 360 / fn,
    n = len(P),
    o_v = [ for(i=[0:n-1]) atan2(P[(i+1)%n][1] - P[i][1], P[(i+1)%n][0] - P[i][0]) + 90 + 360 ]
)
[ 
        for(i=[0:n-1])
            let 
            (
                n1 = i, 
                n2 = (i+1)%n,
                w1 = o_v[n1],
                w2 = (o_v[n2] < w1) ? o_v[n2] : o_v[n2] - 360
            )
            for (w=[w1:-step:w2]) 
                [ cos(w)*r+P[n2][0], sin(w)*r+P[n2][1] ] 
] ;
