/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

DIN4=true;
WIO03=true;
L=100; // WIO03=94 WIOpico01=100
W=72;

// Box
difference() {
  translate([0,-2,0]) cube([W, L, 44]);
  translate([2,0,2]) cube([W-4, L-4, 42]);
  // flip switch
  rotate([90,0,0]) translate([W/2,25,0]) cylinder(2, 3.1, 3.1);
  // 3mm LED
  rotate([90,0,0]) translate([W/2+15,25,0]) cylinder(2, 1.6, 1.6);

  // DIN-5 chassis
  rotate([0,90,0]) translate([-14,44,0]) cylinder(DIN4?100:2, 8, 8);
  rotate([0,90,0]) translate([-14,68,0]) cylinder(DIN4?100:2, 8, 8);

  // XT60 chassis
  translate([29.8,-2,6]) cube([12.2, 2, 8.2]);
  rotate([90,0,0]) translate([42.2,10.1,0]) cylinder(2, 4.1, 4.1);

}

// Battery Box
difference() {
  translate([(W-41)/2,L-69.5-10,0]) cube([41, 71, 14]);
  translate([(W-37)/2,L-69.5-10+2,2]) cube([37, 67, 12]);
}


if( !WIO03 ) {
  // WIO-T1: Four M3 screw terminals:
  translate([5.5,7,2]){
    difference() {
      cylinder(22, 4, 4);
      cylinder(22, 1.5, 1.5);
    }
  }
  translate([56.5,7,2]){
    difference() {
      cylinder(22, 4, 4);
      cylinder(22, 1.5, 1.5);
    }
  }
  translate([5.5,83,2]){
    difference() {
      cylinder(22, 4, 4);
      cylinder(22, 1.5, 1.5);
    }
  }
  translate([56.5,83,2]){
    difference() {
      cylinder(22, 4, 4);
      cylinder(22, 1.5, 1.5);
    }
  }
}
else {
  // WIO-03: 36 x 58 hole 1.5
  // WIOpico-01: 40 x 73 hole 1.2
  PCBw = 40;
  PCBh = 73;
  PCBs = 1.2; // screw hole
  
  translate([(W-PCBw)/2,L-7.5,2]){
    difference() {
      cylinder(22, 4, 4);
      cylinder(22, PCBs, PCBs);
    }
  }
  translate([(W-PCBw)/2+PCBw,L-7.5,2]){
    difference() {
      cylinder(22, 4, 4);
      cylinder(22, PCBs, PCBs);
    }
  }
  
  translate([(W-PCBw)/2,L-7.5-PCBh,2]){
    difference() {
      cylinder(22, 4, 4);
      cylinder(22, PCBs, PCBs);
    }
    translate([-(W-PCBw)/2,-1,0]) cube([(W-PCBw)/2-2, 2, 16]);
  }
  translate([(W-PCBw)/2+PCBw,L-7.5-PCBh,2]){
    difference() {
      cylinder(22, 4, 4);
      cylinder(22, PCBs, PCBs);
    }
    translate([2,-1,0]) cube([(W-PCBw)/2-2, 2, 16]);
  }
  
/*  
  // WIO03
  translate([(W-PCBw)/2-4.5,86.5-PCBh-2,2]){
    cube([4, 4, 22]);
  }  
  translate([(W-PCBw)/2+PCBw+.5,86.5-PCBh-2,2]){
    cube([4, 4, 22]);
  }
*/  
}



