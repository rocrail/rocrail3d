/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../lib/shapes.scad>


L=83.4;
W=47.2;
H=2;

BASE=0;
TOP=1;
BUTTON=0;
LED=0;
LED2=0;

// PCB screw mount
module mount(x,y,diff) {
  difference() {
    if( !diff )
     translate([x,y,0]) cylinder(2*H, 3.5, 3.5);
     translate([x,y,diff?.4:0]) cylinder(2.1*H, 1, 1);
  }
}

// LED light: print with transparent filament
if( LED ) translate([20,-20,0]) {
  cylinder(5.0,1.9,1.9);
  cylinder(1.6,3.5,3.5);
}
if( LED2 ) translate([20,-20,0]) {
  cylinder(4.2,1.9,1.9);
  cylinder(2.2,2.5,2.5);
  translate([0,0,4.2]) sphere(1.9);
}

// The BOOTSEL button
if( BUTTON ) translate([0,-20,0]) {
  cylinder(6,3.5/2,3.5/2);
  cylinder(1,3,3);

  difference() {
    translate([10,0,0]) cylinder(2.4,2.5,2.5);
    translate([10,0,1]) cylinder(2,3.5/2,3.5/2);
  }
}


// Top of the box
if( TOP ) translate([0,0,0]) {
  
  // power side top
  difference() {
    translate([-4,-4,21]) cube([14, W+8, 3]);
    translate([-4,W/2-23/2+1-1,20]) cube([L+8, 21, 2]);
    // USB cut
    translate([-4, W/2-14/2-1, 24-8]) cube([14, 13, 10]);
    // LED hole
    translate([19,16,0]) cylinder(40,1.95,1.95);
  }

  // logo side top
  difference() {
    translate([L-8-9,-4,21]) cube([12+9, W+8, 3]);
    translate([-4,W/2-23/2+1-1,20]) cube([L+8, 21, 2]);
  }
  
  // middle top
  difference() {
    translate([-4,W/2-23/2-2+1,21]) cube([L+8, 23, 3]);
    translate([-4,W/2-23/2-2+2,20]) cube([L+8, 21, 2]);

    // button hole
    translate([25.5,18.5,0]) cylinder(40,2,2);
    // LED hole
    translate([18.5,16,0]) cylinder(40,1.95,1.95);

    // Logos:
    translate([L-24,32,24-.4]){
      rotate([0,0,270]) linear_extrude(0.4) text("WIO",7);
    }
    translate([L-32,31,24-.4])
      rotate([0,0,270]) linear_extrude(0.4) text("C",7);
    translate([L-40,31,24-.4])
      rotate([0,0,270]) linear_extrude(0.4) text("A",7);
    translate([L-48,31,24-.4])
      rotate([0,0,270]) linear_extrude(0.4) text("N",7);

      // USB cut
    translate([-4,W/2-14/2-1,24-8]) cube([14, 14, 10]);
  }
  
  // power sides
  translate([-4,-4,0]) cube([14, 2, 24]);
  translate([-4,W+2,0]) cube([14, 2, 24]);

  // logo sides
  translate([L-17,-4,0]) cube([21, 2, 24]);
  translate([L-17,W+2,0]) cube([21, 2, 24]);


  // bottom sides 
  difference() {
    translate([-4,-4,0]) cube([L+8, 2, 10]);
        
    translate([20,-3.6,2])
    rotate([90,0,0]) linear_extrude(0.4) text("J1",5); 
    translate([22+28,-3.6,2])
    rotate([90,0,0]) linear_extrude(0.4) text("J2",5); 
  }



  difference() {
    translate([-4,W+2,0]) cube([L+8, 2, 10]);
    
    translate([24,W+3.6,2])
    rotate([90,0,180]) linear_extrude(0.4) text("I2C",5); 
  
    translate([38+3*8,W+3.6,2])
    rotate([90,0,180]) linear_extrude(0.4) text("4",5); 
    translate([38+2*8,W+3.6,2])
    rotate([90,0,180]) linear_extrude(0.4) text("3",5); 
    translate([38+1*8,W+3.6,2])
    rotate([90,0,180]) linear_extrude(0.4) text("2",5); 
    translate([38,W+3.6,2])
    rotate([90,0,180]) linear_extrude(0.4) text("1",5); 
  }


  // power supply front
  difference() {
    translate([-4,-4,0]) cube([2, W+8, 24]);
    // base cut
    translate([-10,W/2-36/2,0]) cube([L+4+16, 36, 2]);
    translate([-10,W/2-36/2,0]) cube([L+4+16, 2, 6]);
    translate([-10,W/2+36/2-2,0]) cube([L+4+16, 2, 6]);
    // CAN connector cut
    translate([-10,18,0]) cube([L+4+16, W-2*10-9, 12.5]);

    translate([-3.6,45.5,8]) rotate([90,0,270]) linear_extrude(0.4) text("+ L              H -",4); 

    // USB cut
    translate([-5,W/2-14/2-1,24-10]) cube([10, 13, 10]);
    translate([-3.6,14,15]) rotate([90,0,270]) linear_extrude(0.4) text("USB",5); 
  }

  // logo front
  translate([L+6,0,0]) difference() {
    translate([-4,-4,0]) cube([2, W+8, 24]);
    // base cut
    translate([-10,W/2-36/2,0]) cube([L+4+16, 36, 2]);
    translate([-10,W/2-36/2,0]) cube([L+4+16, 2, 6]);
    translate([-10,W/2+36/2-2,0]) cube([L+4+16, 2, 6]);
    // Power connector cut
    translate([-10,14,0]) cube([L+4+16, W-2*10-16, 12.5]);

    translate([L+3.6,12.5,13]) rotate([90,0,90]) linear_extrude(0.4) text("+ -",8); 
  }
  translate([L+3.9,12.5,13]) rotate([90,0,90]) linear_extrude(0.4) text("+ -",8); 


}


// Base of the box
if( BASE ) translate([0,0,0]) {
  // PCB room
  difference() {
    translate([-2,-2,0]) cube([L+4, W+4, 3*H]);
    translate([0,0,H]) cube([L, W, 3*H]);
    // CAN connector cut
    translate([-2,18,2*H+1]) cube([4, W-28, 3*H+6]);
    // power connector cut
    translate([L,13,2*H+1]) cube([4, W-35, 3*H+6]);

    mount(4.4,4.8,true);
    mount(4.4,W-4.0,true);
    mount(L-5.0,4.8,true);
    mount(L-5.0,W-4.0,true);
  }

  // PCB screw mount
  mount(4.4,4.8,false);
  mount(4.4,W-4.0,false);
  mount(L-5.0,4.8,false);
  mount(L-5.0,W-4.0,false);
  
  // base screw mount 
  difference() {
    translate([-10,W/2-36/2,0]) cube([L+4+16, 36, 2]);
    translate([-7,15,0]) cylinder(2*H, 1.25, 1.25);
    translate([L+7,W-15,0]) cylinder(2*H, 1.25, 1.25);
  }

  translate([L+10,(W/2)-18,2]){
    rotate(90) prism(2, 8, 4);
  }
  translate([L+10,(W/2)+16,2]){
    rotate(90) prism(2, 8, 4);
  }
  translate([-10,(W/2)-16,2]){
    rotate(270) prism(2, 8, 4);
  }
  translate([-10,(W/2)+18,2]){
    rotate(270) prism(2, 8, 4);
  }
  
}

  
  