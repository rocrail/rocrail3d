/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2025 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../lib/shapes.scad>

// PCB size
L=64;
W=56;
H=2;
MH=3.5; // mounting hole from sides

Vsize=30;
Vh=10;

SHOW_PCB=0;
SHOW_FAN=0;
SHOW_TOP=0;

BOTTOM=1;
TOP=0;
FEET=0;
FANROAST=0;
MOUNTING=1;


module pss(l) {
  difference() {
    translate([0,0,0]) cube([l, 6, 10]);
    translate([-1,2,2]) cube([l+2, 6, 10]);
  }
}

module connector(l) {
  difference() {
    translate([0,0,0]) cube([l, 9.5, 6]);
    translate([1,2,1]) cube([l-2, 8, 4]);
  }
}

module cool(x,y) {
  color("dimgray") difference() {
    translate([x,y,H+.5]) cube([10, 13, 12]);
    translate([x-1,y+3,H+.5]) cube([12, 7, 11]);
    translate([x-1,y-1,H+.5]) cube([12, 3, 11]);
    translate([x-1,y+11,H+.5]) cube([12, 3, 11]);
  }
}

module pcb(x,y,z) {
  color("green") translate([x,y,z]) difference() {
    translate([0,0,0]) cube([L,W,H]);
    translate([MH,MH,-1]) cylinder(5, 1.6, 1.6);
    translate([L-MH,MH,-1]) cylinder(5, 1.6, 1.6);
    translate([MH,W-MH,-1]) cylinder(5, 1.6, 1.6);
    translate([L-MH,W-MH,-1]) cylinder(5, 1.6, 1.6);
  }
  translate([x,y,z]) {
    // Elcos
    color("cyan") {
      translate([57.5,13,H+.5]) cylinder(24, 5.5, 5.5);
      translate([50,7,H+.5]) cylinder(10, 2.5, 2.5);
      translate([11,50,H+.5]) cylinder(10, 2.5, 2.5);
      translate([17,50,H+.5]) cylinder(10, 2.5, 2.5);
    }
    // Rs
    color("silver") {
      translate([33,40,H+.5+12]) rotate([0,90,0]) cylinder(17, 3.5, 3.5);
      translate([29,15.5,H+.5+12]) rotate([0,90,0]) cylinder(17, 3.5, 3.5);
    }
    // Diodes
    color("orange") translate([60,25,H+.5+2.75]) rotate([270,90,0]) cylinder(9.5, 2.75, 2.75);
    // H-Bridge
    color("dimgray") translate([21,22,H+.5]) cube([16, 11, 4]);
    cool(8,21);
    cool(40,21);
    // Connectors
    color("gold") {
      // ser1
      translate([9,5,H+.5]) rotate([90,0,0]) pss(10);
      // J1
      translate([20,5,H+.5]) rotate([90,0,0]) pss(26);
      
      translate([7,38,H+.5]) rotate([0,0,90]) pss(8);
      translate([40.5,48.5,H+.5]) pss(5.5);
      
      translate([22.5,47,H+.5]) connector(17);
      translate([47.5,47,H+.5]) connector(9);
    }
  }
}


module fancut(x,y,z,rx,ry,rz,diff,hole,s) {
  translate([Vsize/2,Vsize/2,-5]) cylinder(Vh+10, Vsize/2.2, Vsize/2.2);
  translate([MH,MH,-5]) cylinder(20, hole, hole);
  translate([Vsize-MH,MH,-5]) cylinder(20, hole, hole);
  translate([MH,Vsize-MH,-5]) cylinder(20, hole, hole);
  translate([Vsize-MH,Vsize-MH,-5]) cylinder(20, hole, hole);

  if( s ) {
    translate([Vsize-MH,MH,-2.1]) cylinder(1, hole*2, hole);
    translate([Vsize-MH,Vsize-MH,-2.1]) cylinder(1, hole*2, hole);
  }
}

module fan(x,y,z,rx,ry,rz,diff,hole,s) {
  translate([x,y,z])  {
    rotate([rx,ry,rz]) {
      if(diff) {
        fancut(x,y,z,rx,ry,rz,diff,hole,s);
      }
      else {
        color("brown") difference() {
          translate([0,0,0]) cube([Vsize,Vsize,Vh]);
          fancut(x,y,z,rx,ry,rz,diff,hole,s);
        }
      }
    }
  }
}



module feet(x,y,z) {
  translate([x,y,z]) cylinder(2, 5, 5);
  translate([x,y,z]) cylinder(4, 1.15, 1.15);
}
if( FEET ) {
  feet(0,-50,0);
}


module ring(r,d,h) {
  difference() {
    translate([0,0,0]) cylinder(h, r, r);
    translate([0,0,-1]) cylinder(h+2, r-d, r-d);
  }
}

module fanroast() {
  color("blue") 
  rotate([0,0,90]) {
    difference() {
      translate([-(W-Vsize)/2,6,0]) cube([W,Vsize-6,2]);
      fan(0,0,-2, 0,00,0,true,1.6,true);

      translate([3,Vsize-3,0]) cylinder(1, 1.5*2, 1.5);
      translate([Vsize-3,Vsize-3,0]) cylinder(1, 1.5*2, 1.5);
    }
    
    difference() {
      translate([-(W-Vsize)/2,0,2]) cube([W,16,1.2]);
      translate([-1,0,0]) cube([Vsize+2,16,6]);
    }
    
    
    
    translate([-(W-Vsize)/2 + W/2, Vsize/2, 0]) {
      translate([0,0,0]) cylinder(1, 3, 3);
      translate([0,0,0]) ring(Vsize/2-3, 1, 1);
      translate([0,0,0]) ring(Vsize/2-6, 1, 1);
      translate([0,0,0]) ring(Vsize/2-9, 1, 1);

      translate([-(Vsize/2),-.5,0]) cube([Vsize, 1, 1]);
      translate([-.5, -(Vsize/2)+3,0]) cube([1, Vsize-3, 1]);
  
    }
  }
  
}
if( FANROAST ) {
  translate([-80,-70,0]) fanroast();
}


module top() {
  difference() {
    translate([0,0,0]) cube([L+18+4+10,W+6+4,36]);
    translate([2,2,2]) cube([L+18+10,W+6,36]);
    // fan cut
    translate([-1,7,4]) cube([4,W-4,36]);
    translate([L+18+4+10-2,7,4]) cube([4,W-4,36]);

    // air cuts top
    translate([5,(W+6+4)/2-1,-1]) cube([L+22,2,10]);
    translate([5,(W+6+4)/2+6,-1]) cube([L+22,2,10]);
    translate([5,(W+6+4)/2+13,-1]) cube([L+22,2,10]);
    translate([5,(W+6+4)/2-2-6,-1]) cube([L+22,2,10]);
    translate([5,(W+6+4)/2-2-13,-1]) cube([L+22,2,10]);

    translate([5,(W+6+4)/2+20,-1]) cube([L+22,2,10]);
    translate([5,(W+6+4)/2-2-20,-1]) cube([L+22,2,10]);

    // air cuts head
    /*
    translate([40,(W+6+4)/2-1,6]) cube([50,2,26]);
    translate([40,(W+6+4)/2+6,6]) cube([50,2,26]);
    translate([40,(W+6+4)/2+13,6]) cube([50,2,26]);
    translate([40,(W+6+4)/2-2-6,6]) cube([50,2,26]);
    translate([40,(W+6+4)/2-2-13,6]) cube([50,2,26]);
    */
    // power+rail connector
    translate([36,-4,20]) cube([39,20,14]);
 
    // WIO connector
    translate([23,50,20]) cube([40,20,20]);

    
    translate([0,0,0]) cube([L+22+10,3,3]);
    translate([0,W+6+1,0]) cube([L+22+10,3,3]);
    
    /*
    translate([0,W+8,0]){
      rotate(0) prism(L+30, 10, 10);
    }
    translate([L+30,2,0]){
      rotate(180) prism(L+30, 10, 10);
    }
    translate([2,0,0]){
      rotate(90) prism(W+20, 10, 10);
    }
    translate([L+20,W+20,0]){
      rotate(270) prism(W+20, 10, 10);
    }
*/  
  }
  
  difference() {
    translate([0,3,3]) rotate([0,90,0]) cylinder(L+22+10, 3, 3);
    translate([2,2,2]) cube([L+18+10,W+6,5]);
   } 
  difference() {
    translate([0,W+6+1,3]) rotate([0,90,0]) cylinder(L+22+10, 3, 3);
    translate([2,2,2]) cube([L+18+10,W+6,5]);
   } 
}

if( TOP ) {
  translate([-L/2,+(W/2)+40,0]) {
    translate([-16,-5,0]) top();
  }
}

module mount() {
  difference() {
    translate([0,0,0]) cube([8,40,2]);
    translate([4,20,0]) cylinder(4, 1.6, 1.6);
  }
}

if( !BOTTOM && MOUNTING ) {
  translate([0,0,0]) mount();
}



module ventMount() {
    // vent mount
    difference() {
      translate([-14,-3,0]) cube([2,W+6,10]);
      fan(-12,(W-Vsize)/2,34, 0,90,0,true,1.6,true);
    }
    translate([-14,-3,0]) cube([12,2,10]);
    translate([-14,W+1,0]) cube([12,2,10]);
    difference() {
      translate([-2,-3,0]) cube([2,W+6,10]);
      fan(0,(W-Vsize)/2,34, 0,90,0,true,1.2,false);
    }
}

if( BOTTOM ) {
  if(SHOW_PCB) pcb(-L/2,-W/2,7);

  translate([-L/2,-W/2,0]) {
    if(SHOW_FAN) {
      fan(-12,(W-Vsize)/2,34, 0,90,0,false, 1.6);
      feet(MH,MH,-2);
      feet(L-MH,MH,-2);
      feet(MH,W-MH,-2);
      feet(L-MH,W-MH,-2);
      
      translate([-14,13,4]) rotate([0,90,0]) fanroast();
      
      if( SHOW_TOP )
        translate([-16,W+5,38]) rotate([180,0,0]) top();
    }

    if( MOUNTING ) {
      translate([-24,W/2-40/2,0]) mount();
      translate([70+10,W/2-40/2,0]) mount();
    }
    
    // bottom
    difference() {
      translate([-16,-5,0]) cube([L+18+4+10,W+6+4,2]);
      
      // air cuts
      translate([5,W/2-1,-1]) cube([55,2,10]);
      translate([5,W/2+6,-1]) cube([55,2,10]);
      translate([5,W/2+13,-1]) cube([55,2,10]);
      translate([5,W/2-2-6,-1]) cube([55,2,10]);
      translate([5,W/2-2-13,-1]) cube([55,2,10]);
      
      // feet holes
      translate([MH,MH,-1]) cylinder(10, 1.2, 1.2);
      translate([L-MH,MH,-1]) cylinder(10, 1.2, 1.2);
      translate([MH,W-MH,-1]) cylinder(10, 1.2, 1.2);
      translate([L-MH,W-MH,-1]) cylinder(10, 1.2, 1.2);
    }

    // vent mount
    translate([0,0,0]) ventMount();
    translate([L,0,0]) mirror([1,0,0]) ventMount();
    
    // rand
    difference() {
      translate([-14,-3,2]) cube([L+18,W+6,2]);
      translate([-14+2,-3+2,2]) cube([L+18-4+10,W+6-4,5]);
    }

    // 4 x PCB mounting
    difference() {
      translate([MH,MH,0]) cylinder(7, 5, 4);
      translate([MH,MH,-1]) cylinder(10, 1.2, 1.2);
    }
    difference() {
      translate([L-MH,MH,0]) cylinder(7, 5, 4);
      translate([L-MH,MH,-1]) cylinder(10, 1.2, 1.2);
    }
    difference() {
      translate([MH,W-MH,0]) cylinder(7, 5, 4);
      translate([MH,W-MH,-1]) cylinder(10, 1.2, 1.2);
    }
    difference() {
      translate([L-MH,W-MH,0]) cylinder(7, 5, 4);
      translate([L-MH,W-MH,-1]) cylinder(10, 1.2, 1.2);
    }
  }  
}

