/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/

// WIOpi Throttle top
$fn=128;

// Left
difference() {
  rotate([270,0,0]) translate([14,-14,0]) cylinder( 140, 14, 14); 
  rotate([270,0,0]) translate([14,-14,2]) cylinder( 136, 12, 12); 
  translate([0,0,14]) cube([28, 140, 14]);
  translate([14,0,2]) cube([40, 140, 14]);
  // SH1106
  translate([17,10,0]) cube([34, 19, 2]);
}
translate([2,2,8]) cube([1, 136, 7]);

// Right
difference() {
  rotate([270,0,0]) translate([54,-14,0]) cylinder( 140, 14, 14); 
  rotate([270,0,0]) translate([54,-14,2]) cylinder( 136, 12, 12); 
  translate([40,0,14]) cube([28, 140, 14]);
  translate([14,0,2]) cube([40, 140, 14]);
  // SH1106
  translate([17,10,0]) cube([34, 19, 2]);
}
translate([65,2,8]) cube([1, 136, 7]);

// Top
difference() {
  translate([14,0,0]) cube([40, 140, 2]);
  // SH1106
  translate([17,10,0]) cube([34, 19, 2]);
  // Rotary
  translate([34,45,0]) cylinder(2, 3.5, 3.5);
  // Buttons flatcable
  translate([32,85,0]) cube([4, 14, 2]);
  // Logos:
  translate([24,7,0]){
    rotate(180) mirror([1,0,0]) linear_extrude(0.4) text("WIOpi",5);
  }
  translate([24,136,0]){
    rotate(180) mirror([1,0,0]) linear_extrude(0.4) text("Rocrail",5);
  }
} 

// Rotary mounting
translate([34,45,0]){
  difference() {
    cylinder(6, 5.5, 5.5);
    cylinder(6, 3.5, 3.5);
  }
}

// Header
difference() {
  translate([14,0,0]) cube([40, 2, 20]);
  translate([14,0,14]) cube([40, 1, 6]);
}
// Footer
difference() {
  translate([14,138,0]) cube([40, 2, 20]);
  translate([14,139,14]) cube([40, 1, 6]);
}
 
// Display mounting
difference() {
  translate([14,4,2]){ 
    cube([12, 6, 2]);
  }
  translate([18.5,7,2]){ 
    cylinder(2, 0.5, 0.5);
  }
}
difference() {
  translate([42,4,2]){ 
    cube([12, 6, 2]);
  }
  translate([49.5,7,2]){ 
    cylinder(2, 0.5, 0.5);
  }
}

// WIOpi-01 mounting
translate([34,72,0]){
  difference() {
    cylinder(6, 3, 3);
    cylinder(6, 1.5, 1.5);
  }
}
translate([34,130,0]){
  difference() {
    cylinder(6, 3, 3);
    cylinder(6, 1.5, 1.5);
  }
}




