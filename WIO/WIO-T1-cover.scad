/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

include <../lib/shapes.scad>

W=72;
H=100;

LEDX=30+4;
LEDY=46+4;

LEDWINDOW=0;
COVER=1;
SWITCHNR=0;

SCREWMOUNT=1;


if( SWITCHNR ) {
  translate([00,-30,0]) {
    translate([0,0,0]) linear_extrude(.6) text("1",10);
  }
  translate([15,-30,0]) {
    translate([0,0,0]) linear_extrude(.6) text("2",10);
  }
  translate([30,-30,0]) {
    translate([0,0,0]) linear_extrude(.6) text("3",10);
  }
  translate([45,-30,0]) {
    translate([0,0,0]) linear_extrude(.6) text("4",10);
  }
}

if( LEDWINDOW ) {
  translate([100,0,0]) {
    translate([-.5,-.5,0]) cube([8.7, 8.7, .6]);
    translate([0,0,0]) cube([7.7, 7.7, 2.6]);
  }
}

// Cover
if( COVER ) {
  difference() {
    translate([0,0,0]) cube([W+4, H+4, 10]);
    translate([1.8,1.8,2]) cube([W+.4, H+.4, 8]);

    // LEDWINDOW
    translate([W+2-LEDX,H+2-LEDY,0]) cube([8, 8, 2]);
    translate([W+2-LEDX-.5,H+2-LEDY-.5,1.2]) cube([9, 9, .8]);
    
    
    for( b =[1:35] ) {
      translate([1.7+b*2,2.4+1.5,0]) cube([.4, H-4, .2]);
    }

    // Top roundings
    translate([W+4,2,0]){
      rotate(180) prism(W+4, 10, 10);
    }
    translate([0,H+2,0]){
      rotate(0) prism(W+4, 10, 10);
    }
    translate([2,0,0]){
      rotate(90) prism(H+4, 10, 10);
    }
    translate([W+2,H+4,0]){
      rotate(270) prism(H+4, 10, 10);
    }
    
    if( SCREWMOUNT ) {
      // Screw terminal holes
      translate([(W+4)/2,2,10])rotate([90,0,0]) cylinder(2, 1.5, 1.5);
      translate([(W+4)/2,H+4,10])rotate([90,0,0]) cylinder(2, 1.5, 1.5);
    } 
  }

  // LEDWINDOW
  difference() {
    translate([W+2-LEDX-1,H+2-LEDY-1,0]) cube([10, 10, 1.2]);
    translate([W+2-LEDX,H+2-LEDY,0]) cube([8, 8, 1.2]);
  }

  if( SCREWMOUNT ) {
    // Screw terminals
    difference() {
      translate([(W+4)/2,1.8,10])rotate([90,0,0]) cylinder(1.8, 5, 5);
      translate([(W+4)/2,1.8,10])rotate([90,0,0]) cylinder(1.8, 1.5, 1.5);
    }
    difference() {
      translate([(W+4)/2,H+4,10])rotate([90,0,0]) cylinder(1.8, 5, 5);
      translate([(W+4)/2,H+4,10])rotate([90,0,0]) cylinder(1.8, 1.5, 1.5);
    }
  }

}
