/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../lib/shapes.scad>

// WIO-03
SIDEMOUNT=false;
SIDEMOUNTW=false;
PLATEMOUNT=false;
PLATEMOUNTW=true;

LENGTH=66.0;
WIDTH=43.5;

HOLESLENGTH=58.5;
HOLESWIDTH=36.0;

translate([0,0,0]) {
  difference() {
    translate([0,0,0]) cube([LENGTH, WIDTH, 2]);
    translate([10,10,0]) cube([LENGTH-20, WIDTH-20, 2]);

    if( PLATEMOUNTW ) {
      // Plate mount holes
      translate([LENGTH/2,(WIDTH-HOLESWIDTH)/2,0]) cylinder(2, 1.5, 1.5);
      translate([LENGTH/2,WIDTH-5,0]) cylinder(2, 1.5, 1.5);
    }
    
    if( PLATEMOUNT ) {
      // Plate mount holes
      translate([(LENGTH-HOLESLENGTH)/2,(WIDTH-HOLESWIDTH)/2+6,0]) cylinder(2, 1.0, 2.5);
      translate([(LENGTH-HOLESLENGTH)/2+HOLESLENGTH,(WIDTH-HOLESWIDTH)/2+6,0]) cylinder(2, 1.0, 2.5);
      translate([(LENGTH-HOLESLENGTH)/2,(WIDTH-HOLESWIDTH)/2+HOLESWIDTH-6,0]) cylinder(2, 1.0, 2.5);
      translate([(LENGTH-HOLESLENGTH)/2+HOLESLENGTH,(WIDTH-HOLESWIDTH)/2+HOLESWIDTH-6,0]) cylinder(2, 1.0, 2.5);
      

      translate([(LENGTH-HOLESLENGTH)/2+6,(WIDTH-HOLESWIDTH)/2,0]) cylinder(2, 1.0, 2.5);
      translate([(LENGTH-HOLESLENGTH)/2+HOLESLENGTH-6,(WIDTH-HOLESWIDTH)/2,0]) cylinder(2, 1.0, 2.5);
      translate([(LENGTH-HOLESLENGTH)/2+6,(WIDTH-HOLESWIDTH)/2+HOLESWIDTH,0]) cylinder(2, 1.0, 2.5);
      translate([(LENGTH-HOLESLENGTH)/2+HOLESLENGTH-6,(WIDTH-HOLESWIDTH)/2+HOLESWIDTH,0]) cylinder(2, 1.0, 2.5);
    }
    
    // WIO-03 mount holes
    translate([(LENGTH-HOLESLENGTH)/2,(WIDTH-HOLESWIDTH)/2,0]) cylinder(5, 1.4, 1.4);
    translate([(LENGTH-HOLESLENGTH)/2+HOLESLENGTH,(WIDTH-HOLESWIDTH)/2,0]) cylinder(5, 1.4, 1.4);
    translate([(LENGTH-HOLESLENGTH)/2,(WIDTH-HOLESWIDTH)/2+HOLESWIDTH,0]) cylinder(5, 1.4, 1.4);
    translate([(LENGTH-HOLESLENGTH)/2+HOLESLENGTH,(WIDTH-HOLESWIDTH)/2+HOLESWIDTH,0]) cylinder(5, 1.4, 1.4);

  }
  
  // WIO-03 mounting
  difference() {
    translate([(LENGTH-HOLESLENGTH)/2,(WIDTH-HOLESWIDTH)/2,0]) cylinder(6, 3.5, 3);
    translate([(LENGTH-HOLESLENGTH)/2,(WIDTH-HOLESWIDTH)/2,0]) cylinder(6, 1.4, 1.4);
  }
  difference() {
    translate([(LENGTH-HOLESLENGTH)/2+HOLESLENGTH,(WIDTH-HOLESWIDTH)/2,0]) cylinder(6, 3.5, 3);
    translate([(LENGTH-HOLESLENGTH)/2+HOLESLENGTH,(WIDTH-HOLESWIDTH)/2,0]) cylinder(6, 1.4, 1.4);
  }
  difference() {
    translate([(LENGTH-HOLESLENGTH)/2,(WIDTH-HOLESWIDTH)/2+HOLESWIDTH,0]) cylinder(6, 3.5, 3);
    translate([(LENGTH-HOLESLENGTH)/2,(WIDTH-HOLESWIDTH)/2+HOLESWIDTH,0]) cylinder(6, 1.4, 1.4);
  }
  difference() {
    translate([(LENGTH-HOLESLENGTH)/2+HOLESLENGTH,(WIDTH-HOLESWIDTH)/2+HOLESWIDTH,0]) cylinder(6, 3.5, 3);
    translate([(LENGTH-HOLESLENGTH)/2+HOLESLENGTH,(WIDTH-HOLESWIDTH)/2+HOLESWIDTH,0]) cylinder(6, 1.4, 1.4);
  }
  
  if( SIDEMOUNT ) {
    // left
    difference() {
      translate([-4,(WIDTH)/2-6,0]) cube([4, 12, 2]);
      translate([-4,(WIDTH)/2,0]) cylinder(2, 1.5, 3.0);
    }  
    difference() {
      translate([-4,(WIDTH)/2,0]) cylinder(2, 6, 6);
      translate([-4,(WIDTH)/2,0]) cylinder(2, 1.5, 3.0);
    }
    // right
    difference() {
      translate([LENGTH,(WIDTH)/2-6,0]) cube([4, 12, 2]);
      translate([LENGTH+4,(WIDTH)/2,0]) cylinder(2, 1.5, 3.0);
    }  
    difference() {
      translate([LENGTH+4,(WIDTH)/2,0]) cylinder(2, 6, 6);
      translate([LENGTH+4,(WIDTH)/2,0]) cylinder(2, 1.5, 3.0);
    }
  }
  
  if( SIDEMOUNTW ) {
    rotate([0,0,90]) translate([0,-(LENGTH-11),0]) {
    // left
    difference() {
      translate([-4,(WIDTH)/2-6,0]) cube([4, 12, 2]);
      translate([-4,(WIDTH)/2,0]) cylinder(2, 1.5, 1.5);
    }  
    difference() {
      translate([-4,(WIDTH)/2,0]) cylinder(2, 6, 6);
      translate([-4,(WIDTH)/2,0]) cylinder(2, 1.5, 1.5);
    }
    // right
    difference() {
      translate([WIDTH,(WIDTH)/2-6,0]) cube([4, 12, 2]);
      translate([WIDTH+4,(WIDTH)/2,0]) cylinder(2, 1.5, 1.5);
    }  
    difference() {
      translate([WIDTH+4,(WIDTH)/2,0]) cylinder(2, 6, 6);
      translate([WIDTH+4,(WIDTH)/2,0]) cylinder(2, 1.5, 1.5);
    }
  }
  }
  
}
