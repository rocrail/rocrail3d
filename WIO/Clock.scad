/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
//include <../../lib/shapes.scad>


D=38;
d=36;
B=24;
H=12;
H2=6;
M=(H+H2)/2;

RING=1;
BASE=1;
TOP=1;
POLE=0;
MOUNT=0;
ISO=0;

if( BASE ) {
  difference() {
    translate([0,0,0]) cylinder(H, D/2+2, D/2+2);
    translate([0,0,2.5]) cylinder(H, D/2+1, D/2+1);
    translate([0,0,0]) cylinder(H, d/2, d/2);
    translate([-B/2,-50,1]) cube([B, 50, 20]);
  }

  difference() {
    translate([-B/2-1,-28,0]) cube([B+2, 12, H]);
    translate([-B/2,-26.5,1]) cube([B, 10.5, H]);
    translate([0,0,0]) cylinder(20, d/2, d/2);
    translate([0,-46,M]) rotate([0,90,90]) cylinder(20, 3, 3);
  }

  difference() {
    translate([0,-36.5,M]) rotate([0,90,90]) cylinder(10, 3, 3);
    translate([0,-50,M]) rotate([0,90,90]) cylinder(40, 2, 2);

  }
}

if( TOP ) translate([50,0,0]) {
  difference() {
    translate([0,0,0]) cylinder(H2, D/2+2, D/2+2);
    translate([0,0,2.5]) cylinder(H2, D/2+1, D/2+1);
    translate([0,0,0]) cylinder(H2, d/2, d/2);
    translate([-B/2,-50,1]) cube([B, 50, 20]);
  }

  difference() {
    translate([-B/2-1,-28,0]) cube([B+2, 12, H2]);
    translate([-B/2,-26.5,1]) cube([B, 10.5, H2]);
    translate([0,0,0]) cylinder(20, d/2, d/2);
    translate([0,-46,M]) rotate([0,90,90]) cylinder(20, 3, 3);
  }

}

if( RING ) {
  translate([50,50,0]) difference() {
    translate([0,0,0]) cylinder(H-3, D/2+1.2, D/2+1.2);
    translate([0,0,0]) cylinder(10, D/2-1, D/2-1);
    translate([-18/2,-50,1]) cube([18, 50, 20]);
  }
}

if( ISO ) {
  translate([0,0,0]) cube([18, 18, .6]);
}

if( POLE ) translate([0,30,0]) {
  difference() {
    translate([0,0,4]) rotate([0,90,90]) cylinder(100, 4.2, 4.2);
    translate([0,0,4]) rotate([0,90,90]) cylinder(120, 3.1, 3.1);

  }
}

if( MOUNT ) translate([-50,0,0]) {
  difference() {
    translate([0,0,6]) cylinder(8, 3.1, 3.1);
    translate([0,0,6]) cylinder(8, 2, 2);
  }
  difference() {
    translate([0,0,0]) cylinder(6, 25, 22);
    //translate([0,0,0]) cylinder(4, 24, 21);
    translate([0,0,0]) cylinder(100, 2, 2);
    translate([0,0,0]) cylinder(4, 5, 5);
    translate([0,0,2.5]) rotate([0,90,90]) cylinder(120, 2, 2);

    translate([15,0,0]) cylinder(6, 1.5, 1.5);
    translate([15,0,5]) cylinder(1, 1.5, 3);
    translate([-15,0,0]) cylinder(6, 1.5, 1.5);
    translate([-15,0,5]) cylinder(1, 1.5, 3);
  
  }
}

