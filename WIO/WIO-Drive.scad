/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../lib/shapes.scad>

// WIO-03
SIDEMOUNT=true;
PLATEMOUNT=false;

LENGTH=51.0;
WIDTH=46.0;

HOLESWIDTH=40.5;

translate([0,0,0]) {
  difference() {
    translate([0,0,0]) cube([LENGTH, WIDTH, 2]);
    translate([10,10,0]) cube([LENGTH-20, WIDTH-20, 2]);

    // Plate mount holes
    translate([LENGTH/2,5,0]) cylinder(2, 1.25, 1.25);
    translate([LENGTH/2,WIDTH-5,0]) cylinder(2, 1.25, 1.25);

  // WIO-Drive mounting
    translate([3,3,0]) cylinder(8, 1.4, 1.4);
    translate([3,WIDTH-3,0]) cylinder(8, 1.4, 1.4);
  }
  
  // WIO-Drive mounting
  difference() {
    translate([3,3,0]) cylinder(8, 3, 3);
    translate([3,3,0]) cylinder(8, 1.4, 1.4);
  }
  difference() {
    translate([3,WIDTH-3,0]) cylinder(8, 3, 3);
    translate([3,WIDTH-3,0]) cylinder(8, 1.4, 1.4);
  }
  translate([LENGTH-3,WIDTH/2-6,0]) cube([3, 12, 8]);
  
}