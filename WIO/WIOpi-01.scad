/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
include <../G/lib/modules.scad>

// WIOpi-01.scad - Mounting board.
$fn=128;

LOGO=false;
LENGTH=79;
WIDTH=52;

WIOpiBase(79,52);

if( LOGO ) {
  // Logos:
  translate([30,13,0]){
    rotate(90) linear_extrude(2.4) text("Rocrail",10);
  }
  translate([48,2,0]){
    rotate(90) linear_extrude(2.4) text("WIOpi",8);
  }
  translate([48,52,0]){
    rotate(90) linear_extrude(2.4) text("GCA",8);
  }
}
