/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../lib/shapes.scad>

H=4;

MARGIN=.2;

LOADERL=27+MARGIN;
LOADERW=17.4+MARGIN;

PICOL=51.2+MARGIN;
PICOW=21+MARGIN;

L=LOADERW+PICOL+1+1+1;
W=LOADERL+1+1;



  difference() {
    translate([0,0,0]) cube([L, W, H]);
    translate([1,1,1]) cube([LOADERW, LOADERL, H]);
    translate([LOADERW+1+1,(W-PICOW)/2,1]) cube([PICOL, PICOW, H]);

    // cable holes
   translate([(1+LOADERW)/2,W/2,0]) cylinder(6, 3.0, 3.0);
   translate([L/2,W/2,0]) cylinder(6, 3.0, 3.0);
   translate([L/2+20,W/2,0]) cylinder(6, 3.0, 3.0);


    // Screw holes:
   translate([L-5,(W-PICOW)/2/2,0]) cylinder(6, 1.0, 1.0);
   translate([L-5,W-(W-PICOW)/2/2,0]) cylinder(6, 1.0, 1.0);


   translate([L-PICOL+5,(W-PICOW)/2/2,0]) cylinder(6, 1.0, 1.0);
   translate([L-PICOL+5,W-(W-PICOW)/2/2,0]) cylinder(6, 1.0, 1.0);

    // USB Loader
    translate([1+LOADERW/2-9/2,0,1]) cube([9, 10, H-1]);
    // USB Pico
    translate([L-1,W/2-8/2,1]) cube([10, 8, H-1]);

  }


