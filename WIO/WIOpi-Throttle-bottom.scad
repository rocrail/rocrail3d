/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

// WIOpi Throttle bottom

// Left
difference() {
  rotate([270,0,0]) translate([14,-14,0]) cylinder( 140, 14, 14); 
  rotate([270,0,0]) translate([14,-14,2]) cylinder( 136, 12, 12); 
  translate([0,0,14]) cube([28, 140, 14]);
  translate([14,0,2]) cube([40, 140, 14]);
}

// Right
difference() {
  rotate([270,0,0]) translate([54,-14,0]) cylinder( 140, 14, 14); 
  rotate([270,0,0]) translate([54,-14,2]) cylinder( 136, 12, 12); 
  translate([40,0,14]) cube([28, 140, 14]);
  translate([14,0,2]) cube([40, 140, 14]);
}

// Bottom
difference() {
  translate([14,0,0]) cube([40, 140, 2]);
  // Logos:
  translate([38,115,0]){
    rotate(90) mirror([1,0,0]) linear_extrude(0.4) text("WIOpi   Rocrail",10);
  }
  // CAM
  translate([34,72,0]) cylinder( 2, 3.5, 3.5); 
} 

// RFID mount
difference() {
  translate([26,6,2]) cube([28.2, 27.2, 3]);
  translate([27,7,2]) cube([26.2, 25.2, 3]);
}

// Header
difference() {
  translate([14,0,0]) cube([40, 2, 14]);
  translate([14,1,8]) cube([40, 1, 6]);
  // Screw holes
  rotate([270,0,0]) translate([20,-11,0]) cylinder(1, 2.5, 1.5);
  rotate([270,0,0]) translate([48,-11,0]) cylinder(1, 2.5, 1.5);
}
// Footer
difference() {
  translate([14,138,0]) cube([40, 2, 14]);
  translate([14,138,8]) cube([40, 1, 6]);
  // Screw holes
  rotate([90,0,0]) translate([20,11,-140]) cylinder(1, 2.5, 1.5);
  rotate([90,0,0]) translate([48,11,-140]) cylinder(1, 2.5, 1.5);
}

