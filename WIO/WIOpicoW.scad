/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../lib/shapes.scad>


L=78;
W=44;
H=2;

BASE=0;
TOP=1;
BUTTON=0;
LED=0;
LED2=0;
USB=1;

// PCB screw mount
module mount(x,y) {
  difference() {
     translate([x,y,0]) cylinder(2*H, 2.5, 2.5);
     translate([x,y,0]) cylinder(2.1*H, 1, 1);
  }
}

// LED light: print with transparent filament
if( LED ) translate([20,-20,0]) {
  cylinder(5.0,1.9,1.9);
  cylinder(1.6,3.5,3.5);
}
if( LED2 ) translate([20,-20,0]) {
  cylinder(4.2,1.9,1.9);
  cylinder(2.2,2.5,2.5);
  translate([0,0,4.2]) sphere(1.9);
}

// The BOOTSEL button
if( BUTTON ) translate([0,-20,0]) {
  cylinder(6,3.5/2,3.5/2);
  cylinder(1,3,3);

  difference() {
    translate([10,0,0]) cylinder(2.4,2.5,2.5);
    translate([10,0,1]) cylinder(2,3.5/2,3.5/2);
  }
}


// Top of the box
if( TOP ) translate([0,0,0]) {  
  
   
  // power side top
  difference() {
    translate([-4,-4,21]) cube([15, W+8, 3]);
    translate([-4,W/2-23/2+1,20]) cube([L+8, 21, 2]);
      // USB cut
    if(USB) translate([-5,W/2-12/2,24-8]) cube([25, 12, 10]);
  }
  translate([-4,W-11,21]) cube([20, 15, 3]);
  
  // logo side top
  difference() {
    translate([L-4,-4,21]) cube([8, W+8, 3]);
    translate([-4,W/2-23/2+1,20]) cube([L+8, 21, 2]);
  }
  
  // middle top
  difference() {
    translate([-4,W/2-23/2,21]) cube([L+8, 23, 3]);
    translate([-4,W/2-23/2+1,20]) cube([L+8, 21, 2]);
    // button hole
    translate([33,18,0]) cylinder(40,2,2);
    // LED hole
    translate([26,15,0]) cylinder(40,1.95,1.95);

    // Logos:
    translate([L-20,31,24-.4]){
      rotate([0,0,270]) linear_extrude(0.4) text("WIO",7);
    }
    translate([L-28,30,24-.4])
      rotate([0,0,270]) linear_extrude(0.4) text("L",7);
    translate([L-36,30,24-.4])
      rotate([0,0,270]) linear_extrude(0.4) text("A",7);
    translate([L-44,30,24-.4])
      rotate([0,0,270]) linear_extrude(0.4) text("N",7);
    
    
      // USB cut
    if(USB) translate([-5,W/2-12/2,24-8]) cube([25, 12, 10]);
  }
  
  
  // power sides
  translate([-4,-4,0]) cube([15, 2, 24]);
  translate([-4,W+2,0]) cube([20, 2, 24]);

  // logo sides
  translate([L-4,-4,0]) cube([8, 2, 24]);
  translate([L-4,W+2,0]) cube([8, 2, 24]);

  // bottom sides 
  difference() {
    translate([-4,-4,0]) cube([L+8, 2, 10]);
      translate([15,-3.6,3])
    rotate([90,0,0]) linear_extrude(0.4) text("TRX   Hall   I2C",3); 
      translate([55,-3.6,3])
    rotate([90,0,0]) linear_extrude(0.4) text("I/O1",4); 
  }
  
  difference() {
    translate([-4,W+2,0]) cube([L+8, 2, 10]);
    translate([55+10,W+3.6,3])
    rotate([90,0,180]) linear_extrude(0.4) text("I/O2",4); 
    translate([44,W+3.6,3])
    rotate([90,0,180]) linear_extrude(0.4) text("4   3   2   1   NP",3); 
  }
  
  
  // power supply front
  difference() {
    translate([-4,-4,0]) cube([2, W+8, 24]);
    // base cut
    translate([-10,W/2-36/2,0]) cube([L+4+16, 36, 2]);
    translate([-10,W/2-36/2,0]) cube([L+4+16, 2, 6]);
    translate([-10,W/2+36/2-2,0]) cube([L+4+16, 2, 6]);
    // power connector cut
    translate([-10,12-2,0]) cube([L+4+16, 10, 3*H+7]);
      // USB cut
    translate([-5,W/2-12/2,24-8]) cube([25, 12, 10]);
    // +/-
    translate([-3.6,40,17])
        rotate([90,0,270]) linear_extrude(0.4) text("USB",4); 
    // +/-
    translate([-3.6,23.5,8])
        rotate([90,0,270]) linear_extrude(0.4) text("-     +",6); 
  }

  // logo front
  translate([L+6,0,0]) difference() {
    translate([-4,-4,0]) cube([2, W+8, 24]);
    // base cut
    translate([-10,W/2-36/2,0]) cube([L+4+16, 36, 2]);
    translate([-10,W/2-36/2,0]) cube([L+4+16, 2, 6]);
    translate([-10,W/2+36/2-2,0]) cube([L+4+16, 2, 6]);
  }
}


// Base of the box
if( BASE ) translate([0,0,0]) {
  // PCB room
  difference() {
    translate([-2,-2,0]) cube([L+4, W+4, 3*H]);
    translate([0,0,H]) cube([L, W, 3*H]);
    // power connector cut
    translate([-2,12-2,2*H+1]) cube([4, 10, 3*H+6]);
  }

  // PCB screw mount
  mount(2.2,2.2);
  mount(2.2,W-2.2);
  mount(L-2.2,2.2);
  mount(L-2.2,W-2.2);
  
  // base screw mount 
  difference() {
    translate([-10,W/2-36/2,0]) cube([L+4+16, 36, 2]);
    translate([-7,W/2,0]) cylinder(2*H, 1.25, 1.25);
    translate([L+7,W/2,0]) cylinder(2*H, 1.25, 1.25);
  }

  translate([L+10,(W/2)-18,2]){
    rotate(90) prism(2, 8, 4);
  }
  translate([L+10,(W/2)+16,2]){
    rotate(90) prism(2, 8, 4);
  }
  translate([-10,(W/2)-16,2]){
    rotate(270) prism(2, 8, 4);
  }
  translate([-10,(W/2)+18,2]){
    rotate(270) prism(2, 8, 4);
  }
  
}

  
  