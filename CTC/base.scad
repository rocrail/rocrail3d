/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
//include <../../lib/shapes.scad>

/*
Module: 27 x 17mm
PCB   : 24.62 x 14.68mm
Wall  : 0.9mm
*/

L=27;
W=17;
top=1.4;
H=16+top;
A=atan(W/L);
B=3.8; // button hole
D=.9;

BASE=1;     // Symbol base for SYMBOL=SYM_* (line 61)
OVERLAY=0;  // Overlay for the base cutout

INLAY=0;        // LED lens
INLAY_SIGNAL=0; // Signal LED lens


RASTER=0;   // Raster plate with size in RL and RW

// Raster size
RL=3;
RW=2;

LEDROAST=0;
LEDROAST2=0;
BUTTON_TOP=0;
BUTTON=1;
CONFILLER=0;


SYM_NONE=-1;
SYM_ALL=0;
SYM_STRAIGHT=1;
SYM_SWITCH_L=2;
SYM_SWITCH_R=3;
SYM_SWITCH_3W=4;
SYM_CROSSING_L=5;
SYM_CROSSING_R=6;
SYM_LEFT=7;
SYM_RIGHT=8;
SYM_STRAIGHT_L=9;
SYM_STRAIGHT_R=10;

SYM_SIGNAL=11;
SYM_SIGNAL_L=0;
SYM_SIGNAL_R=0;

// Select the wanted symbol:
SYMBOL=SYM_SIGNAL;

SYM_CUTOUT=1;   // Cutout in the base for the overlay


if( CONFILLER ) translate([0,-W,0]) {
  translate([0,0,0]) cube([8, 6, 2.8]);
}

if( BUTTON_TOP ) translate([0,-W,0]) {
  difference() {
    translate([0,0,0]) cylinder(3,2.5,2.5);
    translate([0,0,.8]) cylinder(3,3.3/2,3.3/2);
  }  
}


module modraster(l,w,h) {
  difference() {
    //d = D+.1;
    d = D+.1;
    translate([d,d,0]) cube([l-2*d, w-2*d, h]);
    translate([d+1,d+1,0]) cube([l-2*d-2, w-2*d-2, h]);
    translate([0,w/2-5,0]) cube([l, 10, h]);
  }
}

module modheader(h,hole) {
  l=8;
  b=8.2;
  d=1;
  ll=l+d+d;
  bb=b+d+d;
  if( hole ) {
    translate([-ll/2+d,-bb/2+d,0])  cube([l, b, h]);
  }
  translate([-ll/2,-bb/2,0]) {
    translate([ll/2-2/2,0,3]) cube([2, bb, h-3]);
    difference() {
      translate([0,0,0]) cube([ll, bb, h]);
      translate([d,d,0]) cube([l, b, h]);
    }
  }
}

if( RASTER ) translate([L+10,0,0]) {
  hb=3.6;
  h=hb+2;

  for( a =[1:RW-1] ) {
    translate([-2,W*a-1,0]) cube([2,2,hb]);
  }
  for( a =[1:RL-1] ) {
    translate([L*a-1,RW*W,0]) cube([2,2,hb]);
  }
  
  difference() {
    translate([0,0,0]) cube([L*RL,W*RW,hb]);

    for( c =[1:RW-1] ) {
      translate([RL*L-2,W*c-1,0]) cube([2,2,hb]);
    }
    for( a =[1:RL-1] ) {
      translate([L*a-1,0,0]) cube([2,2,hb]);
    }

    for( a =[0:RL] ) {
      for( b =[0:RW-1] ) {
        translate([L*a+L/2,W*b+W/2,0]) cylinder(hb, 4, 4);
        translate([L*a,W*b+W/2,0]) modheader(h,true);

        // cable 
        translate([L*a+L/2-4,W*b,0]) cube([8,W,2]);
        
        // screw holes
        translate([L*a+L/3.5,W*b+W/4,0]) cylinder(hb, 1, 1);
        translate([L*a+L/3.5,W*b+W/4,hb-1]) cylinder(1, 1, 2);
        translate([L*a+L-L/3.5,W*b+W-W/4,0]) cylinder(hb, 1, 1);
        translate([L*a+L-L/3.5,W*b+W-W/4,hb-1]) cylinder(1, 1, 2);
      }
    }
  }
  for( a =[0:RL] ) {
    for( b =[0:RW-1] ) {
      if( a < RL ) {
        difference() {
          translate([L*a,W*b,0]) modraster(L,W,h);
          // cable
          translate([L*a+L/2-4,W*b,0]) cube([8,W,2]);
        }
      }
      if( a > 0 && a < RL ) {
        translate([L*a,W*b+W/2,0]) modheader(h+2,false);
      }
      else if( a == RL ) {
        difference() {
          translate([L*a,W*b+W/2,0]) modheader(h+2,false);
          translate([L*RL,0,0]) cube([10,W*RW,10]);
        }
      }
      else {
        difference() {
          translate([L*a,W*b+W/2,0]) modheader(h+2,false);
          translate([-10,0,0]) cube([10,W*RW,10]);
        }
      }
    }
  }
  
}


module ccube(l,w,h) {
  translate([0,0,0]) cube([l/2, w/2, h]);
  translate([0,-(w/2),0]) cube([l/2, w/2, h]);
}

module mask(l,w,h) {
  difference() {
    translate([-10,-10,0]) cube([l+2*10, w+2*10, h]);
    translate([0,0,0]) cube([l, w, h]);
  }
}


if( INLAY ) translate([0,2*W+10,0]) {
  translate([0,0,0]) cube([6.5, 1.9, 2.7]);
  translate([1,-.5,0]) cube([4.5, 1.9+1, .6]);
}  
if( INLAY_SIGNAL ) translate([10,2*W+10,0]) {
  if( SYMBOL == SYM_SIGNAL ) {
    translate([0,0,0]) cube([3,3,2]);
    translate([0,1.5,0]) cylinder(2, 1.5, 1.5);
    translate([3,1.5,0]) cylinder(2, 1.5, 1.5);
    translate([0,-.7,0]) cube([3, 4.4, .6]);
  }
  else {
    translate([0,0,0]) cylinder(2.5,2,2);
    translate([0,-2,0]) cube([2, 4, 2.5]);
    translate([2,0,0]) cylinder(2.5,2,2);
    translate([-1,-2.5,0]) cube([4, 5, .6]);
  }
  
}


if( OVERLAY ) translate([0,0,0]) {
  color("yellow") symbol(.8);
}
module symbol(h) {
  w=3;

  if( BUTTON ) {
    difference() {
      translate([L/2,W/2,0]) cylinder(h,B/1.2,B/1.2);
      translate([L/2,W/2,0]) cylinder(H,B/2,B/2);
    }
  }
  
  if( SYMBOL == SYM_SIGNAL ) {
    difference() {
      translate([3,W/2-7,0]) cube([3,4,h]);
      translate([3,W/2-7+.5,0]) cube([3,3,h+1]);
    }
    
    difference() {
      translate([3,W/2-7+2,0]) cylinder(h, 2, 2);
      translate([3,W/2-7+2,0]) cylinder(h+1, 1.5, 1.5);
      translate([3,W/2-7+.5,0]) cube([3,3,h+1]);
    }
    difference() {
      translate([2+4,W/2-7+2,0]) cylinder(h, 2, 2);
      translate([2+4,W/2-7+2,0]) cylinder(h+1, 1.5, 1.5);
      translate([3,W/2-7+.5,0]) cube([3,3,h+1]);
    }
    
    translate([-.5,-.5,0]) difference() {
      translate([2+6,W/2-1,0]) cylinder(h, 4, 4);
      translate([2+6,W/2-1,0]) cylinder(h, 3, 3);
      translate([4,W/2-6,0]) cube([4,8,h+1]);
      translate([4,W/2-0,0]) cube([8,4,h+1]);
    }
  }
  
  if( SYMBOL == SYM_ALL || SYMBOL == SYM_STRAIGHT || SYMBOL == SYM_STRAIGHT_L || SYMBOL == SYM_STRAIGHT_R || SYMBOL == SYM_SWITCH_L || SYMBOL == SYM_SWITCH_R || SYMBOL == SYM_SWITCH_3W || SYMBOL == SYM_CROSSING_L || SYMBOL == SYM_CROSSING_R || SYMBOL == SYM_SIGNAL ) {
    difference() {
      translate([L/2,W/2,0]) rotate([0,0,0]) ccube(L, w, h); 
      translate([L/2,W/2,0]) rotate([0,0,0]) difference() {
        translate([0,0,0]) ccube(L-2, 2, 1);
        translate([0,0,0]) ccube(12, 2, 1);
      }
      if( BUTTON ) translate([L/2,W/2,0]) cylinder(H,B/2,B/2);
    }
  }
  
  if( SYMBOL == SYM_ALL || SYMBOL == SYM_STRAIGHT || SYMBOL == SYM_SWITCH_L || SYMBOL == SYM_SWITCH_R || SYMBOL == SYM_SWITCH_3W || SYMBOL == SYM_CROSSING_L || SYMBOL == SYM_CROSSING_R || SYMBOL == SYM_SIGNAL ) {
    difference() {
      translate([L/2,W/2,0]) rotate([0,0,180]) ccube(L, w, h); 
      translate([L/2,W/2,0]) rotate([0,0,180]) difference() {
        translate([0,0,0]) ccube(L-2, 2, 1);
        translate([0,0,0]) ccube(12, 2, 1);
      }
      if( BUTTON ) translate([L/2,W/2,0]) cylinder(H,B/2,B/2);
    }
  }

  if( SYMBOL == SYM_ALL || SYMBOL == SYM_LEFT || SYMBOL == SYM_SWITCH_R || SYMBOL == SYM_SWITCH_3W || SYMBOL == SYM_CROSSING_R ) {
    // right bottom
    difference() {
      translate([L/2,W/2,0]) rotate([0,0,A]) ccube(L+5, w, h); 
      translate([L/2,W/2,0]) rotate([0,0,A]) difference() {
        translate([0,0,0]) ccube(L-1, 2, 1);
        translate([0,0,0]) ccube(13, 2, 1);
      }
      mask(L,W,H);
      if( BUTTON ) translate([L/2,W/2,0]) cylinder(H,B/2,B/2);
    }
  }
  
  if( SYMBOL == SYM_ALL || SYMBOL == SYM_RIGHT || SYMBOL == SYM_SWITCH_L || SYMBOL == SYM_SWITCH_3W || SYMBOL == SYM_CROSSING_L ) {
      // right top
    difference() {
      translate([L/2,W/2,0]) rotate([0,0,-A]) ccube(L+5, w, h); 
      translate([L/2,W/2,0]) rotate([0,0,-A]) difference() {
        translate([0,0,0]) ccube(L-1, 2, 1);
        translate([0,0,0]) ccube(13, 2, 1);
      }
      mask(L,W,H);
      if( BUTTON ) translate([L/2,W/2,0]) cylinder(H,B/2,B/2);
    }
  }

  if( SYMBOL == SYM_ALL || SYMBOL == SYM_STRAIGHT_L || SYMBOL == SYM_LEFT || SYMBOL == SYM_CROSSING_R ) {
      // left top
    difference() {
      translate([L/2,W/2,0]) rotate([0,0,180+A]) ccube(L+5, w, h); 
      translate([L/2,W/2,0]) rotate([0,0,180+A]) difference() {
        translate([0,0,0]) ccube(L-1, 2, 1);
        translate([0,0,0]) ccube(13, 2, 1);
      }
      mask(L,W,H);
      if( BUTTON ) translate([L/2,W/2,0]) cylinder(H,B/2,B/2);
    }
  }

    if( SYMBOL == SYM_ALL || SYMBOL == SYM_STRAIGHT_R || SYMBOL == SYM_RIGHT || SYMBOL == SYM_CROSSING_L ) {
      // left bottom
    difference() {
      translate([L/2,W/2,0]) rotate([0,0,180-A]) ccube(L+5, w, h); 
      translate([L/2,W/2,0]) rotate([0,0,180-A]) difference() {
        translate([0,0,0]) ccube(L-1, 2, 1);
        translate([0,0,0]) ccube(13, 2, 1);
      }
      mask(L,W,H);
      if( BUTTON ) translate([L/2,W/2,0]) cylinder(H,B/2,B/2);
    }
  }

}


if( BASE ) translate([0,0,0]) {
  difference() {
    translate([0,0,0]) cube([L, W, H]);
    translate([D,D,top]) cube([L-2*D, W-2*D, H-1]);

    // Symbol cutout
    if( SYM_CUTOUT ) {
      translate([0,0,0]) symbol(1);
    }
    
    if( SYM_SIGNAL_L ) {
      translate([L-7,3.5,0]) cylinder(H,2,2);
      translate([L-7,3.5-2,0]) cube([2, 4, 2]);
      translate([L-5,3.5,0]) cylinder(H,2,2);
    }
    if( SYM_SIGNAL_L ) {
      translate([L-7,W-3.5,0]) cylinder(H,2,2);
      translate([L-7,W-3.5-2,0]) cube([2, 4, 2]);
      translate([L-5,W-3.5,0]) cylinder(H,2,2);
    }
    
    if( SYMBOL == SYM_SIGNAL ) {
      translate([3,W/2-7+.5,0]) cube([3,3,H]);
      translate([3,W/2-7+2,0]) cylinder(H, 1.5, 1.5);
      translate([2+4,W/2-7+2,0]) cylinder(H, 1.5, 1.5);
    }    

    translate([0,W/2-5,8]) cube([L, 10, H]);

    if( BUTTON ) {
      // button
      translate([L/2,W/2,0]) cylinder(H,B/2,B/2);
    }

    // straight
    if( SYMBOL == SYM_ALL || SYMBOL == SYM_STRAIGHT_L || SYMBOL == SYM_STRAIGHT_R || SYMBOL == SYM_STRAIGHT || SYMBOL == SYM_SWITCH_L || SYMBOL == SYM_SWITCH_R || SYMBOL == SYM_SWITCH_3W || SYMBOL == SYM_CROSSING_L || SYMBOL == SYM_CROSSING_R || SYMBOL == SYM_SIGNAL) {
      translate([L/2,W/2,0]) rotate([0,0,0])  
      difference() {
        translate([0,0,0]) ccube(L-2, 2, top);
        translate([0,0,0]) ccube(12, 2, top);
      }
    }
    if( SYMBOL == SYM_ALL || SYMBOL == SYM_STRAIGHT || SYMBOL == SYM_SWITCH_L || SYMBOL == SYM_SWITCH_R || SYMBOL == SYM_SWITCH_3W || SYMBOL == SYM_CROSSING_L || SYMBOL == SYM_CROSSING_R || SYMBOL == SYM_SIGNAL ) {
      translate([L/2,W/2,0]) rotate([0,0,180])  
      difference() {
        translate([0,0,0]) ccube(L-2, 2, top);
        translate([0,0,0]) ccube(12, 2, top);
      }
    }
    
    if( SYMBOL == SYM_ALL || SYMBOL == SYM_LEFT || SYMBOL == SYM_SWITCH_R || SYMBOL == SYM_SWITCH_3W || SYMBOL == SYM_CROSSING_R ) {
      // right bottom
      translate([L/2,W/2,0]) rotate([0,0,A])  
      difference() {
        translate([0,0,0]) ccube(L-1, 2, top);
        translate([0,0,0]) ccube(13, 2, top);
      }
    }
    if( SYMBOL == SYM_ALL || SYMBOL == SYM_STRAIGHT_L ||SYMBOL == SYM_LEFT ||  SYMBOL == SYM_CROSSING_R ) {
      // left top
      translate([L/2,W/2,0]) rotate([0,0,A+180])  
      difference() {
        translate([0,0,0]) ccube(L-1, 2, top);
        translate([0,0,0]) ccube(13, 2, top);
      }
    }
    if( SYMBOL == SYM_ALL || SYMBOL == SYM_RIGHT || SYMBOL == SYM_SWITCH_L || SYMBOL == SYM_SWITCH_3W || SYMBOL == SYM_CROSSING_L ) {
      // right top
      translate([L/2,W/2,0]) rotate([0,0,-A])  
      difference() {
        translate([0,0,0]) ccube(L-1, 2, top);
        translate([0,0,0]) ccube(13, 2, top);
      }
    }
    if( SYMBOL == SYM_ALL || SYMBOL == SYM_STRAIGHT_R || SYMBOL == SYM_RIGHT || SYMBOL == SYM_CROSSING_L ) {
      // left bottom
      translate([L/2,W/2,0]) rotate([0,0,-A+180])  
      difference() {
        translate([0,0,0]) ccube(L-1, 2, top);
        translate([0,0,0]) ccube(13, 2, top);
      }
    }

  }

  // lichtkamers
  LCH=3.5;
  rotate([0,0,90]) translate([8.5,-23.5,0]) LEDRoast2(LCH, top);

}


if( LEDROAST2 ) {
  LCH=3.5;
  LEDRoast2(LCH, 0);
}

module LEDRoast2(LCH, z) {
  h=20;
  //translate([0,h-1,z]) cube([13, 1, LCH]);
  //translate([0,0,z]) cube([13, 1, LCH]);
  
  // button
  difference() {
    translate([-W/2,h/2-8/2,z]) cube([W, 8, LCH-.5]);
    translate([-W/2+1,h/2-8/2+1,z]) cube([W-2, 6, LCH]);
  }

  difference() {
    translate([-2.6,0,z]) cube([1, 20, LCH]);
    translate([-W/2+1,h/2-8/2+1,z]) cube([W-2, 6, LCH]);
  }
  difference() {
    translate([1.6,0,z]) cube([1, 20, LCH]);
    translate([-W/2+1,h/2-8/2+1,z]) cube([W-2, 6, LCH]);
  }

  difference() {
    translate([-2.6,-(L-h)/2,z]) cube([1, L, LCH-2]);
    translate([-W/2+1,h/2-8/2+1,z]) cube([W-2, 6, LCH]);
  }
  difference() {
    translate([1.6,-(L-h)/2,z]) cube([1, L, LCH-2]);
    translate([-W/2+1,h/2-8/2+1,z]) cube([W-2, 6, LCH]);
  }

}


module LEDRoast(LCH) {
  difference() {
    translate([L/2,W/2,top]) cylinder(LCH,5.75,5.75);
    translate([L/2,W/2,top]) cylinder(LCH,5.75-D,5.75-D);
  }

  difference() {
    translate([L/2-D/2,0,top]) cube([D, W, LCH]);
    translate([L/2,W/2,top]) cylinder(LCH,5.5,5.5);
  }

  difference() {
    translate([0,W/2-3/2-D,top]) cube([L, D, LCH]);
    translate([L/2,W/2,top]) cylinder(LCH,5.5,5.5);
  }

  difference() {
    translate([0,W/2+3/2,top]) cube([L, D, LCH]);
    translate([L/2,W/2,top]) cylinder(LCH,5.5,5.5);
  }
}



