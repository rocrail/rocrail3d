/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
//include <../../lib/shapes.scad>


DISPLAY=1;
CLIPS=0;
HELP=0;

NOBUT=1; // display only, no button(s)
DBUT=0;  // dual buttons
DBUTW=51.5;
DLEDW=65.5;

// Module size
W=27 * (DBUT?3:2);  // 
D=17;
top=1.4;
H=16+top;
// Wall thickness
T=.9;

DW=27;  // display width
DD=11.6;  // display depth
DMD=12; // display pcb depth
B=3.8; // button hole

if( HELP ) translate([0,-20,0]) {
  difference() {
    translate([0,0,0]) cube([6, 15, 3.4]);
    translate([0,1.5,2.4]) cube([6, 12, 1]);
  }
}

if( DISPLAY ) {
  translate([0,0,0]) {
    // Module
    difference() {
      translate([0,0,0]) cube([W, D, H]);
      translate([T,T,T]) cube([W-(2*T), D-(2*T), H-T]);

      if( DBUT ) {
        // display
        translate([W/2-DW/2, (D-11.5)/2, 0]) cube([27, 11.5, top]);
        // buttons
        translate([W/2-DBUTW/2,D/2,0]) cylinder(top,B/2,B/2);
        translate([W/2+DBUTW/2,D/2,0]) cylinder(top,B/2,B/2);
        
        // cube([6.5, 1.9, 2.7]);
        // LEDs
        translate([W/2-DLEDW/2-1,D/2-6.5/2,0]) cube([1.9,6.5,1]);
        translate([W/2+DLEDW/2-1,D/2-6.5/2,0]) cube([1.9,6.5,1]);
      }
      else {
        if( !NOBUT ) {
          // display
          translate([9, (D-11.5)/2, 0]) cube([27, 11.5, top]);
          // button
          translate([47,D/2,0]) cylinder(top,B/2,B/2);
        }
        else {
          // display
          translate([(W-DW)/2, (D-11.5)/2, 0]) cube([27, 11.5, top]);
        }
      }
      
      translate([0,D/2-5,8]) cube([W, 10, H]);
    }
    
    // Display window
    /*
    difference() {
      translate([(W-DW)/2-T, (D-DD)/2-T, 0]) cube([DW+1*T, DD+2*T, 2]);
      translate([(W-DW)/2, (D-DD)/2, 0]) cube([DW, DD, 2]);
    }

    // Display mount
    difference() {
      translate([(W-DW)/2, (D-DMD)/2-T, 0]) cube([DW, DMD+2*T, 4]);
      translate([(W-DW)/2, (D-DMD)/2, 0]) cube([DW, DMD, 4]);
    }
    */
    // Clips
    if( CLIPS ) {
      translate([(W-DW)/2, (D-DMD)/2-T, 4-.2]) cube([DW, .2+T, .4]);
      translate([(W-DW)/2, (D-DMD)/2-T+DMD+T-.2, 4-.2]) cube([DW, .2+T, .4]);
    }
      
    // PCB end stop
    DMW = DW + 6;
    //translate([(W-DW)/2+DMW, (D-DMD)/2-T, 0]) cube([T, DMD+2*T, 4]);
    
  }
}





