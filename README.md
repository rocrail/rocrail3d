# Rocrail3D
  
## Copyright
Rocrail - Model Railroad Software  
Copyright (c) 2002-2024 Robert Jan Versluis, [Rocrail.net](https://wiki.rocrail.net)   
All rights reserved.

## License
Proprietary.  
Sources are only provided to use for 3D reproduction.    
Modifiyng source code or forking/publishing this project ist not allowed.  
Commercial use is forbidden.  

## Documentation
All designs are made with OpenSCAD.
- [OpenSCAD](https://www.openscad.org)
- [Rocrail Wiki](https://wiki.rocrail.net)

