/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../../lib/shapes.scad>

// WIOpi-01.scad - Mounting board.

// Bottom plate with holes:
difference() {
  translate([-5,0,0]) cube([62, 79, 2]);
  // bottom plate holes
  translate([-1,44,0]) cylinder(2, 1.5, 1.5);
  translate([53,44,0]) cylinder(2, 1.5, 1.5);
  translate([-1,44,1]) cylinder(1, 1.5, 3);
  translate([53,44,1]) cylinder(1, 1.5, 3);
  
  // WIOpi-01 holes
  translate([3.65,5.3,0]) cylinder(2, 1.5, 1.5);
  translate([26.65,5.3,0]) cylinder(2, 1.5, 1.5);
  translate([3.66,63.3,0]) cylinder(2, 1.5, 1.5);
  translate([26.65,63.3,0]) cylinder(2, 1.5, 1.5);

  // Logos:
  translate([5,20,1.6]){
    rotate(0) linear_extrude(0.4) text("BR103 WIOpi",5);
  }
}


// Four M3 screw terminals:
translate([3.65,5.3,0]){
  difference() {
    cylinder(4, 3, 3);
    cylinder(4, 1.5, 1.5);
  }
}
translate([26.65,5.3,0]){
  difference() {
    cylinder(4, 3, 3);
    cylinder(4, 1.5, 1.5);
  }
}
translate([3.65,63.3,0]){
  difference() {
    cylinder(4, 3, 3);
    cylinder(4, 1.5, 1.5);
  }
}
translate([26.65,63.3,0]){
  difference() {
    cylinder(4, 3, 3);
    cylinder(4, 1.5, 1.5);
  }
}

