/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
difference() {
  //translate([0,0,0]) cube([106, 52, 2]);
  translate([0,0,0]) linear_extrude(2) polygon(points=[[7,0],[0,25],[0,52],[40,52],[106,10],[106,0]]);
  
  // bottom plate holes
  translate([5,47,0]) cylinder(2, 1.5, 1.5);
  translate([101,47,0]) cylinder(2, 1.5, 1.5);
  translate([12,5,0]) cylinder(2, 1.5, 1.5);
  translate([101,5,0]) cylinder(2, 1.5, 1.5);

  // Panto lever gap
  translate([0,31,0]) cube([10, 8, 2]);
  translate([10,35,0]) cylinder(2, 4, 4);

  // Servo lever gap
  translate([26,35,0]) cylinder(2, 4, 4);
  translate([26,31,0]) cube([14, 8, 2]);
  translate([40,35,0]) cylinder(2, 4, 4);

  // Logos:
  translate([56,6,1.6]){
    rotate(0) linear_extrude(0.4) text("BR103 Panto",5);
  }
}

// Servo mount left
difference() {
  translate([16,14,2]) cube([12, 6, 12]);
  // Screw hole
  rotate([90,0,0]) translate([25.5,8,-20]) cylinder(6, 1.5, 1.5);
}
// Servo mount right
difference() {
  translate([51,14,2]) cube([12, 6, 12]);
  // Screw hole
  rotate([90,0,0]) translate([53.5,8,-20]) cylinder(6, 1.5, 1.5);
}



