/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
difference() {
  translate([0,0,0]) cube([90, 75, 20]);
  translate([5,0,2]) cube([80, 75, 18]);
  translate([2,0,12]) cube([86, 75, 18]);
  translate([2,0,2]) cube([2, 75, 18]);
  translate([86,0,2]) cube([2, 75, 18]);
  translate([40,0,0]) cube([10, 5, 2]);

  translate([19,14,1]) cylinder(1,2,4);
  translate([71,14,1]) cylinder(1,2,4);
  translate([19,66,1]) cylinder(1,2,4);
  translate([71,66,1]) cylinder(1,2,4);

  translate([19,14,0]) cylinder(1,2,2);
  translate([71,14,0]) cylinder(1,2,2);
  translate([19,66,0]) cylinder(1,2,2);
  translate([71,66,0]) cylinder(1,2,2);
  
  rotate([0,90,0]) translate([-15,15,0]) cylinder(2, 2.5, 2.5);
  rotate([0,90,0]) translate([-15,60,0]) cylinder(2, 2.5, 2.5);
  rotate([0,90,0]) translate([-15,15,88]) cylinder(2, 2.5, 2.5);
  rotate([0,90,0]) translate([-15,60,88]) cylinder(2, 2.5, 2.5);

  // Logos:
  translate([68,35,1.6]){
    rotate(180) linear_extrude(0.4) text("BR103 Battery",5);
  }
}



