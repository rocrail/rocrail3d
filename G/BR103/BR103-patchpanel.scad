/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
// Bottom plate with holes:
difference() {
  translate([0,0,0]) cube([30, 70, 1.4]);
  // bottom plate holes
  translate([3.75,3,0]) cylinder(2, 1.5, 1.5);
  translate([26.25,3,0]) cylinder(2, 1.5, 1.5);
  translate([3.75,67,0]) cylinder(2, 1.5, 1.5);
  translate([26.25,67,0]) cylinder(2, 1.5, 1.5);
  
  // bottom plate holes
  translate([20,18.5,0]) cylinder(2, 1.5, 1.5);
  translate([20,35,0]) cylinder(2, 1.5, 1.5);
  translate([20,51.5,0]) cylinder(2, 1.5, 1.5);
  
  // Logos:
  translate([7,11,1.2]){
    rotate(90) linear_extrude(0.2) text("BR103 Patch Panel",4);
  }
}


