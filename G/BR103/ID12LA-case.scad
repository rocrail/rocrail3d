/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../../lib/shapes.scad>

difference() {
  translate([0,0,0]) cube([43, 30, 22]);
  translate([2,2,2]) cube([39, 26, 20]);
  translate([1.6,1.6,12]) cube([39.8, 26.8, 10]);
  // Logos:
  translate([5,18,0]){
    rotate(180) mirror([1,0,0]) linear_extrude(0.4) text("ID12LA",7);
  }
  
   
  // Edges
  translate([43,2,0]){
    rotate(180) prism(43, 10, 10);
  }
  translate([0,28,0]){
    rotate(0) prism(43, 10, 10);
  }
  translate([2,0,0]){
    rotate(90) prism(30, 10, 10);
  }
  translate([41,30,0]){
    rotate(270) prism(30, 10, 10);
  }

  
}



difference() {
  translate([2,2,2]) cube([5.5, 6, 10]);
  translate([5,5,2]) cylinder(10.5,1.2,1.2);
}
difference() {
  translate([35.5,2,2]) cube([5.5, 6, 10]);
  translate([38,5,2]) cylinder(10.5,1.2,1.2);
}
difference() {
  translate([2,22,2]) cube([5.5, 6, 10]);
  translate([5,25,2]) cylinder(10.5,1.2,1.2);
}
difference() {
  translate([35.5,22,2]) cube([5.5, 6, 10]);
  translate([38,25,2]) cylinder(10.5,1.2,1.2);
}
