/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
// Bottom plate with holes:
difference() {
translate([0,0,0]) cube([32, 16, 2]);
  // mounting hole
  //translate([16,18,0]) cylinder(1, 2.5, 1.5);
}
translate([0,14,2]) cube([9, 2, 6]);
translate([23,14,2]) cube([9, 2, 6]);
difference() {
  translate([9,0,0]) cube([14, 26, 2]);
  // mounting hole
  translate([16,18,0]) cylinder(2, 1.5, 1.5);
  //translate([16,18,0]) cylinder(1, 2.5, 1.5);
}


