/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=48;

echo( str("OpenSCAD version: ", version()) );
// !!!!! Do not forget to set the wanted type:
SWITCHTYPE="LEFT";
//SWITCHTYPE="RIGHT";

echo( str("Creating a ", SWITCHTYPE, " switch type."));

// Bottom
difference() {
  translate([0,-22,0]) cube([36, 77, 4]);
  
  // Drain
  translate([17,20,0]) cylinder(4, 0.75, 0.75);

  // Cutout under WA2
  translate([8,12,2]) cube([17, 35, 2]);
  
  // Cutout cable room
  translate([2,-14,2]) cube([30, 27.5, 2]);
  
  // WA2 screw holes
  translate([4,43,1]) cylinder(7, 0.75, 0.75);
  translate([29,19,1]) cylinder(7, 0.75, 0.75);

  // Top screw terminal holes
  translate([17,6,0]) cylinder(8, 1, 1);
  translate([17,6,0]) cylinder(2, 3, 1);
}

// Top layer for mounting the WA2
difference() {
  translate([0,-20,4]) cube([34, 73, 4]);
  
  // Cutout for WA2 body for 4mm hight
  translate([0,13.5,4]) cube([32, 36.5, 4]);
  
  // Cutout cable room
  translate([2,-14,0]) cube([30, 27.5, 8]);
  
  // WA2 screw holes
  translate([4,44,0]) cylinder(7, 0.75, 0.75);
  translate([29,20,0]) cylinder(7, 0.75, 0.75);
  
  // Backlever cutout
  rotate([0,90,0]) translate([-6,16.5,32]) cylinder(6, 2, 2);
  translate([32,14.5,6]) cube([2, 4, 4]);

  // Cable outlet
  rotate([90,0,0]) translate([16,8,14]) cylinder(6, 2, 2);
  translate([10,-17,4]) cylinder(4, 0.75, 0.75);
  translate([22,-17,4]) cylinder(4, 0.75, 0.75);
    
}

// Top screw terminal
difference() {
  translate([17,6,2]) cylinder(4, 5, 3);
  translate([17,6,2]) cylinder(4, 1, 1);
}



if( SWITCHTYPE == "RIGHT" ) {
  // Track mounting point R1
  translate([-7,-22.5,0]){
    difference() {
      cylinder(4, 3.5, 3.5);
      cylinder(4, 1.25, 1.25);
    }
  }
  difference() {
    translate([-7,-26,0]) cube([43, 7, 4]);
    translate([-7,-22.5,0]) cylinder(4, 1.25, 1.25);
  }
  // Track mounting point R2
  translate([-7,29.5,0]){
    difference() {
      cylinder(4, 3.5, 3.5);
      cylinder(4, 1.25, 1.25);
    }
  }
  difference() {
    translate([-7,26,0]) cube([39, 7, 4]);
    translate([-7,29.5,0]) cylinder(4, 1.25, 1.25);
    translate([8,12,2]) cube([17, 35, 2]);
  }
}

if( SWITCHTYPE == "LEFT" ) {
  // Track mounting point L1
  translate([-7,3.5,0]){
    difference() {
      cylinder(4, 3.5, 3.5);
      cylinder(4, 1.25, 1.25);
    }
  }
  difference() {
    translate([-7,0,0]) cube([39, 7, 4]);
    translate([-7,3.5,0]) cylinder(4, 1.25, 1.25);
    translate([2,0,0]) cube([30, 14, 6]);
  }

  // Track mounting point L2
  translate([-7,55.5,0]){
    difference() {
      cylinder(4, 3.5, 3.5);
      cylinder(4, 1.25, 1.25);
    }
  }
  difference() {
    translate([-7,52,0]) cube([43, 7, 4]);
    translate([-7,55.5,0]) cylinder(4, 1.25, 1.25);
  }
}
