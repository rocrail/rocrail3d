/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=48;
include <../../lib/shapes.scad>

difference() {
  translate([0,0,0]) cube([8, 64, 2]);
  
  // Screw holes for slip
  translate([4,26,0]) cylinder(2, 1, 1);
  translate([4,60,0]) cylinder(2, 1, 1);
  translate([4,26,0]) cylinder(1, 2, 1);
  translate([4,60,0]) cylinder(1, 2, 1);
    
  // Trim
  translate([0,32,0]) cube([2, 22, 2]);
  translate([6,32,0]) cube([2, 22, 2]);

  // Opening for switchmotor lever
  translate([2,3,0]) cube([4, 2.5, 2]);
}



difference() {
  translate([2,26,2]) cube([4, 34, 2.5]);
  translate([4,26,2]) cylinder(2.5, 1, 1);
  translate([4,60,2]) cylinder(2.5, 1, 1);
}

difference() {
  translate([4,26,2]) cylinder(2.5, 2, 2);
  translate([4,26,2]) cylinder(2.5, 1, 1);
}
difference() {
  translate([4,60,2]) cylinder(2.5, 2, 2);
  translate([4,60,2]) cylinder(2.5, 1, 1);
}