/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=96;


// Cable clamp
difference() {
  translate([1,0,0]) cube([18, 5, 4]);
  // Screw holes
  translate([ 4,2.5,0]) cylinder(5, 1, 1);
  translate([4,2.5,0]) cylinder(1, 2, 1);
  translate([16,2.5,0]) cylinder(5, 1, 1);
  translate([16,2.5,0]) cylinder(1, 2, 1);
  // Cable cutout
  rotate([90,0,0]) translate([10,4.1,-5]) cylinder(5, 2, 2);
}
difference() {
  translate([1,2.5,0]) cylinder(4, 2.5, 2.5);
  translate([4,2.5,0]) cylinder(5, 1, 1);
  translate([4,2.5,0]) cylinder(1, 2, 1);
}
difference() {
  translate([19,2.5,0]) cylinder(4, 2.5, 2.5);
  translate([16,2.5,0]) cylinder(5, 1, 1);
  translate([16,2.5,0]) cylinder(1, 2, 1);
}


