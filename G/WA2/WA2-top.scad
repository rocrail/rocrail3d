/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=48;

include <../../lib/shapes.scad>

// Roof
difference() {
  translate([0,0,0]) cube([38, 77, 16]);
  translate([1.8,1.8,1.6]) cube([34.4, 73.4, 14.4]);
  translate([36.2,0,14]) cube([1.8, 77, 2]);
  
  // Lever opening
  translate([0,37,12]) cube([2, 4, 12]);
  translate([36,37,12]) cube([2, 4, 12]);
  rotate([0,90,0]) translate([-12,39,0]) cylinder(38, 2, 2);
  
  // Cable
  translate([20.2,2,12.5])rotate([90,0,0]) cylinder(2, 2, 2);
  translate([18.2,0,12.5]) cube([4, 2, 4]);

  // Edges
  translate([38,2,0]){
    rotate(180) prism(38, 10, 10);
  }
  translate([0,75,0]){
    rotate(0) prism(38, 10, 10);
  }
  translate([2,0,0]){
    rotate(90) prism(77, 10, 10);
  }
  translate([36,77,0]){
    rotate(270) prism(77, 10, 10);
  }
  /*
  translate([47,2,23]) {
    rotate(90) mirror([0,0,1]) prism(73, 10, 10);
  }
  */
  // Logos:
  /*
  translate([13,72,0]){
    rotate(90) mirror([1,0,0]) linear_extrude(0.4) text("SW-01",7);
  }
  translate([23,5,0]){
    rotate(270) mirror([1,0,0]) linear_extrude(0.4) text("SW-01",7);
  }
  */
}



// Screw terminal
difference() {
  translate([19,28,0]) cylinder(8, 5, 3);
  translate([19,28,0]) cylinder(8, 1, 1);
}

// Drippers
difference() {
  translate([41,34,9.5]){
    rotate(90) mirror([0,0,1]) prism(42, 3, 4);
  }
  translate([38,35,8.5]) cube([1, 40, 1]);
}

difference() {
  translate([-3,44,10]){
    rotate(270) mirror([0,0,1]) prism(10, 3, 4);
  }
  translate([-1,35,9]) cube([1, 8, 1]);
}

// Cable dripper
difference() {
  translate([15.2,-3,10.5]){
    rotate(0) mirror([0,0,1]) prism(10, 3, 4);
  }
  translate([16.2,-1,10.5]) cube([8, 1, 1]);
}

