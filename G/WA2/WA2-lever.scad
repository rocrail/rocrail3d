/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=48;
include <../../lib/shapes.scad>

difference() {
  translate([0,10,0]) cube([8, 62, 2]);
  
  // Screw holes for slip
  translate([4,24,0]) cylinder(2, 1, 1);
  translate([4,58,0]) cylinder(2, 1, 1);
  translate([4,24,0]) cylinder(1, 2, 1);
  translate([4,58,0]) cylinder(1, 2, 1);
    
  // Trim
  translate([0,30,0]) cube([2, 22, 2]);
  translate([6,30,0]) cube([2, 22, 2]);

}

difference() {
  translate([0,0,-4]) cube([8, 18, 4]);
  translate([0,0,-2]) cube([8, 10, 2]);
  // Opening for switchmotor lever
  translate([2,3,-4]) cube([4, 2.5, 2]);
}
translate([8,22,0]){
  rotate(180) mirror([0,0,1]) prism(8, 4, 4);
}


difference() {
  translate([2,24,2]) cube([4, 34, 2.5]);
  translate([4,24,2]) cylinder(2.5, 1, 1);
  translate([4,58,2]) cylinder(2.5, 1, 1);
}

difference() {
  translate([4,24,2]) cylinder(2.5, 2, 2);
  translate([4,24,2]) cylinder(2.5, 1, 1);
}
difference() {
  translate([4,58,2]) cylinder(2.5, 2, 2);
  translate([4,58,2]) cylinder(2.5, 1, 1);
}