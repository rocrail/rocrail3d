/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../../lib/shapes.scad>

// VS98: WIO-03 box

translate([0,0,0]) {
  difference() {
    translate([0,0,0]) cube([90, 70, 24]);
    translate([2,2,2]) cube([90-4, 70-4, 24-2]);
 
    for( b =[0:15] ) {
      translate([4,4+b*4,0]) cube([90-8, 2, 2]);
    }

      // Top roundings
  translate([90,2,0]){
    rotate(180) prism(90, 10, 10);
  }
  translate([0,70-2,0]){
    rotate(0) prism(90, 10, 10);
  }
  translate([2,0,0]){
    rotate(90) prism(70, 10, 10);
  }
  translate([90-2,70,0]){
    rotate(270) prism(70, 10, 10);
  }
    
    
    
  }
}

