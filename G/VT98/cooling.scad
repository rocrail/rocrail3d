/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../../lib/shapes.scad>

// VT98 H-Bridge cooling

difference() {
  translate([0,0,0]) cube([55, 60, 2]);
  translate([0,0,0]) cube([31, 30, 2]);
  translate([29,60-4,0]) cylinder(2, 1.5, 2.5);
 
}
