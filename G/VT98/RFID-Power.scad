/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../../lib/shapes.scad>

// VT98 bottom case for RFID and power.

RFID=false;
BOX=true;
BOXFIX=true;
BATTBOX=false;
INDOOR_RFID=false;


if( INDOOR_RFID ) {
  translate([0,100,0]) {
    // low front
    difference() {
      translate([0,0,0]) cube([69, 28, 11]);
      // Side
      translate([0,0,2]) cube([10, 28, 9]);
      translate([69-10,0,2]) cube([10, 28, 9]);
      
      // Room
      translate([12,0,0]) cube([69-24, 28, 9]);
      
      // Connector hole
      translate([69-12,5,0]) cube([11, 10, 10]);

      // LED
      translate([(69-34)/2,(28-20)/2+10,0]) cylinder(11, 2, 2);

      // PCB holes
      translate([(69-34)/2,(28-20)/2,0]) cylinder(11, 0.75, 0.75);
      translate([(69-34)/2,(28-20)/2+20,0]) cylinder(11, 0.75, 0.75);
      translate([(69-34)/2+34,(28-20)/2,0]) cylinder(11, 0.75, 0.75);
      translate([(69-34)/2+34,(28-20)/2+20,0]) cylinder(11, 0.75, 0.75);
      
      // Mounting holes
      translate([2.5,(28-24)/2,0]) cylinder(2, 1.3, 1.3);
      translate([2.5,(28-24)/2+24,0]) cylinder(2, 1.3, 1.3);
      translate([69-2.5,(28-24)/2,0]) cylinder(2, 1.3, 1.3);
      translate([69-2.5,(28-24)/2+24,0]) cylinder(2, 1.3, 1.3);
    }
  }
}


// RFID mount
if( RFID ) {
  translate([0,60,0]) {
    // low front
    translate([-2,0,0]) cube([2, 31.5, 5]);
    
    // box
    difference() {
      translate([0,0,0]) cube([42, 31.5, 21.5-5]);
      translate([0,2,2]) cube([40, 27.5, 20]);
      translate([3,(31.5-20)/2,0]) cylinder(12, 1, 1);
      translate([3+34,(31.5-20)/2+20,0]) cylinder(12, 1, 1);
    }
    
    difference() {
      translate([3,(31.5-20)/2,2]) cylinder(10, 2.5, 2.5);
      translate([3,(31.5-20)/2,0]) cylinder(12, 1, 1);
    }  
    translate([3-1.5,0,2])  cube([3, 4, 10]);
    
    difference() {
      translate([3,(31.5-20)/2+20,2]) cylinder(10, 2.5, 2.5);
      translate([3,(31.5-20)/2+20,4]) cylinder(8, 1, 1);
    }  
    translate([3-1.5,31.5-4,2])  cube([3, 4, 10]);

    difference() {
      translate([3+34,(31.5-20)/2,2]) cylinder(10, 2.5, 2.5);
      translate([3+34,(31.5-20)/2,4]) cylinder(8, 1, 1);
    }  
    translate([3-1.5+34,0,2])  cube([3, 4, 10]);

    difference() {
      translate([3+34,(31.5-20)/2+20,2]) cylinder(10, 2.5, 2.5);
      translate([3+34,(31.5-20)/2+20,0]) cylinder(12, 1, 1);
    }  
    translate([3-1.5+34,31.5-4,2])  cube([3, 4, 10]);
    
    // ID12LA socket
    translate([1.0,0,0]) {
      difference() {
        translate([(40-28.2)/2, (31.5-27.2)/2,2]) cube([28.2, 27.2, 5]);
        translate([(40-28.2)/2+1, (31.5-27.2)/2+1,2]) cube([26.2, 25.2, 5]);
        
      translate([33,18,4]) cube([2, 5, 5]);
      }
    }
  }
}


// Bottom plate with holes:
if( BOX ) {
  BOX_WIDTH=36; // 24 for VT, 14 for VS
  BOX_HEIGHT=14; // 24 for VT, 14 for VS

  if( BATTBOX ) {  
    BATT_WIDTH=41; // 24 for VT, 14 for VS
    translate([80,-((BATT_WIDTH-BOX_WIDTH)/2),0]) {  
      difference() {
        translate([0,0,0]) cube([71, 41, BOX_HEIGHT]);
        translate([2,2,2]) cube([67, 37, BOX_HEIGHT-2]);
        translate([0,4.5,2]) cube([71, 32, BOX_HEIGHT-2]);
      }  
    }
  }
  
if( BOXFIX ) {  
  difference() {
    translate([-14,(36-28)/2,0]) cube([174+28, 28, 2]);
    translate([-8.5,36/2,0]) cylinder(2, 2.5, 1.5);
    translate([174+8.5,36/2,0]) cylinder(2, 2.5, 1.5);
  }
}
  
difference() {
  translate([0,0,0]) cube([174, 36, BOX_HEIGHT]);
  translate([2,2,2]) cube([170, 32, BOX_HEIGHT-2]);
  if( BATTBOX ) {  
    translate([82,0,2]) cube([67, 37, BOX_HEIGHT-2]);
  }
 /*
  // Screw holes 
  translate([17,(36-12)/2,0]) cylinder(2, 2.5, 1.5);
  translate([17+140,(36-12)/2,0]) cylinder(2, 2.5, 1.5);
  
  translate([17+35,(36-12)/2+12,0]) cylinder(2, 2.5, 1.5);
  translate([17+140-35,(36-12)/2+12,0]) cylinder(2, 2.5, 1.5);
*/  
  translate([17,0,0]) cylinder(BOX_HEIGHT, 3.5, 3.5);
  translate([17,36,0]) cylinder(BOX_HEIGHT, 3.5, 3.5);
  translate([17+140,0,0]) cylinder(BOX_HEIGHT, 3.5, 3.5);
  translate([17+140,36,0]) cylinder(BOX_HEIGHT, 3.5, 3.5);

  // flip switch
  rotate([90,0,0]) translate([30,6.5,-36]) cylinder(4, 3.1, 3.1);

  // XT60 chassis
  translate([10,0,0]) {
    translate([29.8,0,3]) cube([12.2, 4, 8.2]);
    rotate([90,0,0]) translate([42.2,7,-4]) cylinder(4, 4.1, 4.1);
  }
}


difference() {
  translate([17,0,0]) cylinder(BOX_HEIGHT, 4.5, 4.5);
  translate([17,0,0]) cylinder(BOX_HEIGHT, 2.0, 2.0);
  translate([17,0,10]) cylinder(BOX_HEIGHT, 3.5, 3.5);
  translate([17,0,0]) cylinder(6, 3.5, 3.5);
  translate([0,-4.5,10]) cube([174, 4.5, 24]);
}
difference() {
  translate([17,36,0]) cylinder(BOX_HEIGHT, 4.5, 4.5);
  translate([17,36,0]) cylinder(BOX_HEIGHT, 2.0, 2.0);
  translate([17,36,10]) cylinder(BOX_HEIGHT, 3.5, 3.5);
  translate([17,36,0]) cylinder(6, 3.5, 3.5);
  translate([0,36,10]) cube([174, 4.5, 24]);
}
difference() {
  translate([17+140,0,0]) cylinder(BOX_HEIGHT, 4.5, 4.5);
  translate([17+140,0,0]) cylinder(BOX_HEIGHT, 2.0, 2.0);
  translate([17+140,0,10]) cylinder(BOX_HEIGHT, 3.5, 3.5);
  translate([17+140,0,0]) cylinder(6, 3.5, 3.5);
  translate([0,-4.5,10]) cube([174, 4.5, 24]);
}
difference() {
  translate([17+140,36,0]) cylinder(BOX_HEIGHT, 4.5, 4.5);
  translate([17+140,36,0]) cylinder(BOX_HEIGHT, 2.0, 2.0);
  translate([17+140,36,10]) cylinder(BOX_HEIGHT, 3.5, 3.5);
  translate([17+140,36,0]) cylinder(6, 3.5, 3.5);
  translate([0,36,10]) cube([174, 4.5, 24]);
}
}
