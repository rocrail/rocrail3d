/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../../lib/shapes.scad>

// VT98 bottom case for RFID and power.
BATTERYBOX=true;
BATTERYTOP1=false;
BATTERYTOP2=false;

if( BATTERYTOP1 ) {
  translate([0,100,0]) {
    difference() {
      translate([0,0,0]) cube([98, 76, 10]);
      translate([1.8,1.8,2]) cube([94.4, 72.4, 8]);
      rotate([0,90,0]) translate([-7,76/2,0]) cylinder(100, 1.5, 1.5);
    

  // Top roundings
  translate([98,2,0]){
    rotate(180) prism(98, 10, 10);
  }
  translate([0,74,0]){
    rotate(0) prism(98, 10, 10);
  }
  translate([2,0,0]){
    rotate(90) prism(76, 10, 10);
  }
  translate([96,76,0]){
    rotate(270) prism(76, 10, 10);
  }
    }
    
  }
}

if( BATTERYTOP2 ) {
  translate([110,0,0]) {
    difference() {
      translate([0,0,0]) cube([94, 72, 2]);
    }
    difference() {
      translate([2.2,2.2,2]) cube([89.6, 67.6, 4]);
      translate([4,4,2]) cube([86, 64, 4]);
    }  
  }
}

if( BATTERYBOX ) {
  // Bottom plate with holes:
  difference() {
    translate([0,0,0]) cube([94, 72, 48]);
    translate([2,2,2]) cube([90, 68, 46]);
    
    // motor cut
    translate([(94-58)/2,(72-34)/2,0]) cube([58, 34, 2]);
    
    translate([(94-72)/2,(72-28)/2,0]) cube([72, 2.5, 2]);
    translate([(94-72)/2,(72-28)/2+25.5,0]) cube([72, 2.5, 2]);
    
    translate([(94-58)/2,(72/2)-8,0]) cylinder(2, 2, 2);
    translate([(94-58)/2,(72/2)+8,0]) cylinder(2, 2, 2);
    
    translate([(94/2)-36,(72/2)-6.5,0]) cylinder(2, 1.5, 1.5);
    translate([(94/2)+36,(72/2)-6.5,0]) cylinder(2, 1.5, 1.5);
    
    // cable opening
    translate([8,72-10,0]) cylinder(2, 4.5, 4.5);
    translate([10+10,72-10,0]) cylinder(2, 4.5, 4.5);
    translate([8,72-14.5,0]) cube([12, 9, 2]);
    
  }
}