/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../../lib/shapes.scad>

// VT98 LED holder
LEDHOLDER=true;
MOUNT=true;

WIDTH=77.5;
HEIGHT=10.0;
if( LEDHOLDER ) {
  difference() {
    translate([0,0,0]) cube([WIDTH, HEIGHT, 2]);
    translate([5,5,0]) cylinder(2, 2.6, 2.6);
    translate([WIDTH-5,5,0]) cylinder(2, 2.6, 2.6);

    translate([WIDTH/2,2.5,0]) cylinder(2, 1, 1);
    translate([WIDTH/2,HEIGHT-2.5,0]) cylinder(2, 1, 1);
  }
  // Isolator
  translate([10,(HEIGHT/2)-.7,2]) cube([WIDTH-20, 1.4, 5]);
}

if( MOUNT ) {
  // Mount
  difference() {
    translate([(WIDTH/2)-7,-8,0]) cube([14, 8, 2]);
    translate([WIDTH/2,-4,0]) cylinder(2, 1.2, 1.2);
  }
}

