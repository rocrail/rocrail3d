/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../../lib/shapes.scad>


CABLEROOM=16;
SWITCHTYPE=0; // 0=left, 1=right

COVER=true;
SWITCHNR=false;

// Cover
if( COVER ) {
mirror([SWITCHTYPE,0,0]) {
  difference() {
    translate([0,0-CABLEROOM,0]) cube([40, 55+CABLEROOM, 14.8]);
    // Add 0.1 in both directions to fit smoothly
    translate([1.9,1.9-CABLEROOM,2]) cube([36.2, 51.2+CABLEROOM, 12.8]);
    
    for( b =[1:17] ) {
      translate([1.7+b*2,2.4-CABLEROOM+1.5,0]) cube([.4, 55+CABLEROOM-5-3, .2]);
    }

    
    // Cut for sleepers
    translate([0,0-CABLEROOM,12.8-.5]) cube([2, 55+CABLEROOM, 2.5]);
    
    // Cable cutout
    translate([18,0-CABLEROOM,11.5]) cube([4, 2, 4]);
    rotate([90,0,0]) translate([20,11.5,CABLEROOM-2]) cylinder(6, 2, 2);

    // Edges
    translate([40,2-CABLEROOM,0]){
      rotate(180) prism(40, 10, 10);
    }
    translate([0,53,0]){
      rotate(0) prism(40, 10, 10);
    }
    translate([2,0-CABLEROOM,0]){
      rotate(90) prism(55+CABLEROOM, 10, 10);
    }
    translate([38,55,0]){
      rotate(270) prism(55+CABLEROOM, 10, 10);
    }
    
    // Water damp outlet above cable outlet
    translate([15.2,-2-CABLEROOM,4]) cube([3.5, 4, 2]);
    translate([15.2+6,-2-CABLEROOM,4]) cube([3.5, 4, 2]);

    // Water damp outlet above rack
    translate([-1, 40.5, 4]) cube([3.5, 4, 2]);
    translate([-1, 40.5-6, 4]) cube([3., 4, 2]);

  }

  // Screw terminals
  difference() {
    translate([20,3,1]) cylinder(7, 4.5, 3);
    translate([20,3,1]) cylinder(7, 1, 1);
  }
  difference() {
    translate([20,3+45,1]) cylinder(7, 4.5, 3);
    translate([20,3+45,1]) cylinder(7, 1, 1);
  }

  // Rack dripper
  difference() {
    translate([-6,49,8.0]){
      rotate(270) mirror([0,0,1]) prism(20, 6, 6);
    }
    translate([-5,49-1,8.0]){
      rotate(270) mirror([0,0,1]) prism(18, 5, 5);
    }
    //translate([-4,32,8.5]) cube([4, 14, 1]);
  }

  // Cable dripper
  difference() {
    translate([10.2,-6-CABLEROOM,8]){
      rotate(0) mirror([0,0,1]) prism(20, 6, 6);
    }
    translate([11.2,-5-CABLEROOM,8]){
      rotate(0) mirror([0,0,1]) prism(18, 5, 5);
    }
    //translate([16.2,-4-CABLEROOM,6.5]) cube([8, 4, 3]);
  }
}
}


if( SWITCHNR ) {
  translate([00,-30,0]) {
    translate([4,0,0]) cube([24, .6, .6]);
    translate([0,0,0]) linear_extrude(.8) text("sw01",10);
  }
  translate([35,-30,0]) {
    translate([4,0,0]) cube([26, .6, .6]);
    translate([0,0,0]) linear_extrude(.8) text("sw02",10);
  }
  translate([70,-30,0]) {
    translate([4,0,0]) cube([26, .6, .6]);
    translate([0,0,0]) linear_extrude(.8) text("sw03",10);
  }
  translate([105,-30,0]) {
    translate([4,0,0]) cube([26, .6, .6]);
    translate([0,0,0]) linear_extrude(.8) text("sw04",10);
  }
}

