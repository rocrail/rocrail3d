/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../../lib/shapes.scad>

SWITCHTYPE=0; // 0=LEFT, 1=RIGHT
CABLEROOM=16;
CABLEROOMB=14;
LGBSUPPORT=false;
BOTTOM=true;
PCB=1.5;
PCBADJ=2.0; // 2.0 for 1.5 PCB, 0.0 in case of no PCB

PCBDUMMY=false;

if( PCBDUMMY ) {
  translate([10,0-CABLEROOMB,0]) {  
    difference() {
      // PCB
      translate([0,0,0]) cube([30, 40, PCB]);
      
      // Top mount hole
      translate([15,9,0]) cylinder(2, 4.5, 4.5);
      
      // Servo mount left
      translate([0,29.5,0]) cube([5, 7.5, PCB]);
      // Servo mount right
      translate([30-5,29.5,0]) cube([5, 7.5, PCB]);
      
      // Drill servo connector left
      translate([5,5,0]) cylinder(PCB, .5, .5);
      translate([5,5+2*2.56,0]) cylinder(PCB, .5, .5);
      // Drill servo connector right
      translate([30-5,5,0]) cylinder(PCB, .5, .5);
      translate([30-5,5+2*2.56,0]) cylinder(PCB, .5, .5);
      // Drill HALL left
      translate([2.5,38.5,0]) cylinder(PCB, .5, .5);
      translate([4,38.5,0]) cylinder(PCB, .5, .5);
      translate([5.5,38.5,0]) cylinder(PCB, .5, .5);
      // Drill HALL right
      translate([30-2.5,38.5,0]) cylinder(PCB, .5, .5);
      translate([30-4,38.5,0]) cylinder(PCB, .5, .5);
      translate([30-5.5,38.5,0]) cylinder(PCB, .5, .5);
      
    }
    
    // Cable pads
    for( a =[0:4] ) {
      translate([((30/2)-(2*3)) + (a*3),2,0]) cylinder(PCB+.2, 1.25, 1.25);
    }
    
  }
}

// The LGB ties are 2mm higher than the ones from Thiel.
// This rectangle can be placed and glued under the bottom.
if( LGBSUPPORT ) {
  translate([10,0-CABLEROOMB,0]) {  
    difference() {
      translate([0,0,0]) cube([36, 52 + 3.5+ CABLEROOMB+3, 2]);
      translate([4,4,0]) cube([28, 44 + 3.5+ CABLEROOMB+3, 2]);
    }

  }
}

if( BOTTOM ) {
mirror([SWITCHTYPE,0,0]) {
  // Bottom
  difference() {
    translate([0,0-CABLEROOMB,0]) cube([36, 52 + 3.5+ CABLEROOMB, 4]);
    // Slider rack cutout
    translate([2, 3.5 + (26+13) - 2.6, 2]) cube([34, 5.2, 2]);
    // Cableroom cutout
    translate([2,10-CABLEROOM,2]) cube([32, 16+CABLEROOM, 2]);
    
    if( PCBADJ > 0 ) {
      translate([2,2,2]) cube([32, 36, 2]);
    }
    
    // Spare room at right (type is LEFT)
    translate([2,47.5,2]) cube([32, 8, 2]);
    
    // Drain
    translate([18,20,0]) cylinder(4, 0.75, 2.0);
    translate([24,6,0]) cylinder(4, 0.75, 2.0);
    translate([24,51,0]) cylinder(4, 0.75, 2.0);
    translate([24,42.5,0]) cylinder(4, 0.75, 2.0);
    
    // Top screw terminal holes
    translate([18,6,0]) cylinder(8, 1, 1);
    translate([18,6,0]) cylinder(2, 3, 1);
    translate([18,51,0]) cylinder(8, 1, 1);
    translate([18,51,0]) cylinder(2, 3, 1);
  }
  
  // Servo bed
  difference() {
    translate([6,10,2]) cube([24, 23.5, 2.5-PCBADJ]);
    translate([10,14,2]) cube([16, 15.5, 2.5-PCBADJ]);
  }

  // Strip for cover
  difference() {
    translate([0,5-CABLEROOM,4]) cube([36, 51+CABLEROOM, 4]);
    translate([2,10-CABLEROOM,4]) cube([32, 44+CABLEROOM, 4]);
    // Slider rack cutout
    translate([2, 3.5 + (26+13) - 2.6, 4]) cube([34, 5.2, 4]);

    // Cable cutout
    //translate([18,5-CABLEROOM,7]) cube([4, 4, 1]);

    // Cable outlet
    rotate([90,0,0]) translate([18,8,6]) cylinder(6, 2, 2);
    translate([12,-8,4]) cylinder(6, 0.75, 0.75);
    translate([24,-8,4]) cylinder(6, 0.75, 0.75);
    
    
    
    // Servo mounting cut
    translate([1, 28.7, 4]) cube([34, 2.8, 12]);
  }

  // Servo mounting
  difference() {
    translate([0,3.5 + 28, 2]) cube([36, 2, 14]);
    translate([6.5,3.5 + 28, 2]) cube([23, 2, 14]);
  }
  SERVOPAD=4.8; // MG90S (Use 4.5 for SG90)
  difference() {
    translate([0,3.5 + 28 - SERVOPAD, 2]) cube([36, 2, 14]);
    translate([6.5,3.5 + 28 - SERVOPAD, 2]) cube([23, 2, 14]);
  }
  translate([0,3.5 + 28 - SERVOPAD,4]) cube([1, 6.5, 12]);
  translate([35,3.5 + 28 - SERVOPAD,4]) cube([1, 6.5, 12]);


  // Top screw terminals
  difference() {
    translate([18,6,2]) cylinder(4, 4, 3);
    translate([18,6,2]) cylinder(4, 1, 1);
  }
  difference() {
    translate([18,51,2]) cylinder(4, 4, 3);
    translate([18,51,2]) cylinder(4, 1, 1);
  }


  // Base track mounting point
  //translate([0,0,0]) cube([36, 7, 4]);
  difference() {
    translate([0,52,0]) cube([36, 7, 4]);
    translate([2,10-CABLEROOM,2]) cube([32, 44+CABLEROOM, 2]);
    // Top screw terminal holes
    translate([18,51,0]) cylinder(8, 1, 1);
    translate([18,51,0]) cylinder(2, 3, 1);
  }
  
  // Track mounting point L1
  translate([43,3.5,0]){
    difference() {
      cylinder(4, 3.5, 3.5);
      cylinder(4, 1.25, 1.25);
    }
  }
  difference() {
    translate([36,0,0]) cube([7, 7, 4]);
    translate([43,3.5,0]) cylinder(4, 1.25, 1.25);
  }

  // Track mounting point L2
  translate([43,55.5,0]){
    difference() {
      cylinder(4, 3.5, 3.5);
      cylinder(4, 1.25, 1.25);
    }
  }
  difference() {
    translate([36,52,0]) cube([7, 7, 4]);
    translate([43,55.5,0]) cylinder(4, 1.25, 1.25);
  }



}

}

