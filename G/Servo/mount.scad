/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../../lib/shapes.scad>

WIDTH=0; // Extra width for 1.3" display mount

difference() {
  translate([0,0,0]) cube([34+WIDTH, 20, 12]);
  translate([0,0,2]) cube([34+WIDTH, 18, 10]);
  translate([5.5,18,2]) cube([23+WIDTH, 2, 10]);
  
  // Servo mount holes
  translate([3,18,8]) rotate([270,0,0]) cylinder(2, .8, .8);
  translate([3+28+WIDTH,18,8]) rotate([270,0,0]) cylinder(2, .8, .8);
  
  // Bottom mount holes
  translate([0,5,0]) cylinder(2, 1.5, 1.5);
  translate([0,11,0]) cylinder(2, 1.5, 1.5);
  translate([0-1.5,5,0]) cube([3, 6, 2]);

  translate([34+WIDTH,5,0]) cylinder(2, 1.5, 1.5);
  translate([34+WIDTH,11,0]) cylinder(2, 1.5, 1.5);
  translate([34+WIDTH-1.5,5,0]) cube([3, 6, 2]);
  
  
  // Logos:
  translate([6,5,1.6]){
    rotate(0) linear_extrude(0.4) text("Rocrail",5);
  }
}

// Bottom mount left
difference() {
  translate([0,5,0]) cylinder(2, 5, 5);
  translate([0,5,0]) cylinder(2, 1.5, 1.5);
  translate([0,11,0]) cylinder(2, 1.5, 1.5);
  translate([0-1.5,5,0]) cube([3, 6, 2]);
}
difference() {
  translate([0,11,0]) cylinder(2, 5, 5);
  translate([0,11,0]) cylinder(2, 1.5, 1.5);
  translate([0,5,0]) cylinder(2, 1.5, 1.5);
  translate([0-1.5,5,0]) cube([3, 6, 2]);
}
translate([-5.0,5,0]) cube([3, 6, 2]);

// Bottom mount right
difference() {
  translate([34+WIDTH,5,0]) cylinder(2, 5, 5);
  translate([34+WIDTH,5,0]) cylinder(2, 1.5, 1.5);
  translate([34+WIDTH,11,0]) cylinder(2, 1.5, 1.5);
  translate([34+WIDTH-1.5,5,0]) cube([3, 6, 2]);
}
difference() {
  translate([34+WIDTH,11,0]) cylinder(2, 5, 5);
  translate([34+WIDTH,11,0]) cylinder(2, 1.5, 1.5);
  translate([34+WIDTH,5,0]) cylinder(2, 1.5, 1.5);
  translate([34+WIDTH-1.5,5,0]) cube([3, 6, 2]);
}
translate([34+WIDTH+2.0,5,0]) cube([3, 6, 2]);

// Extra construction for Servo mounting
translate([0,13,2]) mirror([0,0,0]) prism(2, 5, 10);
translate([32+WIDTH,13,2]) mirror([0,0,0]) prism(2, 5, 10);

translate([0,20,0]) cube([34+WIDTH, 4, 2]);
