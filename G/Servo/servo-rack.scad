/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
/*
https://www.thingiverse.com/thing:1604369
https://github.com/justinotherguy/OpenSCAD-Getriebebibliothek
*/
include <../../lib/Getriebe.scad>

$fn=256;


LEVER=false;
DISK=false;
GEAR=true;
RACK=true;
MAGNET=true;
LGB=false;
SHORTSCREW=0; // use 1 for short screw

GEARAXEL=4.8; // 4.8 or 4.5 for AZ-Delivery

if( DISK ) {
 translate([30,20,0]) {
  difference() {
    translate([0,0,0]) cylinder(5, 3.5, 3.5);
    translate([0,0,0]) cylinder(5, 2.45, 2.45);
  }
  difference() {
    translate([0,0,0]) cylinder(2.0, 2.45, 2.45);
    translate([0,0,0]) cylinder(2.5, 1.3, 1.3);
  }
  difference() {
    translate([0,0,0]) cylinder(1.0, 9, 9);
    translate([0,0,0]) cylinder(2.5, 1.3, 1.3);
    for( a =[0:10] ) {
      rotate([0,0,a*36]) translate([7.5,0,0]) cylinder(4.0, .5, .5);
    }
  }
  
  
 }
}

// Lever
if( LEVER ) {
 translate([20,0,0]) {
  difference() {
    translate([10,0,0]) cylinder(2, 2.5, 2.5);
    translate([10,0,0]) cylinder(5, .5, .5);
    translate([8,0,0]) cylinder(5, .5, .5);
  }
  difference() {
    translate([0,-2.5,0]) cube([10, 5, 2]);
    translate([0,0,0]) cylinder(2, 2.45, 2.45);
    translate([8,0,0]) cylinder(5, .5, .5);
    translate([10,0,0]) cylinder(5, .5, .5);
    translate([8,0,0]) cylinder(5, .5, .5);
    translate([6,0,0]) cylinder(5, .5, .5);
  }
  difference() {
    translate([0,0,0]) cylinder(5, 3.5, 3.5);
    translate([0,0,0]) cylinder(5, 2.45, 2.45);
  }
  difference() {
    translate([0,0,0]) cylinder(2.0, 2.45, 2.45);
    translate([0,0,0]) cylinder(2.5, 1.3, 1.3);
  }
 }
}

// Gear
if( GEAR ) {
  translate([0,0,0]) stirnrad(1, 9, 5, GEARAXEL);
  difference() {
    translate([0,0,SHORTSCREW]) cylinder(1.2, 2.45, 2.45);
    translate([0,0,0]) cylinder(2.5, 1.3, 1.3);
  }
}

if( RACK ) {
  YOFF=14;
  SHORTRACK=LGB?0:4; // Short version. 4mm for HALL.
  // Rack 1 for Thiel switches.
  translate([0,YOFF,0]) {
    difference() {
      translate([-2.4,0,0]) zahnstange(1, 23.5, 3, 5);
      translate([-26,0.3,0]) cube([14, 1.8, 5]);
    }
    translate([-24+SHORTRACK,-3.5,0]) cube([33.5-SHORTRACK, 2, 5]);
    translate([-24+SHORTRACK,-1.5,0]) cube([11-SHORTRACK, 1.8, 5]);
    translate([-30+SHORTRACK,-3.0,1]) cube([2, 6, 3]);
    translate([-30+SHORTRACK,-3.5,0]) cube([8, 2, 5]);

    // Magnet
    if( MAGNET ) {
      difference() {
        translate([9,4,0]) cylinder(5.0, 2.4, 2.4);
        translate([9,4,0]) cylinder(5.0, 1.6, 1.6);
      }
      difference() {
        translate([8-1.4,0-3.5,0]) cube([4, 8, 5]);
        translate([9,4,0]) cylinder(5.0, 1.6, 1.6);
      }
    }
  }
}

