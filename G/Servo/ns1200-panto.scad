/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
//$fn=16;
include <../../lib/shapes.scad>


ISOLATOR=0;
MOUNT=0;
PANTOBASE_1=1;
PANTOBASE_2=0;
PANTOTOP=0;
PANTOCONTACT=0;

LEFT=1;

ML=50; // 
PL=32; // 


if( PANTOCONTACT ) {
  translate([20,0,0]) {

    difference() {
      translate([0,0,0]) cube([70,8,1.4]);
      translate([2,3,0]) cube([70-4,2,1.4]);
    }
    
    translate([(70-49)/2,2,1.4]) cube([49,1,2]);
    translate([(70-49)/2,5,1.4]) cube([49,1,2]);
    
    
    difference() {
      translate([0,0,6]) rotate([0,90,90]) cylinder(8, 6, 6);
      translate([0,0,6]) rotate([0,90,90]) cylinder(8, 6-1.4, 6-1.4);
      translate([-6,0,6]) cube([12,8,6]);
      translate([0,0,0]) cube([6,8,6]);
    }
    
    difference() {
      translate([70,0,6]) rotate([0,90,90]) cylinder(8, 6, 6);
      translate([70,0,6]) rotate([0,90,90]) cylinder(8, 6-1.4, 6-1.4);
      translate([70-6,0,6]) cube([12,8,6]);
      translate([70-6,0,0]) cube([6,8,6]);
    }
    
  }
}

if( PANTOBASE_1 ) {
  translate([20,0,0]) {

    if( LEFT ) {
      difference() {
        translate([40,6,0]) cylinder(1, 2, 2);
        translate([40,6,0]) cylinder(1, .9, .9);
      }
      difference() {
        translate([38,2,0]) cube([4,4,1]);
        translate([40,6,0]) cylinder(1, .9, .9);
      }
    }
    else {
      difference() {
        translate([40,-6,0]) cylinder(1, 2, 2);
        translate([40,-6,0]) cylinder(1, .9, .9);
      }
      difference() {
        translate([38,-6,0]) cube([4,4,1]);
        translate([40,-6,0]) cylinder(1, .9, .9);
      }
    }
   


    difference() {
      translate([0,0,0]) cylinder(1, 2, 2);
      translate([0,0,0]) cylinder(1, .9, .9);
    }
   
    difference() {
      translate([0,-2,0]) cube([45,4,1]);
      translate([0,0,0]) cylinder(1, .9, .9);
      translate([45,0,0]) cylinder(1, .9, .9);
    }
    
    difference() {
      translate([45,0,0]) cylinder(3, 2, 2);
      translate([45,0,0]) cylinder(3, .9, .9);
    }


    difference() {
      translate([45,4,0]) cylinder(1, 2, 2);
      translate([45,4,0]) cylinder(1, .9, .9);
    }
    difference() {
      translate([45-2,0,0]) cube([4,4,1]);
      translate([45,4,0]) cylinder(1, .9, .9);
      translate([45,0,0]) cylinder(3, .9, .9);
    }

  }
}

if( PANTOBASE_2 ) {
  translate([20,20,0]) {

      translate([0,0,0]) cube([20,1,3]);
      translate([0,47-1,0]) cube([20,1,3]);


      translate([2,0,0]) rotate([0,0,70]) cube([49.5,1,3]);
      translate([19,.5,0]) rotate([0,0,180-70]) cube([49.5,1,3]);

  }
}

if( PANTOTOP ) {
  translate([40,40,0]) {

    translate([0,2,0]) cube([49,4,1]);
    translate([0,3.5,0]) cube([49,1,3]);

    difference() {
      translate([0,2,0]) cube([1,4,4]);
      translate([0,2,2]) rotate([0,90,0]) cylinder(1, .9, .9);
      translate([0,2+4,2]) rotate([0,90,0]) cylinder(1, .9, .9);
    }
 
    difference() {
      translate([48,2,0]) cube([1,4,4]);
      translate([48,2,2]) rotate([0,90,0]) cylinder(1, .9, .9);
      translate([48,2+4,2]) rotate([0,90,0]) cylinder(1, .9, .9);
    }

    difference() {
      translate([0,2,2]) rotate([0,90,0]) cylinder(1, 2, 2);
      translate([0,2,2]) rotate([0,90,0]) cylinder(1, .9, .9);
    }
    difference() {
      translate([0,2+4,2]) rotate([0,90,0]) cylinder(1, 2, 2);
      translate([0,2+4,2]) rotate([0,90,0]) cylinder(1, .9, .9);
    }

    difference() {
      translate([48,2,2]) rotate([0,90,0]) cylinder(1, 2, 2);
      translate([48,2,2]) rotate([0,90,0]) cylinder(1, .9, .9);
    }
    difference() {
      translate([48,2+4,2]) rotate([0,90,0]) cylinder(1, 2, 2);
      translate([48,2+4,2]) rotate([0,90,0]) cylinder(1, .9, .9);
    }

  }
}

if( MOUNT ) {
  D=.9;
  translate([20,0,0]) {
    
    difference() {
      translate([-3,10,0]) cube([ML+2*3, 1.2, 6]);
      translate([0,12,3]) rotate([90,0,0]) cylinder(4, 1.75, 1.75);
      translate([ML,12,3]) rotate([90,0,0]) cylinder(4, 1.75, 1.75);
    }

    translate([8,5,0]) cube([ML-2*8, 1.2, 6]);
    difference() {
      translate([8,5,0]) cube([ML-2*8, 5, 2]);
      translate([(ML-PL)/2,8.25,0]) cylinder(10, D,D);
      translate([ML-(ML-PL)/2,8.25,0]) cylinder(10, D,D);
    }

    translate([3.1,10,0]) rotate([0,0,315]) cube([7, 1.2, 6]);
    translate([ML-8,5,0]) rotate([0,0,45]) cube([7, 1.2, 6]);


    difference() {
      translate([ML-8.5,6,2]) rotate([0,90,0]) prism(2,4,4);
      translate([ML-(ML-PL)/2,8.25,0]) cylinder(10, D,D);
    }
    difference() {
      translate([4.5,10,2]) rotate([90,90,0]) prism(2,4,4);
      translate([(ML-PL)/2,8.25,0]) cylinder(10, D,D);
    }


  }
}


if( ISOLATOR ) {
  difference() {
    translate([0,0,0]) cylinder(5, 2.5, 2.5);
    translate([0,0,0]) cylinder(5, 1.75, 1.75);
  }
  difference() {
    translate([0,0,0.8+1]) cylinder(.6, 4.5, 2.5);
    translate([0,0,0.8+1]) cylinder(.6, 1.75, 1.75);
  }
  difference() {
    translate([0,0,0.6+1]) cylinder(.2, 4.5, 4.5);
    translate([0,0,0.6+1]) cylinder(.2, 1.75, 1.75);
  }
  difference() {
    translate([0,0,0+1]) cylinder(.6, 2.5, 4.5);
    translate([0,0,0+1]) cylinder(.6, 1.75, 1.75);
  }
  
  
  difference() {
    translate([0,0,3.8]) cylinder(.6, 3.5, 2.5);
    translate([0,0,3.8]) cylinder(.6, 1.75, 1.75);
  }
  difference() {
    translate([0,0,3.6]) cylinder(.2, 3.5, 3.5);
    translate([0,0,3.6]) cylinder(.2, 1.75, 1.75);
  }
  difference() {
    translate([0,0,3]) cylinder(.6, 2.5, 3.5);
    translate([0,0,3]) cylinder(.6, 1.75, 1.75);
  }
}



