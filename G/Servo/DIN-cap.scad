/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/


/*
IMPORTANT:
Set the layer hight to 0.1mm, otherwise it won't 
print the thread correctly.
PETG:
Printspeed 10...20
Fan 10%
Temp 220/80
No support sructures
*/


include <../../lib/threads.scad>

$fn=360;

SNAPPER=true;
// DIN plug cap.

// Bottom plate
difference() {
  translate([ 0,0,0]) cylinder(2, 8.2, 8.2);
  translate([ 0,0,0]) cylinder(2, .5, .5);
  translate([ 0,9,0]) cylinder(2, 2, 2);
}

// Rope
difference() {
  translate([ 0,9.5,0]) cylinder(2, 4, 4);
  translate([ 0,9.5,0]) cylinder(2, 2, 2);
}
difference() {
  translate([ -4,3,0]) cube([8, 6.5, 2]);
  translate([ 0,9.5,0]) cylinder(2, 2, 2);
}

// Body
difference() {
  translate([ 0,0,0]) cylinder(14, 7.5, 7.5);
  translate([ 0,0,2]) cylinder(13, 6.4, 6.4);
  translate([ 0,0,0]) cylinder(2, .75, .75);
}

// Snapper
if( SNAPPER ) {
  difference() {
    translate([ 0,-6.7,0]) cylinder(8, 1.5, 1.5);
    difference() {
      translate([ 0,0,4]) cylinder(4, 9, 9);
      translate([ 0,0,4]) cylinder(4, 7, 7);
    }
  }
}

difference() {
  translate([ 0,0,7]) ScrewThread(16, 6, 1.0);
  translate([ 0,0,7]) cylinder(6, 6.4, 6.4);
}
