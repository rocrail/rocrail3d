/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

include <../../lib/shapes.scad>

BOTTOMLEN=180;
BOTTOMWIDTH=104;
SIDE=false;
HEAD=false;
ROAST=true;
DOOR=false;
FLIP=0;

if( ROAST ) {
  translate([BOTTOMLEN-20,0,0]) {

    difference() {
      translate([-1,-1,0]) cube([38, 26, .5]);
      translate([3,3,0]) cube([30, 18, 3]);
    }
    difference() {
      translate([0,0,0]) cube([36, 24, 3]);
      translate([2,2,.5]) cube([32, 20, 2.5]);
      translate([3,3,0]) cube([30, 18, 3]);
    }
    for( a =[0:6] ) {
      translate([2,a*3+3.5,0]) rotate([45,0,0]) cube([32, 1, 2]);
    }
    
  }
}

if( DOOR ) {
  translate([BOTTOMLEN+30,0,0]) {
    difference() {
      translate([0,4,0]) cube([60, 74, 2]);
      translate([0+3,4+3,1.2]) cube([60-6, 74-6, .8]);
      for( a =[0:10] ) {
        translate([0+3,a*6.8,1]) cube([60-6, .5, .5]);
      }
    }
    translate([4,4,0]) rotate([0,0,52]) cube([90, 4, 2]);
    translate([1,75,0]) rotate([0,0,-52]) cube([90, 4, 2]);
  }
}

if( SIDE ) {
  mirror([FLIP,0,0]) {
    difference() {
      translate([0,0,0]) cube([BOTTOMLEN-30, 90, 2]);
      for( a =[0:10] ) {
        translate([0,a*8,1.5]) cube([BOTTOMLEN-30, .5, .5]);
      }
      // Roast
      translate([47.9,55.9,0]) cube([36.2, 24.2, 2]);

      translate([BOTTOMLEN-50,4,0]) cube([30, 74, 5]);
    }

    // Door
    translate([87,0,0]) cube([BOTTOMLEN-30-87, 4, 6.2]);
    translate([87,78.2,0]) cube([BOTTOMLEN-30-87, 4, 6.2]);
    translate([87,0,6.2]) cube([BOTTOMLEN-30-87, 4.5, .8]);
    translate([87,77.7,6.2]) cube([BOTTOMLEN-30-87, 4.5, .8]);

    // Holders
    for( b =[0:3] ) {
      // New bottom holders: 30+42
      translate([1+(b*42)+0.1,-8,0]) cube([3.8, 8+90, 3.8]);
    }

    translate([0,0,0]) cube([BOTTOMLEN-30, 4, 3]);
    translate([0,90-4,0]) cube([BOTTOMLEN-30, 4, 3]);

    translate([0,0,0]) cube([4, 90, 3]);

    
    translate([5,2,0]) rotate([0,0,64]) cube([95, 4, 3]);
  }
}



if( HEAD ) {
  translate([-BOTTOMWIDTH-10,0,0]) {
    
    // Base
    difference() {
      translate([0,0,0]) cube([BOTTOMWIDTH, 90, 2]);
      for( a =[0:10] ) {
        translate([0,a*8,1.5]) cube([BOTTOMLEN-30, .5, .5]);
      }
      
      // sluitlichten
      translate([10,18,0]) cylinder(6, 2.5, 2.5);
      translate([BOTTOMWIDTH-10,18,0]) cylinder(6, 2.5, 2.5);

      // Roast
      translate([(BOTTOMWIDTH/2)-(36.2/2),55.9,0]) cube([36.2, 24.2, 2]);
    }
    
    // Construction
    translate([((BOTTOMWIDTH-40)/2)-4+.1,-8,0]) cube([3.8, 109, 2.8]);
    translate([((BOTTOMWIDTH-40)/2)+40+.1,-8,0]) cube([3.8, 109, 2.8]);
    
    translate([0,90-4,0]) cube([BOTTOMWIDTH, 4, 3]);
    
    // sluitlichten
    difference() {
      translate([10,18,2]) cylinder(4, 3.5, 3);
      translate([10,18,0]) cylinder(6, 2.5, 2.5);
    }
    difference() {
      translate([BOTTOMWIDTH-10,18,2]) cylinder(4, 3.5, 3);
      translate([BOTTOMWIDTH-10,18,0]) cylinder(6, 2.5, 2.5);
    }
    
    // Roof rounding
    difference() {
      translate([BOTTOMWIDTH/2,0,0]) cylinder(2, BOTTOMWIDTH, BOTTOMWIDTH);
      translate([-(BOTTOMWIDTH/2),-118,0]) cube([BOTTOMWIDTH*2, BOTTOMWIDTH*2, 2]);

      // Roof screws
      translate([18,94,0]) cylinder(4, 1.2, 1.2); 
      translate([BOTTOMWIDTH-18,94,0]) cylinder(4, 1.2, 1.2); 
      
      for( a =[10:12] ) {
        translate([0,a*8,1.5]) cube([BOTTOMLEN-30, .5, .5]);
      }
      
    }
  }
}




