/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

include <../../lib/shapes.scad>
BOTTOMLEN=180;
BOTTOMWIDTH=104;

SEATWIDTH=42;
SEATLENGTH=16;


module bottom() {
  translate([1.4,0,0]) {  
    difference() {
      translate([5,16,0]) cylinder(SEATWIDTH, 4, 4);
      translate([5,16,0]) cylinder(SEATWIDTH, 3, 3);
      translate([0,8,0]) cube([8, 8, SEATWIDTH]);
      translate([5,12,0]) cube([8, 8, SEATWIDTH]);
    }

    translate([5,19,0]) cube([5, 1, SEATWIDTH]);


    difference() {
      translate([10,23,0]) cylinder(SEATWIDTH, 4, 4);
      translate([10,23,1]) cylinder(SEATWIDTH, 3, 3);
      translate([3,20,1]) cube([8, 8, SEATWIDTH]);
    }
    translate([-1.4,19.3,0]) cube([11+1.4, 13, 1]);

    translate([11,26,0]) cube([1, 10, SEATWIDTH]);
  }
}

// Top rounding
difference() {
  translate([0,0,0]) cylinder(SEATWIDTH, 2, 2);
  translate([0,0,1]) cylinder(SEATWIDTH, 1, 1);
  //translate([-2,0.5,0]) cube([4, 4, SEATWIDTH]);
}

// Back
translate([1,0,0]) rotate([0,0,-5]) cube([1.0, 17, SEATWIDTH]);
translate([-2,0,0]) rotate([0,0,5]) cube([1.0, 17, SEATWIDTH]);

translate([-2,0,0]) cube([4, 8, 1]);
translate([-2.7,8,0]) cube([5.4, 10, 1]);
translate([-4,17.8,0]) cube([8, 9, 1]);

// Sit bottom
translate([0,0,0]) bottom();
translate([0,0,0]) mirror([1,0,0]) bottom();




