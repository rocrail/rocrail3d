/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

include <../../lib/shapes.scad>

PLATFORM=false;
RAILING=false;
BUFFER=0;
BUFFER_SPRING=0;
CHASSIS=0;
STEP=false;
HOLDER=0;
HOLDEROPEN=8; // 13 for freight wagon

STANDARDWIDTH=104;
BOTTOMWIDTH=80; // 104=standard, 80=container
HoH=68;
CONTAINER=1;
PRESSURE=0;
IDPLATE=1;
ELECTRIC=0;

TAILLIGHT=0;
TAILLIGHTLENS=0;
CHASSISLIGHT=0;

module ledlamp(d) {
  // koplamp
  h=6;
  difference() {
    // buitenkant
    translate([0,0,0]) cylinder(h,d/2,d/2);
    // LED cylinder
    translate([0,0,4.5]) cylinder(h-4.5,5/2,5/2);
    // lens opening
    translate([0,0,0]) cylinder(2,(d-2)/2,(d-2)/2);
    // tapse toeloop naar LED
    translate([0,0,2]) cylinder(2.5,(d-2)/2,5/2);
  }
  
  // LED houder
  difference() {
    translate([0,0,h]) cylinder(3,7/2,7/2);
    translate([0,0,h]) cylinder(3,5/2,5/2);
  }
  
}

module ledLens(d) {
  difference() {
    translate([0,0,0]) cylinder(1.8,d/2,d/2);
    translate([0,0,.4]) cylinder(1.8,(d-2)/2,(d-2)/2);
  }
}


if( ELECTRIC ) {
  translate([0,0,0]) {
    translate([0,0,0]) cube([4, 8, 2]);
    translate([6,2,0]) cube([6, 4, 2]);
    translate([14,0,0]) cube([4, 8, 2]);
    translate([-6,2.5,0]) cube([30, 1, 1]);
    translate([-6,4.5,0]) cube([30, 1, 1]);
  }
}

if( IDPLATE ) {
  translate([0,0,0]) {
    translate([0,4,0]) cube([38, 22-4, 1]);
    translate([0,0,0]) rcube(1, 38, 8, 2);
    translate([0,22-.6,0]) rotate([10,0,0]) cube([38, .8, 4]);

    translate([2,14,1]) linear_extrude(.6) text("KB-C005",4);
    translate([2,8,1]) linear_extrude(.6) text("ROCLINES",4);
    translate([2,2,1]) linear_extrude(.6) text("LB 14120  16 t",3);

  }
}


if( PRESSURE ) {
  translate([0,0,0]) {
    difference() {
      translate([0,0,5]) rotate([0,90,0]) cylinder(50, 5, 5);
      translate([0,0,5]) rotate([0,90,0]) cylinder(50, 4, 4);
    }
    translate([5,-5.5/2,9]) cube([2, 5.5, 9]);
    translate([50-5-2,-5.5/2,9]) cube([2, 5.5, 9]);


    difference() {
      translate([0,0,5]) sphere(5);
      translate([0,0,5]) sphere(4);
      translate([0,-5,0]) cube([5, 10, 10]);
    }
    
    difference() {
      translate([50,0,5]) sphere(5);
      translate([50,0,5]) sphere(4);
      translate([45,-5,0]) cube([5, 10, 10]);
    }
  }
}


// Buffer
if( BUFFER ) {
translate([-30,0,0]) {
  // Plate
  roundedcube(17,13,2,2);
  translate([8.5,6.5,2]) cylinder(2, 5, 5);
  // Spring cylinder
  difference() {
    translate([8.5,6.5,0]) cylinder(14, 4, 4);
    translate([8.5,6.5,3]) cylinder(11, BUFFER_SPRING?3.1:3, BUFFER_SPRING?3.1:3);
    if( BUFFER_SPRING )
      translate([8,2,9]) cube([1, 2, 4]);
  }
}
}


if( TAILLIGHT ) translate([(STANDARDWIDTH)/2,40,0]) {
  // de lampen
  translate([-20,0,0]) ledlamp(10);
  translate([20,0,0]) ledlamp(10);
  
  // schroef bevestiging
  difference() {
    translate([0,0,4]) cylinder(6,3,3);
    translate([0,0,4]) cylinder(6,1.2,1.2);
  }
    
  // behuizing
  difference() {
    translate([-20-7,-6,4]) rcube(8,40+2*7,12,5);
    translate([-20-7+1.2,-6+1.2,4+2]) rcube(12,40+2*7-2*1.2,12-2*1.2,3.6);
    translate([-20,0,4]) cylinder(2,8/2,8/2);
    translate([20,0,4]) cylinder(2,8/2,8/2);
  }

  difference() {
    translate([-20-7,-6,0]) rcube(4,7+7,12,5);
    translate([-20-7+1.2,-6+1.2,1.2]) rcube(4,7+7-2*1.2,12-2*1.2,5);
    translate([-20,0,0]) cylinder(4,8/2,8/2);
  }
  difference() {
    translate([20-7,-6,0]) rcube(4,7+7,12,5);
    translate([20-7+1.2,-6+1.2,1.2]) rcube(4,7+7-2*1.2,12-2*1.2,5);
    translate([20,0,0]) cylinder(4,8/2,8/2);
  }
}



if( TAILLIGHTLENS ) translate([(STANDARDWIDTH)/2,40,0]) {
  translate([-20,20,0]) ledLens(8);
}



// Chassis
if( CHASSIS ) {
translate([0,0,0]) {
  
  if( CHASSISLIGHT ) translate([(STANDARDWIDTH)/2,20,0]) {
    difference() {
      translate([-20-8,-13,0]) rcube(2,40+2*8,20,6);
      translate([0,0,0]) cylinder(2,1.5,1.5);
      translate([10,0,0]) cylinder(2,1.5,1.5);
    }
    
    difference() {
      translate([-20-7+1.2,-6+1.2,2]) rcube(3,40+2*7-2*1.2,12-2*1.2,3.6);
      translate([-20-7+2.4,-6+2.4,2]) rcube(3,40+2*7-2*2.4,12-2*2.4,2.6);
    }
  }
  
  // Base plate
  difference() {
    translate([(STANDARDWIDTH-BOTTOMWIDTH)/2,CONTAINER?-1:0,0]) cube([BOTTOMWIDTH, CONTAINER?14:15, 2]);
    
    translate([18,7.5,0]) cylinder(2, 1.25, 1.25);
    translate([STANDARDWIDTH-18,7.5,0]) cylinder(2, 1.25, 1.25);
    
    if( HOLDER ) {
      translate([30,4,0]) cube([6, HOLDEROPEN+.5, 2]);
      translate([BOTTOMWIDTH-(30+6),4,0]) cube([6, HOLDEROPEN+.5, 2]);
    }
  }
  
  // Sub plate
  translate([30,-4,0]) cube([44, 4, 2]);
  
  translate([74,-4,2]){
    rotate([0,90,0]) mirror([0,0,0]) prism(2, 4, 4);
  }
  translate([30,-4,2]){
    rotate([0,90,0]) mirror([0,0,1]) prism(2, 4, 4);
  }
    
  
  // Buffer L connections 68 hoh
  difference() {
  translate([18-6,7.5-6,2]) roundedcube(12,11.5,.8,1);
    translate([18,7.5,0]) cylinder(15, 1.5, 1.5);
  }
  translate([18-4,7.5+4,0]) cylinder(3.5, .75, .75);
  translate([18-4,7.5-4,0]) cylinder(3.5, .75, .75);
  translate([18+4,7.5+4,0]) cylinder(3.5, .75, .75);
  translate([18+4,7.5-4,0]) cylinder(3.5, .75, .75);
  
  difference() {
    translate([18,7.5,2]) cylinder(1.8, 4.5, 4.5);
    translate([18,7.5,2]) cylinder(15, 2, 2);
  }
  difference() {
    translate([18,7.5,2]) cylinder(2.8, 4, 4);
    translate([18,7.5,2]) cylinder(15, 2, 2);
  }
  difference() {
    translate([18,7.5,2]) cylinder(13, 3, 3);
    translate([18,7.5,2]) cylinder(13, 2, 2);
    translate([18,7.5,0]) cylinder(15, 1.5, 1.5);
      
  }
  
  // Buffer R
  difference() {
  translate([STANDARDWIDTH-18-6,7.5-6,2]) roundedcube(12,11.5,.8,1);
    translate([STANDARDWIDTH-18,7.5,0]) cylinder(15, 1.5, 1.5);
  }
  translate([HoH+18-4,7.5+4,0]) cylinder(3.5, .75, .75);
  translate([HoH+18-4,7.5-4,0]) cylinder(3.5, .75, .75);
  translate([HoH+18+4,7.5+4,0]) cylinder(3.5, .75, .75);
  translate([HoH+18+4,7.5-4,0]) cylinder(3.5, .75, .75);

  difference() {
  translate([STANDARDWIDTH-18,7.5,2]) cylinder(1.8, 4.5, 4.5);
    translate([STANDARDWIDTH-18,7.5,2]) cylinder(15, 2, 2);
  }
  difference() {
  translate([STANDARDWIDTH-18,7.5,2]) cylinder(2.8, 4, 4);
    translate([STANDARDWIDTH-18,7.5,2]) cylinder(15, 2, 2);
  }
  difference() {
    translate([STANDARDWIDTH-18,7.5,2]) cylinder(13, 3, 3);
    translate([STANDARDWIDTH-18,7.5,2]) cylinder(13, 2, 2);
    translate([STANDARDWIDTH-18,7.5,0]) cylinder(15, 1.5, 1.5);
  }
  
  // Hook
  translate([104/2-3,6,2]) rotate([0,90,0]) cylinder(6, .8, .8);
  difference() {
    translate([104/2-1,5,2]) rotate([0,90,0]) cylinder(2, 4, 4);
    translate([104/2-1,5-4,-4]) cube([2, 8, 4]);
  }
    
  if( CONTAINER ) {
    translate([12,-20,0]) cube([3, 20, 2]);
    translate([6,-20,0]) rotate([90,0,0]) rcube(1.2, 10, 10, 2);
    translate([15,-17,2]) rotate([0,0,180]) prism(3, 3, 3);

    translate([14,12,1]) rotate([270,0,0]) cylinder(30, 1, 1);
    translate([8,22,1]) rotate([270,0,0]) cylinder(20, 1, 1);
    translate([12,22,1]) rotate([90,0,0]) tube(1, 5);
    translate([11,43,1]) rotate([90,180,90]) tube(1, 3);
    translate([10,42,1]) rotate([90,180,180]) tube(1, 3);
  }  
  
  }
}

// Platform
if( RAILING ) {
  translate([0,0,0]) {
    translate([-5,-15,0]) cube([5, 75, 3]);
    translate([BOTTOMWIDTH,-15,0]) cube([5, 75, 3]);
    difference() {
      translate([-5, 55,0]) cube([BOTTOMWIDTH+10, 5, 3]);
      translate([0, 55,2]) cube([BOTTOMWIDTH, 4, 1]);
    }
    translate([30,15,0]) cube([4, 40, 2]);
    translate([BOTTOMWIDTH-30,15,0]) cube([4, 40, 2]);
    
    // Break
    difference() {
      translate([30+12,15,0]) cube([4, 45, 3]);
      translate([30+12+1,15,1.5]) cube([2, 45, 1.5]);
    }
    translate([30+12+2,15+45,2]) rotate([90,0,0]) cylinder(45, .6, .6);
    translate([30+12,11,0]){
      rotate([0,0,0]) mirror([0,0,0]) prism(4, 4, 4);
    }
    translate([30+12+2,15+45+8,2]) rotate([90,0,0]) cylinder(8, 1, 1);
    
    translate([30,65,0]) cube([20, 2, 4]);
    translate([30+5,15+45+12,2]) rotate([90,0,0]) cylinder(6, 1, 1);
    
  }
}

if( PLATFORM ) {
  translate([0,100,0]) {
    difference() {
      translate([0,0,0]) cube([BOTTOMWIDTH+6, 30, 2]);
      translate([0,4,0]) cube([3, 30-8, 2]);
      translate([BOTTOMWIDTH+3,4,0]) cube([4, 30-8, 2]);
      // Holes for step
      translate([0.9,0.9,0]) cube([2.2, 2.2, 2]);
      translate([0.9,30-3.1,0]) cube([2.2, 2.2, 2]);
      // Holes for step
      translate([BOTTOMWIDTH+2.9,0.9,0]) cube([2.2, 2.2, 2]);
      translate([BOTTOMWIDTH+2.9,30-3.1,0]) cube([2.2, 2.2, 2]);
      
      // Structure
      
      for( a =[0:4] ) {
        translate([3+4,5+a*5,1.6]) cube([BOTTOMWIDTH-8, .4, .4]);
      }  
      translate([3+4,5,1.6]) cube([.4, 20, .4]);
      translate([BOTTOMWIDTH-1,5,1.6]) cube([.4, 20.4, .4]);
      
    }
  }
}

if( STEP ) {
  translate([BOTTOMWIDTH+20,100,0]) {
    difference() {
      translate([0,0,0]) cube([2, 30, 3]);
      translate([0,28,2]) cube([2, 2, 1]);
    }
    difference() {
      translate([30-4,0,0]) cube([2, 30, 3]);
      translate([30-4,28,2]) cube([2, 2, 1]);
    }
    translate([-1,0,0]) cube([30, 2, 8]);
    translate([-1,14,0]) cube([30, 2, 8]);
  }
}
