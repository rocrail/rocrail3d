/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

include <../../lib/shapes.scad>
// minus platform, head, spant, margin
//BOTTOMLEN=180-30-4-2-1.0; // Coach: Spant 
BOTTOMLEN=180-30-2-1.0; // Openload
BOTTOMWIDTH=104;
MIDLEN=126;

CABLELEFT=true;
CABLERIGHT=false;
TANKHOLE=false;
STRUCTURE=true;

FLOOR=true;
MIDFLOOR=false;

if( MIDFLOOR ) {
  translate([160,0,0]) {
    difference() {
      translate([0,0,0]) cube([MIDLEN, BOTTOMWIDTH, 3]);
      translate([4,2,1]) cube([MIDLEN-6, BOTTOMWIDTH-4, 2]);
      if( STRUCTURE ) {
        for( a =[0:17] ) {
          translate([a*8.4,0,0]) cube([.5, BOTTOMWIDTH, .5]);
        }
      }
    }
    
    // Langs profielen met 25mm tussenruimte
    difference() {
      translate([0,37.5,0]) cube([MIDLEN, 2, 3]);
      if( STRUCTURE ) {
        for( a =[0:17] ) {
          translate([a*8.4,0,0]) cube([.5, BOTTOMWIDTH, .5]);
        }
      }
    }
    difference() {
      translate([0,BOTTOMWIDTH-37.5-2,0]) cube([MIDLEN, 2, 3]);
      if( STRUCTURE ) {
        for( a =[0:17] ) {
          translate([a*8.4,0,0]) cube([.5, BOTTOMWIDTH, .5]);
        }
      }
    }
    
    
  }
}


if( FLOOR ) {
  difference() {
    translate([0,0,0]) cube([BOTTOMLEN, BOTTOMWIDTH, 3]);
    translate([4,2,1]) cube([BOTTOMLEN-6, BOTTOMWIDTH-4, 2]);
    if( STRUCTURE ) {
      for( a =[0:17] ) {
        translate([a*8.15,0,0]) cube([.5, BOTTOMWIDTH, .5]);
      }
    }

    // Cable holes
    if( CABLELEFT ) {
      translate([0,6.5,0]) cube([3, 8.5, 3]);
      //translate([5,14,0]) cylinder(1, 2, 2);
    }
    if( CABLERIGHT ) {
      translate([0,BOTTOMWIDTH-(6.5+8.5),0]) cube([3, 8.5, 3]);
      //translate([5,BOTTOMWIDTH-14,0]) cylinder(1, 2, 2);
    }
    
    // Tank hole
    if( TANKHOLE ) {
      translate([BOTTOMLEN-112,BOTTOMWIDTH/2,0]) cylinder(10, 2, 2);
    }
  }

  // Langs profielen met 25mm tussenruimte
  difference() {
    translate([0,37.5,0]) cube([BOTTOMLEN, 2, 3]);
    if( STRUCTURE ) {
      for( a =[0:17] ) {
        translate([a*8.15,0,0]) cube([.5, BOTTOMWIDTH, .5]);
      }
    }
  }
  difference() {
    translate([0,BOTTOMWIDTH-37.5-2,0]) cube([BOTTOMLEN, 2, 3]);
    if( STRUCTURE ) {
      for( a =[0:17] ) {
        translate([a*8.15,0,0]) cube([.5, BOTTOMWIDTH, .5]);
      }
    }
  }
}
