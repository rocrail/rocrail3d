/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;

include <../../lib/shapes.scad>
BOTTOMLEN=245; //180; // 180 standaard, 245 container
BOTTOMWIDTH=80; //104; // 104 standaard, 80 container
MIDLEN=126;

// Print separately!
BOTTOM=0;
MIDBOTTOM=0;
CONN=0;  // rounded (Piko)
CONN2=1; // flat (Rocrail)
CONTAINER=1;
BEAM=0;
FLIP=0;
HOLDERS=0;
HOLDERSTART=30; // Old=10
HOLDERDIST=42; // Old=48

if( MIDBOTTOM ) {
  // Bodemplaat
  translate([200,0,0]) {
    difference() {
      translate([0,0,0]) cube([MIDLEN, BOTTOMWIDTH, 2]);
      translate([0,12,0]) cube([MIDLEN, 80, 2]);
      
      translate([20,6,0]) cylinder(2, 1.7, 1.7);
      translate([MIDLEN/2,6,0]) cylinder(2, 1.7, 1.7);
      translate([MIDLEN-20,6,0]) cylinder(2, 1.7, 1.7);
      
      translate([20,BOTTOMWIDTH-6,0]) cylinder(2, 1.7, 1.7);
      translate([MIDLEN/2,BOTTOMWIDTH-6,0]) cylinder(2, 1.7, 1.7);
      translate([MIDLEN-20,BOTTOMWIDTH-6,0]) cylinder(2, 1.7, 1.7);
    }

    // Kopse kanten
    difference() {
      translate([0,12,0]) cube([2, BOTTOMWIDTH-24, 10]);
      translate([0,18,5]) rotate([0,90,0]) cylinder(20, 1.7, 1.7);
      translate([0,BOTTOMWIDTH/2,5]) rotate([0,90,0]) cylinder(20, 1.7, 1.7);
      translate([0,BOTTOMWIDTH-18,5]) rotate([0,90,0]) cylinder(20, 1.7, 1.7);
    }
    difference() {
      translate([MIDLEN-2,12,0]) cube([2, BOTTOMWIDTH-24, 10]);
      translate([MIDLEN-2,18,5]) rotate([0,90,0]) cylinder(20, 1.7, 1.7);
      translate([MIDLEN-2,BOTTOMWIDTH/2,5]) rotate([0,90,0]) cylinder(20, 1.7, 1.7);
      translate([MIDLEN-2,BOTTOMWIDTH-18,5]) rotate([0,90,0]) cylinder(20, 1.7, 1.7);
    }


    // Sleuven voor de H-profielen
    difference() {
      translate([0,0,2]) cube([MIDLEN, 12, 8]);
      translate([0,2,2]) cube([MIDLEN, 8, 8]);
    }
    difference() {
      translate([0,92,2]) cube([MIDLEN, 12, 8]);
      translate([0,94,2]) cube([MIDLEN, 8, 8]);
    }

    // Langs profielen met 25mm tussenruimte
    difference() {
      translate([0,37.5,0]) cube([MIDLEN, 2, 6]);
    }
    difference() {
      translate([0,37.5+27,0]) cube([MIDLEN, 2, 6]);
    }

    // Dwarsbalken
    // WIO-Room 84mm
    translate([MIDLEN/2-40,12,0]) cube([2, 80, 8]);
    translate([MIDLEN/2-1,12,0]) cube([2, 80, 8]);
    translate([MIDLEN/2+38,12,0]) cube([2, 80, 8]);

    // Holders
    if( HOLDERS ) {
      for( a =[0:2] ) {
        translate([(HOLDERDIST/2-3)+a*HOLDERDIST,-5,0]) holder();
        translate([(HOLDERDIST/2-3)+a*HOLDERDIST,BOTTOMWIDTH+5,0]) mirror([0,1,0]) holder();
      }
    }
  }
    
}

// Bodemplaat
if( BOTTOM ) {
  difference() {
    translate([0,0,0]) cube([BOTTOMLEN, BOTTOMWIDTH, 2]);
    translate([20,12,0]) cube([BOTTOMLEN-20-10, BOTTOMWIDTH-24, 2]);

    translate([20,6,0]) cylinder(2, 3, 1.7);
    translate([BOTTOMLEN/2,6,0]) cylinder(2, 3, 1.7);
    translate([BOTTOMLEN-20,6,0]) cylinder(2, 3, 1.7);

    translate([20,BOTTOMWIDTH-6,0]) cylinder(2, 3, 1.7);
    translate([BOTTOMLEN/2,BOTTOMWIDTH-6,0]) cylinder(2, 3, 1.7);
    translate([BOTTOMLEN-20,BOTTOMWIDTH-6,0]) cylinder(2, 3, 1.7);
  }

  // Kopse kant buffers
  difference() {
    translate([0,0,0]) cube([4, BOTTOMWIDTH, 14]);
    translate([0,18,5]) rotate([0,90,0]) cylinder(20, 1.7, 1.7);
    translate([0,BOTTOMWIDTH/2,5]) rotate([0,90,0]) cylinder(20, 1.7, 1.7);
    translate([0,BOTTOMWIDTH-18,5]) rotate([0,90,0]) cylinder(20, 1.7, 1.7);

    translate([0,BOTTOMWIDTH/2+68/2,5.5]) rotate([0,90,0]) cylinder(20, .8, .8);
    translate([0,BOTTOMWIDTH/2-68/2,5.5]) rotate([0,90,0]) cylinder(20, .8, .8);
  }

  // Kopse kant midden
  difference() {
    translate([BOTTOMLEN-2,12,0]) cube([2, BOTTOMWIDTH-24, 10]);
    translate([BOTTOMLEN-2,18,5]) rotate([0,90,0]) cylinder(20, 1.7, 1.7);
    translate([BOTTOMLEN-2,BOTTOMWIDTH/2,5]) rotate([0,90,0]) cylinder(20, 1.7, 1.7);
    translate([BOTTOMLEN-2,BOTTOMWIDTH-18,5]) rotate([0,90,0]) cylinder(20, 1.7, 1.7);
  }

  // Sleuven voor de H-profielen
  difference() {
    translate([0,0,2]) cube([BOTTOMLEN, 12, 8]);
    translate([0,2,2]) cube([BOTTOMLEN, 8, 8]);
  }
  difference() {
    translate([0,BOTTOMWIDTH-12,2]) cube([BOTTOMLEN, 12, 8]);
    translate([0,BOTTOMWIDTH-12+2,2]) cube([BOTTOMLEN, 8, 8]);
  }

  if( HOLDERS ) {
    // Dwarsbalk voor coach head holders
    translate([28,12,0]) cube([2, 80, 6]);
    translate([34,((BOTTOMWIDTH-40)/2)-5,0]) rotate(90) holder(6);
    translate([34,((BOTTOMWIDTH-40)/2)+39,0]) rotate(90) holder(6);
  }


  // Dwarsbalk 1
  difference() {
    translate([60,12,0]) cube([2, BOTTOMWIDTH-24, 8]);
    translate([60,BOTTOMWIDTH/2-36/2,6]) cube([16, 36, 2]);
  }
  // Dwarsbalk 2
  difference() {
    translate([60+2+12,12,0]) cube([2, BOTTOMWIDTH-24, 8]);
    translate([60+2+12,BOTTOMWIDTH/2-36/2,6]) cube([16, 36, 2]);
  }
  // Dwarsbalk 3
  difference() {
    translate([BOTTOMLEN/2+15+8,12,0]) cube([2, BOTTOMWIDTH-24, CONTAINER?10:6]);
    // Hole for shortcoupler spring.
    translate([140,BOTTOMWIDTH/2,CONTAINER?6:3]) rotate([0,90,0]) cylinder(20, 1.7, 1.7);
    translate([140,18,5]) rotate([0,90,0]) cylinder(20, 1.5, 1.5);
    translate([140,BOTTOMWIDTH-18,5]) rotate([0,90,0]) cylinder(20, 1.5, 1.5);
  }
  
  
  // Langs profielen met 25mm tussenruimte
  difference() {
    translate([0,BOTTOMWIDTH/2-29/2,0]) cube([BOTTOMLEN, 2, 6]);
  }
  difference() {
    translate([0,BOTTOMWIDTH/2-29/2+27,0]) cube([BOTTOMLEN, 2, 6]);
  }

  // Draaipunt bevestiging
  difference() {
    translate([60,BOTTOMWIDTH/2-29/2,0]) cube([16, 29, 2]);
    translate([68,BOTTOMWIDTH/2,0]) cylinder(6, 1.7, 1.7);
  }
  
  if( CONTAINER ) {
    // container M3 mount
    difference() {
      translate([BOTTOMLEN/2+5-16/2,BOTTOMWIDTH/2-29/2,0]) cube([16, 29, 2]);
      translate([BOTTOMLEN/2+5+3,BOTTOMWIDTH/2,0]) cylinder(6, 1.7, 1.7);
      translate([BOTTOMLEN/2+5-3,BOTTOMWIDTH/2,0]) cylinder(6, 1.7, 1.7);
      translate([BOTTOMLEN/2+5-3,BOTTOMWIDTH/2-1.7,0]) cube([6,2*1.7,2]);
    }
    // HoH=86, 86-80=6
    // 2 * 20'
    difference() {
      translate([BOTTOMLEN-8-1  ,-7,0]) cube([8,7,6]);
      translate([BOTTOMLEN-8-1+1,-7,4]) cube([6,7,6]);
      translate([BOTTOMLEN-8-1+4,-3,0]) cylinder(6, 1.5, 1.5);
      translate([BOTTOMLEN-8-1+4,-3,0]) cylinder(1, 2.5, 2.5);
    }
    difference() {
      translate([BOTTOMLEN-8-1-226,-7,0]) cube([8,7,6]);
      translate([BOTTOMLEN-8-1-226+1,-7,4]) cube([6,7,2]);
      translate([BOTTOMLEN-8-1-226+4,-3,0]) cylinder(6, 1.5, 1.5);
      translate([BOTTOMLEN-8-1-226+4,-3,0]) cylinder(1, 2.5, 2.5);
    }
    
    difference() {
      translate([BOTTOMLEN-8-1,BOTTOMWIDTH,0]) cube([8,7,6]);
      translate([BOTTOMLEN-8-1+1,BOTTOMWIDTH,4]) cube([6,7,4]);
      translate([BOTTOMLEN-8-1+4,BOTTOMWIDTH+3,0]) cylinder(6, 1.5, 1.5);
      translate([BOTTOMLEN-8-1+4,BOTTOMWIDTH+3,0]) cylinder(1, 2.5, 2.5);
    }
    difference() {
      translate([BOTTOMLEN-8-1-226,BOTTOMWIDTH,0]) cube([8,7,6]);
      translate([BOTTOMLEN-8-1-226+1,BOTTOMWIDTH,4]) cube([6,7,4]);
      translate([BOTTOMLEN-8-1-226+4,BOTTOMWIDTH+3,0]) cylinder(6, 1.5, 1.5);
      translate([BOTTOMLEN-8-1-226+4,BOTTOMWIDTH+3,0]) cylinder(1, 2.5, 2.5);
    }

    // 1 * 30'
    difference() {
      translate([BOTTOMLEN-169.5-4,-12,0]) cube([8,12,6]);
      translate([BOTTOMLEN-169.5-4,-12,4]) cube([8,5,6]);
      translate([BOTTOMLEN-169.5-4+1,-12,4]) cube([6,12,4]);
      translate([BOTTOMLEN-169.5,-3,0]) cylinder(6, 1.5, 1.5);
      translate([BOTTOMLEN-169.5,-3,0]) cylinder(1, 2.5, 2.5);
    }
    difference() {
      translate([BOTTOMLEN-169.5-4,BOTTOMWIDTH,0]) cube([8,12,6]);
      translate([BOTTOMLEN-169.5-4,BOTTOMWIDTH+7,4]) cube([8,5,6]);
      translate([BOTTOMLEN-169.5-4+1,BOTTOMWIDTH,4]) cube([6,12,4]);
      translate([BOTTOMLEN-169.5,BOTTOMWIDTH+3,0]) cylinder(6, 1.5, 1.5);
      translate([BOTTOMLEN-169.5,BOTTOMWIDTH+3,0]) cylinder(1, 2.5, 2.5);
    }
    
    // 30' versteviging
    translate([BOTTOMLEN-169.5-10,-12,0]) cube([20,2,4]);
    translate([BOTTOMLEN-169.5-8.6,-10.5,0]) rotate([0,0,45+90]) cube([17,2,4]);
    translate([BOTTOMLEN-169.5+10,-12,0]) rotate([0,0,45]) cube([17,2,4]);
    
    translate([BOTTOMLEN-169.5-10,BOTTOMWIDTH+10,0]) cube([20,2,4]);
    translate([BOTTOMLEN-169.5-20.6,BOTTOMWIDTH-1.5,0]) rotate([0,0,45]) cube([17,2,4]);
    translate([BOTTOMLEN-169.5+22,BOTTOMWIDTH,0]) rotate([0,0,45+90]) cube([17,2,4]);
  }
  /*
  difference() {
    translate([67,37.5,0]) cube([2, 29, 6]);
    translate([68,52-9,0]) cylinder(6, 1, 1);
    translate([68,52+9,0]) cylinder(6, 1, 1);
    translate([68,52,0]) cylinder(6, 4, 4);
  }
  */
  difference() {
    translate([68,BOTTOMWIDTH/2-9,0]) cylinder(6, 4, 4);
    translate([68,BOTTOMWIDTH/2-9,0]) cylinder(6, 1, 1);
  }
  difference() {
    translate([68,BOTTOMWIDTH/2+9,0]) cylinder(6, 4, 4);
    translate([68,BOTTOMWIDTH/2+9,0]) cylinder(6, 1, 1);
  }
  
  // Holders
  if( HOLDERS ) {
    for( a =[0:3] ) {
      translate([HOLDERSTART+a*HOLDERDIST,-5,0]) holder();
      translate([HOLDERSTART+a*HOLDERDIST,BOTTOMWIDTH+5,0]) mirror([0,1,0]) holder();
    }
    translate([-5,30+6,0]) rotate(270) holder();
    translate([-5,BOTTOMWIDTH-30,0]) rotate(270) holder();
  }

}


// Draaistel draaipunt
if( CONN ) {
translate([0,120,0]) {
  difference() {
    translate([0,0,0]) cube([36, 16, 3]);
    translate([9,8,0]) cylinder(3, 1.5, 1.5);
    translate([9,8,2]) cylinder(1, 1.5, 2.5);
    translate([9+18,8,0]) cylinder(3, 1.5, 1.5);
    translate([9+18,8,2]) cylinder(1, 1.5, 2.5);
  }
  difference() {
    translate([18,8,0]) cylinder(13, 3.2, 3.2);
    translate([18,8,0]) cylinder(13, 1.0, 1.0);
  }
  difference() {
    translate([0,8,-14.5]) rotate([90,0,90]) cylinder(3, 20, 20);
    translate([0,-12,-50]) cube([3, 50, 50]);
    translate([0,-10,0]) cube([36, 10, 3]);
    translate([0,16,0]) cube([36, 10, 3]);
    translate([0,-5,0]) cube([3, 5, 5]);
    translate([0,16,0]) cube([3, 5, 5]);
  }
  difference() {
    translate([33,8,-14.5]) rotate([90,0,90]) cylinder(3, 20, 20);
    translate([33,-12,-50]) cube([3, 50, 50]);
    translate([0,-10,0]) cube([36, 10, 3]);
    translate([0,16,0]) cube([36, 10, 3]);
    translate([33,-5,0]) cube([3, 5, 5]);
    translate([33,16,0]) cube([3, 5, 5]);
  }
}
}

// Draaistel draaipunt
if( CONN2 ) {
translate([0,120,0]) {
  difference() {
    translate([.2,0,0]) cube([35.6, 16, 3]);
    translate([5.2,0,3]) cube([25.6, 16, 3]);
    
    translate([9,8,0]) cylinder(3, 1.5, 1.5);
    translate([9,8,2]) cylinder(1, 1.5, 2.5);
    translate([9+18,8,0]) cylinder(3, 1.5, 1.5);
    translate([9+18,8,2]) cylinder(1, 1.5, 2.5);
  }
  difference() {
    translate([18,8,0]) cylinder(13.5, 3.5, 3.5);
    translate([18,8,0]) cylinder(13.5, 1.0, 1.0);
    translate([18,8,11]) cylinder(2, 1.0, 1.5);
  }
  difference() {
    translate([18,8,0]) cylinder(5, 6, 6);
    translate([18,8,0]) cylinder(13, 1.0, 1.0);
  }
  difference() {
    translate([18,8,0]) cylinder(6, 36/2, 36/2);
    translate([18,8,0]) cylinder(6, 36/2-4, 36/2-4);
  }
  /*
  difference() {
    translate([0,8,-14.5]) rotate([90,0,90]) cylinder(3, 20, 20);
    translate([0,-12,-50]) cube([3, 50, 50]);
    translate([0,-10,0]) cube([36, 10, 3]);
    translate([0,16,0]) cube([36, 10, 3]);
    translate([0,-5,0]) cube([3, 5, 5]);
    translate([0,16,0]) cube([3, 5, 5]);
  }
  difference() {
    translate([33,8,-14.5]) rotate([90,0,90]) cylinder(3, 20, 20);
    translate([33,-12,-50]) cube([3, 50, 50]);
    translate([0,-10,0]) cube([36, 10, 3]);
    translate([0,16,0]) cube([36, 10, 3]);
    translate([33,-5,0]) cube([3, 5, 5]);
    translate([33,16,0]) cube([3, 5, 5]);
  }
  */
}
}

if( BEAM ) {
  beam();
  if( CONTAINER ) {
    translate([204,0,0]) mirror([1,0,0]) beam();
  }
}

CONTAINER_BEAMLEN=102; // 86=standard, 102=container
BEAM_DIFF=CONTAINER_BEAMLEN-86;
BEAM_H=CONTAINER?6:12;
module beam() {
  mirror([FLIP,0,0]) {
    translate([0,150,0]) {
      difference() {
        translate([0,-4,0]) cube([CONTAINER_BEAMLEN, 4, BEAM_H]);
        if( !CONTAINER ) {
          translate([0,10,10]) rotate([90,180,90]) prism(22, 10, 10);
        }
      }
      
      difference() {
        translate([0,-14,0]) cube([2, 10, BEAM_H]);
        translate([-5,-9,BEAM_H/2]) rotate([0,90,0]) cylinder(13, 1.6, 1.6);
      }
      
      difference() {
        translate([76,0,0]) cube([10+BEAM_DIFF, 16, BEAM_H]);
        translate([76,0,CONTAINER?2:0]) cube([6+BEAM_DIFF+(CONTAINER?4:0), CONTAINER?13:11.2, BEAM_H]);
      }
      
      difference() {
        translate([0,0,0]) rotate([270,270,0]) prism(BEAM_H, 76, 16);
        translate([CONTAINER?17:20,0,CONTAINER?2:0]) rotate([270,270,0]) prism(BEAM_H, CONTAINER?74:70, CONTAINER?16.2:14);
        if( !CONTAINER ) {
          translate([0,10,10]) rotate([90,180,90]) prism(22, 10, 10);
        }
      }
      
      if( !CONTAINER ) {
        // Holders
        translate([0,-8,12]) cube([3.8, 4+4, 3.8]);
        //HOLDERDIST
        translate([HOLDERDIST,-8,12]) cube([3.8, 4+4, 3.8]);
      }
    }
  }
}



