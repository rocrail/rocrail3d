/*
 GCA
 Copyright (c) 2021 Peter Giling
 All rights reserved.
*/
include <../../lib/shapes.scad>
$fn=256;
//$fn=64;

// container size 1:26 (rounded values)
TOTALHEIGTH = 92;
TOTALWIDTH  = 90;
TOTALDEPTH  = 230;

INSIDE_depth  = TOTALDEPTH-10;
INSIDE_heigth = TOTALHEIGTH-10; 
INSIDE_width  = TOTALWIDTH-10;

patternsize = 13.75;

SIDEPANEL   = 0;
BACKPANEL   = 0;
TOPPANEL    = 0;
BOTTOMPANEL = 0;
FRONTPANEL  = 0;

TANK = 1; // Container-Tank type
TANKBODY = 0; // Half
TANKFRONT = 1; // redering takes very long...
TANKRING = 0; // Glue ring
TANKMOUNT = 0;
TANKTOP = 0;
TANKTOPTOP = 0; // rounded top
TANKLADDER = 0;

SUPPORT = 0;
SUPPORT_H = 8;
SUPPORT_M3 = 1; // M3 cutout for fixing the container

GLUEFRAME = 0;
GLUEFRAMEMID = 0;


module profile(X,heigth) {   
  translate([X,0,0]) cube ([13.75,heigth,1]);
  difference() {
    translate ([X+2.375,0,1]) cube ([9,heigth,4]);
    // save filament
    translate ([X+2.375+2,1,1]) cube ([9-4,heigth-2,4-1]);
    
    translate ([X+2.375,0,1])  rotate ([0,-65,0]) cube ([5,heigth,5]);
    translate ([X+11.375,0,1])  rotate ([0,-25,0]) cube ([5,heigth,5]);
  }
}    


module sidehook(X,Y,Z,b) {  
  difference() {   
    translate([X,Y,Z])cube ([5.8,5.8,5.8]);
    translate([X,Y+2.9,Z+2.9]) rotate([0,90,0]) cylinder(6,1.5,1.5);  
    translate([X+2.9,Y+2.9,Z]) rotate   ([0,0,0]) cylinder(6,1.5,1.5);
    if( b )
      translate([X+2.9,Y+2.9,Z+2.9]) rotate([90,90,0]) cylinder(6,1.5,1.5);
  }
} 


module rightdoor() {   
  translate([2,1,1]) cube([INSIDE_width/2-5,25,2]);
  translate([2,30,1]) cube([INSIDE_width/2-5,21,2]);
  translate([2,55,1]) cube([INSIDE_width/2-5,25,2]);
  translate([32,1,1]) cube([6,INSIDE_heigth-3,2.2]);     
  translate([2,1,1]) cube([6,INSIDE_heigth-3,2.2]);     
  
  //  the shutter rods
  translate([11,-2,3]) rotate([-90,0,0]) cylinder(TOTALHEIGTH-6,0.6,0.6);
  translate([11-1,-3,1]) cube([2,2,3]);     


  translate([21,-2,3]) rotate([-90,0,0]) cylinder(TOTALHEIGTH-6,0.6,0.6);
  translate([21-1,-3,1]) cube([2,2,3]);     

  translate([11,6,3.2]) rotate([-90,0,0]) cylinder(2,.8,.8);
  translate([11,34,3.2]) rotate([-90,0,0]) cylinder(2,.8,.8);
  translate([11,70,3.2]) rotate([-90,0,0]) cylinder(2,.8,.8);
  translate([21,6,3.2]) rotate([-90,0,0]) cylinder(2,.8,.8);
  translate([21,34,3.2]) rotate([-90,0,0]) cylinder(2,.8,.8);
  translate([21,70,3.2]) rotate([-90,0,0]) cylinder(2,.8,.8);

  // lockhandles
  translate([10.6,12,3.2]) cube([5.8,.8,1]);
  translate([15.2,10,3]) cube([1.2,2.5,2.1]);
  translate([20.6,12,3.2]) cube([5.8,.8,1]);
  translate([25.2,10,3]) cube([1.2,2.5,2.1]);

}


module HINGES() {
  translate([INSIDE_width+3,2,3])  cube([2,2,2]); //hinge
  difference() {
    translate([INSIDE_width-2,2,1])  cube([7,2,2]); //hinge
    translate([INSIDE_width+4,2,1]) rotate([0,60,0]) cube ([3,3,3]);
  }
  translate([INSIDE_width+3,27,3])  cube([2,2,2]); //hinge
  difference() {
    translate([INSIDE_width-2,27,1])  cube([7,2,2]); //hinge
    translate([INSIDE_width+4,27,1]) rotate([0,60,0]) cube ([3,3,3]);
  }
  translate([INSIDE_width+3,52,3])  cube([2,2,2]); //hinge
  difference() {
    translate([INSIDE_width-2,52,1])  cube([7,2,2]); //hinge
    translate([INSIDE_width+4,52,1]) rotate([0,60,0]) cube ([3,3,3]);
  }
  translate([INSIDE_width+3,77,3])  cube([2,2,2]); //hinge
  difference() {
    translate([INSIDE_width-2,77,1])  cube([7,2,2]); //hinge
    translate([INSIDE_width+4,77,1]) rotate([0,60,0]) cube ([3,3,3]);
  }
}


module hingepost() {
  difference() {
    translate([0,0,0])  cube([5,INSIDE_heigth,5]);

    translate([0,1,1])  cube([5,4,4]);
    translate([-10,2,1])  cube([6,3,4]);

    translate([0,26,1])  cube([5,4,4]);
    translate([0,51,1])  cube([5,4,4]);
    translate([0,76,1])  cube([5,4,4]);    
  }
}    

    // Ladder
if( TANKLADDER ) {
  translate([70,0,0]) {
    translate([0,0,1]) rotate([270,0,0]) cylinder(72, 1, 1);
    translate([18,0,1]) rotate([270,0,0]) cylinder(72, 1, 1);

    translate([-2,0,0]) cube([22,1,2]);
    translate([-2,72-1,0]) cube([22,1,2]);
    
    for( a =[1:5] ) {
      translate([0,a*12,1]) rotate([0,90,0]) cylinder(18, 1, 1);
    }
  }
}


if(TANKMOUNT) translate([0,150,0]) {
  difference() {
    translate([0,0,0]) cylinder(8, 80/2+3, 80/2+3);
    translate([0,0,0]) cylinder(8, 80/2, 80/2);
    translate([-88/2,-88/2+24,0])  cube([88,88,8]);    
    translate([0,0,8/2]) rotate([90,90,0]) cylinder(60, 1.5, 1.5);
  }
  translate([-88/2+5,-88/2+14,0])  cube([2,10,8]);    
  translate([88/2-5-2,-88/2+14,0])  cube([2,10,8]);    

  difference() {
    translate([-80/2-5.5,-88/2+10+3.5,0])  cube([INSIDE_width+2*5.5,4,8]);    
    translate([0,0,0]) cylinder(8, 80/2, 80/2);
    translate([-80/2-5.5,-88/2+10+3.5,2.4])  cube([5.5,2,3.2]);    
    translate([80/2,-88/2+10+3.5,2.4])  cube([5.5,2,3.2]);    
  }
}

if(TANKRING) translate([100,150,0]) {
  difference() {
    translate([0,0,0]) cylinder(10, 80/2-2.25, 80/2-2.25);
    translate([0,0,0]) cylinder(10, 80/2-4, 80/2-4);
  }
}

if(TANKBODY) translate([200,150,0]) {
  difference() {
    translate([0,0,0]) cylinder(100, 80/2, 80/2);
    translate([0,0,0]) cylinder(100, 80/2-2.2, 80/2-2.2);
  }
}

module tankfronPart(d, h, t) { // 80,8
  difference() {
    translate([0,0,0]) rotate([90,0,0]) ellipse(.7,d/2,h);
    //translate([0,0,0]) rotate([90,0,0]) ellipse(.7,d/2-t,h-t);
    translate([-d/2,-.7,-h])  cube([d,.7,h]);    
  }
}


if(TANKTOP) translate([0,150,0]) {
  difference() {
    translate([0,0,0]) cylinder(8, 15, 15);
    translate([0,0,0]) cylinder(7, 13, 13);
    translate([-15,0,-80/2+6]) rotate([0,90,0]) cylinder(30, 80/2, 80/2);
  }
}


if(TANKTOPTOP) translate([0,150,0]) {
  for (a=[0:180]) {
    translate([0,0,0]) rotate([0,0,a]) tankfronPart(30,2,1);
  }
}


if(TANKFRONT) translate([300,150,0]) {
  for (a=[0:180]) {
    translate([0,0,2]) rotate([0,0,a]) tankfronPart(80,8,2);
  }
  // glue ring
  difference() {
    translate([0,0,0]) cylinder(4, 80/2-2.25, 80/2-2.25);
    //translate([0,0,0]) cylinder(4, 80/2-4, 80/2-4);
  }
}    
    
//start 
if(SIDEPANEL) {
  translate([10,10,0]) {
    if( TANK ) {
      //bottom cube
      translate([0,-5,0])  cube([INSIDE_depth,2,5]);
      // U-Top
      translate([0,8,0])  cube([INSIDE_depth,2,5]);
      difference() {
        translate([0,-5,0])  cube([INSIDE_depth,15,2]);
        for (a=[0:13]) {
          translate([12+a*15.1,2,0]) cylinder(5,4,4);
        }
      }
      //top cube
      translate ([0,INSIDE_heigth,0]) cube([INSIDE_depth,5,5]); 
      // left support
      translate([0,-5,0])  cube([1,INSIDE_heigth+10,4]);
      // right support
      translate([INSIDE_depth-1,-5,0])  cube([1,INSIDE_heigth+10,4]);

      // hoek verstevigingen
      
      difference() {
        translate ([0,INSIDE_heigth-20,0]) rotate([0,0,45]) cube([30,5,4]); 
        translate([-4,-5,0])  cube([4,INSIDE_heigth+10,4]);
      }
      difference() {
        translate ([INSIDE_depth,INSIDE_heigth-20,0]) mirror([1,0,0]) rotate([0,0,45]) cube([30,5,4]); 
        translate([INSIDE_depth,-5,0])  cube([4,INSIDE_heigth+10,4]);
      }      
      
      translate([11,10,2]) rotate([0,90,90]) prism(2,10,10);
      //translate([1,INSIDE_heigth-10,2]) rotate([0,90,0]) prism(2,10,10);
      translate([INSIDE_depth-11,10,2]) mirror([1,0,0]) rotate([0,90,90]) prism(2,10,10);
      //translate([INSIDE_depth-1,INSIDE_heigth-10,2]) mirror([1,0,0]) rotate([0,90,0]) prism(2,10,10);

      // tank houder versteviging
      translate ([32,-3,0]) cube([5,12,5]); 
      translate ([32+1,-3,0]) cube([3,15,5]); 
      
      translate ([INSIDE_depth-32-5,-3,0]) cube([5,12,5]); 
      translate ([INSIDE_depth-32-5+1,-3,0]) cube([3,15,5]); 

      translate ([INSIDE_depth/2-2.5,-3,0]) cube([5,12,5]); 
      translate ([INSIDE_depth/2-2.5+1,-3,0]) cube([3,15,5]); 

    }
    else {
      //bottom cube
      translate([0,-5,0])  cube([INSIDE_depth,5,5]);
      difference() {
        for (a=[0:15]) {
          profile(a*patternsize,INSIDE_heigth);
        }
      }
      // Logo (Needs supports)
      translate([INSIDE_depth/2-60,INSIDE_heigth/2-10,4])  cube([120,20,1]);
      translate([INSIDE_depth/2-60,INSIDE_heigth/2-10+4,0]) rotate([90,0,0]) prism(120,4,4);

      translate([INSIDE_depth/2-60,INSIDE_heigth/2+10,0+4]) rotate([180,0,0]) prism(120,4,4);
      
      translate([INSIDE_depth/2-60+4
      ,INSIDE_heigth/2-10+2.5,5]) linear_extrude(.6) text("ROCLINES",16);

      translate([INSIDE_depth/2-60,INSIDE_heigth/2-.6,5])  cube([54,.8,.6]);
    }
  }
}
    

if( BACKPANEL ) {   
  translate([20,-110,0]) {       
    sidehook(-5.8,-5.8,0,true);//bottom left at zero rotation 
    sidehook(INSIDE_width,-5.8,0,true); //  bottom right at zero rotation
    sidehook(-5.8,INSIDE_heigth,0,false);//top left(at zero rotation 
    sidehook(INSIDE_width,INSIDE_heigth,0,false); //  top right at zero rotation

    //bottom cube
    translate ([0,-5,0]) cube([INSIDE_width,5,5]);
    //top cube
    translate ([0,INSIDE_heigth,0]) cube([INSIDE_width,5,5]); 
    
    //leftside cube
    translate ([-5,0,0]) cube([5,INSIDE_heigth,5]);
    //rightside cube
    translate ([INSIDE_width,0,0]) cube([5,INSIDE_heigth,5]); 
    
    if( TANK ) {
      translate ([20,0,0]) rotate([0,0,135]) cube([30,5,4]); 
      translate ([0,INSIDE_heigth-20,0]) rotate([0,0,45]) cube([30,5,4]); 
      translate ([INSIDE_width-20,0,0]) mirror([1,0,0]) rotate([0,0,135]) cube([30,5,4]); 
      translate ([INSIDE_width,INSIDE_heigth-20,0]) mirror([1,0,0]) rotate([0,0,45]) cube([30,5,4]); 
    }
    else {
      difference() {    
        for (a=[0:5]) {
            profile(-1.25+a*patternsize,INSIDE_heigth);
        }   
        // glueframe cut
        difference() {    
          translate ([0,0,0]) cube([INSIDE_width,INSIDE_heigth,.6]); 
          translate ([1.2,1.2,0]) cube([INSIDE_width-2.4,INSIDE_heigth-2.4,.6]); 
        }
      }  
    }

  }
}


if (SUPPORT) {
  translate ([120,-110,0]) {   
    difference() {
      translate([0,0,0]) cube([INSIDE_width,INSIDE_heigth,SUPPORT_H]);
      translate([2,2,0]) cube([INSIDE_width-4,INSIDE_heigth-4,SUPPORT_H]);
    
      if( SUPPORT_M3 ) {
        translate([-10, INSIDE_heigth/2, SUPPORT_H/2]) rotate([90,0,90]) cylinder(100,1.4,1.4);

        translate([INSIDE_width/2, -10, SUPPORT_H/2]) rotate([270,0,0]) cylinder(100,1.4,1.4);
      }
    }
    
    translate([1.4,0,0])  rotate([0,0,45.8]) cube([INSIDE_heigth*1.35,2,SUPPORT_H]);
    translate([INSIDE_width,1.4,0])  rotate([0,0,134.4]) cube([INSIDE_heigth*1.35,2,SUPPORT_H]);
  }   
  
}    


if (TOPPANEL)
{
  translate([-220,10,0]) {
    if( TANK ) {
      H=4;
      // frame
      translate([-27,0,0]) cube([54, 1,H]);
      translate([-27,INSIDE_width-1,0]) cube([54, 1,H]);
 
      // cross
      difference() {
        translate([0,INSIDE_width/2,2]) rotate([90,0,30]) cylinder(INSIDE_width/2+10, 2, 2);
        translate([-27,-10,0]) cube([54, 10,5]);
      }
      difference() {
        translate([0,INSIDE_width/2,2]) rotate([90,0,-30]) cylinder(INSIDE_width/2+10, 2, 2);
        translate([-27,-10,0]) cube([54, 10,5]);
      }
      difference() {
        translate([0,INSIDE_width/2,2]) rotate([90,0,150]) cylinder(INSIDE_width/2+10, 2, 2);
        translate([-27,INSIDE_width,0]) cube([54, 10,5]);
      }
      difference() {
        translate([0,INSIDE_width/2,2]) rotate([90,0,-150]) cylinder(INSIDE_width/2+10, 2, 2);
        translate([-27,INSIDE_width,0]) cube([54, 10,5]);
      }
      
    }
    else {
      translate([0,-5,0]) cube([INSIDE_depth,5,5]);

      for (a=[0:15]) {
        profile(a*patternsize,INSIDE_width);
      }
      translate([0,INSIDE_width,0]) cube([INSIDE_depth,5,5]);
    }
  }
}  


if (BOTTOMPANEL)// should be printed twice
{
  translate([-220,10,0]) {
    for (a=[0:15]) {
      profile(a*patternsize,INSIDE_width);
    }
  }
}  


if( FRONTPANEL ) {
  translate([-110,-110,0]) {       
    difference() {    
      translate([0,0,0]) cube([INSIDE_width,INSIDE_heigth,1]);
      // glueframe cut
      difference() {    
        translate ([0,0,0]) cube([INSIDE_width,INSIDE_heigth,.6]); 
        translate ([1.2,1.2,0]) cube([INSIDE_width-2.4,INSIDE_heigth-2.4,.6]); 
      }
    }
    
    sidehook(-5.8,-5.8,0,true);//bottom left
    sidehook(INSIDE_width,-5.8,0,true); //  bottom right 
    sidehook(-5.8,INSIDE_heigth,0,false);//top left 
    sidehook(INSIDE_width,INSIDE_heigth,0,false); //  top right 
    translate ([0,-5,0]) cube([INSIDE_width,5,1.5]); //bottom cube
    translate ([0,INSIDE_heigth,0]) cube([INSIDE_width,5,5]); //top cube

    translate([-5,0,0]) hingepost();
    translate([INSIDE_width,0,0]) hingepost();
    //       translate([0,INSIDE_heigth,0])   cube([INSIDE_width,5,5]);    
    translate([-4.2,0,3.2]) rotate([-90,0,0]) cylinder(INSIDE_heigth,0.8,0.8);      
    translate([INSIDE_width+4.2,0,3.2]) rotate([-90,0,0]) cylinder(INSIDE_heigth,0.8,0.8); 

    HINGES();
    translate([80,0,0]) mirror([1,0,0]) HINGES(); 
    translate ([(INSIDE_width/2),0,0]) {
      rightdoor();
      mirror([1,0,0]) rightdoor(); 
    }

  }
}


if( GLUEFRAME ) {
  // plak strip
  translate([220,-110,0])  
  {
    difference() {    
      translate ([0,0,0]) cube([INSIDE_width,INSIDE_heigth,GLUEFRAMEMID?SUPPORT_H:4]); 
      translate ([GLUEFRAMEMID?2:1,GLUEFRAMEMID?2:1,0]) cube([INSIDE_width-(GLUEFRAMEMID?4:2),INSIDE_heigth-(GLUEFRAMEMID?4:2),GLUEFRAMEMID?SUPPORT_H:4]); 
    }
    
    if( GLUEFRAMEMID ) {
      translate([1.4,0,0])  rotate([0,0,45.8]) cube([INSIDE_heigth*1.35,2,SUPPORT_H]);
      translate([INSIDE_width,1.4,0])  rotate([0,0,134.4]) cube([INSIDE_heigth*1.35,2,SUPPORT_H]);
    }
  }
}
