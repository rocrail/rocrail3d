/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

include <../../lib/shapes.scad>
BOTTOMLEN=180;
BOTTOMWIDTH=104;
MIDLEN=126;

// Print separately!
CONN=false;
DRAWBAR=false;
GUIDEWAY=false;
GUIDEWAY2=false;
GUIDEWAY3=true;
NOTCH=false;
CENTER=false;
BAR=false;
SPRINGCONN=false;


if( SPRINGCONN ) {
  translate([0,120,0]) {
    difference() {
      translate([0,0,0]) cube([6, 12, 2]);
      translate([3,3,0]) cylinder(2, 1.7, 1.7);
      translate([3,12,0]) cylinder(2, 1, 1);
    }    
    difference() {
      translate([3,12,0]) cylinder(2, 3, 3);
      translate([3,12,0]) cylinder(2, 1, 1);
    }    
  }
}


if( BAR ) {
  translate([0,100,0]) {
    difference() {
      translate([0,0,0]) cube([80, 12, 4]);
      translate([0,2,2]) cube([80, 8, 2]);
    }    
  }
}


if( CENTER ) {
// Centert part
  translate([90,80,0]) {
    difference() {
      translate([2,-5,0]) cube([16, 72, 7]);
      translate([4,5,2]) cube([12, 52, 5]);
      translate([10,31,0]) cylinder(2, 3.5, 3.5);

      translate([2,-5,0]) cube([16, 6, 3.2]);
      translate([10,-2,0]) cylinder(7, 1.0, 1.0);

      translate([2,61,0]) cube([16, 6, 3.2]);
      translate([10,64,0]) cylinder(7, 1.0, 1.0);
    }
    difference() {
      translate([2,26,0]) cube([16, 10, 7]);
      translate([10,31,0]) cylinder(7, 3.5, 3.5);
    }
    translate([30,0,0]) {
      difference() {
        cylinder(2, 5, 5);
        cylinder(2, 1.5, 1.5);
      }
    }
  }
}

if( NOTCH ) {
  translate([-17,47,0]) {
    translate([0,0,0]) cylinder(1, 3, 3);
    difference() {
      translate([0,0,0]) cylinder(7, 1.9, 1.9);
      translate([0,0,3]) cylinder(5, 0.9, 0.9);
    }
    difference() {
      translate([0,10,0]) cylinder(1.5, 3, 3);
      translate([0,10,0]) cylinder(1.5, 1.1, 1.1);
    }
  }
}


if( GUIDEWAY3 ) {
  translate([90,40,0]) {
    //translate([90,0,0]) triangle(2, 70, 25);


    translate([45,25,0]) cube([10, 3, 2]);
    
        
    difference() {
      translate([15,5,0]) cube([70, 25, 2]);
      difference() {
        translate([50,15.45,0]) cylinder(4, 9.25, 9.25);
        translate([50,15.45,0]) cylinder(4, 5.3, 5.3);
        translate([47,-4,0]) rotate([0,0,55]) cube([20, 20, 4]);
        translate([53,-4,0]) mirror([1,0,0]) rotate([0,0,55]) cube([20, 20, 4]);
      }
      
      translate([0,0,0]) mirror([0,0,0]) {
        difference() {
          translate([27,42,0]) ellipse(4, 30, 30);
          translate([27,42,0]) ellipse(4, 26, 26);
          translate([-5,4,0]) cube([35, 70, 4]);
          translate([65,10,0]) rotate([0,0,55]) cube([30, 80, 4]);
        }
      }
      translate([100,0,0]) mirror([1,0,0]) {
        difference() {
          translate([27,42,0]) ellipse(4, 30, 30);
          translate([27,42,0]) ellipse(4, 26, 26);
          translate([-5,4,0]) cube([35, 70, 4]);
          translate([65,10,0]) rotate([0,0,55]) cube([30, 80, 4]);
        }
      }
      // cut material
      translate([10,5,0]) rotate([0,0,35]) cube([50, 20, 4]);
      
      translate([90,5,0]) mirror([1,0,0]) rotate([0,0,35]) cube([50, 20, 4]);
      
    }
    
    
    
    // Buffer construction horizontal
    difference() {
      translate([10,5,0]) cube([80, 6, 2]);
    }
    
    // Buffer connections vertical
    difference() {
      translate([10,5,2]) cube([20.5, 2, 8]);
      rotate([90,0,0]) translate([16,5,-10]) cylinder(10, .8, .8);
    }
    difference() {
      translate([69.5,5,2]) cube([20.5, 2, 8]);
      rotate([90,0,0]) translate([16+68,5,-10]) cylinder(10, .8, .8);
  }
    
    
  }
}


if( GUIDEWAY2 ) {
  translate([80,0,0]) {
    //translate([90,0,0]) triangle(2, 70, 25);
    
    translate([45,25,0]) cube([10, 3, 2]);
    difference() {
      translate([15,5,0]) cube([70, 25, 2]);
      translate([15+31.5,21.2,0]) cube([7, 4, 2]);
      translate([25,5,0]) rotate([0,0,35]) cube([30, 4, 4]);
      
      translate([75,5,0]) mirror([1,0,0]) rotate([0,0,35]) cube([30, 4, 4]);
      //translate([78,8,0]) rotate([0,0,145]) cube([30, 4, 4]);
      
      translate([10,5,0]) rotate([0,0,35]) cube([50, 20, 4]);
      
      translate([90,5,0]) mirror([1,0,0]) rotate([0,0,35]) cube([50, 20, 4]);
      // cylinder cut to save material
      //translate([50,-5,0]) ellipse(2, 20, 20);
      
      difference() {
        //translate([50,-5,0]) ellipse(2, 80, 70);
        //translate([50,-5,0]) ellipse(2, 36, 46);
      }
    }
    
    // Distance beam for the nipple
    difference() {
      //translate([50,-5,0]) ellipse(3.5, 36, 46);
      //translate([50,-5,0]) ellipse(3.5, 34, 44);
      //translate([0,-55,0]) cube([100, 60, 3.5]);
    }
    
    
    
    // Buffer construction horizontal
    difference() {
      translate([10,5,0]) cube([80, 6, 2]);
      // cylinder cut to save material
      //translate([50,0,0]) ellipse(2, 20, 30);
    }
    
    // Buffer connections vertical
    difference() {
      translate([10,5,2]) cube([20.5, 2, 8]);
      rotate([90,0,0]) translate([16,5,-10]) cylinder(10, .8, .8);
    }
    difference() {
      translate([69.5,5,2]) cube([20.5, 2, 8]);
      rotate([90,0,0]) translate([16+68,5,-10]) cylinder(10, .8, .8);
  }
    
    // Triangle construction
    translate([22,23,0]){
      //rotate([0,90,180]) mirror([1,0,0]) prism(2, 12, 12);
    }
    translate([78,23,2]){
      //rotate([0,90,0]) mirror([0,1,0]) prism(2, 12, 12);
    }
    
  }
}

if( GUIDEWAY ) {
  translate([0,0,0]) {
    difference() {
      translate([15,5,0]) cube([70, 40, 2]);
      difference() {
        translate([50,0,0]) ellipse(2, 30, 40);
        translate([50,0,0]) ellipse(2, 26, 36);
      }
      // cylinder cut to save material
      translate([50,0,0]) ellipse(2, 20, 30);
      
      difference() {
        translate([50,0,0]) ellipse(2, 80, 70);
        translate([50,0,0]) ellipse(2, 36, 46);
      }
    }
    
    
    
    // Distance beam for the nipple
    difference() {
      translate([50,0,0]) ellipse(3.5, 36, 46);
      translate([50,0,0]) ellipse(3.5, 34, 44);
      translate([0,-55,0]) cube([100, 60, 3.5]);
    }
    difference() {
      translate([50,0,0]) ellipse(3.5, 22, 32);
      translate([50,0,0]) ellipse(3.5, 20, 30);
      translate([0,-55,0]) cube([100, 60, 3.5]);
    }
    
    // Buffer construction horizontal
    difference() {
      translate([10,5,0]) cube([80, 6, 2]);
      // cylinder cut to save material
      translate([50,0,0]) ellipse(2, 20, 30);
    }
    
    // Buffer connections vertical
    difference() {
      translate([10,5,2]) cube([20.5, 2, 8]);
      rotate([90,0,0]) translate([16,5,-10]) cylinder(10, .8, .8);
    }
    difference() {
      translate([69.5,5,2]) cube([20.5, 2, 8]);
      rotate([90,0,0]) translate([16+68,5,-10]) cylinder(10, .8, .8);
  }
    
    // Triangle construction
    translate([22,23,0]){
      rotate([0,90,180]) mirror([1,0,0]) prism(2, 12, 12);
    }
    translate([78,23,2]){
      rotate([0,90,0]) mirror([0,1,0]) prism(2, 12, 12);
    }
    
  }
}

if( DRAWBAR ) {
  translate([0,40,0]) {
    difference() {
      translate([-10,0,0]) cube([82, 14, 3]);
      translate([-5,3.5,0]) cube([50, 7, 3]);
      // smaller
      //translate([50,0,0]) cube([40, 2, 2]);
      //translate([50,12,0]) cube([40, 2, 2]);
      //translate([100-28,0,0]) cube([28, 3, 2]);
      //translate([100-28,11,0]) cube([28, 3, 2]);
      translate([-10,7,0]) cylinder(3, .6, .6);
      translate([65,7,0]) cylinder(4.5, 2, 2);
    }
    
    translate([-20+88,0,0]) cube([4, 14, 12]);
    translate([-20+88,3,10]) cube([20, 8, 4]);
    translate([-20+88,2,10]) cube([20, 10, 2]);
    //translate([-20+88,0,12]) cube([40, 14, 2]);
    
    translate([58,0,0]){
      rotate([0,0,90]) mirror([0,1,0]) prism(2, 10, 10);
    }
    translate([58,12,0]){
      rotate([0,0,90]) mirror([0,1,0]) prism(2, 10, 10);
    }

    /*
    difference() {
      translate([65,7,0]) cylinder(4.5, 2, 2);
      translate([65,7,0]) cylinder(4.5, .6, .6);
    }
    */
    difference() {
      translate([-10,7,0]) cylinder(3, 2, 2);
      translate([-10,7,0]) cylinder(3, .6, .6);
    }
  }
}

// Draaistel draaipunt
if( CONN ) {
translate([0,80,0]) {
  // bottom
  difference() {
    translate([0,0,0]) cube([36, 16, 3]);
    translate([9,8,0]) cylinder(3, 1.5, 1.5);
    translate([9,8,2]) cylinder(1, 1.5, 2.5);
    translate([9+18,8,0]) cylinder(3, 1.5, 1.5);
    translate([9+18,8,2]) cylinder(1, 1.5, 2.5);
  }
  // boggy connector
  difference() {
    translate([18,8,0]) cylinder(14, 3.2, 3.2);
    translate([18,8,0]) cylinder(14, 1.0, 1.0);
  }
  // left bearing
  difference() {
    translate([0,8,-13.5]) rotate([90,0,90]) cylinder(3, 20, 20);
    translate([0,-12,-50]) cube([3, 50, 50]);
    translate([0,-10,0]) cube([36, 10, 3]);
    translate([0,16,0]) cube([36, 10, 3]);
    translate([0,-5,0]) cube([3, 5, 8]);
    translate([0,16,0]) cube([3, 5, 8]);
  }
  // right bearing
  difference() {
    translate([33,8,-13.5]) rotate([90,0,90]) cylinder(3, 20, 20);
    translate([33,-12,-50]) cube([3, 50, 50]);
    translate([0,-10,0]) cube([36, 10, 3]);
    translate([0,16,0]) cube([36, 10, 3]);
    translate([33,-5,0]) cube([3, 5, 8]);
    translate([33,16,0]) cube([3, 5, 8]);
  }
}
}
