/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

include <../../lib/shapes.scad>


BOTTOMLEN=180;
BOTTOMWIDTH=104;
MIDLEN=126;

HEIGHT=30;
HOLDERDIST=42;

SIDE=false;
MIDSIDE=false;
FLIP=0;
HEAD=true;
ROADNUMBER=false;
LEDBOX=false;
LAMP=false;
STAKE=false;


if( MIDSIDE ) {
  translate([BOTTOMLEN,0,0]) {
    difference() {
      translate([-1,0,0]) cube([MIDLEN+1, HEIGHT, 2]);
      translate([-1,0,1]) cube([1, HEIGHT, 1]);
      translate([MIDLEN-1,0,0]) cube([1, HEIGHT, 1]);
      for( a =[0:8] ) {
        translate([0,a*6.5,1.5]) cube([MIDLEN+1, .5, .5]);
        translate([0,a*6.5,0]) cube([MIDLEN+1, .5, .2]);
      }
      translate([0,HEIGHT-4,0]) cube([MIDLEN+1, .5, .5]);
    }
    
    // Holders
    for( b =[0:2] ) {
      // New bottom holders: 30+42
      difference() {
        translate([(HOLDERDIST/2-3)+1+(b*42)+0.1,-6,0]) cube([3.8, HEIGHT+4, 3.8]);
        translate([0,HEIGHT-4,0]) cube([MIDLEN+1, .5, .5]);
      }
    }
    
    
    difference() {
      translate([0,0,0]) cube([MIDLEN, 4, 3]);
      translate([MIDLEN-1,0,0]) cube([1, HEIGHT, 1]);
    }
    
    difference() {
      translate([0,HEIGHT-4,0]) cube([MIDLEN, 4, 5]);
      translate([0,HEIGHT-6,10]) mirror([0,0,1]) rotate(0) prism(MIDLEN, 10, 10);
      translate([MIDLEN-1,0,0]) cube([1, HEIGHT, 1]);
      translate([0,HEIGHT-4,0]) cube([MIDLEN+1, .5, .5]);
    }

    
  }
}

if( SIDE ) {
  mirror([FLIP,0,0]) {
    difference() {
      translate([0,0,0]) cube([BOTTOMLEN-30+FLIP, HEIGHT, 2]);
  
      if( FLIP==0 )
        translate([BOTTOMLEN-30-1,0,0]) cube([1, HEIGHT, 1]);
      else
        translate([BOTTOMLEN-30,0,1]) cube([1, HEIGHT, 1]);

      for( a =[0:8] ) {
        translate([0,a*6.5,1.5]) cube([BOTTOMLEN-30+FLIP, .5, .5]);
        translate([0,a*6.5,0]) cube([BOTTOMLEN-30+FLIP, .5, .2]);
      }
      translate([0,HEIGHT-4,0]) cube([BOTTOMLEN-30+FLIP, .5, .5]);
    }
  
    // Holders
    for( b =[0:3] ) {
      // New bottom holders: 30+42
      difference() {
        translate([1+(b*42)+0.1,-6,0]) cube([3.8, HEIGHT+4, 3.8]);
        translate([0,HEIGHT-4,0]) cube([BOTTOMLEN-30+FLIP, .5, .5]);
      }
    }

    
    difference() {
      translate([0,0,0]) cube([BOTTOMLEN-30, 4, 3]);
      if( FLIP==0 )
        translate([BOTTOMLEN-30-1,0,0]) cube([1, HEIGHT, 1]);
    }
    
    difference() {
      translate([-2,HEIGHT-4,0]) cube([BOTTOMLEN-30+2, 4, 5]);
      translate([-3,HEIGHT-6,10]) mirror([0,0,1]) rotate(0) prism(BOTTOMLEN-30+3, 10, 10);
      if( FLIP==0 )
        translate([BOTTOMLEN-30-1,0,0]) cube([1, HEIGHT, 1]);
      translate([0,HEIGHT-4,0]) cube([BOTTOMLEN-30+FLIP, .5, .5]);
    }
    
  }
}


if( HEAD ) {
  translate([-BOTTOMWIDTH-10,0,0]) {
    
    // Base
    difference() {
      translate([0,0,0]) cube([BOTTOMWIDTH, HEIGHT, 2]);
      translate([10,18,0]) cylinder(6, 2.5, 2.5);
      for( a =[0:8] ) {
        translate([0,a*6.5,1.5]) cube([BOTTOMWIDTH, .5, .5]);
        translate([0,a*6.5,0]) cube([BOTTOMWIDTH, .5, .2]);
      }
      translate([0,HEIGHT-4,0]) cube([BOTTOMWIDTH, .5, .5]);
    }

    difference() {
      translate([0,HEIGHT-4,0]) cube([BOTTOMWIDTH, 4, 5]);
      translate([0,HEIGHT-6,10]) mirror([0,0,1]) rotate(0) prism(BOTTOMWIDTH, 10, 10);
      translate([0,HEIGHT-4,0]) cube([BOTTOMWIDTH, .5, .5]);
    }
     
    // Lights
    difference() {
      translate([10,18,2]) cylinder(4, 4, 3);
      translate([10,18,0]) cylinder(6, 2.5, 2.5);
    }
    
    
    // Construction
    difference() {
      translate([((BOTTOMWIDTH-40)/2)-4+.1,-6,0]) cube([3.8, HEIGHT+4, 2.8]);
      translate([0,HEIGHT-4,0]) cube([BOTTOMWIDTH, .5, .5]);
    }
    
    difference() {
      translate([((BOTTOMWIDTH-40)/2)+40+.1,-6,0]) cube([3.8, HEIGHT+4, 2.8]);
      translate([0,HEIGHT-4,0]) cube([BOTTOMWIDTH, .5, .5]);
    }
    
    translate([0,0,0]) cube([BOTTOMWIDTH, 4, 2.8]);
    
  }
}


if( ROADNUMBER ) {
  translate([00,-30,0]) {
    translate([0,0,0]) roundedcube(37, 11, .8, 2)
    translate([0,0,0]) cube([37, 11, .8]);
    translate([2,2.5,0]) linear_extrude(1.6) text("HsDh101",6);
  }
}


if( LEDBOX ) {
  translate([45,-40,0]) {
    difference() {
      translate([0,0,0]) cube([17, 22, 8]);
      translate([1,0,1]) cube([15, 21, 7]);
    }
  }
}

if( LAMP ) {
  translate([70,-40,0]) {
    
    difference() {
      translate([0,0,0]) cube([10, 12, 7]);
      translate([1,0,1]) cube([8, 11, 6]);
      // Light window
      translate([2,2,0]) cube([2.5, 6, 1]);
      translate([5.5,2,0]) cube([2.5, 6, 1]);
      // Drain
      translate([5,12,4]) rotate([90,0,0]) cylinder(3, 1, 1);
    }
    
    // Cap
    
    difference() {
      translate([5,8.5,0]) cylinder(7, 10, 10);
      translate([5,8.5,0]) cylinder(7, 8.5, 9);
      translate([-5,0.5,0]) cube([20, 20, 7]);
    }    
    
    
    translate([5,-1,3]) rotate([90,0,0]) cylinder(3, 2, 2);
    translate([5,-4,3]) rotate([90,0,0]) cylinder(1, 3, 3);
  }
}
