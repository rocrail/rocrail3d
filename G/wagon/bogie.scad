/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

include <../../lib/shapes.scad>
include <../lib/modules.scad>

CENTER=0;
CENTERRING=0;
SIDE=1;
SIDEHEAD=0;

module beam() {
  difference() {
    translate([0,0,0]) ucube(72, 7, 5);
    translate([3,3.5,0]) cylinder(8, 1.1, 1.1);
    translate([72-3,3.5,0]) cylinder(8, 1.1, 1.1);
  }
}



if( CENTER ) {
  
difference() {
  translate([62.5,-5,4]) rotate([90,0,90]) beam();
  //translate([61,25.,4+6]) cube([16, 12, 7]);
}
// Centert part
difference() {
  translate([10,31,0]) cylinder(2, 36/2, 36/2);
  translate([10,31,0]) cylinder(3, 6, 6);
}
difference() {
  translate([2,-5-3,0]) cube([16, 72+6, 7]);
  translate([4,5,2]) cube([12, 52, 5]);
  translate([10,31,0]) cylinder(2, 3.75, 3.75);

  translate([2,-5-3,0]) cube([16, 6+3, 3.2]);
  translate([10,-2,0]) cylinder(7, 1.0, 1.0);

  translate([2,61,0]) cube([16, 6+3, 3.2]);
  translate([10,64,0]) cylinder(7, 1.0, 1.0);
}
difference() {
  translate([2,23,0]) cube([16, 16, 7]);
  translate([10,31,0]) cylinder(7, 3.75, 3.75);
}

// Coupler connection
translate([16,31,0]) {
  difference() {
    translate([0,-8,0]) cube([24, 16, 7]);
    translate([2,-6,2]) cube([20, 12, 5]);
    translate([19,0,0]) cylinder(7, 1.5, 1.5);
  }

  difference() {
    translate([45,-6,8-1]) cube([33, 12, 6+3]);
    translate([45+9,-6,8-1]) cube([24, 12, 2]);
    translate([47,-6,10+2]) cube([31, 12, 4]);
    translate([56,0,8-1]) cylinder(6+3, 1, 1);
    translate([56+7,0,8-1]) cylinder(6+3, 1.25, 1.25);
    translate([56+7,0,9]) cylinder(1, 2.5, 1.25);
  }
  
  difference() {
    translate([54,-6,8-1]) rotate([90,0,90]) prism(12,2,4);
    translate([56,0,8-1]) cylinder(6+3, 1, 1);
  }

  difference() {
    translate([72,0,8+2]) cylinder(6, 3.8, 3.8);
    translate([72,0,8+2]) cylinder(6, 2.5, 2.5);
  }
  difference() {
    translate([45,-3.8,10+2]) cube([26, 7.6, 2]);
    translate([71,0,8+2]) cylinder(6, 2.5, 2.5);
    translate([56,0,8+2]) cylinder(6, 1, 1);
    translate([56+7,0,8+2]) cylinder(6, 1.25, 1.25);
  }

}
  // Down to coupler connector
  translate([40,31,0]) {
    difference() {
      rotate([0, -14, 0]) translate([0,-8,0]) cube([30, 16, 7]);
      rotate([0, -14, 0]) translate([2,-6,2]) cube([28, 12, 5]);
      translate([3,-8,10]) cube([32,16,6]);
    }
  }

  if( CENTERRING ) {
    translate([110,31,0]) {
      difference() {
        cylinder(1.6, 8, 8);
        cylinder(1.6, 1.25, 1.25);
      }
    }
  }

}


module ucube(l,w,h) {
  difference() {
    cube([l, w, h]);
    translate([0,w/4,(h/3)*2]) cube([l, w/2, h/3]);
  }
}

/*
module AxGear(x,y) {
  translate([x,y,0]) {
    difference() {
      translate([-12,-7,0]) cube([24, 2, 8]);
      //translate([-13,-7,5]) cube([26, 1, 1]);
      //translate([-13,-7,0]) cube([1, 1, 6]);
      //translate([12,-7,0]) cube([1, 1, 6]);
    }
    
    translate([-9,7,4]) rotate([90,0,0]) bogieSpring(10, 2.5, 6);
    translate([-9,7,3]) rotate([90, 0, 0]) cylinder(12, 2.5, 2.5);

    translate([9,7,4]) rotate([90,0,0]) bogieSpring(10, 2.5, 6);
    translate([9,7,3]) rotate([90, 0, 0]) cylinder(12, 2.5, 2.5);


    difference() {
      translate([0,0,0]) cylinder(8, 5, 5);
      translate([0,0,0]) cylinder(8, 1.7, 1.7);
      if( BALLBEARING )
        translate([0,0,0]) cylinder(3.5, BBrad, BBrad);
    }
    difference() {
      translate([0,0,6]) sphere(r = 5);
      translate([0,0,0]) cylinder(8, 1.7, 1.7);
      if( BALLBEARING )
        translate([0,0,0]) cylinder(3.5, BBrad, BBrad);
    }
    difference() {
      translate([-5,-5,0]) cube([10, 12, 6]);
      translate([-4,5,0]) cube([8, 2, 6]);
      translate([0,0,0]) cylinder(8, 1.7, 1.7);
      if( BALLBEARING )
        translate([0,0,0]) cylinder(3.5, BBrad, BBrad);
    }
  }
}
*/

if( SIDEHEAD ) {
  translate([00,110,0]) {
    beam();
  }
}


if( SIDE ) {
  HoH=65;
  translate([0,80,0]) {
    AxGear(0,0);
    AxGear(65,0);

    translate([32.5-8-3,10,0]) cube([3, 4, 6]);
    translate([32.5+8,10,0]) cube([3, 4, 6]);

    
    // Center beam
    translate([-20,7,0]) ucube(105, 3, 6);
    
    difference() {
    translate([-20,7,0]) cube([5, 7, 6]);
      translate([-25,10.5,3]) rotate([90,0,90]) cylinder(120, 1.1, 1.1);
    }
    difference() {
      translate([80,7,0]) cube([5, 7, 6]);
      translate([-25,10.5,3]) rotate([90,0,90]) cylinder(120, 1.1, 1.1);
    }
    
    // Upper beam
    difference() {
      translate([10,14,0]) ucube(45, 3, 6);
      // Manifold hole: Repair by set Z=2...
      translate([10+22.5,17,3]) rotate([90, 0, 0]) cylinder(3, 1, 1);
    }
    
    translate([-19,11,0]) rotate(6) ucube(30, 3, 6);
    translate([55,17,0]) mirror([0,1,0]) rotate(6) ucube(30, 3, 6);

    // Lower beam    
    difference() {
      translate([21,-2,0]) ucube(23, 5, 6);
      translate([11.5,7.5,4]) rotate(-45) cube([12.5, 3.75, 6]);
      translate([42.25,1.5,4]) mirror([0,1,0]) rotate(-45) cube([12.5, 3.7, 6]);
    }    
    
    difference() {
      translate([11.5,7.5,0]) rotate(-45) ucube(13.5, 5, 6);
      // Subtract center beam
      translate([-20,7,0]) cube([105, 7, 6]);
      translate([21.5,-2,0]) ucube(22, 5, 10);
    }
    
    difference() {
      translate([40.5,1.5,0]) mirror([0,1,0]) rotate(-45) ucube(13.5, 5, 6);
      // Subtract center beam
      translate([-20,7,0]) cube([105, 7, 6]);
      translate([21.5,-2,0]) ucube(22, 5, 10);
    }
    
    translate([10,-9,8]) rotate([0, 90, 0]) prism(8, 2, 2);
    translate([-12,-7,8]) rotate([90, 90, 0]) prism(8, 2, 2);
    translate([-10,-9,0]) cube([20, 2, 8]);
    
    translate([HoH+10,-9,8]) rotate([0, 90, 0]) prism(8, 2, 2);
    translate([HoH-12,-7,8]) rotate([90, 90, 0]) prism(8, 2, 2);
    translate([HoH-10,-9,0]) cube([20, 2, 8]);
    
  }
}




