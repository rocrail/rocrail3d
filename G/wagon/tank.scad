/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;

BOTTOMLEN=180;
BOTTOMWIDTH=104;

include <../../lib/shapes.scad>

TANK=false;
CONN=false;
CAP=false;
MOUNT=false;
TOP=false;
TOPCAP=false;
TOPWHEEL=false;
HEAD=false;
LOGO=1;
ROADNUMBER=0;

TOPMOUNT=false;
TOPMOUNTSHORT=false;
TOPMOUNTPLATFORM=false;
TOPMOUNTRAILING=false;
TOPMOUNTLADDER=0;

if( LOGO ) {
  translate([1.6,-.8,0]) cube([94, .8, .4]);
  linear_extrude(0.4) text("RocBlue",14);
  translate([74,-.8,0]) linear_extrude(0.4) text("H",20);
  translate([93,-.8,0]) linear_extrude(0.4) text("2",10);
  //translate([64.8,0,0]) cube([.8, 16, .6]);
}

if( ROADNUMBER ) {
  translate([00,-30,0]) {
    translate([0,0,0]) roundedcube(46, 11, .8, 2)
    translate([0,0,0]) cube([37, 11, .8]);
    translate([6,2.5,0]) linear_extrude(1.6) text("HsDh102",6);
  }
}




if( TANK ) {
  translate([0,0,0]) {
    difference() {
      translate([0,0,0]) cylinder(130, 50, 50);
      translate([0,0,0]) cylinder(130, 48, 48);
      translate([0,0,112]) rotate([0,90,0]) cylinder(100, 1.7, 1.7);
    }
  }
}

if( CONN ) {
  translate([105,0,0]) {
    difference() {
      translate([0,0,0]) cylinder(16, 48, 48);
      translate([0,0,0]) cylinder(16, 46, 46);
    }
    translate([-47,-1,0]) cube([100-6, 2, 16]);
    translate([0-1,-47,0]) cube([2, 100-6, 16]);
  }
}

if( CAP ) {
  translate([0,105,0]) {
    difference() {
      translate([0,0,0]) cylinder(5, 48, 48);
      //translate([0,0,0]) cylinder(20, 50, 50);
      translate([0,0,0]) cylinder(5, 46, 46);
    }
    translate([0,0,-89]) difference() {
      translate([0,0,6.2]) sphere(r = 100);
      translate([0,0,-100]) cylinder(194, 100, 100);
      translate([0,0,6.2]) sphere(r = 98);
    }
  }
}


translate([200,200,0]) {   
  if( TOPMOUNT ) {
    translate([0,0,0]) {
      difference() {
        translate([0,0,0]) cube([TOPMOUNTSHORT?18:32, 12, 4]);
        translate([32,53,0]) cylinder(4, 50.2, 50.2);
      }
      difference() {
        translate([-3,0,0]) cube([3, 3, 4]);
        translate([-2.2,0,0.8]) cube([2.2, 3, 2.2]);
      }
    }
  }

  if( TOPMOUNTPLATFORM ) {
    translate([40,0,0]) {
      difference() {
        translate([0,0,0]) cube([18, 180, 2]);
        translate([2,2,0]) cube([2, 176, 2]);
        translate([6,2,0]) cube([2, 176, 2]);
        translate([10,2,0]) cube([2, 176, 2]);
        translate([14,2,0]) cube([2, 176, 2]);
      }
      translate([0,44,0]) cube([18, 2, 2]);
      translate([0,89,0]) cube([18, 2, 2]);
      translate([0,134,0]) cube([18, 2, 2]);
    }
  }    
    // Railing
  if( TOPMOUNTRAILING ) {
    translate([100,0,0]) {
      translate([0,180,1]) rotate([90,0,0]) cylinder(180, 1, 1);
      translate([0,0,0]) cube([18, 2, 2]);
      translate([0,44,0]) cube([18, 2, 2]);
      translate([0,89,0]) cube([18, 2, 2]);
      translate([0,134,0]) cube([18, 2, 2]);
      translate([0,178,0]) cube([18, 2, 2]);
    }
  }
    
    // Ladder
  if( TOPMOUNTLADDER ) {
    translate([70,0,0]) {
      translate([0,125,1]) rotate([90,0,0]) cylinder(125, 1, 1);
      translate([18,125,1]) rotate([90,0,0]) cylinder(125, 1, 1);
      
      for( a =[1:9] ) {
        translate([0,a*12,1]) rotate([0,90,0]) cylinder(18, 1, 1);
      }
      translate([0,108,0]) rotate([-15,0,0]) {
        translate([0,0,1]) cylinder(10, 1, 1);
        translate([18,0,1]) cylinder(10, 1, 1);
        translate([-1,-1,10]) cube([20, 2, 2]);
      }
      
    }
  }
    
}

if( MOUNT ) {
  translate([0,200,0]) {
    difference() {
      translate([-3.8,0,0]) cube([BOTTOMWIDTH+3.8+3.8, 26, 3.8]);
      translate([0,0,0]) cube([BOTTOMWIDTH, 8, 3.8]);
      //translate([0,0,0]) cube([10.1, 6, 4]);
      //translate([100-10.1,0,0]) cube([10.1, 6, 4]);
      translate([BOTTOMWIDTH/2,62,0]) cylinder(4, 50.2, 50.2);

      translate([-4,10,0]){
        rotate([0,90,0]) mirror([1,0,0]) prism(4, 16, 10);
      }
      translate([BOTTOMWIDTH+4,10,4]){
        rotate([0,90,0]) mirror([0,0,1]) prism(4, 16, 10);
      }

      translate([7,20,0]){
        rotate([0,270,0]) mirror([0,1,0]) prism(6, 8, 5);
      }
      translate([BOTTOMWIDTH-7,20,6]){
        rotate([0,90,0]) mirror([0,1,0]) prism(6, 8, 5);
      }

      translate([7,12,0]) cube([10, 8, 3.8]);
      translate([BOTTOMWIDTH-17,12,0]) cube([10, 8, 3.8]);
    }
            
  }
}


if( TOP ) {
  translate([210,0,0]) {
    difference() {
      translate([0,0,0]) cylinder(20, 16, 16);
      translate([0,0,0]) cylinder(20, 14, 14);
      rotate([0,90,0]) translate([-67,0,-16]) cylinder(32, 50, 50);
    }
    /*
    difference() {
      translate([0,0,-5.3]) sphere(r = 30);
      translate([0,0,-5.3]) sphere(r = 28);
      translate([0,0,-44]) cylinder(64, 32, 32);
    }
    */
    
  }
}

if( TOPCAP ) {
  translate([210,0,0]) {
    translate([40,0,0]) {
      difference() {
        translate([0,0,0]) cylinder(5, 13.9, 13.9);
        translate([0,0,0]) cylinder(5, 12, 12);
      }
      difference() {
        translate([0,0,-21.5]) sphere(r = 31);
        translate([0,0,-21.5]) sphere(r = 28);
        translate([0,0,-58.9]) cylinder(64, 32, 32);
        translate([0,0,0]) cylinder(64, 1.1, 1.1);
      }
    }
  }
}


if( TOPWHEEL ) {
  translate([210,50,0]) {
    
    difference() {
      //translate([0,0,0]) cylinder(2, 10, 10);
      //translate([0,0,0]) cylinder(2, 9, 9);
    }    

    translate([0,0,0]) cylinder(3, 3, 3);
    translate([0,0,0]) cylinder(5, 1, 1);
    rotate([0,90,0]) translate([-1,0,-9.5]) cylinder(19, 1, 1);
    rotate([0,90,90]) translate([-1,0,-9.5]) cylinder(19, 1, 1);
    
    
    fn=96;
    outer_radius = 1;
    bending_radius = 9.5;
    wall_thickness = 1;
    height = 3.1415 * bending_radius / 2;

    translate([outer_radius-1, 0, 1])
      rotate([0, 0, 0]) intersection() {
        rotate_extrude($fn = fn, convexity = 4)
        translate([bending_radius, 0, 0])
        difference() {
          circle(r = outer_radius, $fn = fn);
          circle(r = outer_radius-wall_thickness, $fn = fn);
        }
      }
        
            

  }
 
}


if( HEAD ) {
  translate([-BOTTOMWIDTH-10,0,0]) {
    
    // Base
    difference() {
      translate([0,0,0]) cube([BOTTOMWIDTH, 50-22, 2]);
      translate([10,18,0]) cylinder(6, 2.5, 2.5);
    }

    difference() {
      translate([0,46-22,0]) cube([BOTTOMWIDTH, 4, 2.8]);
    }
     
    // Lights
    difference() {
      translate([10,18,2]) cylinder(4, 3.5, 3);
      translate([10,18,0]) cylinder(6, 2.5, 2.5);
    }
    
    
    // Construction
    translate([((BOTTOMWIDTH-40)/2)-4+.1,-8,0]) cube([3.8, 54-20, 2.8]);
    translate([((BOTTOMWIDTH-40)/2)+40+.1,-8,0]) cube([3.8, 54-20, 2.8]);
    
    translate([0,0,0]) cube([BOTTOMWIDTH, 4, 2.8]);
    translate([0,0,0]) cube([4, 28, 2.8]);
    translate([BOTTOMWIDTH-4,0,0]) cube([4, 28, 2.8]);
    
  }
}



