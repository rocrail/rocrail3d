/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

include <../../lib/shapes.scad>

BOTTOMLEN=180;
BOTTOMWIDTH=104;
SIDE=false;
HEAD=false;
SPANT=false;
SPANTHEAD=false;
ROOF=false;
ROOFSCREW=false;
FLIP=1;
WINDOWFRAME_S=false;
WINDOWFRAME_H=false;
WINDOWFRAME_DOOR=false;
ROADNUMBER=false;
DOOR=false;
SHORTROOF=25; // 25 Used for the boxload wagon

module window(width, height, cut) {
  if( cut ) {
    translate([0,0,0]) cube([width, height-5, 2]);

    translate([5,height-5,0]) cylinder(2, 5, 5);
    translate([width-5,height-5,0]) cylinder(2, 5, 5);

    translate([5,0,0]) cube([width-(2*5), height, 2]);
  }
  else {
    difference() {
      translate([-.5,0,0]) cube([width+1, height-5, .6]);
      translate([2,2,0]) cube([width-4, height-4, 4]);
    }
    translate([5,height-2,0]) cube([width-10, 2, 3.5]);
    difference() {
      translate([0,0,0]) cube([width, height-5, 3.5]);
      translate([2,2,0]) cube([width-4, height-(5+2), 4]);
    }
    // left upper corner
    difference() {
      translate([5,height-5,0]) cylinder(3.5, 5, 5);
      translate([5,height-5,0]) cylinder(3.5, 3, 3);
      translate([5,height-10,0]) cube([10, 10, 4]);
      translate([0,height-15,0]) cube([10, 10, 4]);
    }
    // right upper corner
    difference() {
      translate([width-5,height-5,0]) cylinder(3.5, 5, 5);
      translate([width-5,height-5,0]) cylinder(3.5, 3, 3);
      translate([width-15,height-10,0]) cube([10, 10, 4]);
      translate([width-10,height-15,0]) cube([10, 10, 4]);
    }

    // upper window
    translate([0,height-12,0]) cube([width, 1.4, 2]);
    

  }
}

if( WINDOWFRAME_S ) {
  translate([0,-80,0]) {
    window(30-.2, 40-.2, false);
  }
}

if( WINDOWFRAME_H ) {
  translate([50,-80,0]) {
    window(14-.2, 40-.2, false);
  }
}

if( WINDOWFRAME_DOOR ) {
  translate([130,-100,0]) {
    window(24-.2, 30-.2, false);
  }
}

if( DOOR ) {
  translate([80,-100,0]) {
    difference() {
      translate([-.5,0.2+0.5,0]) cube([41, 78.5-0.5, 1]);
      for( a =[0:6] ) {
        translate([0,a*4.8,0.5]) cube([41, .5, .5]);
      }
      translate([8,40,0]) window(24, 30, true);
    }
    difference() {
      translate([0.2,0.2+0.5,0]) cube([40-.2, 78-.2-.5, 3.5]);
      translate([2,0,1.5]) cube([36, 76, 2]);
      translate([8,40,0]) window(24, 30, true);
      translate([8,0,1.3]) cube([.4, 76, .4]);
      translate([32,0,1.3]) cube([.4, 76, .4]);
      translate([8,6,.9]) cube([24, 28, .8]);
    }
    translate([4,36,1]) cube([8, 1, 1.5]);
  }
}


if( SIDE ) {
  mirror([FLIP,0,0]) {
    difference() {
      translate([0,0,0]) cube([BOTTOMLEN-30+FLIP, 90, 2]);

      if( FLIP==0 )
        translate([BOTTOMLEN-30-1,0,0]) cube([1, 90, 1]);
      else
        translate([BOTTOMLEN-30,0,1]) cube([1, 90, 1]);

      for( a =[0:10] ) {
        translate([0,a*8,1.5]) cube([BOTTOMLEN-30, .5, .5]);
        translate([0,a*6.5,0]) cube([BOTTOMLEN-30+FLIP, .5, .2]);
      }
      for( a =[0:2] ) {
        translate([15+a*48,40,0]) window(30, 40, true);
      }
    }

    // Holders
    for( b =[0:3] ) {
      // New bottom holders: 30+42
      translate([1+(b*42)+0.1,-8,0]) cube([3.8, 32, 3.8]);
    }

    difference() {
      translate([0,0,0]) cube([BOTTOMLEN-30, 4, 3]);
      if( FLIP==0 )
        translate([BOTTOMLEN-30-1,0,0]) cube([1, 90, 1]);
    }
    
    difference() {
    translate([0,90-4,0]) cube([BOTTOMLEN-30, 4, 3]);
      if( FLIP==0 )
        translate([BOTTOMLEN-30-1,0,0]) cube([1, 90, 1]);
    }
    difference() {
    translate([0,24,0]) cube([BOTTOMLEN-30, 4, 3]);
      if( FLIP==0 )
        translate([BOTTOMLEN-30-1,0,0]) cube([1, 90, 1]);
    }

    difference() {
      translate([0,0,0]) cube([4, 90, 3]);
      if( FLIP==0 )
        translate([BOTTOMLEN-30-1,0,0]) cube([1, 90, 1]);
    }
    
    for( c =[0:2] ) {
      if( c == 2 )
        difference() {
          translate([15+c*48+37,24,0]) cube([2, 90-24, 3]);
          if( FLIP==0 )
            translate([BOTTOMLEN-30-1,0,0]) cube([1, 90, 1]);
        }
      else  
        translate([15+c*48+37,24,0]) cube([4, 90-24, 3]);
    }
  }
}

if( HEAD ) {
  translate([-BOTTOMWIDTH-10,0,0]) {
    
    // Base
    difference() {
      translate([0,0,0]) cube([BOTTOMWIDTH, 90, 2]);
      translate([(BOTTOMWIDTH-40)/2,2,0]) cube([40, 78, 2]);
      translate([8,40,0]) window(14, 40, true);
      translate([BOTTOMWIDTH-10-12,40,0]) window(14, 40, true);

      for( a =[0:10] ) {
        translate([0,a*8,1.5]) cube([BOTTOMLEN-30, .5, .5]);
        translate([0,a*8,0]) cube([BOTTOMLEN-30, .5, .2]);
      }
      
      // sluitlichten
      translate([10,18,0]) cylinder(6, 2.5, 2.5);
      translate([BOTTOMWIDTH-10,18,0]) cylinder(6, 2.5, 2.5);
    }
    
    // Construction
    translate([((BOTTOMWIDTH-40)/2)-4+.1,-8,0]) cube([3.8, 109, 2.8]);
    translate([((BOTTOMWIDTH-40)/2)+40+.1,-8,0]) cube([3.8, 109, 2.8]);
    
    translate([0,90-4,0]) cube([BOTTOMWIDTH, 4, 3]);
    translate([0,24,0]) cube([(BOTTOMWIDTH-40)/2, 4, 3]);
    translate([(BOTTOMWIDTH-40)/2+40,24,0]) cube([(BOTTOMWIDTH-40)/2, 4, 3]);
    
    // sluitlichten
    difference() {
      translate([10,18,2]) cylinder(4, 4, 3);
      translate([10,18,0]) cylinder(6, 2.5, 2.5);
    }
    difference() {
      translate([BOTTOMWIDTH-10,18,2]) cylinder(4, 4, 3);
      translate([BOTTOMWIDTH-10,18,0]) cylinder(6, 2.5, 2.5);
    }
    
    // Roof rounding
    difference() {
      translate([BOTTOMWIDTH/2,0,0]) cylinder(2, BOTTOMWIDTH, BOTTOMWIDTH);
      translate([-(BOTTOMWIDTH/2),-118,0]) cube([BOTTOMWIDTH*2, BOTTOMWIDTH*2, 2]);

      // Roof screws
      translate([18,94,0]) cylinder(4, 1.2, 1.2); 
      translate([BOTTOMWIDTH-18,94,0]) cylinder(4, 1.2, 1.2); 
      
      for( a =[10:12] ) {
        translate([0,a*8,1.5]) cube([BOTTOMLEN-30, .5, .5]);
      }
      
    }
  }
}

if( SPANTHEAD ) {
  translate([-BOTTOMWIDTH-10,-120,0]) {
    difference() {
      translate([60,0,0]) cube([BOTTOMWIDTH/6, BOTTOMWIDTH, 4]);
      translate([60+2,2,0]) cube([BOTTOMWIDTH/6-2, BOTTOMWIDTH-4, 4]);
    }

  }
}

if( SPANT ) {
  translate([-BOTTOMWIDTH-10,-120,0]) {
    // Base
    difference() {
      translate([0,0,0]) cube([BOTTOMWIDTH/2, BOTTOMWIDTH, 4]);
      translate([2,2,0]) cube([BOTTOMWIDTH/2-4, BOTTOMWIDTH-4, 4]);
    }
    translate([2,1,0]) rotate([0,0,64]) cube([113, 2, 4]);
    translate([BOTTOMWIDTH/2-1,2,0]) rotate([0,0,115.5]) cube([113, 2, 4]);
    

    /*
    difference() {
      translate([0,0,0]) cube([BOTTOMWIDTH, 90, 2]);
      translate([10,10,0]) cube([BOTTOMWIDTH-20, 70, 2]);
      //translate([20,85,0]) cube([BOTTOMWIDTH-40, 10, 2]);

      translate([15,85,0]) cylinder(2, 1.7, 1.7); 
      translate([BOTTOMWIDTH-15,85,0]) cylinder(2, 1.7, 1.7); 
    }
    
    translate([8,10,0]) cube([2, 70, 6]);
    
    difference() {
      translate([13,-10,0]) cube([BOTTOMWIDTH-26, 10, 2]);
      translate([18,-5,0]) cylinder(2, 1.7, 1.7); 
      translate([BOTTOMWIDTH-18,-5,0]) cylinder(2, 1.7, 1.7); 
      translate([24,-10,0]) cube([BOTTOMWIDTH-48, 10, 2]);
    }
    */    
    
    // Roof rounding
    /*
    difference() {
      translate([BOTTOMWIDTH/2,0,0]) cylinder(2, BOTTOMWIDTH, BOTTOMWIDTH);
      translate([-(BOTTOMWIDTH/2),-118,0]) cube([BOTTOMWIDTH*2, BOTTOMWIDTH*2, 2]);
    }
    */
  }
  
}

if( ROOF ) {
  translate([-BOTTOMWIDTH-10,40,0]) {
    difference() {
      translate([BOTTOMWIDTH/2,3,0]) cylinder(BOTTOMLEN-SHORTROOF, BOTTOMWIDTH+2, BOTTOMWIDTH+2);

      translate([BOTTOMWIDTH/2,3,2]) cylinder(46, BOTTOMWIDTH, BOTTOMWIDTH);
      translate([BOTTOMWIDTH/2,3,2+48]) cylinder(47, BOTTOMWIDTH, BOTTOMWIDTH);
      translate([BOTTOMWIDTH/2,3,2+48+49]) cylinder(46, BOTTOMWIDTH, BOTTOMWIDTH);
      translate([BOTTOMWIDTH/2,3,2+48+49+48]) cylinder(33, BOTTOMWIDTH, BOTTOMWIDTH);


      translate([0,89,48]) cube([BOTTOMWIDTH, 4, 2]);

      translate([-(BOTTOMWIDTH/2)-2,-119,0]) cube([BOTTOMWIDTH*2+4, BOTTOMWIDTH*2+4, BOTTOMLEN]);
      translate([18,96,0]) cylinder(BOTTOMLEN, 1.2, 1.2); 
      translate([BOTTOMWIDTH-18,96,0]) cylinder(BOTTOMLEN, 1.2, 1.2); 
    }
 
    translate([0,89,0]) cube([BOTTOMWIDTH, 4, 2]);
    translate([0,89,48]) cube([BOTTOMWIDTH, 4, 2]);
    translate([0,89,48+49]) cube([BOTTOMWIDTH, 4, 2]);
    translate([0,89,48+49+48]) cube([BOTTOMWIDTH, 4, 2]);

    difference() {
      translate([0,89,0]) cube([2, 6, BOTTOMLEN-33]);
      //translate([0,89,2]) cube([BOTTOMWIDTH, 4, 2.5]);
    }
    
    difference() {
      translate([BOTTOMWIDTH-2,89,0]) cube([2, 6, BOTTOMLEN-33]);
      //translate([0,89,2]) cube([BOTTOMWIDTH, 4, 2.5]);
    }

    difference() {
      translate([BOTTOMWIDTH/2-1,89,0]) cube([2, 20, BOTTOMLEN-33]);
      //translate([0,89,2]) cube([BOTTOMWIDTH, 4, 2.5]);
    }
    
    translate([-6,93,0]) cube([6, 2, BOTTOMLEN-SHORTROOF]);
    translate([BOTTOMWIDTH,93,0]) cube([6, 2, BOTTOMLEN-SHORTROOF]);


  }
}

/*
if( ROOFCON ) {
  // Connector
  translate([-BOTTOMWIDTH-10,0,0]) {
    difference() {
      translate([BOTTOMWIDTH/2,3,0]) cylinder(20, BOTTOMWIDTH+4, BOTTOMWIDTH+4);
      translate([BOTTOMWIDTH/2,3,0]) cylinder(20, BOTTOMWIDTH+2, BOTTOMWIDTH+2);
      translate([-(BOTTOMWIDTH/2)-4,-115,0]) cube([BOTTOMWIDTH*2+4+4, BOTTOMWIDTH*2+4, 60]);
    translate([-1-4.5,90,0]) rotate([0,0,-30]) cube([4,20,30]);
    translate([BOTTOMWIDTH-3+3.5,90,0]) rotate([0,0,30])  cube([4,20,30]);
    }
  }
}
*/
if( ROADNUMBER ) {
  translate([00,-30,0]) {
    translate([0,0,0]) roundedcube(37, 11, .8, 2)
    translate([0,0,0]) cube([37, 11, .8]);
    translate([2,2.5,0]) linear_extrude(1.6) text("HsDh001",6);
  }
}

// M3 Thread
if( ROOFSCREW ) {
  translate([80,-40,0]) {
    translate([0,0,0]) cylinder(1, 8, 8);
    translate([0,0,5]) cylinder(1, 8, 8);
    translate([-7,-0.5,0]) cube([14, 1, 5]);
    translate([-0.5,-7,0]) cube([1, 14, 5]);
    difference() {
      translate([0,0,0]) cylinder(10, 4, 4);
      translate([0,0,1]) cylinder(9, 1.3, 1.3);
    }
  }
}







