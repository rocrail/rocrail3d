/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

include <../../lib/shapes.scad>
BOTTOMLEN=180;
BOTTOMWIDTH=104;


// WIOpi-01 M3 screw terminals:
translate([71,56,1]) rotate([0,0,90]) {
  translate([3.65,5.3,0]){
    difference() {
      cylinder(11, 5, 3);
      cylinder(11, 1.2, 1.2);
    }
  }
  translate([26.65,5.3,0]){
    difference() {
      cylinder(11, 5, 3);
      cylinder(11, 1.2, 1.2);
    }
  }
  translate([3.65,63.3,0]){
    difference() {
      cylinder(11, 5, 3);
      cylinder(11, 1.2, 1.2);
    }
  }
  translate([26.65,63.3,0]){
    difference() {
      cylinder(11, 5, 3);
      cylinder(11, 1.2, 1.2);
    }
  }
}

translate([0,2,0]) {
  difference() {
    translate([0,0,0]) cube([84, 80, 12]);
    translate([2,0,2]) cube([80, 80, 10]);
    translate([27,(84-27.2)/2+1,0.4]) cube([26.2, 25.2, 5]);
    // Drain
    translate([42,10,0]) cylinder(4, 1.5, 1.5);
    translate([42,70,0]) cylinder(4, 1.5, 1.5);
  }

  // RFID mount
  difference() {
    translate([26,(84-27.2)/2,2]) cube([28.2, 27.2, 3]);
    translate([27,(84-27.2)/2+1,2]) cube([26.2, 25.2, 3]);
  }
}


translate([0,-10,12]) {
  difference() {
    translate([0,0,0]) cube([84, 104, 18]);
    translate([2,2,0]) cube([80, 100, 18]);

    // flip switch
    rotate([90,0,0]) translate([18,4,-2]) cylinder(2, 3.1, 3.1);

    // XT60 chassis
    translate([29.8,0,0]) cube([12.2, 2, 8.2]);
    rotate([90,0,0]) translate([42.2,4,-2]) cylinder(2, 4.1, 4.1);
  }
}

// Clips
translate([0,-10,40]) rotate([90,0,90]) {
  difference() {
    translate([13,-10,0]) cube([BOTTOMWIDTH-26, 10, 2]);
    translate([18,-5,0]) cylinder(2, 1.7, 1.7); 
    translate([BOTTOMWIDTH-18,-5,0]) cylinder(2, 1.7, 1.7); 
    translate([24,-10,0]) cube([BOTTOMWIDTH-48, 10, 2]);
  }
  difference() {
    translate([13,0,0]) cube([BOTTOMWIDTH-26, 2, 3.5]);
    translate([24,0,0]) cube([BOTTOMWIDTH-48, 2, 4]);
  }
}
translate([-2,3,0]) cube([2, 2, 42]);
translate([-2,12,0]) cube([2, 2, 42]);
translate([-2,BOTTOMWIDTH-25,0]) cube([2, 2, 42]);
translate([-2,BOTTOMWIDTH-34,0]) cube([2, 2, 42]);

// Clips
translate([84,94,40]) rotate([90,0,270]) {
  difference() {
    translate([13,-10,0]) cube([BOTTOMWIDTH-26, 10, 2]);
    translate([18,-5,0]) cylinder(2, 1.7, 1.7); 
    translate([BOTTOMWIDTH-18,-5,0]) cylinder(2, 1.7, 1.7); 
    translate([24,-10,0]) cube([BOTTOMWIDTH-48, 10, 2]);
  }
  difference() {
    translate([13,0,0]) cube([BOTTOMWIDTH-26, 2, 3.5]);
    translate([24,0,0]) cube([BOTTOMWIDTH-48, 2, 4]);
  }
}
translate([84,3,0]) cube([2, 2, 42]);
translate([84,12,0]) cube([2, 2, 42]);
translate([84,BOTTOMWIDTH-25,0]) cube([2, 2, 42]);
translate([84,BOTTOMWIDTH-34,0]) cube([2, 2, 42]);

  
translate([0,2,0]) {
  difference() {
    translate([0,0,12]) rotate([0,90,0]) cylinder(84, 12, 12);
    translate([2,0,12]) rotate([0,90,0]) cylinder(80, 10, 10);
    translate([0,0,0]) cube([84, 12, 24]);
    translate([0,-12,12]) cube([84, 12, 24]);
  }
}

translate([0,82,0]) {
  difference() {
    translate([0,0,12]) rotate([0,90,0]) cylinder(84, 12, 12);
    translate([2,0,12]) rotate([0,90,0]) cylinder(80, 10, 10);
    translate([0,-12,0]) cube([84, 12, 24]);
    translate([0,0,12]) cube([84, 12, 24]);
  }
}
