/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

include <../../lib/shapes.scad>

POLEHOLE=7.8;
POLEHEIGHT=240;


BOTTOM=0;
ARM=0;
ARMMAGNET=0;
ARMRED=0;   // red border for the arm
ARMBLACK=0; // black border for the arm backside
POLE=1;     // print WITOUT support to make room for cable
                // rotate hotizontally 45° on small printers
WARN=0;     // white plate
WARNRED=0;  // red parts for the white plate
TOP=1;
LED=0;
LEDCOVER=0;
LEDWINDOW=0;
HALLRED=0;

// D32 LED window for top
if( LEDWINDOW ) {
  translate([140,0,0]) {
    translate([-.5,-.5,0]) cube([8.7, 8.7, .6]);
    translate([0,0,0]) cube([7.7, 7.7, 2.6]);
  }
}


if( LED ) {
  translate([80,120,0]) {
    difference() {
      translate([0,0,0]) cube([12, 8, 7]);
      translate([10/2, 8/2, 0]) cylinder(7, 2.7, 2.7);
      translate([10/2, 8/2, 2]) cylinder(5, 3.1, 3.1);
      // cable cut
      translate([6,2,4]) cube([5, 4, 3]);
    }
    
    // top
    translate([0,0,0]) rotate([0,270,0]) pyramid(7,8,2);
    
    // mounting clip top
    translate([2,8,0]) cube([1.2, 4, 6]);
    difference() {
      translate([1.7,10.5,1]) cube([1, 1, 4]);
      translate([1.7,10.5,2]) cube([1, 1, 2]);
    }

    // mounting clip bottom
    translate([10-1.2,8,0]) cube([1.2, 4, 6]);
    difference() {
      translate([10+1.0-1.7,10.5,1]) cube([1, 1, 4]);
      translate([10+1.0-1.7,10.5,2]) cube([1, 1, 2]);
    }
    
    // bottom
    translate([12,0,7]) rotate([0,90,0]) pyramid(7,8,5);
  }
}
if( LEDCOVER ) {
  translate([80,120,0]) {
    // cover
    translate([10/2, 14+8/2, 0]) cylinder(2, 2.95, 2.95);
    translate([10/2-8/2, 14+8/2-6.5/2, 0]) cube([8, 6.5, .8]);
  }
}


if( TOP ) {
  translate([80,0,0]) {
    difference() {
      translate([0,0,0]) cube([BOTTEMWIDTH+4, BOTTEMLEN+4, 16]);
      translate([1.9,1.9,2]) cube([BOTTEMWIDTH+.2, BOTTEMLEN+.2, 16-2]);
      
      for( b =[1:19] ) {
        translate([1.8+b*2,4,0]) cube([.4, BOTTEMLEN-4, .2]);
      }
      

      // pole mount
      translate([BOTTEMWIDTH-POLEHOLE-2, BOTTEMLEN-(POLEHOLE+2), 0]) cube([POLEHOLE+4, POLEHOLE+4+4, 22]);
      
      // steldraad
      translate([BOTTEMWIDTH-POLEHOLE-2-5, BOTTEMLEN-POLEHOLE+3, 0]) cube([6, 6, 22]);
    
      // Edges
      translate([BOTTEMWIDTH+4,2,0]){
        rotate(180) prism(BOTTEMWIDTH+4, 10, 10);
      }
      translate([0,BOTTEMLEN+2,0]){
        rotate(0) prism((BOTTEMWIDTH+4)/2, 10, 10);
      }
      translate([2,0,0]){
        rotate(90) prism(BOTTEMLEN+4, 10, 10);
      }
      translate([BOTTEMWIDTH+2,BOTTEMLEN-12,0]){
        rotate(270) prism(BOTTEMLEN+4, 10, 10);
      }

      // Cut for sleepers
      translate([0,0,16-4.0]) cube([2, BOTTEMLEN+4, 4.0]);

      // switch cut
      translate([2+3, BOTTEMLEN+2, 9]) cube([6, 2, 7]);
      translate([2+3+3, BOTTEMLEN+2+2, 9]) rotate([90,0,0]) cylinder(6, 3, 3);
    
      // USB cut
      translate([2+7.5, 0, 16-4-7.5]) cube([11, 4, 7]);

      // D32 LED cut
      translate([BOTTEMWIDTH+4-20-5, 8, 0]) cube([8, 8, 2]);
      translate([BOTTEMWIDTH+4-20-5-.5, 8-.5, 1.2]) cube([9, 9, .8]);

      // servo lever 45° cut
      translate([BOTTEMWIDTH-24.7,BOTTEMLEN-4.7,10]){
        rotate([0,180,270]) prism(6, 10, 10);
      }

      // Hall mark
      translate([BOTTEMWIDTH+3.4, 52, 7]) rotate([90,0,90]) cylinder(2, 3, 3);
    }
    // D32 LED border
    difference() {
      translate([BOTTEMWIDTH+4-20-5-1.5, 8-1.5, 0]) cube([11, 11, 1]);
      translate([BOTTEMWIDTH+4-20-5, 8, 0]) cube([8, 8, 1]);
    }

    // screw terminal
    difference() {
      translate([6,6,1.9]) cylinder(5, 5, 3);
      translate([6,6,1.9]) cylinder(5, 1, 1);
    }
    // screw terminal
    difference() {
      translate([BOTTEMWIDTH-2,6,1.9]) cylinder(5, 5, 3);
      translate([BOTTEMWIDTH-2,6,1.9]) cylinder(5, 1, 1);
    }
    // screw terminal
    difference() {
      translate([BOTTEMWIDTH-2,72,1.9]) cylinder(5, 5, 3);
      translate([BOTTEMWIDTH-2,72,1.9]) cylinder(5, 1, 1);
      translate([BOTTEMWIDTH-9,67,1.9]) cube([5, 10, 10]);
    }
    
    // USB dripper
    difference() {
      translate([7.5,-2.5,4.5]){
        rotate(0) mirror([0,0,1]) prism(15, 2.5, 2.5);
      }
    }

    
  }
}



module clamb(x, y) {
  translate([x,y,0]) {
    difference() {
      translate([0,0,0]) cube([4, 2, 7.5-1.2]);
      translate([1,0,1]) cube([2, 2, 1.6]);
      translate([1,0,7.5-1.2-1-1.6]) cube([2, 2, 1.6]);
    }
  }
}

if( WARNRED ) {
  translate([-80,0,0]) {
    for( a = [1:4] ) {
      translate([0,a*((POLEHEIGHT-75)/7+4),0]) cube([7.5, ((POLEHEIGHT-75)/7), .6]);
    }
  }
}

if( WARN ) {
  // length appr. 84mm
  translate([-60,0,0]) {
    difference() {
      translate([0,35,0]) cube([7.5, POLEHEIGHT-75, 1.2]);
      for( a = [1:6] ) {
        translate([0,35+a*((POLEHEIGHT-75)/7),0]) cube([7.5, .4, .2]);
      }
    }
    translate([7.5/2, 35+10, 0]) cylinder(5, 1.0, 1.0);
    translate([7.5/2, 35+(POLEHEIGHT-75)/2, 0]) cylinder(5, 1.0, 1.0);
    translate([7.5/2, 35+POLEHEIGHT-75-10, 0]) cylinder(5, 1.0, 1.0);

    translate([7.5/2, 35+10, 0]) cylinder(3, 2.0, 2.0);
    translate([7.5/2, 35+(POLEHEIGHT-75)/2, 0]) cylinder(3, 2.0, 2.0);
    translate([7.5/2, 35+POLEHEIGHT-75-10, 0]) cylinder(3, 2.0, 2.0);

  }
}

if( POLE ) {
  // length appr. 84mm
  translate([-40,0,0]) {
    difference() {
      translate([0,0,0]) cube([7.5, POLEHEIGHT, 7.5]);
      //translate([2,0,2]) cube([7.5-4, POLEHEIGHT, 7.5-4]);

      translate([7.5/2, 0, 7.5/2]) rotate([0,90,90]) cylinder(POLEHEIGHT-20, 2.25, 2.25);
      // arm mount
      translate([7.5/2, POLEHEIGHT-10, 2]) cylinder(7.5, 1.0, 1.0);
      
      // cable cut
      translate([7.5-4, POLEHEIGHT-50, 7.5/2]) rotate([0,90,60])cylinder(10, 1.5, 1.5);
      
      // cable cut at foot
      translate([7.5/2, 0, 0]) cylinder(7.5, 2.0, 2.0);
      
      // warn mount
      translate([7.5/2, 35+10, 7.5-2]) cylinder(5, 1.2, 1.2);
      translate([7.5/2, 35+(POLEHEIGHT-75)/2, 7.5-2]) cylinder(5, 1.2, 1.2);
      translate([7.5/2, 35+POLEHEIGHT-75-10, 7.5-2]) cylinder(5, 1.2, 1.2);
    }
    
    // arm mount
    difference() {
      translate([7.5/2, POLEHEIGHT-10, 2]) cylinder(5.5, 3.0, 3.0);
      translate([7.5/2, POLEHEIGHT-10, 2]) cylinder(7.5, 1.0, 1.0);
    }

    
    CLAMBDIST = (POLEHEIGHT - 50 -50 - 10) / 8;
    for( a = [0:8] ) {
      clamb(-4, 50+CLAMBDIST*a);
    }
    // LED
    clamb(7.5, POLEHEIGHT-25);
    clamb(7.5, POLEHEIGHT-35);
  
  // top of pole
  translate([0,POLEHEIGHT,7.5]) rotate([270,0,0]) pyramid(7.5,7.5,5);
  }
}


if( ARMRED ) {
  // red part
  translate([-20,90,0]) {
    difference() {
      translate([0,0,0]) cube([8, 62.5, .6]);
      translate([1.5,1.5,0]) cube([5, 62.5, .6]);
    }
    difference() {
      translate([4, 68.5, 0]) cylinder(.6, 7.5, 7.5);
      translate([4, 68.5, 0]) cylinder(.6, 6.0, 6.0);
    }

    if( ARMBLACK ) {
      difference() {
        // draaipunt for black -> backside
        translate([4, 12, 0]) cylinder(.6, 3.0, 3.0);
        translate([4, 12, 0]) cylinder(.6, 1.5, 1.5);
      }
    } 
  }
}

if( HALLRED ) {
  // Hall ring
  translate([-20,120,0]) {
    difference() {
      translate([4, 68.5, 0]) cylinder(.4, 4.5, 4.5);
      translate([4, 68.5, 0]) cylinder(.4, 3.5, 3.5);
    }
  }
}

if( ARM ) {
  // length appr. 84mm
  translate([-20,0,0]) {
    difference() {
      translate([0,0,0]) cube([8, 62.5, 2]);
      translate([1,18,1.5]) cube([6, 40, 2]);
      
      if( !ARMMAGNET ) {
        // draaipunt
        translate([4, 12, 0]) cylinder(2, 1.5, 1.5);
        // drijfstang
        translate([4,  3, 0]) cylinder(2, 1, 1);
      }
    }
    translate([3, 20, 0]) cube([2, 36, 2.5]);
    
    difference() {
      translate([4, 68.5, 0]) cylinder(2, 7.5, 7.5);
      translate([4, 68.5, 1.5]) cylinder(1, 6.0, 6.0);
      if( ARMMAGNET ) {
        translate([4, 68.5, 0]) cylinder(5.0, 1.6, 1.6);
      }
    }

    difference() {
      translate([4, 68.5, 0]) cylinder(ARMMAGNET?5:2.5, 5, ARMMAGNET?3:4);
      if( ARMMAGNET ) {
        translate([4, 68.5, 0.4]) cylinder(5.0, 1.6, 1.6);
      }
    }  
  }
}


module trackmount(x, y, h, l) {
  translate([x+l+7, y+3.5, 0]){
    difference() {
      cylinder(h, 3.5, 3.5);
      cylinder(h, 1.25, 1.25);
    }
  }
  difference() {
    translate([x, y, 0]) cube([l+7, 7, h]);
    translate([x+l+7, y+3.5, 0]) cylinder(h, 1.25, 1.25);
  }
}

BOTTEMLEN=98;
BOTTEMWIDTH=40;

if( BOTTOM ) {
  translate([0,0,0]) {
    // bottom plate
    difference() {
      translate([0, -2, 0]) cube([BOTTEMWIDTH, BOTTEMLEN+4, 2]);
      
      // pole drain
      translate([2+POLEHOLE/2, BOTTEMLEN-(POLEHOLE/2)-2, 0]) cylinder(2, 2.0, 2.0);
      // servo drain
      translate([BOTTEMWIDTH/2, BOTTEMLEN/2+40, 0]) cylinder(2, .75, 2.0);
      // battery drain
      translate([BOTTEMWIDTH/2, BOTTEMLEN/2-20, 0]) cylinder(2, .75, 2.0);
      
      // Top screw terminal holes
      translate([4,4,0]) cylinder(2, 2, 1);
      translate([BOTTEMWIDTH-4,4,0]) cylinder(2, 2, 1);
      translate([4,70,0]) cylinder(2, 2, 1);
      
    }
    
    // house
    difference() {
      translate([0,0,0]) cube([BOTTEMWIDTH, BOTTEMLEN, 8]);
      translate([2,2,2]) cube([BOTTEMWIDTH-4, BOTTEMLEN-4, 6]);
      // pole drain
      translate([2+POLEHOLE/2, BOTTEMLEN-(POLEHOLE/2)-2, 0]) cylinder(2, 2.0, 2.0);
      // servo drain
      translate([BOTTEMWIDTH/2, BOTTEMLEN/2+40, 0]) cylinder(2, .75, 2.0);
      // battery drain
      translate([BOTTEMWIDTH/2, BOTTEMLEN/2-20, 0]) cylinder(2, .75, 2.0);
      // Top screw terminal holes
      translate([BOTTEMWIDTH-4,4,0]) cylinder(2, 2, 1);
      translate([4,4,0]) cylinder(2, 2, 1);
      translate([4,70,0]) cylinder(2, 2, 1);
      
      // flip switch mount
      translate([BOTTEMWIDTH-6, BOTTEMLEN, 8]) rotate([90,0,0]) cylinder(2, 3, 3);

      
    }

    // Top screw terminal support
    difference() { 
      translate([BOTTEMWIDTH-4,4,2]) cylinder(2, 2.4, 2.4);
      translate([BOTTEMWIDTH-4,4,2]) cylinder(2, 1, 1);
    }
    difference() { 
      translate([4,4,2]) cylinder(2, 2.4, 2.4);
      translate([4,4,2]) cylinder(2, 1, 1);
    }
    difference() { 
      translate([4,70,2]) cylinder(2, 2.4, 2.4);
      translate([4,70,2]) cylinder(2, 1, 1);
    }
    
    // D32 mount cube
    difference() { 
      translate([0,56,0]) cube([40, 2, 6]);
      translate([40/2-22/2,57,0]) cylinder(7, 1.0, 1.0);
      translate([40/2+22/2,57,0]) cylinder(7, 1.0, 1.0);
      // drain cut
      translate([40/2-3, 56, 0]) cube([6, 2, 6]);
    }
    
    // D32 mount screw terminals
    difference() {
      translate([40/2-22/2,57,0]) cylinder(8, 2.0, 2.0);
      translate([40/2-22/2,57,0]) cylinder(8, 1.0, 1.0);
    }
    difference() {
      translate([40/2+22/2,57,0]) cylinder(8, 2.0, 2.0);
      translate([40/2+22/2,57,0]) cylinder(8, 1.0, 1.0);
    }

    // D32 support over battery
    difference() {
      translate([2, 5.5, 2]) cube([40-4, 2, 6]);
      // cable cut
      translate([22, 5.5, 2]) cube([10, 2, 6]);
    }
    
    // pullup battery to let water flow to the drain
    translate([2, 7.5, 2]) cube([4, 50, .5]);
    translate([BOTTEMWIDTH-2-4, 7.5, 2]) cube([4, 50, .5]);


    // Servo mounting
    SERVOPAD=4.8; // MG90S (Use 4.5 for SG90)

    difference() {
      translate([0, 55 + 20, 2]) cube([40, 2, 14]);
      translate([6.5, 55 + 20, 2]) cube([23, 2, 14]);
    }

    difference() {
      translate([0, 55 + 20 + SERVOPAD, 2]) cube([40, 2, 14]);
      translate([6.5, 55 + 20 + SERVOPAD, 2]) cube([23, 2, 14]);
    }

    translate([0, 55 + 22, 2]) cube([2, SERVOPAD, 14]);
    translate([38, 55 + 22, 2]) cube([2, SERVOPAD, 14]);

    // Servo bed; + 1.5 is for pixel cable
    translate([7+1.5, 60, 2]) cube([19, 22, 2]);

    // Track mounting point L1
    trackmount(BOTTEMWIDTH, BOTTEMLEN/2-3.5, 5, 27);

    // flip switch mount
    translate([0, BOTTEMLEN-2, 4]) {
      difference() {
        translate([0, 0, 0]) cube([BOTTEMWIDTH, 2, 10]);
        translate([BOTTEMWIDTH-9, 0, 0]) cube([6, 2, 10]);
      }
      translate([BOTTEMWIDTH-2, -15, 0]) cube([2, 15, 10]);
      translate([BOTTEMWIDTH-2-10, -4, -2]) cube([2, 4, 12]);
    }
    
    // pole mount
    translate([0, BOTTEMLEN-(POLEHOLE+2)-2, 0]) difference() {
      translate([0, 0, 0]) cube([POLEHOLE+4, POLEHOLE+4, 22]);
      translate([2, 2, 0]) cube([POLEHOLE, POLEHOLE, 22]);
      // cable cut
      translate([3, 0, 2]) cube([5, 4, 4]);
    }
    
    // top fillup
    translate([0, BOTTEMLEN, 2]) cube([POLEHOLE+4, 2, 16]);
      //translate([3, BOTTEMLEN-(POLEHOLE+2)-3, 6]) cube([5, 20, 4]);
    
    translate([0, BOTTEMLEN-(POLEHOLE+2)-2, 0]) difference() {
      translate([0,0,22]) rotate([0,0,0]) pyramid(POLEHOLE+4, POLEHOLE+4,5);
      translate([2, 2, 0]) cube([POLEHOLE, POLEHOLE, 30]);
    }

    // HALL mount
    translate([0, 44, 2]) {
      difference() {
        translate([0, 0, 0]) cube([3, 12, 12]);
        translate([2, 0, 0]) cube([1, 12, 7]);
        translate([.5, (12/2)-(4.5/2), 0]) cube([2, 4.5, 12]);
      }
      translate([3,0,7]) rotate([0,0,90]) mirror([0,0,1]) prism(14, 2.5, 2.5);
       
        translate([0, 12, 0]) cube([5, 2, 12]);

    }

    
  }
}


