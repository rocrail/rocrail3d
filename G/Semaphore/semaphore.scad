/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
/*
https://www.thingiverse.com/thing:1604369
https://github.com/justinotherguy/OpenSCAD-Getriebebibliothek
*/
include <../../lib/shapes.scad>

$fn=128;


LEVER=false;
MAST=true;
ARM=true;

if( ARM ) {
  translate([40,0,0]) {
    translate([5,45,0]) rotate([0,270,0]) prism(1.2, 5, 5);
    translate([0,45,1.2]) rotate([0,90,0]) prism(1.2, 5, 5);
    difference() {
      translate([2.5,80,0]) cylinder(1.2, 5, 5);
      translate([2.5,80,0]) cylinder(1.2, 2, 2);
    }
    difference() {
      translate([0,50,0]) cube([5, 27, 1.2]);
      translate([2.5,60,0]) cylinder(2, 1.1, 1.1);
      translate([2.5,53,0]) cylinder(2, .5, .5);
    }
  }
}

if( MAST ) {
  translate([30,0,0]) {
    translate([2.5,80,0]) cylinder(2, 2.5, 2.5);
    difference() {
      translate([0,0,0]) cube([5, 80, 2]);
      translate([2.5,6,0]) cylinder(2, .5, .5);
      translate([2.5,75,0]) cylinder(2, 1.1, 1.1);
    }
  }
}

// Lever
if( LEVER ) {
  difference() {
    translate([10,0,0]) cylinder(2, 2.5, 2.5);
    translate([10,0,0]) cylinder(5, .5, .5);
    translate([8,0,0]) cylinder(5, .5, .5);
  }
  difference() {
    translate([0,-2.5,0]) cube([10, 5, 2]);
    translate([0,0,0]) cylinder(2, 2.45, 2.45);
    translate([8,0,0]) cylinder(5, .5, .5);
    translate([10,0,0]) cylinder(5, .5, .5);
    translate([8,0,0]) cylinder(5, .5, .5);
    translate([6,0,0]) cylinder(5, .5, .5);
  }
  difference() {
    translate([0,0,0]) cylinder(5, 3.5, 3.5);
    translate([0,0,0]) cylinder(5, 2.45, 2.45);
  }
  difference() {
    translate([0,0,0]) cylinder(2.0, 2.45, 2.45);
    translate([0,0,0]) cylinder(2.5, 1.3, 1.3);
  }
}

