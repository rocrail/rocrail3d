/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
include <../../lib/shapes.scad>


module AxGear(x,y) {
BALLBEARING=true;
BBrad=3.55;

  translate([x,y,0]) {
    difference() {
      translate([-12,-7,0]) cube([24, 2, 8]);
      //translate([-13,-7,5]) cube([26, 1, 1]);
      //translate([-13,-7,0]) cube([1, 1, 6]);
      //translate([12,-7,0]) cube([1, 1, 6]);
    }
    
    translate([-9,7,3]) rotate([90,0,0]) bogieSpring(10, 2.25, 6);
    translate([-9,7,3]) rotate([90, 0, 0]) cylinder(12, 2.25, 2.25);

    translate([9,7,3]) rotate([90,0,0]) bogieSpring(10, 2.25, 6);
    translate([9,7,3]) rotate([90, 0, 0]) cylinder(12, 2.25, 2.25);


    difference() {
      translate([0,0,0]) cylinder(8, 5, 5);
      translate([0,0,0]) cylinder(8, 1.7, 1.7);
      if( BALLBEARING )
        translate([0,0,0]) cylinder(3.5, BBrad, BBrad);
    }
    difference() {
      translate([0,0,6]) sphere(r = 5);
      translate([0,0,0]) cylinder(8, 1.7, 1.7);
      if( BALLBEARING )
        translate([0,0,0]) cylinder(3.5, BBrad, BBrad);
    }
    difference() {
      translate([-5,-5,0]) cube([10, 12, 6]);
      translate([-4,5,0]) cube([8, 2, 6]);
      translate([0,0,0]) cylinder(8, 1.7, 1.7);
      if( BALLBEARING )
        translate([0,0,0]) cylinder(3.5, BBrad, BBrad);
    }
  }
}

module WIOpiBase(LENGTH,WIDTH) {
//LENGTH=79;
//WIDTH=52;

// Bottom plate with holes:
difference() {
  cube([WIDTH, LENGTH, 2]);
  translate([14,14,0]) cube([WIDTH-28, LENGTH-34, 2]);
  translate([8,38.5,0]) cylinder(2, 1.5, 1.5);
  translate([44,38.5,0]) cylinder(2, 1.5, 1.5);
  translate([3.65,5.3,0]) cylinder(2, 1.5, 1.5);
  translate([26.65,5.3,0]) cylinder(2, 1.5, 1.5);
  translate([3.66,63.3,0]) cylinder(2, 1.5, 1.5);
  translate([26.65,63.3,0]) cylinder(2, 1.5, 1.5);
  translate([21,(79-60)/2,0]) cylinder(2, 1.5, 1.5);
  translate([21,(79-60)/2+60,0]) cylinder(2, 1.5, 1.5);
  
  
}

// Stable bottom plate holes:
translate([8,38.5,0]){
  difference() {
    cylinder(2.4, 3, 3);
    cylinder(2.4, 1.5, 1.5);
  }
}
translate([44,38.5,0]){
  difference() {
    cylinder(2.4, 3, 3);
    cylinder(2.4, 1.5, 1.5);
  }
}

translate([21,(79-60)/2,0]){
  difference() {
    cylinder(2.4, 3, 3);
    cylinder(2.4, 1.5, 1.5);
  }
}
translate([21,(79-60)/2+60,0]){
  difference() {
    cylinder(2.4, 3, 3);
    cylinder(2.4, 1.5, 1.5);
  }
}


// Four M3 screw terminals:
translate([3.65,5.3,0]){
  difference() {
    cylinder(7, 3.5, 3);
    cylinder(7, 1.5, 1.5);
  }
}
translate([26.65,5.3,0]){
  difference() {
    cylinder(7, 3.5, 3);
    cylinder(7, 1.5, 1.5);
  }
}
translate([3.65,63.3,0]){
  difference() {
    cylinder(7, 3.5, 3);
    cylinder(7, 1.5, 1.5);
  }
}
translate([26.65,63.3,0]){
  difference() {
    cylinder(7, 3.5, 3);
    cylinder(7, 1.5, 1.5);
  }
}
}
