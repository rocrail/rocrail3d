/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;
//$fn=64;
include <../../lib/shapes.scad>


LEDPLAAT_TOP=1;
LEDPLAAT_BOTTOM=0;
LEDPLAAT_NORMAAL=0;
LEDHOUDER=1;
LEDKAP=0;
RAND=0;
LENS=0;
WARNWHITE=0;
WARNRED=0;
MAST=0;
SOCKEL=0;
BODEM=0;
BUZZERKAP=0;

B=30;
H=40;

module LEDkap(x,y,z) {
  translate([x,y,z]) {
    difference() {
      translate([0,0,0]) cylinder(20, 6.1, 6.1);
      translate([0,0,0]) cylinder(20, 5.1, 5.1);
      translate([-10,-16,10]) rotate([-45,0,0]) cube([20,12,30]);
    }
  }
}

if( BODEM ) {
  translate([120,80,0]) {
    difference() {
      translate([0,0,0]) cylinder(20, 32, 32);
      translate([0,0,2]) cylinder(18, 28, 28);
      translate([0,0,18]) cylinder(2, 30, 30);
      translate([20,0,6]) rotate([0,90,0]) cylinder(20, 2, 2);
    }
  }
}

if( BUZZERKAP ) {
  translate([80,80,0]) {
      translate([0,0,1.2]) cylinder(4, 6, 0);
    difference() {
      translate([0,0,0]) cylinder(6, 7, 7);
      translate([0,0,1.2]) cylinder(7.2, 6, 6);
      translate([-10,0,6]) rotate([0,90,0]) cylinder(20, 3.5, 3.5);
      translate([0,10,6]) rotate([90,90,0]) cylinder(20, 3.5, 3.5);
    }
  }
}

if( SOCKEL ) {
  translate([40,80,0]) {
    difference() {
      translate([0,18,1]) cylinder(4, 6, 6);
      translate([0,18,1]) cylinder(4, 5, 5);
    }
    difference() {
      translate([0,0,0]) cylinder(2, 30, 30);
      translate([0,0,0]) cylinder(2, 3, 3);
      translate([0,18,0]) cylinder(4, 5, 5);
      translate([0,18,0]) cylinder(1, 6, 6);
    }
    difference() {
      translate([0,0,0]) cylinder(14, 10, 6);
      translate([0,0,0]) cylinder(14, 4.25, 4.25);
    }
    difference() {
      translate([0,0,2]) cylinder(2, 32, 31);
      translate([0,0,0]) cylinder(4, 3, 3);
      translate([0,18,0]) cylinder(4, 3, 3);
    }
  }
}


if( MAST ) {
  translate([0,80,0]) {
    
    translate([0,5,4]) rotate([90,0,0]) {
      // kruizen bevestiging
      difference() {
        translate([10,0,-4]) cylinder(18, 4, 4);
        translate([10,0,-4]) cylinder(18, 1, 1);
      }
      // bovenste plaat bevestiging
      difference() {
        translate([40,0,0]) cylinder(10, 4, 4);
        translate([40,0,0]) cylinder(10, 1, 1);
      }
      // onderste plaat bevestiging links
      difference() {
        translate([55,0,0]) cylinder(3, 4, 4);
        translate([55,0,0]) cylinder(10, 1, 1);
      }
      // onderste plaat bevestiging rechts
      difference() {
        translate([55,0,7]) cylinder(3, 4, 4);
        translate([55,0,0]) cylinder(10, 1, 1);
      }
      // draaipunt voor de slagboom links
      difference() {
        translate([80,0,0]) cylinder(3, 4, 4);
        translate([80,0,0]) cylinder(10, 1, 1);
      }
      // draaipunt voor de slagboom rechts
      difference() {
        translate([80,0,7]) cylinder(3, 4, 4);
        translate([80,0,0]) cylinder(10, 1, 1);
      }
    }    
    translate([0,0,4]) rotate([0,90,0]) {
      difference() {
        translate([0,0,0]) cylinder(140, 4, 4);
        translate([0,0,0]) cylinder(140, 2.5, 2.5);
        rotate([90,0,0]) translate([0,140,-5]) cylinder(10, 2, 2);
        rotate([90,0,0]) translate([0,47.5,-5]) cylinder(10, 2, 2);
      }
      translate([0,0,0]) sphere(4);
    } 
  }
}

if( LEDPLAAT_TOP ) {
  translate([0,0,0]) {
    translate([0,0,0]) difference() {
      translate([0,0,0]) roundedcube(30, 40, 2, 5);
      translate([B/2,H/4*1.25,0]) cylinder(2, 5.1, 5.1);
      translate([B/2,H/4*2.75,0]) cylinder(2, 5.1, 5.1);

      translate([B/2,H/4*1.25,0]) cylinder(1, 6.1+.1, 6.1+.1);
      translate([B/2,H/4*2.75,0]) cylinder(1, 6.1+.1, 6.1+.1);
    }
    LEDkap(B/2,H/4*1.25,1);
    LEDkap(B/2,H/4*2.75,1);
  }
}

if( LEDPLAAT_BOTTOM ) {
  translate([30,0,0]) {
    difference() {
      translate([B/2-6.1,H/4*1.25,0]) cube([12.2, H/4*1.5, 2 ]);
      translate([B/2,H/4*1.25,0]) cylinder(2, 5.1, 5.1);
      translate([B/2,H/4*2.75,0]) cylinder(2, 5.1, 5.1);
    }
    difference() {
      translate([B/2,H/4*1.25,0]) cylinder(7, 6.1, 6.1);
      translate([B/2,H/4*1.25,0]) cylinder(7, 5.1, 5.1);
    }
    difference() {
      translate([B/2,H/4*2.75,0]) cylinder(7, 6.1, 6.1);
      translate([B/2,H/4*2.75,0]) cylinder(7, 5.1, 5.1);
    }
  }
}




if( LEDPLAAT_NORMAAL ) {
  translate([0,0,0]) {
    difference() {
      translate([0,0,0]) roundedcube(30, 40, 2, 5);
      translate([B/2,H/4*1.25,0]) cylinder(2, 5.1, 5.1);
      translate([B/2,H/4*2.75,0]) cylinder(2, 5.1, 5.1);

      translate([B/2,H/4*1.25,0]) cylinder(1.4, 6.1, 6.1);
      translate([B/2,H/4*2.75,0]) cylinder(1.4, 6.1, 6.1);
    }
    difference() {
      translate([B/2,H/4*1.25,1.4]) cylinder(6-.4, 6.1, 6.1);
      translate([B/2,H/4*1.25,1.4]) cylinder(6-.4, 5.1, 5.1);
    }
    difference() {
      translate([B/2,H/4*2.75,1.4]) cylinder(6-.4, 6.1, 6.1);
      translate([B/2,H/4*2.75,1.4]) cylinder(6-.4, 5.1, 5.1);
    }
  }
}

if( LEDHOUDER ) {
  HH=10;
  translate([50,0,0]) {
    
    difference() {
      translate([B/2-7.2,H/4*1.25,0]) cube([7.2*2, H/4*1.5, HH]);
      translate([B/2-7.2+1,H/4*1.25,2]) cube([7.2*2-2, H/4*1.5, HH-2]);
      // Kabel opening
      translate([B/2,H/2,0]) cylinder(2, 2, 2);

      // Bevestigings gaten
      translate([B/2,H/4*1.25,0]) cylinder(2, 1, 1);
      translate([B/2,H/4*2.75,0]) cylinder(2, 1, 1);
    }
    
    difference() {
      translate([B/2,H/4*1.25,0]) cylinder(HH, 7.2, 7.2);
      translate([B/2,H/4*1.25,2]) cylinder(HH-2, 6.2, 6.2);
      // Bevestigings gat
      translate([B/2,H/4*1.25,0]) cylinder(2, 1, 1);
      translate([B/2-7.2,H/4*1.25,0]) cube([7.2*2, H/4*1.5, HH]);
    }
    difference() {
      translate([B/2,H/4*2.75,0]) cylinder(HH, 7.2, 7.2);
      translate([B/2,H/4*2.75,2]) cylinder(HH-2, 6.2, 6.2);
      // Bevestigings gat
      translate([B/2,H/4*2.75,0]) cylinder(2, 1, 1);
      translate([B/2-7.2,H/4*1.25,0]) cube([7.2*2, H/4*1.5, HH]);
    }
  }
}

if( LEDKAP ) {
  translate([50,20,0]) {
    difference() {
      translate([0,0,0]) cylinder(20, 5.9, 5.9);
      translate([0,0,0]) cylinder(20, 5.1, 5.1);
      translate([-10,-16,10]) rotate([-45,0,0]) cube([20,12,30]);
    }
  }
}

if( RAND ) {
  translate([0,50,0]) {
    difference() {
      translate([0,0,0]) roundedcube(30, 40, .6, 5);
      translate([1.5,1.5,0]) roundedcube(30-3, 40-3, .6, 4);
    }
  }
}


SHORTLED=1;
if( LENS ) {
  translate([-20,20,0]) {
    difference() {
      translate([0,0,0]) cylinder(10, 4.95, 4.95);
      translate([0,0,2]) cylinder(8, 2.55, 2.55);
      if( SHORTLED )
        translate([0,0,6]) cylinder(4, 3.5, 3.5);
    }
  }
}

if( WARNWHITE ) {
  translate([50,50,0]) {
    rotate([0,0,20]) {
      difference() {
        translate([0,0,0]) cube([60, 6, 1.4]);
        translate([60/2,6/2,0]) cylinder(2, 1, 1);
      }
      difference() {
        translate([15,0,0]) cube([30, 6, 2]);
        translate([60/2,6/2,0]) cylinder(2, 1, 1);
      }
      translate([0,6/2,0]) cylinder(1.4, 6/2, 6/2);
      translate([60,6/2,0]) cylinder(1.4, 6/2, 6/2);
    }
  }
}

if( WARNWHITE ) {
  translate([47.9,70.2,0]) {
    rotate([0,0,-20]) {
      difference() {
        translate([0,0,0]) cube([60, 6, 1.4]);
        translate([60/2,6/2,0]) cylinder(2, 1, 1);
      }
      difference() {
        translate([15,0,0]) cube([30, 6, 2]);
        translate([60/2,6/2,0]) cylinder(2, 1, 1);
      }
      translate([0,6/2,0]) cylinder(1.4, 6/2, 6/2);
      translate([60,6/2,0]) cylinder(1.4, 6/2, 6/2);
    }
  }
}

if( WARNRED ) {
  translate([50,30,0]) {
    difference() {
      translate([0,0,0]) cube([60, 6, .6]);
      translate([15,0,0]) cube([30, 6, .6]);
    }
    translate([0,6/2,0]) cylinder(.6, 6/2, 6/2);
    translate([60,6/2,0]) cylinder(.6, 6/2, 6/2);
  }
}

