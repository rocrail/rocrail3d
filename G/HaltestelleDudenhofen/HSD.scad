/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
//$fn=96;

include <../../lib/shapes.scad>

SCALE=2.6;

LENGTH=1000;
WIDTH=225+115+225;
HEIGHT=355;
BASEHEIGHT=12*4;
WINDOWWIDTH=115;
WINDOWHEIGHT=120;

EASTWALL=true;
EASTBASE=false;
WINDOW=false;
WINDOW3D=false;

if( WINDOW3D ) {
  translate([WINDOWWIDTH/SCALE+10,(((HEIGHT+WINDOWHEIGHT)/SCALE)),0]) {

    difference() {
      translate([0,0,0]) cube([(WINDOWWIDTH/SCALE)+4, (WINDOWHEIGHT/SCALE)-(6/SCALE)+4, 8]);
      translate([2,2,0]) cube([(WINDOWWIDTH/SCALE), (WINDOWHEIGHT/SCALE)-(6/SCALE), 8]);
    }

  }
}


if( WINDOW ) {
  translate([0,(((HEIGHT+WINDOWHEIGHT)/SCALE)),0]) {
    difference() {
      translate([0,0,0]) cube([(WINDOWWIDTH/SCALE), (WINDOWHEIGHT/SCALE)-(6/SCALE), 2]);
      translate([6/SCALE,6/SCALE,0]) cube([(WINDOWWIDTH/SCALE)-(12/SCALE), (WINDOWHEIGHT/SCALE)-(6/SCALE)-(12/SCALE), 2]);
    }
    // center beam
    translate([(WINDOWWIDTH/2-3)/SCALE,0,0]) cube([6/SCALE, (WINDOWHEIGHT-6)/SCALE, 2]);

    translate([(WINDOWWIDTH/4)/SCALE,0,0]) cube([3/SCALE, (WINDOWHEIGHT-6)/SCALE, 1]);
    translate([(WINDOWWIDTH/2+WINDOWWIDTH/4-3)/SCALE,0,0]) cube([3/SCALE, (WINDOWHEIGHT-6)/SCALE, 1]);

    translate([0,((WINDOWHEIGHT-12)/3+3)/SCALE,0]) cube([WINDOWWIDTH/SCALE, 3/SCALE, 1]);
    
    translate([0,(((WINDOWHEIGHT-12)/3)*2+3)/SCALE,0]) cube([WINDOWWIDTH/SCALE, 3/SCALE, 1]);
  
  }
}

if( EASTBASE ) {
  translate([0,-((BASEHEIGHT/SCALE)+10),0]) {
    difference() {
      translate([0,0,0]) cube([(WIDTH/SCALE)+4, BASEHEIGHT/SCALE, 2]);
      for( a =[0:2] ) {
        translate([0,a*(24/SCALE),1.8]) cube([(WIDTH/SCALE)+4, .6, .2]);
        
        for( b =[0:12] ) {
          x = ((a%2)*25)/SCALE;
          translate([x + b*(50/SCALE),a*(24/SCALE),1.8]) cube([.6, 24/SCALE, .2]);
        }

      }
      
    translate([2,0,0]){
      rotate(90) prism(BASEHEIGHT/SCALE, 10, 10);
    }
    translate([(WIDTH/SCALE)+2,BASEHEIGHT/SCALE,0]){
      rotate(270) prism(BASEHEIGHT/SCALE, 10, 10);
    }
      
    }
    
       
  }
}
  
  
if( EASTWALL ) {
  translate([0,0,0]) {
    difference() {
      translate([0,0,0]) cube([WIDTH/SCALE, HEIGHT/SCALE, 2]);
      translate([225/SCALE,120/SCALE,0]) cube([WINDOWWIDTH/SCALE, WINDOWHEIGHT/SCALE, 2]);
      
      for( a =[4:30] ) {
        translate([0,a*(12/SCALE),1.8]) cube([WIDTH/SCALE, .6, .2]);

        for( b =[0:22] ) {
          x = ((a%2)*12.5)/SCALE;
          translate([x + b*(25/SCALE),a*(12/SCALE),1.8]) cube([.6, 12/SCALE, .2]);
        }
      }
      
      for( c =[0:15] ) {
        if( c % 2 != 0 )
          translate([0,c*(24/SCALE),0]) cube([2, (24/SCALE), 2]);
        else
          translate([(WIDTH/SCALE)-2,c*(24/SCALE),0]) cube([2, (24/SCALE), 2]);
      }
      
      
    }
    
    translate([(225-25)/SCALE,(120+WINDOWHEIGHT)/SCALE,0]) cube([(WINDOWWIDTH+50)/SCALE, 24/SCALE, 3]);
    translate([(225-10)/SCALE,(WINDOWWIDTH)/SCALE,0]) cube([(WINDOWHEIGHT+20)/SCALE, 6/SCALE, 8]);
    
  }
}

