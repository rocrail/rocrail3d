include <../../lib/shapes.scad>


// Wall
difference() {
  translate([0,0,0]) cube([80, 40, 2]);
  // Window cut
  translate([9.8,9.8,0]) cube([16.4, 20.4, 2]);
  // Door cut
  translate([49.8,0.8,0]) cube([16.4, 29.4, 2]);
  // left 45°
  translate([2,0,2]){
    rotate(90) mirror([0,0,1]) prism(40, 2, 2);
  }
  // Right 45°
  translate([78,40,2]){
    rotate(270) mirror([0,0,1]) prism(40, 2, 2);
  }
}
// Door step
//translate([48,0,0]) cube([20, 1, 5]);


// Window
X1=0;
Y1=50;
difference() {
  translate([X1-1,Y1-1,0]) cube([18, 22, 1]);

  translate([X1+2,Y1+4,0]) cube([5.5, 6.5, 1]);
  translate([X1+8.5,Y1+4,0]) cube([5.5, 6.5, 1]);

  translate([X1+2,Y1+11.5,0]) cube([5.5, 6.5, 1]);
  translate([X1+8.5,Y1+11.5,0]) cube([5.5, 6.5, 1]);
}
difference() {
  translate([X1+0,Y1+0,1]) cube([16, 20, 1]);

  translate([X1+2,Y1+4,1]) cube([5.5, 6.5, 1]);
  translate([X1+8.5,Y1+4,1]) cube([5.5, 6.5, 1]);

  translate([X1+2,Y1+11.5,1]) cube([5.5, 6.5, 1]);
  translate([X1+8.5,Y1+11.5,1]) cube([5.5, 6.5, 1]);
}
// 
translate([X1+0,Y1+0,2]) cube([16, 2, 3]);

// Door
X2=30;
Y2=50;
translate([X2,Y2,0]) {
  difference() {
    translate([-1,-1,0]) cube([18, 31, 1]);
  }
  difference() {
    translate([0,0,1]) cube([16, 29, 0.5]);
    translate([2,0,1.3]) cube([0.2, 30, 0.2]);
    translate([4,0,1.3]) cube([0.2, 30, 0.2]);
    translate([6,0,1.3]) cube([0.2, 30, 0.2]);
    translate([8,0,1.3]) cube([0.2, 30, 0.2]);
    translate([10,0,1.3]) cube([0.2, 30, 0.2]);
    translate([12,0,1.3]) cube([0.2, 30, 0.2]);
  }
  translate([0,0,1]) cube([16, 4, 1]);
  translate([0,4.2,1]) cube([3, 20.6, 1]);
  translate([0,25,1]) cube([16, 4, 1]);
  translate([13,4.2,1]) cube([3, 20.6, 1]);
  translate([13,4.2,1]) rotate(33.7) cube([3, 22.5, 1]);

}



