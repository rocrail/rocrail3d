/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=96;

include <../../lib/shapes.scad>

HIGHT=9;
HOH=175-45;

SILICON=false;

if( SILICON ) {
  difference() {
    translate([0,0,0]) cube([80, 3, HIGHT]);
    translate([(80-48.6)/2,0,0]) cylinder(HIGHT, 1.0, 1.0);
    translate([80/2,0,0]) cylinder(HIGHT, 1.0, 1.0);
    translate([(80-48.6)/2+48,0,0]) cylinder(HIGHT, 1.0, 1.0);
  }

  translate([(80-48.6)/2-4,1,0]) difference() {
    translate([0,0,0]) cube([8, 8, HIGHT]);
    translate([3.1,2,0]) cube([1.8, 6, HIGHT]);
    translate([(8-3.7)/2,2,0]) cube([3.7, 2.8, HIGHT]);
  }

  translate([(80-48.6)/2-4+48,1,0]) difference() {
    translate([0,0,0]) cube([8, 8, HIGHT]);
    translate([3.1,2,0]) cube([1.8, 6, HIGHT]);
    translate([(8-3.7)/2,2,0]) cube([3.7, 2.8, HIGHT]);
  }
}


// Measure meter between 2 parallel tracks
if( !SILICON ) {
  translate([0,20,0]) {
    translate([0,0,0]) cube([HOH+8, 3, HIGHT]);

    translate([0,1,0]) difference() {
      translate([0,0,0]) cube([8, 8, HIGHT]);
      translate([(8-4)/2,2,0]) cube([4, 8, HIGHT]);
    }
  
    translate([HOH,1,0]) difference() {
      translate([0,0,0]) cube([8, 8, HIGHT]);
      translate([(8-4)/2,2,0]) cube([4, 8, HIGHT]);
    }
  }
}