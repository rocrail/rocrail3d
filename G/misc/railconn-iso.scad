  /*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

include <../../lib/shapes.scad>
//include <BOLTS.scad>

difference() {
  translate([0,0,-2]) cube([19, 15, 8]);
  translate([0,4,3]) cube([19, 11, 3]);
  translate([0,2,3]) cube([19, 2, 1]);
  translate([0,3.5,5]) cube([19, 2, 1]);
  translate([0,2,3+1]) mirror([0,0,0]) rotate(0) prism(19, 3, 3);
  translate([19,1,6]) mirror([0,0,1]) rotate(180) prism(19, 3, 3);

  translate([4.5,11,-2]) cylinder(5, 1.5, 1.5);
  translate([14.5,11,-2]) cylinder(5, 1.5, 1.5);

  translate([4.5-2.8,11-3,-1.2]) cube([5.6, 14, 2.4]);
  translate([14.5-2.8,11-3,-1.2]) cube([5.6, 14, 2.4]);
  //translate([4.5,11,1]) rotate(30) ISO4014("M3", 10);
}

translate([9,6,3]) cube([1, 9, 5]);
difference() {
  translate([9,1,3]) cube([1, 7, 8]);
  translate([19,4,11]) mirror([0,0,1]) rotate(180) prism(19, 5, 7);
}

//translate([0,12.5,3]) cube([19, 1.5, 1.8]);

//translate([9,4,8]) cube([1, 3, 4]);


translate([30,0,0]) {
 //linear_extrude(1.6) polygon(points=[[0,0],[5,0],[5,10],[0,10]]);  
/*  
  translate([0,0,-.5]) rotate(0) cube([3.5, 5.5, 1.0]);
  translate([-1.3,2.7,0]) rotate(-60) cube([3.5, 5.5, 2.0]);
  translate([0,5.5,0]) rotate(-120) cube([3.5, 5.5, 3.0]);
  */
}