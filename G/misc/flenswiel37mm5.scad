/*
  Flenswiel 37.5 mm
  Peter Giling
*/

//$fn=360;

flens       = 43.5/2;
wiel        = 37.5/2;
asbushoogte = 7;
as          = 5.4/2; // PETG tolerantie + 0,4
asbus       = 10/2;
d           = 6.6; // wiel hoogte
f           = 1.5+.8; // flens hoogte

REM=1;

module asgat() {
  translate ([0, 0, 0]) cylinder (12, as, as);
}

// basis flens van 1.5 mm dik
difference() {
  translate ([0, 0, 0]) cylinder (1.5, flens, flens);  
  asgat();
}

// flensovergang
difference() {
  translate ([0, 0, 1.5]) cylinder (.8, flens, wiel);  
  asgat();
}

// wiel met uitholling
difference() {
  // het wiel zelf
  translate ([0, 0, f]) cylinder (d-f, wiel, wiel-.1); 
  // de uitholling
  translate ([0, 0, d-2]) cylinder (2, wiel-5, wiel-3);
  asgat();
}

// asbus
difference() {
  // asbus velenging
  translate ([0, 0, 0]) cylinder (asbushoogte, asbus, asbus);
  asgat();
}

// asbus opvulstuk
translate([40,0,0]) difference() {
  translate ([0, 0, 0]) cylinder (2, 16/2, 16/2);
  asgat();
}


if( REM ) translate([0,-50,0]) {
  difference() {
    translate([0,0,0]) cylinder(d-f,wiel+2,wiel+2);
    translate([4,0,0]) cylinder(d-f,wiel,wiel);
    translate([-12,-wiel-3,0]) cube([(wiel+3)*2,(wiel+3)*2,d-f+2]);
  }
  
}


