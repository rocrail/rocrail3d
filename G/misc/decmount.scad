/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

include <../../lib/shapes.scad>

/*
Creditcard format RFID tags shoould be cut to 62mm x 45mm.
Use a flashlight to mark the spool and chip before cutting.

Select only tags which respond at 60mm reader distance.
Used reader: ID12LA, 5V power supply.
*/

CHASSISMOUNT=false;
DECMOUNT=false;
LEDCAP=false;
LEDHOLDER=false;
LEDPLUG=true;

D=0.8;
L=28+2*D;
W=12+2*D;
H=4.5;

if( DECMOUNT ) {
  translate([0,0,0]) {
    difference() {
      translate([0,0,0]) cube([L, W, H]);
      translate([0+D,0+D,0+D]) cube([L-2*D, W-2*D, H-D]);
      translate([0,0+D,.6+D]) cube([D, W-2*D, H-D]);
      translate([L-11,W/2,0]) cylinder(2, 1.2, 2.4);
      translate([11,W/2,0]) cylinder(2, 1.2, 2.4);
    }
    translate([0,0,H-D]) cube([L, 2*D, D]);
    translate([0,W-2*D,H-D]) cube([L, 2*D, D]);

    if( CHASSISMOUNT ) {
      difference() {
        translate([L-11,-2,0]) cylinder(1.6, 3.5, 3.5);
        translate([L-11,-2,0]) cylinder(1.6, 1.2, 1.2);
        translate([0,0,0]) cube([L, 2, 2]);
      }
      difference() {
        translate([L-11-3.5,-2,0]) cube([7, 2, 1.6]);
        translate([L-11,-2,0]) cylinder(1.6, 1.2, 1.2);
      }
    }
  }
}

if( LEDCAP ) {
  translate([0,30,0]) {
    difference() {
      translate([0,0,0]) cylinder(2, 3.5, 3.5);
      translate([0,0,1]) cylinder(2, 3.0, 3.0);
      translate([2.5/2,0,0]) cylinder(2, .8, .8);
      translate([-(2.5/2),0,0]) cylinder(2, .8, .8);
    }
  }
}


if( LEDPLUG ) {
  translate([10,30,0]) {
    difference() {
      translate([0,0,0]) cylinder(4, 2.5, 2.5);
      translate([2.5/2,0,0]) cylinder(4, .8, .8);
      translate([-(2.5/2),0,0]) cylinder(4, .8, .8);
    }
    translate([0,0,4]) {
    difference() {
      translate([0,0,0]) cylinder(2.5, 3.8, 3.8);
      translate([0,0,1]) cylinder(2.5, 3.10, 3.10);
      translate([2.5/2,0,0]) cylinder(2, .8, .8);
      translate([-(2.5/2),0,0]) cylinder(2, .8, .8);
    }
    }
  }
}


LEDBULB=true;
LEDOFFSET=LEDBULB?3:0;

if( LEDHOLDER ) {
  translate([20,30,0]) {
    difference() {
      translate([0,0,0+LEDOFFSET]) cylinder(6+(LEDBULB?-2:0), 3.7, 3.7);
      translate([0,0,1+LEDOFFSET]) cylinder(5, 3.1, 3.1);
      translate([0,0,0]) cylinder(1, 1.6, 1.6);
      translate([2,-1.5,3+(LEDBULB?1:0)]) cube([2, 3, 2]);
    }
    if( LEDBULB ) { 
      translate([0,0,0]) cylinder(3, 2.25, 2.25);
    }
  }
}


