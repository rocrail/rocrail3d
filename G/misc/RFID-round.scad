/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=96;

include <../../lib/shapes.scad>

TOP=true;
BOTTOM=true;

L=43;
W=32;
H=8.5;

if( TOP ) {
translate([0,0,0]) {
  difference() {
    translate([0,0,0]) cube([L, W, H]);
    translate([2,2,1.5]) cube([L-4, W-4, H-1.5]);

    for( b =[0:16] ) {
      translate([2,2+b*2,0]) cube([L-4, .4, .2]);
    }
  
    translate([0,W-2,0]){
      rotate(0) prism(L, 10, 10);
    }
    translate([L,2,0]){
      rotate(180) prism(L, 10, 10);
    }
    translate([2,0,0]){
      rotate(90) prism(W, 10, 10);
    }
    translate([L-2,W,0]){
      rotate(270) prism(W, 10, 10);
    }
  }

  difference() {
    translate([-1,W/2-4,2]) cube([3, 8, H+9.5]);
    translate([2,0,0]){
      rotate(90) prism(W, 10, 10);
    }
  }
  translate([-1,W/2-4,H+9.5]) cube([4, 8, 2]);

  difference() {
    translate([L-2,W/2-4,2]) cube([3, 8, H+9.5]);
    translate([L-2,W,0]){
      rotate(270) prism(W, 10, 10);
    }
  }
  translate([L-3,W/2-4,H+9.5]) cube([4, 8, 2]);
  
  difference() {
    translate([8.5,W/2,1]) cylinder(H-1, 2.25, 1.75);
    translate([8.5,W/2,1]) cylinder(H-1, 0.75, 0.75);
  }
}
}


if( BOTTOM ) {
translate([60,0,1]) {
  difference() {
    translate([(L-20)/2,0,-1]) cube([20, W, 2]);
    translate([L/2,W/2,-1]) cylinder(2, .75, .75);
  }
  difference() {
    translate([0,2,-1]) cube([L, 28, 2]);
    translate([L/2,W/2,-1]) cylinder(2, .75, .75);
    translate([0,W/2-4,-1]) cube([2.4, 8, 14]);
    translate([L-2.4,W/2-4,-1]) cube([2.4, 8, 14]);
    translate([8.5,W/2,-1]) cylinder(2, 2.5, 1);
  }
  /*
  difference() {
    translate([-10,36/2-4,0]) cube([60, 8, 1]);
    translate([20,36/2,0]) cylinder(2, .5, .5);
    translate([-6,36/2,0]) cylinder(2, 1.0, 1.0);
    translate([40+6,36/2,0]) cylinder(2, 1.0, 1.0);
  }
  */
  difference() {
    translate([2,2,0]) cube([L-4, W-4, 2.5]);
    translate([3,3,1]) cube([L-6, W-6, 2.5]);
    translate([L/2,W/2,0]) cylinder(2.5, .75, 2.5);
    translate([0,W/2-4,0]) cube([2.4, 8, 14]);
    translate([L-2.4,W/2-4,0]) cube([2.4, 8, 14]);
    translate([8.5,W/2,0]) cylinder(1, 2.5, 1);
  }
}
}

