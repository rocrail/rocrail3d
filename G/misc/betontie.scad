/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../../lib/shapes.scad>

EDGED=true;


// Tie
translate([0,0,0]) {
  difference() {
    translate([0,0,0]) cube([86, 10, 7]);
    translate([2,2,0]) cube([82, 6, 5]);
    
    if( EDGED ) {
      // bottom
      translate([86,1,7]){
        rotate(180) mirror([0,0,1]) prism(86, 10, 10);
      }
      // top
      translate([0,9,7]){
        rotate(0) mirror([0,0,1]) prism(86, 10, 10);
      }
      // left
      translate([1,0,7]){
        rotate(90) mirror([0,0,1]) prism(10, 10, 10);
      }
      // right
      translate([85,10,7]){
        rotate(270) mirror([0,0,1]) prism(10, 10, 10);
      }
    }
        
  }
    
  
  translate([(86-(48.8+19))/2,(10-7)/2,7]) Iron();
  translate([(86-(48.8+19))/2+48.8,(10-7)/2,7]) Iron();
}


module Iron() {
  difference() {
    translate([-1,0,0]) cube([21, 7, 1]);
  }
  translate([.5,1.5,1]) cylinder(.4,.8,.8);
  translate([.5,7-1.5,1]) cylinder(.4,.8,.8);

  translate([21-2.5,1.5,1]) cylinder(.4,.8,.8);
  translate([21-2.5,7-1.5,1]) cylinder(.4,.8,.8);



  translate([(19-13)/2,(7-4)/2,1]) {
    difference() {
      translate([-1,0,0]) cube([15, 4, 3.4]);
      translate([(13-2)/2,0,0]) cube([2, 4, 3.4]);
      translate([(13-7)/2,0,0]) cube([7, 4, 2]);
    }
    translate([1,2,3.4]) cylinder(.8,1,1);
    translate([15-3,2,3.4]) cylinder(.8,1,1);
  }

}
