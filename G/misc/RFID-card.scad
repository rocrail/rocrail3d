/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=96;

include <../../lib/shapes.scad>

/*
Creditcard format RFID tags should be cut to 62mm x 45mm.
Use a flashlight to mark the spool and chip before cutting.

Select only tags which respond at 60mm reader distance.
Used reader: ID12LA, 5V power supply.
*/

L=68;
W=40;
H=10.0;

BOTTOM=true;

translate([0,0,0]) {
  difference() {
    translate([0,0,0]) cube([L, W, H]);
    translate([2,2,2]) cube([L-4, W-4, H-3]);
    translate([2,0,3]) cube([L-4, W, H-3]);

    for( b =[1:16] ) {
      translate([3,2.85+b*2,0]) cube([L-6, .4, .2]);
    }
  
    translate([0,W-3,0]){
      rotate(0) prism(L, 10, 10);
    }
    translate([L,3,0]){
      rotate(180) prism(L, 10, 10);
    }
    translate([2,0,0]){
      rotate(90) prism(W, 10, 10);
    }
    translate([L-2,W,0]){
      rotate(270) prism(W, 10, 10);
    }
  }
  
  translate([0,-0.5,H-2]) cube([2, W+1, 2]);
  translate([L-2,-0.5,H-2]) cube([2, W+1, 2]);

  difference() {
    translate([-3,W/2-5,2]) cube([3, 10, H+5.5]);
    translate([2,0,0]){
      rotate(90) prism(W, 10, 10);
    }
  }
  translate([-3,W/2-5,H+7.5]) cube([5, 10, 2]);

  difference() {
    translate([L,W/2-5,2]) cube([3, 10, H+5.5]);
    translate([L-2,W,0]){
      rotate(270) prism(W, 10, 10);
    }
  }
  translate([L-2,W/2-5,H+7.5]) cube([5, 10, 2]);
  
  if( BOTTOM ) {
    translate([0,W/2-4,6]) cube([L, 8, 2]);
  }
  translate([2,W/2-4,2]) cube([L-4, 8, 1]);

}
