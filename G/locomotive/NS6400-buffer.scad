/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
//$fn=256;
$fn=128;
//$fn=64;
include <../../lib/shapes.scad>

W=110+4; // breedte over de U-Profielen + 2 x 2mm.

POFFSET=10; // profiel offset

HEAD=1;
BUFFER=0;
FRONTPLATE=0;

if( FRONTPLATE ) {
  translate([W+20,0,0]) {
    // front plaat
    difference() {
      translate([1.5,-1.5,0]) cube([W-3, 16, .8]);
      // buffer openingen
      translate([W/2+68/2,7,0]) cylinder(2, 4.5, 4.5);
      translate([W/2-68/2,7,0]) cylinder(2, 4.5, 4.5);
      // haak opening
      translate([W/2,7,0]) cylinder(2, 1.6, 1.6);
      // hydro openingen
      translate([W/2-36/2,7,0]) cylinder(2, 1.1, 1.1);
      translate([W/2+36/2,7,0]) cylinder(2, 1.1, 1.1);
      translate([W/2-26/2,7,0]) cylinder(2, 1.1, 1.1);
      translate([W/2+26/2,7,0]) cylinder(2, 1.1, 1.1);
    }
    
    // haak bevestiging
    difference() {
      translate([W/2-6,7-5,.8]) rcube(1.2, 12, 10, 1);
      translate([W/2,7,0]) cylinder(2, 1.6, 1.6);
    }    
    
    // hydro bevestigingen
    difference() {
      translate([W/2-36/2,7,.8]) cylinder(1, 2.1, 2.1);
      translate([W/2-36/2,7,0]) cylinder(2, 1.1, 1.1);
    }
    difference() {
      translate([W/2-26/2,7,.8]) cylinder(1, 2.1, 2.1);
      translate([W/2-26/2,7,0]) cylinder(2, 1.1, 1.1);
    }
    difference() {
      translate([W/2+36/2,7,.8]) cylinder(1, 2.1, 2.1);
      translate([W/2+36/2,7,0]) cylinder(2, 1.1, 1.1);
    }
    difference() {
      translate([W/2+26/2,7,.8]) cylinder(1, 2.1, 2.1);
      translate([W/2+26/2,7,0]) cylinder(2, 1.1, 1.1);
    }
    
    // buffer bevestiging links
    difference() {
      translate([W/2-68/2-20/2,0,.8]) rcube(1, 20, 14, 1);
      translate([W/2-68/2,7,0]) cylinder(4, 4.5, 4.5);
    }
    // buffer moeren links
    translate([W/2-68/2-8,7-5,0]) cylinder(2.6, 1, 1);
    translate([W/2-68/2+8,7-5,0]) cylinder(2.6, 1, 1);
    translate([W/2-68/2-8,7+5,0]) cylinder(2.6, 1, 1);
    translate([W/2-68/2+8,7+5,0]) cylinder(2.6, 1, 1);
    
    // buffer bevestiging rechts
    difference() {
      translate([W/2+68/2-20/2,0,.8]) rcube(1, 20, 14, 1);
      translate([W/2+68/2,7,0]) cylinder(4, 4.5, 4.5);
    }
    // buffer moeren rechts
    translate([W/2+68/2-8,7-5,0]) cylinder(2.6, 1, 1);
    translate([W/2+68/2+8,7-5,0]) cylinder(2.6, 1, 1);
    translate([W/2+68/2-8,7+5,0]) cylinder(2.6, 1, 1);
    translate([W/2+68/2+8,7+5,0]) cylinder(2.6, 1, 1);

  }
}

if( BUFFER ) {
  translate([-30,0,0]) {
    // Plate
    translate([20/2-22/2,14/2-17/2,0]) rcube(2, 22, 15, 2);
    translate([20/2,14/2,2]) cylinder(2, 7, 7);
    translate([20/2,11.5,0]) ellipse(2,11,4);
    // Spring cylinder
    difference() {
      translate([20/2,14/2,2]) cylinder(14, 5, 5);
      translate([20/2,14/2,4]) cylinder(12, 4.1, 4.1);
    }
  }
}


if( HEAD ) {
  translate([0,0,0]) {

    // Buffer basis rechts
    difference() {
      translate([W/2+68/2,20+10+26,18]) rotate([90,0,0]) cylinder(10, 4.0, 4.0);
      translate([W/2+68/2,20+10+26,18]) rotate([90,0,0]) cylinder(10, 2.5, 2.5);
    }
    //translate([W/2+68/2,20+2+26,18]) rotate([90,0,0]) cylinder(2, 5.0, 5.0);
    
    // Buffer basis links
    difference() {
      translate([W/2-68/2,20+10+26,18]) rotate([90,0,0]) cylinder(10, 4.0, 4.0);
      translate([W/2-68/2,20+10+26,18]) rotate([90,0,0]) cylinder(10, 2.5, 2.5);
    }
    //translate([W/2-68/2,20+2+26,18]) rotate([90,0,0]) cylinder(2, 5.0, 5.0);

    // Profiel bevestiging links
    translate([POFFSET,0,0]) difference() {
      translate([0,-36,2]) cube([12, 50, 10]);
      translate([2,-36,2]) cube([12-4, 48, 10-2]);
      translate([2,-2,2]) cube([12-4, 48, 10]);
      translate([2,-36,2]) cube([12-4, 8, 10]);
      // screw holes left
      translate([6,-10,2]) cylinder(10, 1.6, 1.6);
      translate([6,-20,2]) cylinder(10, 1.6, 1.6);
      // schuine kan naar profiel hoogte
      translate([0,-28,12]) rotate([180,0,0]) prism(W,8,2);
      translate([2,-36,2]) cube([W-4,8,10]);
    }

    // Profiel bevestiging rechts
    translate([-POFFSET,0,0]) difference() {
      translate([W-10-2,-36,2]) cube([12, 50, 10]);
      translate([W-10-2+2,-36,2]) cube([12-4, 48, 10-2]);
      translate([W-10,-2,2]) cube([12-4, 48, 10]);
      translate([W-10,-36,2]) cube([12-4, 8, 10]);
      // screw holes right
      translate([W-6,-10,2]) cylinder(10, 1.6, 1.6);
      translate([W-6,-20,2]) cylinder(10, 1.6, 1.6);
      // schuine kan naar profiel hoogte
      translate([0,-28,12]) rotate([180,0,0]) prism(W,8,2);
      translate([2,-36,2]) cube([W-4,8,10]);
    }
    
    // front
    difference() {
      translate([0,42,0]) cube([W, 4, 40]);
      // coupler cut
      translate([W/2-50/2,42,32]) cube([50,4,10]);
      // step cut
      translate([0,42,0]) cube([18,4,8]);
      translate([W-18,42,0]) cube([18,4,8]);
      // railing
      translate([3,44,-10]) cylinder(20, 1.1, 1.1);
      translate([W-3,44,-10]) cylinder(20, 1.1, 1.1);
    }

    // step left
    difference() {
      translate([0,12,0]) cube([20, 32, 40]);
      translate([0,12+2,0]) cube([20-2, 32-4, 40-2]);
      translate([0,12+2,0]) cube([2, 32-4, 40]);
      // front cut
      translate([0,42,0]) cube([18,4,8]);
      // railing
      translate([3,8,16]) rotate([0,90,90]) cylinder(20, 1.1, 1.1);
      translate([3,44,-10]) cylinder(20, 1.1, 1.1);
    }
    // steps
    translate([6,12,9]) cube([12, 32, 2]);
    translate([6,12,24]) cube([12, 32, 2]);
    translate([8,12,26]) cube([2, 32, 14]);

    // step right
    difference() {
      translate([W-20,12,0]) cube([20, 32, 40]);
      translate([W-20+2,12+2,0]) cube([20-2, 32-4, 40-2]);
      translate([W-2,12+2,0]) cube([2, 32-4, 40]);
      // front cut
      translate([W-18,42,0]) cube([18,4,8]);
      // railing
      translate([W-3,8,16]) rotate([0,90,90]) cylinder(20, 1.1, 1.1);
      translate([W-3,44,-10]) cylinder(20, 1.1, 1.1);
    }
    // steps
    translate([W-20,12,9]) cube([12, 32, 2]);
    translate([W-20,12,24]) cube([12, 32, 2]);
    translate([W-12,12,26]) cube([2, 32, 14]);
    
    // top
    difference() {
      translate([-2,-36,0]) cube([W+4, 60+22+6, 2]);
      translate([24-2,-40,0]) cube([W+4-2*24, 64-24, 2]);
      // trap opening
      translate([-2,14,0]) cube([18, 32+6, 4]);
      translate([W-20+4,14,0]) cube([18, 32+6, 4]);
      // railing
      translate([1,8,-10]) cylinder(60, 1.6, 1.6);
      translate([W-1,8,-10]) cylinder(60, 1.6, 1.6);
      translate([24,48,-10]) cylinder(60, 1.6, 1.6);
      translate([W-24,48,-10]) cylinder(60, 1.6, 1.6);
    }

    // oversteek zijkanten
    translate([18,46,0]) cube([2, 6, 10]);
    translate([W-18-2,46,0]) cube([2, 6, 10]);
    // opvulling tussen oversteek en trap
    translate([18,46+6,10]) rotate([180,90,0]) prism(2,6,18);
    translate([W,46,10]) rotate([0,90,90]) prism(2,18,6);


  }

  // schuine kanten richting trap
  translate([W-16,46,45]) rotate([90,180,90]) prism(4,5,16);
  translate([16,46-4,45]) rotate([90,180,270]) prism(4,5,16);
  difference() {
    translate([16,42,40]) cube([W-2*16, 4, 5]);
    translate([32,42,40]) cube([W-2*32, 4, 5]);
  }
}

