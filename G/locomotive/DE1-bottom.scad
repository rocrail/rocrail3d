/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../../lib/shapes.scad>


L=180;
W=20;
BW=90;
BBW=118; // overall width + 2 x 2mm wall
BBR=116; // head radius

PROFILE=0;
BOTTEM=0;
RFID=0;
BATTERY=0;
HEAD=0;
HEADBOTTTOM=0;
TECHBOX=1;
TECHPOWER=1;

if( TECHBOX ) {
  translate([220,160,0]) {
    
    if( TECHPOWER ) {
      translate([0,14,0]) {
        difference() {
          translate([8,-10,0]) cube([46, 12, 20]);
          translate([10,-10,0]) cube([42, 10, 18]);
      translate([0,-14,25]) rotate([270,0,0]) prism(L, 25, 15);

          // flip switch
          rotate([90,0,0]) translate([18,4+6,-2]) cylinder(2, 3.1, 3.1);

          // XT60 chassis
          translate([29.8,0,6]) cube([12.2, 2, 8.2]);
          rotate([90,0,0]) translate([42.2,4+6,-2]) cylinder(2, 4.1, 4.1);
        }
      }
    }

  
    
    difference() {
      translate([0,0,0]) cube([L, BBW, 35]);
      // cube uitholling
      translate([0,2,30]) cube([L, BBW-4, 5]);
      translate([2,2,25]) cube([L-4, BBW-4, 5]);
      translate([2,2+12.5,2]) cube([L-4, BBW-4-25, 35]);
      
      translate([0,0,25]) rotate([270,0,0]) prism(L, 25, 15);
      translate([0,BBW-15,0]) rotate([0,0,0]) prism(L, 15, 25);

      translate([2,15+1,2]) rotate([90,0,0]) prism(L-4, 25-(6*.25), 15-(6*.15));
      translate([L-2,BBW-(15+1),2]) rotate([270,180,0]) prism(L-4, 25-(6*.25), 15-(6*.15));

      // power
      if( TECHPOWER )
        translate([8,5,2]) cube([46, 12, 20]);

      // kopsekant schroefgaten
      translate([0,BBW/4,15]) rotate([0,90,0]) cylinder(2, 1.75, 1.75);
      translate([0,BBW-BBW/4,15]) rotate([0,90,0]) cylinder(2, 1.75, 1.75);
      
      // kabel sparing
      translate([0,BBW/2,15]) rotate([0,90,0]) cylinder(2, 5, 5);

      // zijkant schroefgaten
      for( b =[0:4] ) {
        translate([15+b*((L-30)/4),BBW,35-2.5]) rotate([90,90,0]) cylinder(120, 1.2, 1.2);
      }

    }

    // ID12LA socket
    translate([L-45,BBW/2-28.2/2,2]) {
      difference() {
        translate([(40-28.2)/2, (31.5-27.2)/2,0]) cube([28.2, 27.2, 5]);
        translate([(40-28.2)/2+1, (31.5-27.2)/2+1,0]) cube([26.2, 25.2, 5]);
        
      translate([33,18,2]) cube([2, 5, 5]);
      }
      
    // RFID print bevestiging  
    translate([-1,0,0]) {
      difference() {
        translate([3,(31.5-20)/2,0]) cylinder(10, 2.5, 2.5);
        translate([3,(31.5-20)/2,2]) cylinder(12, 1, 1);
      }  
      translate([2.5,3.5,0])  cube([5, 3, 5]);
      
      difference() {
        translate([3,(31.5-20)/2+20,0]) cylinder(10, 2.5, 2.5);
        translate([3,(31.5-20)/2+20,2]) cylinder(8, 1, 1);
      }  
      translate([2.5,25,0])  cube([5, 3, 5]);

      difference() {
        translate([3+34,(31.5-20)/2,0]) cylinder(10, 2.5, 2.5);
        translate([3+34,(31.5-20)/2,2]) cylinder(8, 1, 1);
      }  
      //translate([1.5+34,4,0])  cube([3, 20, 6]);

      difference() {
        translate([3+34,(31.5-20)/2+20,0]) cylinder(10, 2.5, 2.5);
        translate([3+34,(31.5-20)/2+20,2]) cylinder(12, 1, 1);
      }  
    }
      
      
    }
    
  }
}


if( HEADBOTTTOM ) {
  translate([220,160,0]) {
    
    difference() {
      translate([0,0,0]) ellipse(6.2, BBR/2, 82);
      translate([0,0,0]) ellipse(5, BBR/2-2, 82-2);
      translate([0,0,0]) ellipse(6.2, BBR/2-3, 82-3);
      
      translate([-BBR/2,0,0]) cube([BBW, 82, 12]);
      translate([-BBR/2,-7.5,5]) cube([BBW, 82, 12]);
      
      translate([-60,-10,2.5]) rotate([0,90,0]) cylinder(120, 1.2, 1.2);
      translate([0,-10,2.5]) rotate([90,90,0]) cylinder(120, 1.2, 1.2);
    }    
      
    difference() {
      for(i = [0:70]) {
        translate([0, 0, 5+ i*.4])
          difference() {
            ellipse(.4, (BBR/2) - (sin(i/2) * i/2), 82 - (sin(i)/2 * i/2));
            ellipse(.4, (BBR/2-2) - (sin(i/2) * i/2), 80 - (sin(i)/2 * i/2));
          }
        }
      translate([-BBR/2,0,0]) cube([BBR, 82, 5+33]);
      //translate([60,-25,34]) rotate([0,180,0]) prism(120, 26, 26);
        
      translate([-8,-120,10]) cube([7, 100, 8]);
      translate([0,-120,10]) cube([7, 100, 8]);
        
      translate([-60,5,35]) rotate([0,90,0]) cylinder(120, 30, 30);
    }
    
  }
}


if( HEAD ) {
  translate([220,160,0]) {
    
    translate([-(114/2-8),-11,0]) cube([4, 11, 10]);
    translate([(114/2-12),-11,0]) cube([4, 11, 10]);
    
    difference() {
      translate([0,10,0]) cylinder(10, 114/2-5.5, 114/2-5.5);
      translate([0,10,0]) cylinder(10, 114/2-5.5-2, 114/2-5.5-2);
      translate([-114/2,0,0]) cube([114, 80, 12]);
    }   
    
    difference() {
      translate([0,0,0]) ellipse(10, 114/2, 80);
      translate([0,0,2]) ellipse(10, 114/2-2, 80-2);
      translate([0,10,0]) cylinder(2, 114/2-5.5, 114/2-5.5);
      translate([-114/2,0,0]) cube([114, 80, 15]);
    }    
    
    difference() {
      translate([-114/2,0,0]) cube([114, 30, 10]);
      translate([-114/2+2,2,2]) cube([8, 28, 10]);
      translate([114/2-8-2,2,2]) cube([8, 28, 10]);

      translate([-114/2+2+8+2,0,2]) cube([114-4-4-16, 30, 10]);
      translate([-114/2+2+8+2,0,0]) cube([114-4-4-16, 30, 10]);

      translate([114/2-2-4,8,0]) cylinder(20, 1.6, 1.6);
      translate([114/2-2-4,24,0]) cylinder(20, 1.6, 1.6);

      translate([-114/2+2+4,8,0]) cylinder(20, 1.6, 1.6);
      translate([-114/2+2+4,24,0]) cylinder(20, 1.6, 1.6);

    }


  }
}


if( BATTERY ) {
  translate([220,60,0]) {
    difference() {
      translate([0,0,0]) cube([78, 72, 20]);
      translate([2,2,2]) cube([74, 68, 18]);
      // save material
      translate([78/2-20,72/2-20,0]) cube([40, 40, 2]);
      // tierap holes
      translate([78/2,74,14]) rotate([90,0,0]) cylinder(80, 3, 3);
      translate([0,72/2,14]) rotate([0,90,0]) cylinder(80, 3, 3);
      
      // mount holes
      translate([0+10,0+10,0]) cylinder(20, 1.6, 1.6);
      translate([78-10,0+10,0]) cylinder(20, 1.6, 1.6);
      translate([0+10,72-10,0]) cylinder(20, 1.6, 1.6);
      translate([78-10,72-10,0]) cylinder(20, 1.6, 1.6);
    }

  }
}


if( BOTTEM ) {
  translate([0,2*W,0]) {
    difference() {
      translate([0,0,0]) cube([L, BW, 2]);
      //translate([L/3*1.75,BW/3,0]) cube([L/3, BW/3, 2]);

      // bevestigings gaten profiel HoH 84
      for( b =[0:4] ) {
        translate([15+b*((L-30)/4),(BW-84)/2,0]) cylinder(2, 1.6, 1.6);
        translate([15+b*((L-30)/4),BW-(BW-84)/2,0]) cylinder(2, 1.6, 1.6);
      }
      
      // RFID/Power kabel doorvoer
      translate([40,BW/2,0]) cylinder(12, 9, 9);
      
    }
    
    // Zijdelingse versteviging
    translate([0,10,0]) cube([L, 2, 8]);
    translate([0,BW-10-2,0]) cube([L, 2, 8]);
    
    // Kopse versteviging
    difference() {
      translate([0,10,0]) cube([2, BW-20, 8]);
    }
    difference() {
      translate([L-2,10,0]) cube([2, BW-20, 8]);
    }
    difference() {
      translate([L/2-2,10,0]) cube([2, BW-20, 8]);
    }
    
  }
}


if( PROFILE ) {
  translate([0,0,0]) {
    
    // Zijkant
    difference() {
      translate([0,0,0]) cube([L, W, 2]);
      
      for( b =[0:4] ) {
        translate([15+b*((L-30)/4),W-3,0]) cylinder(2, 1.1, 1.1);
        translate([15+b*((L-30)/4),2,0]) cylinder(2, 1.1, 1.1);
      }
    }

    // Bodem
    difference() {
      translate([0,4,0]) cube([L, 2, 18]);

      for( b =[0:4] ) {
        translate([15+b*((L-30)/4),W-3,6]) rotate([90,0,0]) cylinder(20, 1.6, 1.6);
        translate([15+b*((L-30)/4),W-3,15]) rotate([90,0,0]) cylinder(20, 1.6, 1.6);
      }
    }
    
    // H-Profiel binnen zijde
    translate([0,4,10]) cube([L, 3, 2]);
    
  }
}


// RFID mount
if( RFID ) {
  translate([220,0,0]) {
   
   translate([0,7,35]) { 
     translate([0,0,0]) rotate([180,0,0]) prism(10, 5, 5);
     difference() {
       translate([0,-5,0]) cube([10, 5, 5]);
       translate([5,-3,0]) cylinder(8, 1, 1);
     }
   }
   translate([-2,29.5,35]) { 
     translate([42,-5,0]) rotate([0,180,0]) prism(10, 5, 5);
     difference() {
       translate([32,-5,0]) cube([10, 5, 5]);
       translate([32+5,-2,0]) cylinder(8, 1, 1);
     }
   }
    
    
    // low front
    translate([-2,0,0]) cube([2, 31.5, 5]);
    
    // box
    difference() {
      translate([0,0,0]) cube([42, 31.5, 40]);
      translate([0,2,2]) cube([40, 27.5, 38]);
      translate([3,(31.5-20)/2,0]) cylinder(12, 1, 1);
      translate([3+34,(31.5-20)/2+20,0]) cylinder(12, 1, 1);
    }
    
    difference() {
      translate([3,(31.5-20)/2,2]) cylinder(10, 2.5, 2.5);
      translate([3,(31.5-20)/2,0]) cylinder(12, 1, 1);
    }  
    translate([3-1.5,0,2])  cube([3, 4, 10]);
    
    difference() {
      translate([3,(31.5-20)/2+20,2]) cylinder(10, 2.5, 2.5);
      translate([3,(31.5-20)/2+20,4]) cylinder(8, 1, 1);
    }  
    translate([3-1.5,31.5-4,2])  cube([3, 4, 10]);

    difference() {
      translate([3+34,(31.5-20)/2,2]) cylinder(10, 2.5, 2.5);
      translate([3+34,(31.5-20)/2,4]) cylinder(8, 1, 1);
    }  
    translate([3-1.5+34,0,2])  cube([3, 4, 10]);

    difference() {
      translate([3+34,(31.5-20)/2+20,2]) cylinder(10, 2.5, 2.5);
      translate([3+34,(31.5-20)/2+20,0]) cylinder(12, 1, 1);
    }  
    translate([3-1.5+34,31.5-4,2])  cube([3, 4, 10]);
    
    // ID12LA socket
    translate([1.0,0,0]) {
      difference() {
        translate([(40-28.2)/2, (31.5-27.2)/2,2]) cube([28.2, 27.2, 5]);
        translate([(40-28.2)/2+1, (31.5-27.2)/2+1,2]) cube([26.2, 25.2, 5]);
        
      translate([33,18,4]) cube([2, 5, 5]);
      }
    }
  }
}




