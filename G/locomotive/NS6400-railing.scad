/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;
//$fn=64;
include <../../lib/shapes.scad>

SIDEBACK=0;
SIDEFRONT=0;
FRONT=1;
MIRROR=1;
CLAXON=0;

module vstang(r,h,rr,c) {
  translate([0,0,r]) rotate([0,90,90]) cylinder(h,r,r);
  translate([0,-2.5,rr]) rotate([0,90,90]) cylinder(2.5,rr,rr);
  translate([-r,0,c]) cube([r*2,1.4,r*3]);
}

module trapstang(r,h) {
  l=14;
  translate([-8,-13.9,r]) rotate([0,90,90]) cylinder(h+l,r,r);
  translate([4-8-2.1,-13.5,r]) rotate([90,0,0]) tube(r, 3);
  translate([4-9,-16.5,1]) rotate([0,90,0]) cylinder(2.5,1,1);
}

module sideBack() {
  r=1.2;
  h=36;
  w=88;
  // rondingen
  translate([4-8,h+.7,r]) rotate([90,0,270]) tube(r, 4);
  translate([0,0,0]) trapstang(r,h);
  translate([88-4,h+.7,r]) rotate([270,0,270]) tube(r, 4);

  // links/rechts verticale stang
  translate([0,0,0]) vstang(r,h+4,1.5,0);
  translate([w,0,0]) vstang(r,h,1.3,0);

  // boven horizontale stang
  translate([4-8,h+4-.5,r]) rotate([0,90,0]) cylinder(w+8-2*4,r,r);
  
  // midden horizontale stang
  translate([0,h/2,r]) rotate([0,90,0]) cylinder(88,r,r);

  // dummy midden verticale stang
  translate([w/2,0,r]) rotate([0,90,90]) cylinder(h+4,r,r);
  translate([w/2-r,0,0]) cube([r*2,1.4,r*3]);

}

if( SIDEBACK ) {
  mirror([0,0,MIRROR?1:0]) sideBack();
}

module sideFront() {
  r=1.2;
  h=36;
  w=224;
  // rondingen
  translate([4-8,h+.7,r]) rotate([90,0,270]) tube(r, 4);
  translate([0,0,0]) trapstang(r,h);
  translate([w-4,h+.7,r]) rotate([270,0,270]) tube(r, 4);

  // links/rechts verticale stang
  translate([0,0,0]) vstang(r,h+4,1.5,0);
  translate([w,0,0]) vstang(r,h,1.3,0);

  // boven horizontale stang
  translate([4-8,h+4-.5,r]) rotate([0,90,0]) cylinder(w+8-2*4,r,r);

  // midden horizontale stang
  translate([0,h/2,r]) rotate([0,90,0]) cylinder(w,r,r);

  translate([78,0,0]) vstang(r,h+4,1.3,0);
  translate([78+48,0,0]) vstang(r,h+4,1.3,0);
  translate([78+48+48,0,0]) vstang(r,h+4,1.3,0);


}

if( SIDEFRONT ) {
  mirror([0,0,MIRROR?1:0]) sideFront();
}

if( FRONT ) {
  r=1.2;
  h=18;
  w=66;
  ww=108;
  // links/rechts verticale stang
  translate([-w/2,0,0]) vstang(r,h,1.4,-r);
  translate([w/2,0,0]) vstang(r,h,1.4,-r);

  translate([-ww/2,-8,4]) vstang(r,h+8,1,-r);
  translate([ww/2,-8,4]) vstang(r,h+8,1,-r);

  translate([-ww/2,h,5.2]) sphere(r);
  translate([ww/2,h,5.2]) sphere(r);
  

  // midden horizontale stang
  translate([-w/2,h,r]) rotate([0,90,0]) cylinder(w,r,r);

  // dwars stangen zijkant
  translate([w/2,h,r]) rotate([0,79,0]) cylinder(21,r,r);
  translate([-w/2,h,r]) rotate([0,281,0]) cylinder(21,r,r);
  
  
  // naar voren hellende stangen
  translate([-w/2,h,r]) rotate([0,100,90]) cylinder(h,r,r);
  translate([w/2,h,r]) rotate([0,100,90]) cylinder(h,r,r);

  // midden stang
  translate([-w/2,h*1.5,-.4]) rotate([0,90,0]) cylinder(w,r,r);
  // boven stang
  translate([-w/2-5,h*2,-2]) rotate([0,90,0]) cylinder(w+10,r,r);

  // schuine zijkanten
  translate([ww/2,h,4+r]) rotate([0,106,132]) cylinder(26,r,r);
  translate([-ww/2,h,4+r]) rotate([0,106,47]) cylinder(26,r,r);
}


module claxon(h) {
  translate([0,0,0]) cylinder(2,3,3);
  translate([0,0,2]) cylinder(3,3,1.4);
  difference() {
    translate([0,0,5]) cylinder(h,1.4,3);
    translate([0,0,7]) cylinder(h-2,0,2.4);
  }
  
  translate([-2,0,.6]) cube([4,6,1.4]);
  translate([-3,5,0]) cube([6,1.2,6]);
}

if( CLAXON ) {
  translate([0,0,0]) claxon(12);
  translate([6,0,0]) claxon(8);
}