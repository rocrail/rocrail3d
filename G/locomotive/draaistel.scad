/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
//$fn=64;
include <../../lib/shapes.scad>
include <../lib/modules.scad>

W=70;
//D=120; // HoH 80
D=160; // HoH 120
//D=170; // HoH 130
//D=130; // HoH 90
//D=110; // NS300 Motorsteun lengte

H=32.6;

//HoH=80.5; // 200mm 14mm tandwiel, 
HoH=120.5;  // (280 - 39) / 2 = 120.5 (DE1/NS300)
//HoH=130.5;  // (300 - 39) / 2 = 130.5 (NS1200)
//HoH=90.5;  // (220 - 39) / 2 = 90.5 (NS2400/NS6400)

//MSW=40;   // motorsteun breedte
MSW=42;   // motorsteunshort breedte
ASR=5.6;  // as straal
MSO=5;    // motorsteun offset

POFFSET=10; // profiel offset: 0=2400 10=6400 !!!

SIK=1; // NS300

/* 
158mm riem tussen motor en as.
Motor pully: 30/6
As pully: 20/5
Motor type: Modelcraft 1:30 (RB350030-0A101R)
*/
DRAAISTEL=1;
MSHORT=1; // Draaistel voor short motorsteun
ASSEN3=0; // NS1200
MOTORSTEUN=0; // deprecated
MOTORSTEUNSHORT=0;
DRAAIPUNTOPENING=0;
DRAAIPUNT=0;
DRAAIPUNTRING=0;
DRAAIPUNTMIDDEN=0;
CHASISOPHANGING=0; // deprecated

SIER=0; // DE1 en met ASSEN3 voor de NS1200

SIER2400=0;
SIER2400L=0; // linkse uitvoering
SIER6400=0;

ASBUIS=0; // Midden as NS1200

/*
module AxGear(x,y) {
  translate([x,y,0]) {
    difference() {
      translate([-12,-7,0]) cube([24, 2, 8]);
      //translate([-13,-7,5]) cube([26, 1, 1]);
      //translate([-13,-7,0]) cube([1, 1, 6]);
      //translate([12,-7,0]) cube([1, 1, 6]);
    }
    
    translate([-9,9,4]) rotate([90, 0, 0]) cylinder(10, 2.5, 2.5);
    difference() {
      translate([-9,3,4]) rotate([90, 0, 0]) cylinder(10, 3, 3);
      translate([-13,-7,5]) cube([26, 1, 1]);
    }

    translate([9,9,4]) rotate([90, 0, 0]) cylinder(10, 2.5, 2.5);
    difference() {
      translate([9,3,4]) rotate([90, 0, 0]) cylinder(10, 3, 3);
      translate([-13,-7,5]) cube([26, 1, 1]);
    }


    difference() {
      translate([0,0,2]) cylinder(8, 5, 5);
      translate([0,0,2]) cylinder(8, 2.7, 2.7);
    }
    difference() {
      translate([0,0,8]) sphere(r = 5);
      translate([0,0,2]) cylinder(8, 2.7, 2.7);
    }
    difference() {
      translate([-5,-5,0]) cube([10, 12, 7]);
      translate([-4,5,0]) cube([8, 2, 7]);
      translate([0,0,0]) cylinder(8, 2.7, 2.7);
    }
  }
}
*/

module AxGear2(x,y) {
  translate([x,y,0]) {
    translate([-9,9,4]) rotate([90, 0, 0]) cylinder(10, 2.75, 2.75);
    translate([9,9,4]) rotate([90, 0, 0]) cylinder(10, 2.75, 2.75);

    difference() {
      translate([-13,3,0]) rotate([90,0,0]) rcube(5, 26, 8,3);
      translate([0,0,0]) cylinder(8, 2.7, 2.7);
    }

    difference() {
      translate([0,0,0]) cylinder(8, 6.5, 6.5);
      translate([0,0,0]) cylinder(8, 2.7, 2.7);
    }
    difference() {
      translate([0,0,8]) cylinder(1, 5, 4);
    }
  }
}

module ucube(l,w,h) {
  difference() {
    cube([l, w, h]);
    translate([0,w/4,(h/3)*2]) cube([l, w/2, h/3]);
  }
}


if( SIER6400 ) {
  translate([(W+60),0,0]) {

    AxGear2(0,0);
    AxGear2(HoH,0);

    difference() {
      translate([0,0,0]) cylinder(.8, 9, 9);
      translate([0,0,0]) cylinder(8, 2.7, 2.7);
      translate([-20,0,0]) cube([D,9,2]);
    }
    difference() {
      translate([HoH,0,0]) cylinder(.8, 9, 9);
      translate([HoH,0,0]) cylinder(8, 2.7, 2.7);
      translate([-20,0,0]) cube([D,9,2]);
    }

    // As veren
    translate([-9,10,4]) rotate([90,0,0]) bogieSpring(10, 3, 4);
    translate([9,10,4]) rotate([90,0,0]) bogieSpring(10, 3, 4);
    translate([HoH-9,10,4]) rotate([90,0,0]) bogieSpring(10, 3, 4);
    translate([HoH+9,10,4]) rotate([90,0,0]) bogieSpring(10, 3, 4);


    // as verbinder plaat met uitrondingen
    difference() {
      translate([HoH/2-40/2,-2,2]) cube([40,10,4]);
      translate([HoH/2,-2,0]) cylinder(14, 9, 9);
      translate([HoH/2-40/2,-2,0]) cylinder(14, 9, 9);
      translate([HoH/2+40/2,-2,0]) cylinder(14, 9, 9);
    }
    // as verbinders
    translate([12,1,4]) rotate([90,0,90]) cylinder(23, 1.5, 1.5);
    translate([HoH/2+11,1,4]) rotate([90,0,90]) cylinder(23, 1.5, 1.5);

    translate([13,1,3.3]) cylinder(1.5, 2, 2);
    translate([35,1,3.3]) cylinder(1.5, 3, 3);
    translate([55.5,1,3.3]) cylinder(1.5, 3, 3);
    translate([77.5,1,3.3]) cylinder(1.5, 2, 2);

    // Draaipunt vering
    translate([HoH/2-8,20,10+6]) rotate([270,0,0]) cylinder(13, 3.5, 3.5);
    translate([HoH/2+8,20,10+6]) rotate([270,0,0]) cylinder(13, 3.5, 3.5);
    
    translate([HoH/2-8,29+3,10+6]) rotate([90,0,0]) bogieSpring(10, 4, 6);
    translate([HoH/2+8,29+3,10+6]) rotate([90,0,0]) bogieSpring(10, 4, 6);
    
    // vering top plaat
    difference() {
      translate([HoH/2-30/2,28+2,12]) cube([30,3,10]);
      translate([HoH/2+40/2-5,28+2,16+2]) rotate([270,0,90]) prism(3,6,6);
      translate([HoH/2-40/2+5,28+2,16+2]) mirror([1,0,0]) rotate([270,0,90]) prism(3,6,6);
    }

    // vering dwarsbalk
      translate([HoH/2-5/2,22+3,12]) cube([5,6,12]);

    // vering plaat
    translate([HoH/2-40/2,17,8]) cube([40,3,8+5]);
    
    // zijkanten vering plaat
    translate([HoH/2-40/2,17,16+5]) rotate([270,0,90]) prism(3,8+5,8);
    translate([HoH/2+40/2+8,17,8]) rotate([0,0,90]) prism(3,8,8+5);
    // versteviging onder vering plaat
    translate([HoH/2-40/2+2,9,6]) rotate([0,0,0]) prism(3,8,12);
    translate([HoH/2-3/2,9,6]) rotate([0,0,0]) prism(3,8,12);
    translate([HoH/2+40/2-3-2,9,6]) rotate([0,0,0]) prism(3,8,12);

    // hoofd balk
    translate([-((D-HoH)/2),8+12,0]) cube([D,5,2]);
    difference() {
      translate([-((D-HoH)/2),8,0]) ucube(D,12,8);
      // schroefgat links
      translate([0,14,2]) cylinder(14, 2, 2);
      translate([0,14,0]) cylinder(16, 1, 1);
      // schroefgat rechts
      translate([HoH,14,2]) cylinder(14, 2, 2);
      translate([HoH,14,0]) cylinder(16, 1, 1);
    }
    
    // zandhouder links
    translate([-19.75,8,4]) cube([16,12,4]);
    translate([-19.75,-1,8]) cube([2,10,2]);
    translate([-11,18,14]) rotate([115,0,0]) cylinder(2,2,2);
    difference() {
      translate([-19.75,8,8]) cube([18,12,8]);
      translate([-19.75,20,8]) rotate([90,0,0]) prism(20,8,4);
      translate([-19.75,0,10]) rotate([270,180,270]) prism(20,8,4);
    }
    difference() {
      translate([-19.75,0,16]) rotate([0,90,0]) prism(8,8,18);
      translate([115,-4,7]) rotate([90,0,180]) prism(D+10,13,18);
      translate([-19.75,0,10]) rotate([270,180,270]) prism(20,8,4);
    }   


    // zandhouder rechts
    translate([HoH+19.75-16,8,4]) cube([16,12,4]);
    translate([HoH+19.75-2,-1,8]) cube([2,10,2]);
    translate([HoH+11,18,14]) rotate([115,0,0]) cylinder(2,2,2);
    difference() {
      translate([HoH+1.75,8,8]) cube([18,12,8]);
      translate([HoH+1.75,20,8]) rotate([90,0,0]) prism(20,8,4);
      translate([HoH+19.75,20,10]) rotate([270,180,90]) prism(20,8,4);
    }
    difference() {
      translate([HoH+19.75,0,8]) rotate([0,270,0]) prism(8,8,18);
      translate([115,-4,7]) rotate([90,0,180]) prism(D+10,13,18);
      translate([HoH+19.75,20,10]) rotate([270,180,90]) prism(20,8,4);
    }   

  }
}


if( SIER2400 ) {
  translate([(W+60),0,0]) {

    AxGear(0,0);
    AxGear(HoH,0);

    if( SIER2400L )
      translate([-((D-HoH)/2),24-5,0]) cube([D-4,6,2]);
    else
      translate([-((D-HoH)/2)+4,24-5,0]) cube([D-4,6,2]);
    
    difference() {
      translate([-((D-8-HoH)/2),-5,0]) cube([D-8,24,8]);
      
      // Links
      translate([10,-2.5,0]) cylinder(16, 6, 6);
      translate([10,-8.5,0]) cube([6, 6, 16]);
      translate([-((D-HoH)/2),-6.6,0]) cube([30, 10, 16]);
      translate([-((D-HoH)/2)-6,-1.3,0]) rotate([0, 0, -10]) cube([35, 11, 16]);

      translate([0,14,2]) cylinder(14, 2, 2);
      translate([0,14,0]) cylinder(16, 1, 1);

      // Rechts
      translate([HoH-10,-2.5,0]) cylinder(16, 6, 6);
      translate([HoH-10-6,-8.5,0]) cube([6, 6, 16]);
      translate([HoH-10,-6.6,0]) cube([30, 10, 16]);
      translate([HoH-10,-7.6,0]) rotate([0, 0, 10]) cube([35, 11, 16]);

      translate([HoH,14,2]) cylinder(14, 2, 2);
      translate([HoH,14,0]) cylinder(16, 1, 1);
    }

    // Boven rand
    translate([-((D-8-HoH)/2),17,0]) cube([D-8, 2, 9]);
    
    // Onder rand
    translate([18,-5,0]) cube([HoH-40, 2, 9]);
    
    // Links rand
    translate([-15.75,6,0]) cube([2, 12, 9]);
    // Rechts rand
    translate([D-15.75-8-2,6,0]) cube([2, 12, 9]);

    
    // Links
    translate([-18,5,0]) cube([12, 4, 9]);
    translate([12,-7,0]) cube([10, 4, 9]);
    // Rechts
    translate([HoH+6,5,0]) cube([12, 4, 9]);
    translate([HoH-22,-7,0]) cube([10, 4, 9]);
    
    translate([10,-9,8]) rotate([0, 90, 0]) prism(8, 2, 2);
    translate([-12,-7,8]) rotate([90, 90, 0]) prism(8, 2, 2);
    translate([-10,-9,0]) cube([20, 2, 8]);

    translate([HoH+10,-9,8]) rotate([0, 90, 0]) prism(8, 2, 2);
    translate([HoH-12,-7,8]) rotate([90, 90, 0]) prism(8, 2, 2);
    translate([HoH-10,-9,0]) cube([20, 2, 8]);
  }
}


if( SIER ) {
  translate([(W+60),0,0]) {

    if( ASSEN3 ) {
      // links vering
      translate([HoH/4.0,14,6]) rotate([90, 0, 0]) cylinder(19, 3.5, 3.5);
      translate([HoH/4.0-10,14,6]) rotate([90, 0, 0]) cylinder(19, 3.5, 3.5);
      translate([HoH/4.0+10,14,6]) rotate([90, 0, 0]) cylinder(19, 3.5, 3.5);


      translate([HoH/4-15,14,0]) cube([30,4,12]);
      translate([HoH/4-16,15,0]) cube([32,2,13]);

      translate([HoH/4-20.7,-7,0]) cube([41.6,2,10]);


      // rechts vering
      translate([HoH/2+HoH/4.0,14,6]) rotate([90, 0, 0]) cylinder(19, 3.5, 3.5);
      translate([HoH/2+HoH/4.0-10,14,6]) rotate([90, 0, 0]) cylinder(19, 3.5, 3.5);
      translate([HoH/2+HoH/4.0+10,14,6]) rotate([90, 0, 0]) cylinder(19, 3.5, 3.5);

      translate([HoH/2+HoH/4-15,14,0]) cube([30,4,12]);
      translate([HoH/2+HoH/4-16,15,0]) cube([32,2,13]);

      translate([HoH/2+HoH/4-20.7,-7,0]) cube([41.6,2,10]);
    }
    else {
      // midden stuk met vering
      translate([HoH/2,14,6]) rotate([90, 0, 0]) cylinder(18, 3.5, 3.5);
      translate([HoH/2-10,14,6]) rotate([90, 0, 0]) cylinder(18, 3.5, 3.5);
      translate([HoH/2+10,14,6]) rotate([90, 0, 0]) cylinder(18, 3.5, 3.5);

      difference() {
        translate([HoH/2-24,-4,0]) cube([48,12,10]);
        translate([HoH/2-24+3,-4+3,1]) cube([48-6,12-3,10]);
        translate([HoH/2-24,-4+3,8]) cube([48,12-3,10]);
      }
      translate([HoH/2-43,-4,0]) cube([87,2,8]);

      translate([HoH/2-16,14,0]) cube([32,4,12]);
      translate([HoH/2-17,15,0]) cube([34,2,13]);
    }
    
    
    
    difference() {
      translate([-((D-HoH)/2),7,0]) cube([D,18,2]);
      translate([0,7+9,0]) cylinder(20, 1.4, 1.4);
      translate([HoH,7+9,0]) cylinder(20, 1.4, 1.4);
      if( ASSEN3 )
        translate([HoH/2,7+9,0]) cylinder(20, 1.4, 1.4);
    }
    
    difference() {
      translate([0,7+9,0]) cylinder(6, 3, 3);
      translate([0,7+9,0]) cylinder(6, 2, 2);
    }
    difference() {
     translate([HoH,7+9,0]) cylinder(6, 3, 3);
     translate([HoH,7+9,0]) cylinder(6, 2, 2);
    }
    if( ASSEN3 ) {
      difference() {
       translate([HoH/2,7+9,0]) cylinder(6, 3, 3);
       translate([HoH/2,7+9,0]) cylinder(6, 2, 2);
      }
    }

    
    translate([-((D-HoH)/2),7,0]) cube([6,18,8]);
    translate([HoH+((D-HoH)/2)-6,7,0]) cube([6,18,8]);

    if( ASSEN3 ) {
      translate([(HoH/2.35),12,2]) rotate(90) ucube(8,6,6);
      translate([HoH-(HoH/2.35)+6,12,2]) rotate(90) ucube(8,6,6);
    }
    else {
      translate([(HoH/3),12,2]) rotate(90) ucube(8,6,6);
      translate([HoH-(HoH/3)+6,12,2]) rotate(90) ucube(8,6,6);
    }

    translate([(HoH/8),12,2]) rotate(90) ucube(8,6,6);
    translate([HoH-(HoH/8)+6,12,2]) rotate(90) ucube(8,6,6);

    translate([0,0,0]) {
      AxGear(0,0);
      AxGear(HoH,0);
      translate([-((D-HoH)/2),7,2]) ucube(D,6,6);
      translate([-((D-HoH)/2),19,2]) ucube(D,6,6);

      translate([-20,7,0]) rotate(300) cube([16, 2, 8]);
      translate([D-27.5,-7,0]) rotate(60) cube([16, 2, 8]);


      if( ASSEN3 ) {
        AxGear(HoH/2,0);
      }
      else {
        translate([12,-7,0]) rotate(30) cube([28, 2, 8]);
        translate([D-75.5,7,0]) rotate(330) cube([28, 2, 8]);
      }


    }

    //translate([-((D-HoH)/2)+6,6,0]) rotate(90) ucube(12, 6, 8);
    //translate([55,17,0]) mirror([0,1,0]) rotate(6) ucube(30, 3, 6);

  }
}


if( CHASISOPHANGING ) {
  translate([-(W+60),0,0]) {
    difference() {
      translate([MSHORT?1:0,40/2-24/2,0]) cube([16, 24, 3]);
      translate([MSHORT?1:0,40/2-24/2+2-.2,0]) cube([20, 20.4, 2]);
      translate([5,20,0]) cylinder(4, 1.6, 1.6);
    }
    difference() {
      translate([5,20,0]) cylinder(3, 2.5, 2.5);
      translate([5,20,0]) cylinder(3, 1.6, 1.6);
    }
    if( MSHORT ) {
      difference() {
        translate([1,-10,0]) cube([10, 60, 10]);
        translate([-1,-10,3]) cube([10, 60, 7]);
        translate([5,20,0]) cylinder(4, 1.6, 1.6);
        translate([0,40/2-24/2+2-.1,0]) cube([20, 20.2, 2]);
      }
    }
    else {
      difference() {
        translate([-1,-10,0]) cube([12, 60, 10]);
        translate([1,-10,3]) cube([8, 60, 7]);
        translate([5,20,0]) cylinder(4, 1.6, 1.6);
        translate([0,40/2-24/2+2-.1,0]) cube([20, 20.2, 2]);
      }
    }
  }
}

ASBUISLEN=35.3;
ASBUISDIA=5.2;
if( ASBUIS ) {
  translate([-(W+60),0,0]) {
    difference() {
      translate([0,0,0]) cylinder(ASBUISLEN/2, 10/2, 10/2);
      translate([0,0,0]) cylinder(ASBUISLEN/2, ASBUISDIA/2, ASBUISDIA/2);
    }
    difference() {
      translate([0,0,0]) cylinder(2, 12/2, 12/2);
      translate([0,0,0]) cylinder(2, 5.2/2, 5.2/2);
    }
  }
}


if( DRAAIPUNTMIDDEN ) {
  translate([-W-15,20,0]) {
    difference() {
      translate([0,0,0]) cylinder(1.0, 12.5, 12.5);
      translate([0,0,0]) cylinder(10, 7.5, 7.5);
    }
    // draaipunt as
    difference() {
      translate([0,0,0]) cylinder(10, 9.5, 9.5);
      translate([0,0,0]) cylinder(10, 7.5, 7.5);
    }
    // tussen ring
    difference() {
      //translate([40,0,0]) cylinder(.4, 24, 24);
      //translate([40,0,0]) cylinder(10, 9.7, 9.7);
    }
  }
}

if( DRAAIPUNTRING ) {
  translate([-W-15,20,0]) {
    difference() {
      translate([0,0,0]) cylinder(.8, 48/2, 48/2);
      translate([0,0,0]) cylinder(.8, 20/2, 20/2);
    }
  }
}

if( DRAAIPUNT ) {
  translate([-(W+50),0,0]) {

    if( MSHORT ) {
      // glij driehoeken links
      translate([-10+POFFSET,D/2-30,1]) rotate([0,90,0]) prism(1, 20, 13);
      translate([-10+POFFSET,D/2+30,1]) mirror([0,1,0]) rotate([0,90,0]) prism(1, 20, 13);
     
      // glij driehoeken rechts
      translate([W/2+25+POFFSET,0,0]) mirror([1,0,0]) {
        translate([0,D/2-30,1]) rotate([0,90,0]) prism(1, 20, 13);
        translate([0,D/2+30,1]) mirror([0,1,0]) rotate([0,90,0]) prism(1, 20, 13);
      }

     
     // links profiel bevestiging
      translate([POFFSET,0,0]) difference() {
        translate([W/2-55,D/2-60/2,0]) cube([10, 60, 3]);
        //translate([W/2-55,D/2-60/2,0]) cube([8, 60, 8]);
        translate([W/2-55+4,D/2,0]) cylinder(3, 1.6, 1.6);
        translate([W/2-55+4,D/2,0]) cylinder(2, 2.75, 1.6);
      }
      
      translate([POFFSET,0,0]) difference() {
        translate([W/2-55+8,D/2-60/2,0]) cube([2, 60, 10]);
        translate([W/2-55+4,D/2-15,3+3.5]) rotate([90,0,90]) cylinder(100, 1.6, 1.6);
        translate([W/2-55+4,D/2+15,3+3.5]) rotate([90,0,90]) cylinder(100, 1.6, 1.6);
      }      
      
      // rechts profiel bevestiging
      translate([-POFFSET,0,0]) difference() {
        translate([W/2+55-10,D/2-60/2,0]) cube([10, 60, 3]);
        //translate([W/2+55-11+3,D/2-60/2,0]) cube([8, 60, 8]);
        translate([W/2+55+4-11+3,D/2,0]) cylinder(3, 1.6, 1.6);
        translate([W/2+55+4-11+3,D/2,0]) cylinder(2, 2.75, 1.6);
      }
      
      translate([-POFFSET,0,0]) difference() {
        translate([W/2+55-10,D/2-60/2,0]) cube([2, 60, 10]);
        translate([W/2-55+4,D/2-15,3+3.5]) rotate([90,0,90]) cylinder(100, 1.6, 1.6);
        translate([W/2-55+4,D/2+15,3+3.5]) rotate([90,0,90]) cylinder(100, 1.6, 1.6);
       
      }      
 
    }
    
    difference() {
      translate([W/2-55+POFFSET,D/2-10,0]) cube([110-2*POFFSET, 20, 3]);

      // bodem bevestigings gaten
      if( MSHORT ) {
        translate([W/2-55+4+POFFSET,D/2,0]) cylinder(3, 1.6, 1.6);
        translate([W/2+55-4-POFFSET,D/2,0]) cylinder(3, 1.6, 1.6);
        
        translate([W/2-55+4+POFFSET,D/2,0]) cylinder(2, 2.75, 1.6);
        translate([W/2+55-4-POFFSET,D/2,0]) cylinder(2, 2.75, 1.6);
      }
      else {
        translate([W/2-55+4,D/2,0]) cylinder(3, 1.6, 1.6);
        translate([W/2-55+4,D/2,2]) cylinder(1, 1.6, 3.2);

        translate([W/2+55-4,D/2,0]) cylinder(3, 1.6, 1.6);
        translate([W/2+55-4,D/2,2]) cylinder(1, 1.6, 3.2);
      }

      translate([W/2,D/2,0]) cylinder(10, 9.7, 9.7);
    }
    difference() {
      translate([W/2,D/2,0]) cylinder(3, 24, 24);
      translate([W/2,D/2,0]) cylinder(10, 9.6, 9.6);
    }
    difference() {
      //translate([W/2,D/2,0]) cylinder(10, 9.5, 9.5);
      //translate([W/2,D/2,0]) cylinder(10, 5.5, 5.5);
    }
  }
}


if( DRAAISTEL ) {
  translate([0,0,0]) {
    
    if( DRAAIPUNTOPENING ) {
      difference() {
        translate([W/2-2,D/2,0]) cylinder(2, 24, 24);
        translate([W/2-2,D/2,0]) cylinder(4, 10, 10);
      }
    }
    else {
      translate([0,D/2-10,0]) cube([W-2, 20, 2]);
    }
    
    if( MSHORT ) {
    difference() {
      translate([0,22,0]) cube([W-2, 30, 2]);
      translate([28,22,0]) cube([20, 10, 4]);
      // bevestiging motor steun met offset
      translate([(W-MSW)/2+5-2+MSO,4+22,0]) cylinder(20, 1.5, 1.5);
      translate([(W-MSW)/2+(MSW-5)-2+MSO,4+22,0]) cylinder(20, 1.5, 1.5);
    }
    difference() {
      translate([0,D-10-22-20,0]) cube([W-2, 30, 2]);
      translate([28,D-10-22,0]) cube([20, 10, 4]);

      translate([(W-MSW)/2+5-2+MSO,D-4-22,0]) cylinder(20, 1.5, 1.5);
      translate([(W-MSW)/2+(MSW-5)-2+MSO,D-4-22,0]) cylinder(20, 1.5, 1.5);
    }
    }
        
    difference() {
      // Top
      translate([-2,0,0]) cube([W, D, H]);
      translate([1,2,2]) cube([W-6, D-4, 8]);
      translate([1,0,8]) cube([W-6, D, H-2]);

      // gaten voor de coupler plate
      translate([-2+8,D+4,4]) rotate([90,0,0]) cylinder(D+8, 1.4, 1.4);
      translate([-2+W-8,D+4,4]) rotate([90,0,0]) cylinder(D+8, 1.4, 1.4);


      if( MSHORT ) {
        // bevestiging motor steun met offset
        translate([(W-MSW)/2+5-2+MSO,4+22,0]) cylinder(20, 1.5, 1.5);
        translate([(W-MSW)/2+(MSW-5)-2+MSO,4+22,0]) cylinder(20, 1.5, 1.5);
        translate([(W-MSW)/2+5-2+MSO,D-4-22,0]) cylinder(20, 1.5, 1.5);
        translate([(W-MSW)/2+(MSW-5)-2+MSO,D-4-22,0]) cylinder(20, 1.5, 1.5);
      }
  
      else {
        // bevestiging motor steun
        translate([(W-MSW)/2+5-2,4,0]) cylinder(2, 1.5, 1.5);
        translate([(W-MSW)/2+(MSW-5)-2,4,0]) cylinder(2, 1.5, 1.5);

        translate([(W-MSW)/2+5-2,D-4,0]) cylinder(2, 1.5, 1.5);
        translate([(W-MSW)/2+(MSW-5)-2,D-4,0]) cylinder(2, 1.5, 1.5);

        // bevestiging motor steun met offset
        translate([(W-MSW)/2+5-2+MSO,4,0]) cylinder(2, 1.5, 1.5);
        translate([(W-MSW)/2+(MSW-5)-2+MSO,4,0]) cylinder(2, 1.5, 1.5);

        translate([(W-MSW)/2+5-2+MSO,D-4,0]) cylinder(2, 1.5, 1.5);
        translate([(W-MSW)/2+(MSW-5)-2+MSO,D-4,0]) cylinder(2, 1.5, 1.5);
      }

      // openingen in de bodemplaat
      translate([(W-40)/2-2,(D-(HoH+20))/2,0]) cube([40, HoH+20, 2]);

      // openingen aan de zijkanten
      if( ASSEN3 ) {
      translate([-2, D/2-D/3.5, 8]) cube([W, D/6, H-2]);
      translate([-2, D/2+D/3.5-D/6, 8]) cube([W, D/6, H-2]);
      }
      else {
      translate([-2, D-D/2-D/4, 8]) cube([3, D/2, H-2]);
      translate([W-5, D-D/2-D/4, 8]) cube([3, D/2, H-2]);
      }

      // as openingen
      translate([-4,(D-HoH)/2,25]) rotate([0,90,0]) cylinder(W+4, ASR, ASR);
  
    if( ASSEN3 )
        translate([-4,(D-HoH)/2+HoH/2,25]) rotate([0,90,0]) cylinder(W+4, ASR, ASR);

      translate([-4,(D-HoH)/2+HoH,25]) rotate([0,90,0]) cylinder(W+4, ASR, ASR);

      // sier gaten
      translate([-14,(D-HoH)/2,25-14]) rotate([0,90,0]) cylinder(W+20, .75, .75);
      translate([-4,(D-HoH)/2+HoH,25-14]) rotate([0,90,0]) cylinder(W+4, .75, .75);


      // schuine kanten
      translate([-2,15,H]) rotate([180,0,0]) prism(W, 15, 15);
      translate([W-2,D-15,H]) rotate([180,0,180]) prism(W, 15, 15);

      if( ASSEN3 ) {
        translate([W-2,D/2+5,H]) rotate([180,0,180]) prism(W, 16.5, 16.5);
        translate([-2,D/2-5,H]) rotate([180,0,0]) prism(W, 16.5, 16.5);
      }

      translate([W-2,25,H]) rotate([180,0,180]) prism(W, 15, 15);
      translate([-2,D-15-10,H]) rotate([180,0,0]) prism(W, 15, 15);
    
      
    }



    
      // kogellager onder
    difference() {
      translate([-5,(D-HoH)/2,25]) rotate([0,90,0]) cylinder(W+6, ASR+2, ASR+2);
      translate([1,(D-HoH)/2,25]) rotate([0,90,0]) cylinder(W-6,ASR+2, ASR+2);
      translate([-4,(D-HoH)/2,25]) rotate([0,90,0]) cylinder(W+4, ASR, ASR);
      translate([-5,(D-HoH)/2,25]) rotate([0,90,0]) cylinder(W+8, 3, 3);
    }      

    // versteviging as links onder
    difference() {
      translate([-5,(D-HoH)/2-ASR-2,0]) cube([3, 2*(ASR+2), 25]);
      translate([-4,(D-HoH)/2,25]) rotate([0,90,0]) cylinder(W+4, ASR, ASR);
      translate([-5,(D-HoH)/2,25]) rotate([0,90,0]) cylinder(W+8, 3, 3);
      // sier gaten
      translate([-14,(D-HoH)/2,25-14]) rotate([0,90,0]) cylinder(W+20, .75, .75);
      translate([-14,(D-HoH)/2+HoH,25-14]) rotate([0,90,0]) cylinder(W+24, .75, .75);
    }
    // versteviging as rechts onder
    difference() {
      translate([W-2,(D-HoH)/2-ASR-2,0]) cube([3, 2*(ASR+2), 25]);
      translate([-4,(D-HoH)/2,25]) rotate([0,90,0]) cylinder(W+4, ASR, ASR);
      translate([-5,(D-HoH)/2,25]) rotate([0,90,0]) cylinder(W+8, 3, 3);
      // sier gaten
      translate([-14,(D-HoH)/2,25-14]) rotate([0,90,0]) cylinder(W+20, .75, .75);
      translate([-14,(D-HoH)/2+HoH,25-14]) rotate([0,90,0]) cylinder(W+24, .75, .75);
    }
    
    
    if( ASSEN3 ) {
      // kogellager midden
      difference() {
        translate([-5,(D-HoH)/2 + HoH/2,25]) rotate([0,90,0]) cylinder(W+6, ASR+2, ASR+2);
        
        translate([1,(D-HoH)/2+HoH/2,25]) rotate([0,90,0]) cylinder(W-6, ASR+2, ASR+2);
        translate([-4,(D-HoH)/2+HoH/2,25]) rotate([0,90,0]) cylinder(W+4, ASR, ASR);
        translate([-5,(D-HoH)/2+HoH/2,25]) rotate([0,90,0]) cylinder(W+8, 3, 3);
      }      

      // versteviging as links
      difference() {
        translate([-5,(D-HoH)/2-ASR-2+HoH/2,0]) cube([3, 2*(ASR+2), 25]);
        translate([-4,(D-HoH)/2+HoH/2,25]) rotate([0,90,0]) cylinder(W+4, ASR, ASR);
        translate([-5,(D-HoH)/2+HoH/2,25]) rotate([0,90,0]) cylinder(W+8, 3, 3);

      }

      // versteviging as rechts
      difference() {
        translate([W-2,(D-HoH)/2-ASR-2+HoH/2,0]) cube([3, 2*(ASR+2), 25]);
        translate([-4,(D-HoH)/2+HoH/2,25]) rotate([0,90,0]) cylinder(W+4, ASR, ASR);
        translate([-5,(D-HoH)/2+HoH/2,25]) rotate([0,90,0]) cylinder(W+8, 3, 3);

      // sier gaten
      translate([-14,(D-HoH)/2,25-14]) rotate([0,90,0]) cylinder(W+20, .75, .75);
      translate([-14,(D-HoH)/2+HoH,25-14]) rotate([0,90,0]) cylinder(W+24, .75, .75);
      }

    }
    
    
      // kogellager boven
    difference() {
      translate([-5,(D-HoH)/2+HoH,25]) rotate([0,90,0]) cylinder(W+6, ASR+2, ASR+2);
      translate([1,(D-HoH)/2+HoH,25]) rotate([0,90,0]) cylinder(W-6, ASR+2, ASR+2);
      translate([-4,(D-HoH)/2+HoH,25]) rotate([0,90,0]) cylinder(W+4, ASR, ASR);
      translate([-5,(D-HoH)/2+HoH,25]) rotate([0,90,0]) cylinder(W+8, 3, 3);
    }      

    // versteviging as links boven
    difference() {
      translate([-5,(D-HoH)/2-ASR-2+HoH,0]) cube([3, 2*(ASR+2), 25]);
      translate([-4,(D-HoH)/2+HoH,25]) rotate([0,90,0]) cylinder(W+4, ASR, ASR);
      translate([-5,(D-HoH)/2+HoH,25]) rotate([0,90,0]) cylinder(W+8, 3, 3);
      
      // sier gaten
      translate([-14,(D-HoH)/2,25-14]) rotate([0,90,0]) cylinder(W+24, .75, .75);
      translate([-14,(D-HoH)/2+HoH,25-14]) rotate([0,90,0]) cylinder(W+24, .75, .75);
      
    }

    // versteviging as rechts boven
    difference() {
      translate([W-2,(D-HoH)/2-ASR-2+HoH,0]) cube([3, 2*(ASR+2), 25]);
      translate([-4,(D-HoH)/2+HoH,25]) rotate([0,90,0]) cylinder(W+4, ASR, ASR);
      translate([-5,(D-HoH)/2+HoH,25]) rotate([0,90,0]) cylinder(W+8, 3, 3);

      // sier gaten
      translate([-14,(D-HoH)/2,25-14]) rotate([0,90,0]) cylinder(W+24, .75, .75);
      translate([-14,(D-HoH)/2+HoH,25-14]) rotate([0,90,0]) cylinder(W+24, .75, .75);
    }
    
  }
}



if( MOTORSTEUN ) {
  rotate([0,0,0]) translate([W+40,0,0]) {
    
    difference() {
      translate([5,8,0]) cube([2, D-16, MSW]);
      translate([5,10,MSW/2-9]) cube([2, 18, 18]);
      translate([5,50,MSW/2-9]) cube([2, D/2, 18]);
    }

    translate([7,8,0]) cube([4, D-16, 2]);
    translate([7,8,MSW-2]) cube([4, D-16, 2]);

    translate([7,8,0]) cube([4, 2, MSW]);
    translate([7,D-10,0]) cube([4, 2, MSW]);

    
    // motor bevestiging
    difference() {
      translate([5,30,0]) cube([40, 4, MSW]);
      
      // motor as opening
      translate([16,34,MSW/2]) rotate([90,0,0]) cylinder(8, 6.5, 6.5);
      translate([16+20,34,MSW/2]) rotate([90,0,0]) cylinder(8, 6.5, 6.5);
      translate([16,30,MSW/2-6.5]) cube([20, 4, 13]);
      
      // M3 sleuf
      translate([16,34,MSW/2-31/2]) rotate([90,0,0]) cylinder(8, 1.75, 1.75);
      translate([16+20,34,MSW/2-31/2]) rotate([90,0,0]) cylinder(8, 1.75, 1.75);
      translate([16,29,MSW/2-31/2-1.75]) cube([20, 5, 3.5]);
     
      // M3 sleuf
      translate([16,34,MSW/2+31/2]) rotate([90,0,0]) cylinder(8, 1.75, 1.75);
      translate([16+20,34,MSW/2+31/2]) rotate([90,0,0]) cylinder(8, 1.75, 1.75);
      translate([16,29,MSW/2+31/2-1.75]) cube([20, 5, 3.5]);


    }


    // extra stevigheid voor de motor bevestiging
    translate([5+2,10,2]) rotate([0,90,0]) prism(2, 20, 35);
    translate([5+2,10,MSW]) rotate([0,90,0]) prism(2, 20, 35);

    translate([20+2+5,15+15+4,2]) rotate([0,90,90]) prism(2, 20, 20);
    translate([20+2+5,15+15+4,MSW]) rotate([0,90,90]) prism(2, 20, 20);


    // bodemplaat bevestiging
    difference() {
      translate([0,8,0]) cube([2+5, 3, MSW]);
    }
    difference() {
      translate([0,0,0]) cube([2, 10, MSW]);
      translate([0,4,5]) rotate([0,90,0]) cylinder(4, 1.5, 1.5);
      translate([0,4,MSW-5]) rotate([0,90,0]) cylinder(4, 1.5, 1.5);
    }
    
    // bodemplaat bevestiging
    difference() {
      translate([0,D-11,0]) cube([2+5, 3, MSW]);
    }
    difference() {
      translate([0,D-10,0]) cube([2, 10, MSW]);
      translate([0,D-4,5]) rotate([0,90,0]) cylinder(4, 1.5, 1.5);
      translate([0,D-4,MSW-5]) rotate([0,90,0]) cylinder(4, 1.5, 1.5);
    }
    
    
  }
}



if( MOTORSTEUNSHORT ) {
  rotate([0,0,0]) translate([W+40,0,0]) {
    
    difference() {
      translate([5,8+22+1,0]) cube([2, D-16-44-1, MSW]);
      translate([5,10,MSW/2-9]) cube([2, 18, 18]);
      translate([5,50,MSW/2-9]) cube([2, (D-44)/2, 18]);
    }

    translate([7,8+22+1,0]) cube([4, D-16-44-1, 2]);
    translate([7,8+22+1,MSW-2]) cube([4, D-16-44-1, 2]);
    
    // motor bevestiging
    difference() {
      translate([5,31,0]) cube([40, 4, MSW]);
      
      // motor as opening
      translate([16,34+1,MSW/2]) rotate([90,0,0]) cylinder(8, 6.5, 6.5);
      translate([16+20,34+1,MSW/2]) rotate([90,0,0]) cylinder(8, 6.5, 6.5);
      translate([16,30+1,MSW/2-6.5]) cube([20, 4, 13]);
      
      // M3 sleuf
      translate([16,34+1,MSW/2-31/2]) rotate([90,0,0]) cylinder(8, 1.75, 1.75);
      translate([16+20,34+1,MSW/2-31/2]) rotate([90,0,0]) cylinder(8, 1.75, 1.75);
      translate([16,29+1,MSW/2-31/2-1.75]) cube([20, 5, 3.5]);
     
      // M3 sleuf
      translate([16,34+1,MSW/2+31/2]) rotate([90,0,0]) cylinder(8, 1.75, 1.75);
      translate([16+20,34+1,MSW/2+31/2]) rotate([90,0,0]) cylinder(8, 1.75, 1.75);
      translate([16,29+1,MSW/2+31/2-1.75]) cube([20, 5, 3.5]);


    }


    // extra stevigheid voor de motor bevestiging
    //translate([5+2,10,2]) rotate([0,90,0]) prism(2, 20, 35);
    //translate([5+2,10,MSW]) rotate([0,90,0]) prism(2, 20, 35);

    translate([30+2+5,15+15+4,2]) rotate([0,90,90]) prism(2, 30, 40);
    translate([30+2+5,15+15+4,MSW]) rotate([0,90,90]) prism(2, 30, 40);


    // bodemplaat bevestiging
    difference() {
      translate([0,8+22+1,0]) cube([2+5, 3, MSW]);
    }
    difference() {
      translate([0,0+22,0]) cube([3, 10, MSW]);
      translate([0,0+22,10]) cube([3, 10, MSW-20]);
      translate([0,4+22,5]) rotate([0,90,0]) cylinder(4, 1.75, 1.75);
      translate([0,4+22,MSW-5]) rotate([0,90,0]) cylinder(4, 1.75, 1.75);
    }
    
    // bodemplaat bevestiging
    difference() {
      translate([0,D-11-22,0]) cube([2+5, 3, MSW]);
    }
    difference() {
      translate([0,D-10-22,0]) cube([3, 10, MSW]);
      translate([0,D-4-22,5]) rotate([0,90,0]) cylinder(4, 1.75, 1.75);
      translate([0,D-4-22,MSW-5]) rotate([0,90,0]) cylinder(4, 1.75, 1.75);
    }
    
    
  }
}

