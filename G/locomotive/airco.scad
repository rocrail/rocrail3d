/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;
include <../../lib/shapes.scad>


AIRCO=0;
STRIP=0;
RADAR=0;
RADARSOCK=0;
LOGO=0;
LADDER=0;
MOTORBOX=0;
COIN=1;

if( COIN ) {
  translate([0,0,0]) {
    difference() {
      translate([0,0,]) cylinder(2.2, 23.25/2, 23.25/2);
      //translate([-7,-7,2.2-.4]) linear_extrude(.4) text("R",14);
      translate([7,-7,.4]) rotate([0,180,0])linear_extrude(.4) text("R",14);
    }
    
    
  }
}

if( MOTORBOX ) {
  translate([0,0,0]) {
    difference() {
      translate([0,0,0]) cube([100, 170, 50]);
      translate([1.2,1.2,1.2]) cube([100-2*1.2, 170-2*1.2, 50-1.2]);
      translate([100/2,10,50/2]) rotate([90,0,0]) cylinder(20, 10, 10);
    }
  }
}


if( LADDER ) {
  translate([0,0,0]) {
    translate([0,100,1]) rotate([90,0,0]) cylinder(100, 1, 1);
    translate([12,100,1]) rotate([90,0,0]) cylinder(100, 1, 1);
    
    for( a =[1:9] ) {
      translate([0,a*10,1]) rotate([0,90,0]) cylinder(12, 1, 1);
    }

    translate([0,10,1]) cylinder(3, 1, 1);
    translate([12,10,1]) cylinder(3, 1, 1);
    
    translate([0,70,1]) cylinder(3, 1, 1);
    translate([12,70,1]) cylinder(3, 1, 1);
  }
}

if( LOGO ) {
  translate([1,-.8,0]) cube([80, 1, .6]);
  linear_extrude(0.8) text("RocBlue",11);
  translate([60,-.8,0]) linear_extrude(0.8) text("H",14);
  translate([74,-.8,0]) linear_extrude(0.8) text("2",10);
}


if( RADAR ) {
  difference() {
    translate([0,0,-28.2-7]) sphere(r=40);
    translate([0,0,-28.2-7]) sphere(r=38);
    translate([-40,-40,-71-7]) cube([80, 80, 80]);
    if( RADARSOCK )
      translate([0,0,0]) cylinder(7, 2, 2);
  }
  difference() {
    translate([0,0,0]) cylinder(2, 15, 15);
    translate([0,0,0]) cylinder(2, 13, 13);
  }
  difference() {
    translate([0,0,0]) cylinder(RADARSOCK?9:4, 3, 3);
    translate([0,0,0]) cylinder(RADARSOCK?9:4, 1.5, 1.5);
  }
}


if( STRIP ) {
  difference() {
    translate([0,4,0]) cylinder(1, 4, 4);
    translate([0,4,0]) cylinder(1, 1.5, 1.5);
  }
  difference() {
    translate([0,0,0]) cube([220, 8, 1]);
    translate([0,4,0]) cylinder(1, 1.5, 1.5);
  }
}

if( AIRCO ) {
  difference() {
    translate([0,0,0]) cube([220, 50, 6]);
    for( a =[0:10] ) {
      translate([4,a*4+4.0,0]) cube([100-4, 2, 4]);
      translate([120,a*4+4.0,0]) cube([100-4, 2, 4]);
    }
  }

  translate([0,50,6/2]) rotate([90,0,0]) cylinder(50, 3, 3);
  translate([220,50,6/2]) rotate([90,0,0]) cylinder(50, 3, 3);


  translate([4,4,6])  cylinder(4, 3, 3);
  translate([220-4,4,6])  cylinder(4, 3, 3);

  translate([4,50-4,6])  cylinder(4, 3, 3);
  translate([220-4,50-4,6])  cylinder(4, 3, 3);


  translate([220/2,4,6])  cylinder(4, 3, 3);
  translate([220/2,50-4,6])  cylinder(4, 3, 3);
}
