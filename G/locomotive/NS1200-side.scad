/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;
//$fn=16;
include <../../lib/shapes.scad>

W=118;
H=70; // 2mm lager om een ronding te kunnen make naar het dak
L=316/2;
//RL=60/2; //313/2; 
RL=313/2; 

H1=28;
H2=H1/.2;

DW=22; // deur breedte

SIDE=1;
PLAKSTRIP=0;
SPIEGEL=0;
ROAST1=0;
ROAST2=1;
ROOF=0;
WINDOW=0;

ROAST1W=80;
ROAST1H=22;

ROAST2W=80;
ROAST2H=16;

module Spant(x) {
  // spant
  translate([x,7,0]) cube([2,W-14,2]);
  translate([x,8,2]) prism(2,W/2-8,4.5);
  translate([x+2,W-8,2]) rotate([180,180,0]) prism(2,W/2-8,4.5);
}

module pantoSocket(x,y) {
  BASEH=5;
  MH=8;
  difference() {
    translate([x,y,0]) cylinder(BASEH, 4, 4);
    translate([x,y,0]) cylinder(MH, 1.2, 1.2);
  }
  difference() {
    translate([x,y,4]) cylinder(MH-4, 4, 3);
    translate([x,y,0]) cylinder(MH, 1.2, 1.2);
  }
}

module pantoMount(x, y) {
  pantoSocket(-x/2,-y/2);
  pantoSocket( x/2,-y/2);
  pantoSocket(-x/2, y/2);
  pantoSocket( x/2, y/2);

}

if( ROOF ) {
  translate([0,140,0]) {
    
    translate([34,W/2,0]) pantoMount(50, 60);
    
    if( PLAKSTRIP ) {
      translate([RL-5, 3.1+3.5+.5+3, 0]) rotate([4.3,0,0]) cube([10, W/2-12, 1.2]);

      translate([RL-5, W-(3.1+3.5)-.5-3, 1]) rotate([180-4.3,0,0]) cube([10, W/2-12.0, 1.2]);
    }
    
    // dak links
    translate([0, 3.1+3.5+.5, 1.0]) rotate([4.3,0,0]) cube([RL, W/2-7.0, 2]);
      // ronding zijkant
    difference() {
      translate([0,6.9,2]) rotate([0,90,0]) ellipse(RL, 1, 2);
      translate([0,0,0]) cube([RL, W, 2]);
    }
    // onderrand
    translate([6,6.9,0]) cube([RL-12, 2, 2]);

    
    // dak rechts
    translate([0, W-(3.1+3.5)-.5, 2+1.0]) rotate([180-4.3,0,0]) cube([RL, W/2-7.0, 2]);
      // ronding zijkant
    difference() {
      translate([0, W-6.9, 2]) rotate([0,90,0]) ellipse(RL, 1, 2);
      translate([0,0,0]) cube([RL, W, 2]);
    }
    // onderrand
    translate([6,W-6.9-2,0]) cube([RL-12, 2, 2]);

    
    // midden spijl
    translate([0,W/2,5.0]) rotate([0,90,0]) cylinder(RL, 2, 2);

    
    // spant
    Spant(8);
    Spant(RL/2-1);
    Spant(RL-10);
  }
}

if( ROAST1 ) {
  translate([0,140,0]) {
    //translate([0,0,0]) cube([ROAST1W-.2,ROAST1H-.2,.4]);
    // omranding
    difference() {
      translate([0,0,0]) roundedcube(ROAST1W-.2,ROAST1H-.2-(ROAST2?6:0),1.4, 2);
      translate([2,2,.4]) roundedcube(ROAST1W-.2-4,ROAST1H-.2-4-(ROAST2?6:0),2, 1);
    //translate([41,2,0]) cube([ROAST1W/6-2,ROAST1H-4-(ROAST2?6:0),4]);
    }

    for(i = [1:5]) {
      translate([(ROAST1W/6)*i-1,0,0]) cube([2,ROAST1H-.2-(ROAST2?6:0),1.4]);
    }
    
    
    difference() {
    for(i = [1:13-(ROAST2?5:0)]) {
      translate([0, 1.1+(ROAST1H/(ROAST2?15.4:16.4))*i, 0]) cube([ROAST1W-.2,.8,1.0]);
    }

    //translate([41,2,0]) cube([ROAST1W/6-2,ROAST1H-4-(ROAST2?6:0),4]);
  }
  }
}

if( WINDOW ) {
  translate([0,0,0]) {
    difference() {
      translate([.2,.2,0]) roundedcube(DW-6-.4,20-.4,1.4, 2);
      translate([2,2,0]) roundedcube(DW-6-4,20-4,1.4, 1);
    }
  }

}

if( SIDE ) {
  translate([0,0,0]) mirror([SPIEGEL?180:0,0,0]) {
    difference() {
      translate([0,0,0]) cube([L,H,2]);
      
      // deur
      translate([8,14,0]) cube([DW,H,.6]);
      translate([11,H-12,0]) roundedcube(DW-6,20,7, 2);
      // deaur railing gaten
      translate([7,8,-1]) cylinder(10,.8,.8);
      translate([DW+9,8,-1]) cylinder(10,.8,.8);
      // roosters onder
      translate([DW+14,46,0]) cube([80,16,.6]);
    }
    
    if(PLAKSTRIP) {
      translate([L-5,18,2]) cube([10,H-18,2]);
    }

    // binnenwand
    difference() {
      translate([5,18,6]) cube([L-10,H-18+28,1]);
      // deur raam
      translate([11,H-12,0]) roundedcube(DW-6,20,7, 2);
      // deaur railing gaten
      translate([7,8,-1]) cylinder(10,.8,.8);
      translate([DW+9,8,-1]) cylinder(10,.8,.8);
      translate([7,H+11,-1]) cylinder(10,.8,.8);
      translate([DW+9,H+11,-1]) cylinder(10,.8,.8);
    }
    // balken tussen binnen- en buitenwand
    translate([5,18,2]) cube([L-10,2,4]);
    translate([5,(H+18)/2-2,2]) cube([L-10,2,4]);
    difference() {
      translate([5,H-2,2]) cube([L-10,2,4]);
      translate([11,H-12,0]) roundedcube(DW-6,20,7, 2);
    }
    // verticale balken
    translate([5,18,2]) cube([2,H-18,4]);
    translate([L-5-2,18,2]) cube([2,H-18,4]);
    
    
    // schuine kant boven
      // de hoek is 10°
    difference() {
      translate([1.5,H,0]) {
        for(i = [0:H2]) {
          translate([0,i*.2,tan(10)*(i*.2)]) cube([L-1.5,.2,2]);
        }
      }
      translate([8,14,0]) cube([DW,H,.6]);
      translate([11,H-12,0]) roundedcube(DW-6,20,7, 2);
      translate([7,H+11,-1]) cylinder(10,1,1);
      translate([DW+9,H+11,-1]) cylinder(10,1,1);
      // roosters
      translate([DW+14,H,-.2]) rotate([10,0,0])cube([80,22,.6]);
    }
    
    // deur
    difference() {
      translate([8,14,0]) cube([DW,H,3]);
      translate([8,14,2]) cube([DW,4,3]);
      translate([9,14,0]) cube([DW-2,H-2,.6]);
      translate([11,H-12,0]) roundedcube(DW-6,20,7, 2);
    }
    difference() {
      translate([11-2,H-12-2,2]) roundedcube(DW-6+4,20+4,7-2, 2);
      translate([11,H-12,2]) roundedcube(DW-6,20,7-2, 2);
    }


    
  }
}
