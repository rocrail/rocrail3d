/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
//$fn=256;
$fn=128;
include <../../lib/shapes.scad>

W=70; // binnen breedte
H=70;
L=22;
HL=(H+16)/2; // leuning hoogte
LS=30; // strip lengte

HEAD=0;
SNOR=0;
HEADTOP=0;
HEADSTRIP_A=0;  // front
HEADSTRIP_A_TOP=0;  // front
HEADSTRIP_B=0;  // motor
HEADSTRIP_C=0;  // battery (adjusted in NS2400-bottom)
HEADSTRIP_D=0;  // front-side
KOPLAMPLENS_11=0;
KOPLAMPLENS_6=0;

LEDHOUDER=0;

SIDE1=0;
SIDE_MIRROR=1;
SIDE_H=78;
SIDE1_L=245;

SIDE2=0;
SIDE2_L=42;

CABIN_NR=0;
CABIN_SIDE=0;
CABIN_DOOR=0;
CABIN_BEL=1;
CABIN_WINDOW1=0;
CABIN_WINDOW2=0;
CABIN_WINDOW_DRIP=0;
CABINSIDE_L=82+4;
CABIINSIDE_W_CORRECTION=.5; // min .5 om overhel correctie
CABIINSIDE_W=22-CABIINSIDE_W_CORRECTION;
CABIN_W=118-2*CABIINSIDE_W_CORRECTION;
CABIN_TOP=0;
CABIN_LIGHTSTRIP=0;

FRONT_NR=0;

ROOF1=0;
ROOF2=0; // ROOF1 + dak met schoorsteen en rooster
ROOF_L=243; // 243=front 40=back
//ROOF_L=40; // 243=front 40=back
UITLAAT=0;
UITLAAT_LED=0;
UITLAAT_LEDPRINT=0;

RAILING1=0;
RAILING_HOLE=1;
RAILING_HOLE2=0;
RAILING_FIX=1; // fix voor gaten in de bodemplaat welke 2mm tever naar binnen zijn

RAILING2=0;

TEST=0;


if( CABIN_LIGHTSTRIP ) {
  translate([0,0,0]) {
    translate([0,0,0]) cube([108, 20, 2]);
  }
}


if( RAILING2 ) {
  translate([20,0,0]) {
    translate([0,0,1.4]) rotate([270,0,0]) cylinder(47, .8, .8);
  }
}
if( RAILING1 ) {
  translate([0,0,0]) {
    difference() {
      translate([0,0,3]) rotate([270,0,0]) cylinder(33, 1.3, 1.3);
 
      if( RAILING_HOLE ) 
        translate([0,0,0]) cylinder(5, .8, .8); 
    }

    if( RAILING_FIX )
      translate([2,33,3]) rotate([270,0,0]) cylinder(2, 1.5, 1.5);
    else
      translate([0,33,3]) rotate([270,0,0]) cylinder(2, 1.5, 1.5);

    difference() {
      translate([0,0,3]) sphere(r = 2.5);
      if( RAILING_HOLE ) 
        translate([0,0,RAILING_HOLE2?2:0]) cylinder(10, .8, .8); 
    }
    translate([0,33-1.8,3]) rotate([270,0,0]) cylinder(1.8, 3, 3);
  }
}

if( CABIN_NR ) {
  translate([0,0,0]) {
    translate([0,0,0]) linear_extrude(.6) text("2424",12);
    translate([2,0,0]) cube([33, .4, .6]);
  }
}

if( FRONT_NR ) {
  translate([0,16,0]) {
    translate([0,0,0]) linear_extrude(.6) text("2424",7);
    translate([2,0,0]) cube([18, .4, .6]);
  }
}



module doubledoor(w,h,d,a,hl) {
  // a: 0=doubledoor 1=doubleroast 2=singleroast
  difference() {
    translate([0,0,0]) cube([w,h,d]);
    // leuning gaten
    translate([1,hl,0]) cylinder(d,.7,.7);
    translate([w-1,hl,0]) cylinder(d,.7,.7);
    if( a==0 || a==1 ) {
      translate([w/2-.4,0,0]) cube([.8,h,d]);
      // leuning gaten
      translate([w/2+2,hl,0]) cylinder(d,.7,.7);
      translate([w/2-2,hl,0]) cylinder(d,.7,.7);
    }
    if( a==1 ) {
      for(y = [0:16]) {
        translate([1,2.75+y*2,0]) cube([w/2-2,.8,d]);
        translate([w/2+1,2.75+y*2,0]) cube([w/2-2,.8,d]);
      }
      for(y = [0:13]) {
        translate([1,hl+2.75+y*2,0]) cube([w/2-2,.8,d]);
        translate([w/2+1,hl+2.75+y*2,0]) cube([w/2-2,.8,d]);
      }
    }
    if( a==2 ) {
      for(y = [0:31]) {
        for(x = [0:19]) {
          translate([2.75+x*2,2.75+y*2,0]) cube([1.2,1.2,d]);
        }
      }
    }
  }
  
  if( a==1 ) {
    translate([w/4-.5,0,0]) cube([1,h,d]);
    translate([w/2+w/4-.5,0,0]) cube([1,h,d]);
  }
  
  if( a!=2 ) {
    translate([w/2+2,h-8,d]) rotate([270,0,0]) cylinder(8,.5,.5);
    translate([w/2-2,h-8,d]) rotate([270,0,0]) cylinder(8,.5,.5);
    translate([w/2+2,0,d]) rotate([270,0,0]) cylinder(4,.5,.5);
    translate([w/2-2,0,d]) rotate([270,0,0]) cylinder(4,.5,.5);
  }
  
  if( a==2 ) {
    difference() {
      translate([0,0,0]) cube([w,h,d+.2]);
      translate([2,2,0]) cube([w-4,h-4,d+.2]);
      // leuning gaten
      translate([1,hl,0]) cylinder(d+.2,.7,.7);
      translate([w-1,hl,0]) cylinder(d+.2,.7,.7);
    }
  }  
}


CABINWIN_L=40;
CABINWIN_L2=15;
CABINWIN_H=21;

if( CABIN_WINDOW1 ) {
  difference() {
    translate([0,0,0]) rcube(1, CABINWIN_L, CABINWIN_H-.8, 5);
    translate([1.5,1.5,0]) rcube(1, CABINWIN_L-3, CABINWIN_H-.8-3, 3.5);
  }
  translate([CABINWIN_L/2-.5,0,0]) cube([1,CABINWIN_H-.8*2,1]);
}

if( CABIN_WINDOW_DRIP ) {
  translate([0,CABINWIN_H+10,0]) difference() {
    translate([0,0,0]) rcube(2, CABINSIDE_L-8, CABINWIN_H, 8);
    translate([1,1,0]) rcube(2, CABINSIDE_L-8-2, CABINWIN_H-2, 7);
    translate([0,0,0]) cube([CABINSIDE_L,CABINWIN_H-4,2]);
  }
}

if( CABIN_WINDOW2 ) {
  translate([CABINWIN_L+5,0,0]) difference() {
    translate([0,0,0]) rcube(1, CABINWIN_L2, CABINWIN_H-.8, 5);
    translate([1.5,1.5,0]) rcube(1, CABINWIN_L2-3, CABINWIN_H-.8-3, 4);
  }
}

if( CABIN_BEL ) {
  translate([0,0,7]) difference() {
    translate([0,0,-2]) sphere(r = 6);
    translate([-6,-6,-8]) cube([12,12, 8]);
  }
  translate([0,0,0]) cylinder(10,1.5,1.5);
  
}

if( CABIN_DOOR ) {
  difference() {
    translate([0,0,0]) cube([18,SIDE_H-10, .6]);
    translate([18/2-CABINWIN_L2/2,SIDE_H-CABINWIN_H-12,0]) rcube(4, CABINWIN_L2, CABINWIN_H, 5);
    translate([17,29,0]) cylinder(2,.8,.8);
  }
  translate([0,5,.7]) rotate([270,0,0]) cylinder(4,.7,.7);
  translate([0,SIDE_H-10-8,.7]) rotate([270,0,0]) cylinder(4,.7,.7);
    
  translate([15,39,0]) cube([2,6, 1.2]);
  
  translate([15,SIDE_H-10-2,0]) cube([2,3,1.2]);

  translate([16.5,42,.8]) rotate([270,0,90]) cylinder(4.5,.7,.7);
  
}

if( CABIN_SIDE ) {
  difference() {
    translate([0,0,0]) cube([CABINSIDE_L,CABIINSIDE_W,SIDE_H]);
    translate([2,3,0]) cube([CABINSIDE_L-4,CABIINSIDE_W-2,SIDE_H]);
    //translate([2,2,0]) cube([CABINSIDE_L-4,SIDE_W-2,46]);
    //translate([CABINSIDE_L/2-CABINWIN_L/2,4,49]) rotate([90,0,0]) rcube(8, CABINWIN_L, CABINWIN_H, 5);
    translate([CABINSIDE_L/2-CABINWIN_L/2,2.5,49]) rotate([90,0,0]) rcube(8, CABINWIN_L, CABINWIN_H, 5);
    translate([CABINSIDE_L/2-CABINWIN_L/2+.5,5,49+.5]) rotate([90,0,0]) rcube(8, CABINWIN_L-1, CABINWIN_H-1, 5);


    //translate([-2,CABINWIN_H/2-CABINWIN_L2/2,49]) rotate([90,0,90]) rcube(CABINSIDE_L+4, CABINWIN_L2, CABINWIN_H, 5);
    translate([-2,CABINWIN_H/2-CABINWIN_L2/2,49]) rotate([90,0,90]) rcube(3.5, CABINWIN_L2, CABINWIN_H, 5);
    translate([-2,CABINWIN_H/2-CABINWIN_L2/2+.5,49+.5]) rotate([90,0,90]) rcube(5, CABINWIN_L2-1, CABINWIN_H-1, 5);

    translate([CABINSIDE_L-1.5,CABINWIN_H/2-CABINWIN_L2/2,49]) rotate([90,0,90]) rcube(3.5, CABINWIN_L2, CABINWIN_H, 5);
    translate([CABINSIDE_L-5,CABINWIN_H/2-CABINWIN_L2/2+.5,49+.5]) rotate([90,0,90]) rcube(5, CABINWIN_L2-1, CABINWIN_H-1, 5);
  
  translate([-2,2.7,33]) rotate([90,0,90]) cylinder(CABINSIDE_L+4,.7,.7);
  }
  
  // lijm fixatie voor de window drip
  translate([CABINSIDE_L/2-62/2,-.4,72]) cube([62,.4,.4]);
  
}

if( CABIN_TOP ) {
  // lijm strip
  translate([3,2,0]) difference() {
    translate([0,0,0]) cube([CABIN_W-6, CABINSIDE_L-4,4]);
    translate([2,2,0]) cube([CABIN_W-10, CABINSIDE_L-8,4]);
    translate([2+8,0,0]) cube([CABIN_W-6-20, CABINSIDE_L,4]);
  }

  // verstevigingen
  translate([CABIN_W/2-1,0,4]) cube([2, CABINSIDE_L,12]);
  difference() {
    translate([CABIN_W/2,CABINSIDE_L/2,4]) rotate([270,0,0]) ellipse(2, CABIN_W/2-3, 10);
    translate([0,0,-10]) cube([CABIN_W, CABINSIDE_L,14]);
    
    // kabel doorvoer
    translate([CABIN_W/2-10,-10,7]) rotate([270,0,0]) cylinder(CABINSIDE_L+20,1.5,1.5);
    translate([CABIN_W/2+10,-10,7]) rotate([270,0,0]) cylinder(CABINSIDE_L+20,1.5,1.5);
  }

  // top
  translate([CABIN_W/2,0,0]) {
    // bel
    difference() {
      translate([-CABIN_W/2+22,11,8]) cylinder(8,3,2.6);
      translate([-CABIN_W/2+22,11,0]) cylinder(16,1.6,1.6);
    }
    difference() {
      translate([0,0,4]) rotate([270,0,0]) ellipse(CABINSIDE_L, CABIN_W/2, 12);
      translate([0,4,4]) rotate([270,0,0]) ellipse(CABINSIDE_L-8, CABIN_W/2-5, 10);
      translate([-CABIN_W/2,0,-10]) cube([CABIN_W, CABINSIDE_L,14]);
      // bel
      translate([-CABIN_W/2+22,11,0]) cylinder(16,1.6,1.6);


    // kabel doorvoer
      translate([-10,-10,7]) rotate([270,0,0]) cylinder(CABINSIDE_L+20,1.5,1.5);
      translate([+10,-10,7]) rotate([270,0,0]) cylinder(CABINSIDE_L+20,1.5,1.5);
    }
  }

}


if( SIDE2 ) mirror([SIDE_MIRROR?1:0,0,0]) {
  difference() {
    translate([0,0,0]) cube([SIDE2_L,SIDE_H,2]);

    // bevestigings gaten
    translate([10,2,0]) cylinder(2,.7,.7);
    translate([SIDE2_L-10,2,0]) cylinder(2,.7,.7);
    // leuning gaten
    translate([5,HL,0]) cylinder(20,.7,.7);
    translate([20,HL,0]) cylinder(20,.7,.7);
    translate([SIDE2_L-3,HL,0]) cylinder(20,.7,.7);
    translate([24,HL,0]) cylinder(20,.7,.7);
  }


  translate([4,4,2]) doubledoor(SIDE2_L-6,70,.6,1, HL-4);

  // lijm strip
  translate([-2,0,0]) cube([2,SIDE_H,8]);
}


if( SIDE1 ) mirror([SIDE_MIRROR?1:0,0,0]) {
  difference() {
    translate([0,0,0]) cube([SIDE1_L,SIDE_H,2]);
    // bevestigings gaten
    for(x = [0:4]) {
      translate([10+((SIDE1_L-20)/4)*x,2,0]) cylinder(2,.7,.7);
    }
    
    // leuning gaten
    for(i = [0:4]) {
      translate([SIDE1_L/5*i +1   ,HL,0]) cylinder(2,.7,.7);
      translate([SIDE1_L/5*i +45-1,HL,0]) cylinder(2,.7,.7);
      if( i != 1 ) {
        translate([SIDE1_L/5*i + 45/2+2,HL,0]) cylinder(2,.7,.7);
        translate([SIDE1_L/5*i + 45/2-2,HL,0]) cylinder(2,.7,.7);
      }
    }
  }

  for(i = [0:4]) {
    if( i == 0 || i == 4 )
      translate([SIDE1_L/5*i,4,2]) doubledoor(45,70,.6,1, HL-4);
    else if( i == 1 )
      translate([SIDE1_L/5*i,4,2]) doubledoor(45,70,.6,2, HL-4);
    else
      translate([SIDE1_L/5*i,4,2]) doubledoor(45,70,.6,0, HL-4);
  }
  
  // lijm strip
  translate([SIDE1_L,0,0]) cube([2,SIDE_H,8]);
}


if( TEST ) {
  difference() {
    translate([0,0,0]) cube([250,210,1]);
    translate([2,2,0]) cube([250-4,210-4,1]);
  }
  translate([250/2-1,0,0]) cube([2,210,1]);
  translate([0,210/2+1,0]) cube([250,2,1]);
}

if( KOPLAMPLENS_11 ) {
  difference() {
    translate([0,0,0]) cylinder(1.8,11/2,11/2);
    translate([0,0,.4]) cylinder(1.8,9/2,9/2);
  }
}
if( KOPLAMPLENS_6 ) {
  difference() {
    translate([20,0,0]) cylinder(1.8,6/2,6/2);
    translate([20,0,.4]) cylinder(1.8,5/2,5/2);
  }
}

if( UITLAAT_LED ) {
  difference() {
    translate([0,0,0]) cube([16,8,6]);
    translate([1,1,2]) cube([14,6,6]);
    translate([16/2,8/2,0]) cylinder(2,5/2,5/2);
    translate([1,1,0]) cube([3,6,2]);
    translate([16-4,1,0]) cube([3,6,2]);
  }
}
if( UITLAAT_LEDPRINT ) {
  translate([0,0,5]) {
    difference() {
      translate([0,8,0]) cube([16,16,1]);
      translate([16/2,8+4,0]) cylinder(2,1.2/2,1.2/2);
      translate([16/2,8+4+2,0]) cylinder(2,1.2/2,1.2/2);
      translate([16/2,8+4+4,0]) cylinder(2,1.2/2,1.2/2);

      translate([16/4,8+4,0]) cylinder(2,1.2/2,1.2/2);
      translate([16/4,8+4+8,0]) cylinder(2,1.2/2,1.2/2);

      translate([16-16/4,8+4,0]) cylinder(2,2.5/2,2.5/2);
      translate([16-16/4,8+4+8,0]) cylinder(2,2.5/2,2.5/2);
    }
  }
}

if( UITLAAT ) {
  difference() {
    translate([0,0,0]) cube([16,8,12]);
    translate([1.2,1.2,0]) cube([16-2*1.2,8-2*1.2,20]);
  }
  
  translate([16,1.2,12]) rotate([0,0,180]) prism(16,1.2,1.2);
  translate([0,8-1.2,12]) rotate([0,0,0]) prism(16,1.2,1.2);
  translate([1.2,0,12]) rotate([0,0,90]) prism(8,1.2,1.2);
  translate([16-1.2,8,12]) rotate([0,0,270]) prism(8,1.2,1.2);
  
  difference() {
    translate([-1,-1,11.4]) cube([18,10,2]);
    translate([-1+1.2,-1+1.2,11.4]) cube([18-2*1.2,10-2*1.2,20]);
  }
  
  for(i = [0:2]) {
    translate([0,1.6+i*2,13]) cube([16,.6,.4]);
  }
  for(i = [0:6]) {
    translate([1.65+i*2,0,13]) cube([.6,8,.4]);
  }
  
}

if( ROOF1 ) {
  // bovenkant 
  difference() {
    translate([-W/2,16,12]) rotate([0,-5,0]) cube([W/2,ROOF_L,2]);
    if( ROOF2 ) translate([-16/2,16+60,0]) cube([16,8,40]);
  }
  difference() {
    translate([W/2,16,12]) mirror([1,0,0]) rotate([0,-5,0]) cube([W/2,ROOF_L,2]);
    if( ROOF2 ) translate([-16/2,16+60,0]) cube([16,8,40]);
  }
  
  // opvul buis stukjes links
  translate([-W/2,16,12]) rotate([270,0,0]) cylinder(ROOF_L, 2,2);
    
  // opvul buis stukjes midden
  difference() {
    translate([0,16,15.1]) rotate([270,0,0]) cylinder(ROOF_L, 2,2);
    if( ROOF2 ) translate([-16/2,16+60,0]) cube([16,8,40]);
  }
    
  // opvul buis stukjes rechts
  translate([W/2,16,12]) rotate([270,0,0]) cylinder(ROOF_L, 2,2);

  // opvulling wand links/rechts
  translate([-W/2-2,16,8]) cube([2,ROOF_L,4]);
  translate([W/2,16,8]) cube([2,ROOF_L,4]);

  // lijm strip links/rechts
  translate([-W/2,16,4]) cube([2,ROOF_L,8]);
  translate([W/2-2,16,4]) cube([2,ROOF_L,8]);

  // kopse kant voor opvullen
  difference() {
    translate([-W/2,16,4]) cube([W,2,8]);
    // kabel doorvoer
    translate([-10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
    translate([10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
  }
  
  difference() {
    translate([0,18,16]) rotate([90,180,90]) prism(2,4,W/2);
    // kabel doorvoer
    translate([-10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
    translate([10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
  }
  difference() {
    translate([0,18,16]) mirror([1,0,0]) rotate([90,180,90]) prism(2,4,W/2);
    // kabel doorvoer
    translate([-10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
    translate([10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
  }

  // kopse kant achter opvullen
  difference() {
    translate([-W/2,ROOF_L-2+16,4]) cube([W,2,8]);
    // kabel doorvoer
    translate([-10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
    translate([10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
  }
  difference() {
    translate([0,ROOF_L-2+18,16]) rotate([90,180,90]) prism(2,4,W/2);
    // kabel doorvoer
    translate([-10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
    translate([10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
  }
  difference() {
    translate([0,ROOF_L-2+18,16]) mirror([1,0,0]) rotate([90,180,90]) prism(2,4,W/2);
    // kabel doorvoer
    translate([-10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
    translate([10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
  }

  // midden spant
  difference() {
    translate([-W/2,ROOF_L/2-2+16,4]) cube([W,2,8]);
    // kabel doorvoer
    translate([-10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L/2+20, 1.5,1.5);
    translate([10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L/2+20, 1.5,1.5);
  }
  difference() {
    translate([0,ROOF_L/2-2+18,16]) rotate([90,180,90]) prism(2,4,W/2);
    // kabel doorvoer
    translate([-10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L/2+20, 1.5,1.5);
    translate([10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L/2+20, 1.5,1.5);
  }
  difference() {
    translate([0,ROOF_L/2-2+18,16]) mirror([1,0,0]) rotate([90,180,90]) prism(2,4,W/2);
    // kabel doorvoer
    translate([-10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L/2+20, 1.5,1.5);
    translate([10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L/2+20, 1.5,1.5);
  }



  if( ROOF2 ) {
    // schoorsteen
    difference() {
      translate([-18/2,16+60-1,10]) cube([18,10,10]);
      translate([-16/2,16+60,10]) cube([16,8,40]);
    }
    // rooster
      translate([-W/2,200,14]) rotate([0,-5,0]) {
        difference() {
          cube([W/2,50,2]);
          translate([2,2,1]) cube([W/2-3,50-4,2]);
        }
        for(i = [0:20]) {
          translate([2,2+1+2.15*i,0]) cube([W/2-3,1,1.6]);
        }
        for(i = [0:13]) {
          translate([2+1+2.2*i,2,0]) cube([1,50-4,1.6]);
        }
      }
      translate([W/2,200,14]) mirror([1,0,0]) rotate([0,-5,0]) { 
        difference() {
          cube([W/2,50,2]);
          translate([2,2,1]) cube([W/2-3,50-4,2]);
        }
        for(i = [0:20]) {
          translate([2,2+1+2.15*i,0]) cube([W/2-3,1,1.6]);
        }
        for(i = [0:13]) {
          translate([2+1+2.2*i,2,0]) cube([1,50-4,1.6]);
        }
      }
      translate([0,200,17]) rotate([270,0,0]) cylinder(50, 2,2);
  }
}


module koplamp(x,y,z,d) {
  translate([x,y,z]) {
    // koplamp
    difference() {
      translate([0,-1,0]) rotate([90,0,0]) cylinder(6,d/2,d/2);
      translate([0,-1,0]) rotate([90,0,0]) cylinder(6,5/2,5/2);
      translate([0,-4.5,0]) rotate([90,0,0]) cylinder(2.5,(d-2)/2,(d-2)/2);
      translate([0,-3,0]) rotate([90,0,0]) cylinder(2,5/2,(d-2)/2);
    }
    difference() {
      translate([0,2,0]) rotate([90,0,0]) cylinder(3,7/2,7/2);
      translate([0,2,0]) rotate([90,0,0]) cylinder(3,5/2,5/2);
      translate([0,-3,0]) rotate([90,0,0]) cylinder(2,5/2,(d-2)/2);
    }
  }
}

if( HEADTOP ) translate([0,0,70]) {
  translate([0,-4,0]) {
    // 90° buizen links/midden/rechts
    translate([-W/2,14,0]) rotate([0,0,90]) tube( 2, 12);
    translate([0,10.8,3.1]) rotate([0,0,90]) tube( 2, 12);
    translate([W/2,14,0]) rotate([0,0,90]) tube( 2, 12);
    
    // ronde opvulling links
    difference() {
      translate([-W/2-2,16,0]) rotate([90,0,90]) cylinder( 2, 12,12);
      translate([-W/2-2,0,-32]) cube([W+4,32,32]);
      translate([-W/2-2,16,-16]) cube([W+4,32,32]);
    }
    
    // ronde opvulling rechts
    difference() {
      translate([W/2,16,0]) rotate([90,0,90]) cylinder( 2, 12,12);
      translate([-W/2-2,0,-32]) cube([W+4,32,32]);
      translate([-W/2-2,16,-16]) cube([W+4,32,32]);
    }
    
    // opvulling wand links/rechts
    translate([-W/2-2,16,0]) cube([2,10,12]);
    translate([W/2,16,0]) cube([2,10,12]);
    
    // opvul buis stukjes links
    translate([-W/2,16,12]) rotate([270,0,0]) cylinder(10, 2,2);
    
    // opvul buis stukjes midden
    translate([0,16-3.2,15.1]) rotate([270,0,0]) cylinder(10+3.2, 2,2);
    translate([0,-3.2+4,0]) cylinder(3.1, 2,2);
    
    // opvul buis stukjes rechts
    translate([W/2,16,12]) rotate([270,0,0]) cylinder(10, 2,2);

    // bovenkant 
    translate([-W/2,16,12]) rotate([0,-5,0]) cube([W/2,10,2]);
    translate([W/2,16,12]) mirror([1,0,0]) rotate([0,-5,0]) cube([W/2,10,2]);

    // bovenkant driehoeken
    translate([-W/2,16,14]) rotate([0,90,270]) rotate([0,0,5]) prism(2,W/2,3.2);
    translate([W/2,16,14]) mirror([1,0,0]) rotate([0,90,270]) rotate([0,0,5]) prism(2,W/2,3.2);
    

    // onderkant driehoeken
    translate([-W/2,4,0]) rotate([0,0,270]) rotate([0,0,-5]) prism(2,W/2,3.2);
    mirror([1,0,0]) {
      translate([-W/2,4,0]) rotate([0,0,270]) rotate([0,0,-5]) prism(2,W/2,3.2);
    }
    //translate([W/2,4,0]) mirror([1,0,0]) rotate([0,0,270]) rotate([0,0,-5]) prism(2,W/2,3.2);


    // ronding links
    difference() {
      translate([-W/2+1,13.6,2.75]) rotate([0,86,-4.5]) cylinder(W/2+.2,11.0,12);
      translate([-W/2+1,13.6,2.75]) rotate([0,86,-4.5]) cylinder(W/2+1,11.0-2,12-2);
      translate([-W/2-2,-6,-16]) cube([W+4,40,16]);
      translate([-W/2-2,16,0]) cube([W+4,20,32]);
    }

    // ronding rechts
    difference() {
      translate([W/2-1,13.6,2.75]) mirror([1,0,0]) rotate([0,86,-4.5]) cylinder(W/2+.2,11.0,12);
      translate([W/2-1,13.6,2.75]) mirror([1,0,0]) rotate([0,86,-4.5]) cylinder(W/2+1,11.0-2,12-2);
      translate([-W/2-2,-6,-16]) cube([W+4,40,16]);
      translate([-W/2-2,16,0]) cube([W+4,20,32]);
    }


    difference() {
      translate([-W/2,16,0]) rotate([90,0,90]) cylinder( W, 14,14);
      translate([-W/2,16,0]) rotate([90,0,90]) cylinder( W, 12,12);
      translate([-W/2-2,0,-32]) cube([W+4,32,32]);
      translate([-W/2-2,16,-16]) cube([W+4,32,32]);
    }

    // versteviging
    translate([-W/2-2,16,0]) cube([W+4,10,2]);
    difference() {
      translate([-W/2-2,16+8,0]) cube([W+4,2,12]);
      // kabel doorvoeringen
      translate([-10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
      translate([10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
    }
    
    difference() {
      translate([0,26,16]) rotate([90,180,90]) prism(2,4,W/2);
      // kabel doorvoeringen
      translate([-10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
      translate([10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
    }
    difference() {
      translate([0,26,16]) mirror([1,0,0]) rotate([90,180,90]) prism(2,4,W/2);
      // kabel doorvoeringen
      translate([-10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
      translate([10,16-10,4+8]) rotate([270,0,0]) cylinder(ROOF_L+20, 1.5,1.5);
    }

  }
}


if( HEADSTRIP_A ) {
  translate([0,-50,0]) {
    difference() {
      translate([-W/2,0-8,0]) cube([W,LS+8,12]);
      translate([-W/2+2,2-8,0]) cube([W-4,LS-2+8,12]);
      translate([-W/2-2,+LS-6-8,0]) cube([W+4,6+8,2]);
    }
    difference() {
      translate([-W/2-4,0-8,0]) cube([W+8,LS+8,2]);
      translate([-W/2+2,2-8,0]) cube([W-4,LS-2+8,12]);
    }

    difference() {
      translate([-W/2,-2-12,4.2]) rotate([0,0,-5]) cube([W/2,2,7.8]);
      // bevestiging head
      translate([0,-20,8]) rotate([270,0,0]) cylinder(20, .7,.7);
    }
    difference() {
      translate([0,-5.2-12,4.2]) rotate([0,0,5]) cube([W/2,2,7.8]);
      // bevestiging head
      translate([0,-20,8]) rotate([270,0,0]) cylinder(20, .7,.7);
    }

    translate([-W/2,-14,4.2]) cube([2,20,7.8]);
    translate([W/2-2,-14,4.2]) cube([2,20,7.8]);


  }
}


if( HEADSTRIP_A_TOP ) {
  translate([0,-50,0]) {
    translate([-W/2,-2-12,0]) rotate([0,0,-5]) cube([W/2,2,10]);
    translate([0,-5.2-12,0]) rotate([0,0,5]) cube([W/2,2,10]);

    translate([-W/2,-14,0]) cube([2,20,10]);
    translate([W/2-2,-14,0]) cube([2,20,10]);


  }
}


module stripB() {
  translate([0,-W/2,0]) cube([60,2,12]);
  translate([0,-W/2-6,0]) cube([60,8,2]);
  
  translate([0,W/2-2,0]) cube([60,2,12]);
  translate([0,W/2-2,0]) cube([60,8,2]);
  
  translate([0,-W/2,4]) cube([2,W,8]);
}

if( HEADSTRIP_B ) {
  translate([0,-100,0]) {
    difference() {
      stripB();
      translate([12-4,-W/2-4,0]) cube([14,W+8,4]);
    }

  }
}


if( HEADSTRIP_C ) {
  translate([0,-170,0]) {
    translate([0,0,0]) cube([150,8,2]);
  }
}

if( HEADSTRIP_D ) {
  translate([0,-170,0]) {
    translate([0,0,0]) cube([60,20,2]);
  }
}



module snor() {
      difference() {
        translate([-W/2,-5,5]) rotate([0,0,-5]) cube([8,10,16]);
        translate([-W/2+8,0,13]) rotate([90,0,0]) cylinder(6,8,8);
      }
}

if( SNOR ) {
  translate([0,0,0]) {
    difference() {
      translate([-W/2,-4,5]) rotate([0,0,-5]) cube([W/2-.8,.4,16]);
      translate([-W/2+8,0,13]) rotate([90,0,0]) cylinder(6,6,6);
      koplamp(-W/2+8,0,13,13);
      snor();
    }
    /*
    mirror([1,0,0]) difference() {
      translate([-W/2,-4,5]) rotate([0,0,-5]) cube([W/2-.8,.4,16]);
      translate([-W/2+8,0,13]) rotate([90,0,0]) cylinder(6,6,6);
      koplamp(-W/2+8,0,13,13);
      snor();
    }
    */
  }
}
  
if( HEAD ) {
  translate([0,0,0]) {
    // zijwand links
    difference() {
      translate([-W/2-2,0,0]) cube([2,L,H]);
      // leuning
      translate([-W/2-5,18,HL]) rotate([90,0,90]) cylinder(W+10,.7,.7);
    }
    // zijwand rechts
    difference() {
      translate([W/2,0,0]) cube([2,L,H]);
      // leuning
      translate([-W/2-5,18,HL]) rotate([90,0,90]) cylinder(W+10,.7,.7);
    }
    
    // voorkant links
    difference() {
      translate([-W/2,0,0]) rotate([0,0,90]) cylinder(H,2,2);
      translate([-W/2,0,0]) cube([4,4,H]);
    }
    difference() {
      translate([-W/2,-2,0]) rotate([0,0,-5]) cube([W/2,2,H]);
      // koplampen
      translate([-W/2+8,0,13]) rotate([90,0,0]) cylinder(6,5/2,5/2);
      translate([-W/2+8,0,H-8]) rotate([90,0,0]) cylinder(6,5/2,5/2);
      translate([0,0,32]) rotate([90,0,0]) cylinder(6,5/2,5/2);
      // leuning
      translate([-14,0,HL]) rotate([90,0,0]) cylinder(H,.7,.7);
      // slang
      translate([0,0,13]) rotate([90,0,0]) cylinder(H,1,1);
      // bevestiging
      translate([0,0,4]) rotate([90,0,0]) cylinder(H,.7,.7);
    }
    
    // voorkant midden
    difference() {
      translate([0,-3.2,0]) rotate([0,0,90]) cylinder(H,2,2);
      translate([-2,-4,0]) cube([4,4,H]);
      // koplamp
      translate([0,0,32]) rotate([90,0,0]) cylinder(6,5/2,5/2);
      // slang
      translate([0,0,13]) rotate([90,0,0]) cylinder(H,1,1);
      // bevestiging
      translate([0,0,4]) rotate([90,0,0]) cylinder(H,.7,.7);
    }
    
    // voorkant rechts
    difference() {
      translate([W/2,0,0]) rotate([0,0,90]) cylinder(H,2,2);
      translate([W/2-4,0,0]) cube([4,4,H]);
    }
    difference() {
      mirror([1,0,0]) {
        translate([-W/2,-2,0]) rotate([0,0,-5]) cube([W/2,2,H]);
      }
      // koplampen
      translate([W/2-8,0,13]) rotate([90,0,0]) cylinder(6,5/2,5/2);
      translate([W/2-8,0,H-8]) rotate([90,0,0]) cylinder(6,5/2,5/2);
      translate([0,0,32]) rotate([90,0,0]) cylinder(6,5/2,5/2);
      // leuning
      translate([14,0,HL]) rotate([90,0,0]) cylinder(H,.7,.7);
      // slang
      translate([0,0,13]) rotate([90,0,0]) cylinder(H,1,1);
      // bevestiging
      translate([0,0,4]) rotate([90,0,0]) cylinder(H,.7,.7);
    }
    
    // koplamp links
    koplamp(-W/2+8,0,13,13);
    koplamp(-W/2+8,1.3,H-8,8);
    // koplamp midden
    koplamp(0,-3,32,13);
    // koplamp rechts
    koplamp(W/2-8,0,13,13);
    koplamp(W/2-8,1.3,H-8,8);
    
    // LED houder bevestigingen
    difference() {
      translate([0,1.5,60]) rotate([90,0,0]) cylinder(5,3,3);
      translate([0,1.5,60]) rotate([90,0,0]) cylinder(5,1.3,1.3);
    }    
    difference() {
      translate([0,1.5,20]) rotate([90,0,0]) cylinder(5,3,3);
      translate([0,1.5,20]) rotate([90,0,0]) cylinder(5,1.3,1.3);
    }    

  }

}

module LEDHOLES(x,y) {
  translate([x,y-2.5/2,0]) cylinder(10,.75,.75);
  translate([x,y+2.5/2,0]) cylinder(10,.75,.75);
}

module LEDCUP(x,y,cut) {
  difference() {
    translate([x,y,0]) cylinder(3.5,4,4);
    if( !cut ) {
      translate([x,y,2]) cylinder(5,3,3);
      translate([x,y-2.5/2,0]) cylinder(4,.75,.75);
      translate([x,y+2.5/2,0]) cylinder(4,.75,.75);
    }
  }
}


if( LEDHOUDER ) {
  // links onder
  translate([-W/2+8,13,1.5]) LEDCUP(0,0,false);
  // links boven
  translate([-W/2+8,H-8,0]) LEDCUP(0,0,false);
  // midden
  translate([0,32,3]) LEDCUP(0,0,false);;
  // rechts onder
  translate([W/2-8,13,1.5]) LEDCUP(0,0,false);
  // rechts boven
  translate([W/2-8,H-8,0]) LEDCUP(0,0,false);

  // verbinding onder
  difference() {
    translate([-W/2+3,13-4,0]) cube([W-6, 8, 3]);
    translate([-W/2+8,13,0]) LEDHOLES(0,0);
    translate([W/2-8,13,0]) LEDHOLES(0,0);
    translate([-15,13,0]) cylinder(15,1.5,1.5);
    translate([15,13,0]) cylinder(15,1.5,1.5);
  }
  // verbinding midden
  difference() {
    translate([0-4,13-4,0]) cube([8, H-13, 3]);
    translate([0,32,0]) LEDHOLES(0,0);;
    translate([0,20,0]) cylinder(15,1.6,1.6);
    translate([0,60,0]) cylinder(15,1.6,1.6);
    translate([0,45,0]) cylinder(15,1.5,1.5);
  }
  
  // verbinding boven
  difference() {
    translate([-W/2+3,H-8-4,0]) cube([W-6, 8, 2]);
    translate([-W/2+8,H-8,0]) LEDHOLES(0,0);
    translate([W/2-8,H-8,0]) LEDHOLES(0,0);
    translate([0,60,0]) cylinder(15,1.6,1.6);
    translate([-15,H-8,0]) cylinder(15,1.5,1.5);
    translate([15,H-8,0]) cylinder(15,1.5,1.5);
  }
}




