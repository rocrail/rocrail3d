/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
//$fn=64;
include <../../lib/shapes.scad>

W=110+2*2; // breedte over de U-Profielen
L=312/2;
L2=70; // sparing lengte
W2=70; // sparing breedte
GLUEBAR=0;
RAILINGFRONT=0;

NS2400=0; // railing sparingen
NS6400=1; // railing sparingen
POFFSET=10; // profiel offset 0=2400 10=6400



BOTTOM=0;
BATTERY=0;
BATTERYTOP=0;
LINERED=1;

COLORTEST=0; // rounded cube

BUS=0;

// Battery box size
BL=153;
BW=66;
BH=14;
BHT=39;

WIOPI_CX=89;
WIOPI_CY=52;
// print = 89 x 52 (cx/cy)
module WIOPI(h) {
  
  translate([16,4,0]) {
    translate([0,0,0]) cylinder(h, 1.5,1.5);
    translate([58,0,0]) cylinder(h, 1.5,1.5);
    
    translate([0,23,0]) cylinder(h, 1.5,1.5);
    translate([58,23,0]) cylinder(h, 1.5,1.5);
  }
}


if( COLORTEST ) {
  translate([0,0,0]) rcube(.6,20,30,5);
}


if( LINERED ) {
  translate([W+40,0,0]) {
    translate([0,0,0]) cube([L, 4, .6]);
  }
}




if( BUS ) {
  difference() {
    translate([0,0,0]) cylinder(2,2.75,2.75);
    translate([0,0,0]) cylinder(2,1.75,1.75);
  }
}

if( BATTERY ) {
  translate([0,-100,0]) {
    difference() {
      translate([0,-2,0]) cube([BL,BW+4,BH]);
      translate([2,0,0]) cube([BL-4,BW,BH]);
      // stekker openingen
      translate([0,BW/2-18/2,2]) cube([BL,18,10]);
      
      // schroefgaten top
      translate([0,10,(BH-2)/2+2]) rotate([90,0,90]) cylinder(BL+4,1.5,1.5);
      translate([0,BW-10,(BH-2)/2+2]) rotate([90,0,90]) cylinder(BL+4,1.5,1.5);
      
    }
    
    // Accu basis
    difference() {
      translate([0,-2,6]) cube([BL,BW+4,2]);
      // battery cutout
      translate([2,5,0]) cube([BL-4,BW-10,BH]);
      // schuine kanten
      translate([2,5,6]) rotate([90,0,0]) prism(BL,2,2);
      translate([2,BW-3,8]) rotate([180,0,0]) prism(BL,2,2);
      
      // stekker openingen
      translate([0,BW/2-18/2,2]) cube([BL,18,10]);

      // schroefgaten top
      translate([0,10,(BH-2)/2+2]) rotate([90,0,90]) cylinder(BL+4,1.5,1.5);
      translate([0,BW-10,(BH-2)/2+2]) rotate([90,0,90]) cylinder(BL+4,1.5,1.5);
      
    }
    
    // versteviging in het midden
    translate([BL/2-5,-2,0]) cube([10,BW+4,1]);
  }

  // bevestiging op bodemplaat
  translate([0,-100,0]) difference() {
    translate([-10,0,0]) cube([BL+2*10,BW,2]);
    translate([2,0,0]) cube([BL-4,BW,BH-2]);
    translate([-5,10,0]) cylinder(40,1.6,1.6);
    translate([-5,BW-10,0]) cylinder(40,1.6,1.6);
    translate([BL+10-5,10,0]) cylinder(40,1.6,1.6);
    translate([BL+10-5,BW-10,0]) cylinder(40,1.6,1.6);
  }
  /*
  translate([0,-100,-2]) difference() {
    translate([-12,BW/2-69.5/2,0]) cube([BL+2*12,69.5,4]);
    translate([-10,BW/2-69.5/2,0]) cube([BL+2*10,69.5,4]);
  } 
*/  
  
}
  
if( BATTERYTOP ) {
  translate([0,-180,0]) {
    difference() {
      translate([0,0,0]) cube([BL+4,BW,BHT]);
      translate([BL/2-20,5,0]) cube([40,BW-10,BHT]);
      translate([2,2,0]) cube([BL,BW-4,BHT-2]);
      translate([2,0,0]) cube([BL,BW,BHT-2-10]);
      translate([0,10,(BH-2)/2]) rotate([90,0,90]) cylinder(BL+4,1.6,1.6);
      translate([0,BW-10,(BH-2)/2]) rotate([90,0,90]) cylinder(BL+4,1.6,1.6);
      translate([0,BW/2-18/2,0]) cube([BL+4,18,10]);
      
      translate([0,BW/2,BHT/1.6]) rotate([90,0,90]) cylinder(BL+4,9,9);
      
      translate([(BL+4)/2-WIOPI_CX/2,BW/2-WIOPI_CY/2,BHT-2]) WIOPI(10);
    }
    
    difference() {
      translate([-4,0,0]) cube([BL+4+8,2,BHT]);
      translate([0,0,0]) cube([BL+4,2,BHT]);
    }
    difference() {
      translate([-4,BW-2,0]) cube([BL+4+8,2,BHT]);
      translate([0,BW-2,0]) cube([BL+4,2,BHT]);
    }
    
  }
}


if( BOTTOM ) {
    difference() {
    translate([0,-2,0]) cube([L,W+2*2,2]);
    // kabel opening
    //translate([L/6,W/2,0]) cylinder(4,10,10);
    translate([10,W/2-W2/2,0]) rcube(2,30,W2,10);
    translate([L/2.3,W/2,0]) cylinder(4,10,10);
    
    // motor sparing
    translate([L-L2,W/2-W2/2,0]) cube([L2,W2,2]);
    
    // deksel
    if( NS2400 ) {
      translate([52,14,0]) difference() {
        cylinder(.4, 4,4);
        cylinder(.4, 3,3);
      }
      translate([52,W-14,0]) difference() {
        cylinder(.4, 4,4);
        cylinder(.4, 3,3);
      }
    }
    
    // gaten battery bottom
    translate([(BL+10)/2,W/2-(BW-20)/2,0]) cylinder(20, 1.4, 1.4);
    translate([(BL+10)/2,W/2+(BW-20)/2,0]) cylinder(20, 1.4, 1.4);

    
    // gaten voor railing
    if( RAILINGFRONT ) {
      if( NS2400 ) {
        translate([L-38,1,0]) cylinder(4, 1.6, 1.6);
        translate([L-38-70,1,0]) cylinder(4, 1.6, 1.6);
        translate([L-38,W-1,0]) cylinder(4, 1.6, 1.6);
        translate([L-38-70,W-1,0]) cylinder(4, 1.6, 1.6);
      }
      else if( NS6400 ) {
        translate([26,1,0]) cylinder(4, 1.6, 1.6);
        translate([26+48,1,0]) cylinder(4, 1.6, 1.6);
        translate([26+48*2,1,0]) cylinder(4, 1.6, 1.6);
        translate([26,W-1,0]) cylinder(4, 1.6, 1.6);
        translate([26+48,W-1,0]) cylinder(4, 1.6, 1.6);
        translate([26+48*2,W-1,0]) cylinder(4, 1.6, 1.6);
      }
    }
    else {
      if( NS2400 ) {
      // 70 - (L-38-70)
        translate([70 - (L-38-70),1,0]) cylinder(4, 1.6, 1.6);
        translate([70 - (L-38-70),W-1,0]) cylinder(4, 1.6, 1.6);
      }
      else if( NS6400 ) {
        translate([24,1,0]) cylinder(4, 1.6, 1.6);
        translate([24+88,1,0]) cylinder(4, 1.6, 1.6);
        translate([24,W-1,0]) cylinder(4, 1.6, 1.6);
        translate([24+88,W-1,0]) cylinder(4, 1.6, 1.6);
      }
    }
  }

  translate([0,POFFSET,0]) difference() {
    translate([0,0,2]) cube([L,2,8]);
    // bevestigingsgaten
    translate([20,0,6]) rotate([270,0,0]) cylinder(W, .6, .6);
    translate([L-20,0,6]) rotate([270,0,0]) cylinder(W, .6, .6);
  }

  translate([0,-POFFSET,0]) difference() {
    translate([0,W-2,2]) cube([L,2,8]);
    // bevestigingsgaten
    translate([20,0,6]) rotate([270,0,0]) cylinder(W, .6, .6);
    translate([L-20,0,6]) rotate([270,0,0]) cylinder(W, .6, .6);
  }

  // dwars versteviging
  translate([L-L2-8,12+POFFSET,2]) cube([2,W-2*(12+POFFSET),4]);

  if( GLUEBAR ) {
    // lijm strook
    translate([-4,12+POFFSET,2]) cube([8,W-2*(12+POFFSET),2]);
  }

  // front lijm strook
  if( POFFSET > 4 ) {
    difference() {
      translate([L-4,2,2]) cube([8,W-2*2,2]);
      translate([L-4,POFFSET,2]) cube([8,W-2*POFFSET,2]);
    }
  }
  else {
    difference() {
      translate([L-4,8+POFFSET,2]) cube([8,W-2*(8+POFFSET),2]);
      // motor sparing
      translate([L-L2,W/2-W2/2,2]) cube([L2+4,W2,2]);
    }
  }

}
