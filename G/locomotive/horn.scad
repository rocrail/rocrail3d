/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=180;
include <../../lib/shapes.scad>



  difference() {
    translate([0,0,0]) cylinder(20,5,2);
    translate([0,0,0]) cylinder(12,4,1.5);
  }
  difference() {
    translate([0,0,20]) cylinder(6,2,2);
    //translate([0,0,20]) cylinder(6,.75,.75);
  }

  difference() {
    translate([0,0,28]) sphere(5);
    translate([-5,-5,32]) cube([10,10,10]);
    translate([0,5,28]) rotate([90,0,0]) cylinder(5,1,1);
  }