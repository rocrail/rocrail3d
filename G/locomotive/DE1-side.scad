/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../../lib/shapes.scad>


L=200; // head=180 middle=200 middle2=108
W=20;
BW=90;
BBW=118; // overall width + 2 x 2mm wall
BBR=116; // head radius
H1=400*.2+5; // total hight
H2=320*.2+5; // 

SIDE1=1;
SIDETYPE=1; // 0=head, 1=middle
SIDE1FLIP=0;
SIDEBOARD1=0;
BAR=0;
COVER=1;
ROAST=0;
WCFRAME=0;
WCWINDOW=0;
WINFRAME=0;


module doorCut(width, height, z, r) {
  difference() {
    windowCut( width, height, z, r);
    translate([.5,.5,0]) windowCut( width-1.0, height-1.0, z, r-.5);
  }
}

module winFrame(width, hight, flap, flens, z) {
  if( flap )
    translate([0,hight*.7,0]) cube([width, 1, 1]);
  difference() {
    translate([0,0,0]) roundedcube(width, hight, z, 2.5);
    translate([1.2,1.2,0]) roundedcube(width-2.4, hight-2.4, z, 1.75);
  }
  
  if( flens ) {
    difference() {
      translate([-1.2,-1.2,0]) roundedcube(width+2.4, hight+2.4, .4, 2.5);
      translate([1.2,1.2,0]) roundedcube(width-2.4, hight-2.4, 2, 1.75);
    }
    /*
    translate([width+10,0,0]) difference() {
      translate([-1,-1,0]) roundedcube(width+2, hight+2, 1.2, 2.5);
    translate([0,0,0]) roundedcube(width, hight, z, 2.5);
    }
    */
  }

}


if( ROAST ) {
  translate([200,0,0]) {

    difference() {
      translate([0,0,0]) roundedcube(18.5, 28.5, 3, 2.5);
      translate([2,2,.5]) cube([18.5-4, 28.5-4, 2.5]);
      translate([3,3,0]) cube([18.5-6, 28.5-6, 3]);

    }
    for( a =[0:7] ) {
      translate([2,a*3+4.0,0]) rotate([45,0,0]) cube([19-4, 1, 2]);
    }
    
  }
}

if( WINFRAME ) {
  translate([200,0,0]) {
    //winFrame(38.5, 28.5, true, false, 2); // main windows
    //winFrame(18.5, 23.5, true, false, 2); // center door
    //winFrame(10.5, 24.4, false, false, 2); // CAB door
    winFrame(22.0, 30.0, false, true, 4); // CAB side
    //winFrame(30.0, 32.4, false, true, 3.5); // CAB front
  }
}


if( WCFRAME ) {
  translate([200,0,0]) {
    difference() {
      translate([0,0,0]) roundedcube(18.5, 28.5, 3, 2.5);
      translate([2,2,.5]) cube([18.5-4, 28.5-4, 2.5]);
      translate([2.5,2.5,0]) cube([18.5-5, 28.5-5, 3]);
    }
  }
}
if( WCWINDOW ) {
  translate([220,0,0]) {
    translate([2,2,0]) cube([18.5-4-.2, 28.5-4-.2, .6]);
  }
}



if( BAR ) {
  translate([0,-40,0]) {
    difference() {
      translate([0,-3.3,0]) cube([BBW-4, 3.3, 3.3]);
      translate([10,-3.3,0]) cube([BBW-4-20, 3.3, 3.3]);
    }
    
      // schuine kanten
    translate([12,8,3.3]) rotate([180,90,0]) prism(3.3, 11.3, 10);
    translate([BBW-6,-3.3,3.3]) rotate([270,90,0]) prism(3.3, 10, 11.3);
    
    
    
    translate([7.75,0,0]) cube([BBW-4-15.5, 3.3, 3.3]);
    difference() {
      translate([(BBW-4)/2-BW/2,3.3,0]) cube([BW, 5, 3.3]);
      translate([(BBW-4)/2-12.6/2,3.3,0]) cube([12.6, 5, 3.3]);

      translate([(BBW-4)/2-(BW-6)/2,3.3,0]) cube([BW-6-BW/2-7, 2.3, 3.3]);
      translate([(BBW-4)/2-(BW-6)/2+BW/2+7,3.3,0]) cube([BW-6-BW/2-7, 2.3, 3.3]);
    }
  }
}

if( SIDEBOARD1 ) {
  translate([0,-20,0]) {
    translate([0,0,0]) cube([L, 5, 2]);
    if( SIDETYPE == 0 )
      translate([90,0,2]) cube([60, 5, 2]);
  }
}

if( SIDE1 ) {
  mirror([0,SIDE1FLIP,0]) translate([0,0,0]) {
    
    if( SIDETYPE == 0 ) {
      // deurknop
      translate([163,28,0.2]) rotate([90,0,90]) cylinder(5, .5, .5);
      translate([168,28,0.2])  cylinder(1, 1.5, 1.5);
      translate([168,28,-0.4])  cylinder(2, .5, .5);
      
      translate([-5,10,2]) cube([10, H2-10, 1.2]);
    }
    if( SIDETYPE == 1 ) {
      translate([L-5,10,2]) cube([10, H2-10, 1.2]);
    }
    
    if( COVER ) {
      // Cover
      difference() {
        translate([5,10,5]) cube([L-10, H2+6, 1]);
        translate([20,0,0]) translate([0+2,H2-8+2,2]) cube([4, 4, 4]);
        translate([L-(SIDETYPE == 0 ?20:41)-8,0,0]) translate([0+2,H2-8+2,2]) cube([4, 4, 4]);

        translate([10,30,0]) windowCut( 40, 30, 6, 2.5);
        translate([60,30,0]) windowCut( 40, 30, 6, 2.5);
        
        if( SIDETYPE == 1 ) {
          translate([125+5,38,0]) windowCut( 20, 25, 6, 2.5);
          translate([125+5+30,38,0]) windowCut( 20, 25, 6, 2.5);
        }        

        if( SIDETYPE == 0 ) {
          translate([120,30,0]) windowCut( 20, 30, 6, 2.5);
          translate([150+4,30,0]) windowCut( 20-8, 30-4, 6, 2.5);
        }
      }
    }
    
    translate([5,10,2]) cube([2, H2-10, 4]);
    translate([L-5-2,10,2]) cube([2, H2-10, 4]);
    
    translate([5,10,2]) cube([L-10, 2, 4]);
    translate([5,24,2]) cube([L-10, 2, 4]);
    translate([5,H2-2,2]) cube([L-10, 2, 4]);
    
    translate([20,0,0]) difference() {
      translate([0,H2-8,2]) cube([8, 8, 4]);
      translate([0+2,H2-8+2,2]) cube([4, 4, 4]);
    }
    translate([L-(SIDETYPE == 0 ?20:41)-8,0,0]) difference() {
      translate([0,H2-8,2]) cube([8, 8, 4]);
      translate([0+2,H2-8+2,2]) cube([4, 4, 4]);
    }
    
    difference() {
      translate([0,0,0]) cube([L, H2, 2]);

/*
      for( b =[0:4] ) {
        translate([15+b*((L-30)/4),2.5,0]) cylinder(2, 1.1, 1.1);
      }
*/
      translate([10,2.5,0]) cylinder(2, 1.1, 1.1);
      translate([L-10,2.5,0]) cylinder(2, 1.1, 1.1);

      translate([10,30,0]) windowCut( 40, 30, 2, 2.5);
      translate([60,30,0]) windowCut( 40, 30, 2, 2.5);

      if( SIDETYPE == 1 ) {
        translate([125+5,38,0]) windowCut( 20, 25, 2, 2.5);
        translate([125+5+30,38,0]) windowCut( 20, 25, 2, 2.5);
        translate([125+30-.4,2,0]) cube([.8, 66,1.2]);
        translate([125,2,0]) doorCut( 60, 66, .8, 2.5);
        translate([125,2,0]) windowCut( 60, 66, .4, 2.5);
      }
      
      if( SIDETYPE == 0 ) {
        translate([120,30,0]) windowCut( 20, 30, 2, 2.5);
        translate([150,5,0]) doorCut( 20, 55, .6, 2.5);
        translate([146.5,1,0]) cube( [1.5, 35, 1] );
        translate([170+2,1,0]) cube( [1.5, 35, 1] );
        translate([150,5,0]) windowCut( 20, 55, .4, 2.5);
        translate([150+4,30,0]) windowCut( 20-8, 30-4, 2, 2.5);
      }
    }

    translate([10,30,0]) doorCut( 40, 30, 6, 2.5);
    translate([60,30,0]) doorCut( 40, 30, 6, 2.5);
    
    if( SIDETYPE == 1 ) {
      translate([125+5,38,0]) doorCut( 20, 25, 6, 2.5);
      translate([125+5+30,38,0]) doorCut( 20, 25, 6, 2.5);
    }
    if( SIDETYPE == 0 ) {
      translate([120,30,0]) doorCut( 20, 30, 6, 2.5);
      translate([150+4,30,0]) doorCut( 20-8, 30-4, 6, 2.5);
    }
    
    difference() {
      for(i = [0:80]) {
      translate([0,H2+i*.2,(i*.1)/2]) cube([L, .2, 2]);
      }
    }
  }
}