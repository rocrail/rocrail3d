/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
//$fn=64;
include <../../lib/shapes.scad>

W=110; // breedte over de U-Profielen + 2 x 2mm.
L=70;
H=35;

TANK=0;
TECHPOWER=1;
POFFSET=10; // profiel offset
CYLINDER=0;
CYLINDER2=0; // met tank input
TANKDOP=0;
ZANDDOP=1;


module RFID(x,y,z) {
  translate([x,y,z]) rotate([0,0,90]) {
    // ID12LA socket
      difference() {
        translate([(40-28.2)/2, (31.5-27.2)/2,0]) cube([28.2, 27.2, 5]);
        translate([(40-28.2)/2+1, (31.5-27.2)/2+1,0]) cube([26.2, 25.2, 5]);
        
      translate([33,18,2]) cube([2, 5, 5]);
      }
      
    // RFID print bevestiging  
    translate([-1,0,0]) {
      difference() {
        translate([3,(31.5-20)/2,0]) cylinder(10, 2.5, 2.5);
        translate([3,(31.5-20)/2,2]) cylinder(12, 1, 1);
      }  
      translate([2.5,3.5,0])  cube([5, 3, 5]);
      
      difference() {
        translate([3,(31.5-20)/2+20,0]) cylinder(10, 2.5, 2.5);
        translate([3,(31.5-20)/2+20,2]) cylinder(8, 1, 1);
      }  
      translate([2.5,25,0])  cube([5, 3, 5]);

      difference() {
        translate([3+34,(31.5-20)/2,0]) cylinder(10, 2.5, 2.5);
        translate([3+34,(31.5-20)/2,2]) cylinder(8, 1, 1);
      }  
      //translate([1.5+34,4,0])  cube([3, 20, 6]);

      difference() {
        translate([3+34,(31.5-20)/2+20,0]) cylinder(10, 2.5, 2.5);
        translate([3+34,(31.5-20)/2+20,2]) cylinder(12, 1, 1);
      }  
    }
  }   
}




if( TANK ) {
  RFID(L/2+27.2/2+2.3,W/2-20,0);
  
  if( TECHPOWER ) {
    translate([L/2-46/2-8,12,0]) {
      difference() {
        translate([8,-10,0]) cube([46, 12, 20]);
        translate([10,-10,0]) cube([42, 10, 18]);
        translate([0,-15+3,15]) rotate([270,0,0]) prism(L, 15, 15);

        // flip switch
        rotate([90,0,0]) translate([18,4+6,-2]) cylinder(2, 3.1, 3.1);

        // XT60 chassis
        translate([29.8,0,6]) cube([12.2, 2, 8.2]);
        rotate([90,0,0]) translate([42.2,4+6,-2]) cylinder(2, 4.1, 4.1);
      }
    }
  }
  
  
  difference() {
    translate([0,0,0]) cube([L,W,H]);
    translate([2,15,2]) cube([L-4,W-2*15,H]);
    translate([2,2,15]) cube([L-4,W-4,H]);

    translate([0,0,15]) rotate([270,0,0]) prism(L,15,15);
    translate([2,15+0,2]) rotate([90,0,0]) prism(L-4,14,14);

    translate([0,W-15,0]) rotate([0,0,0]) prism(L,15,15);
    translate([2,W-1,14+2]) rotate([180,0,0]) prism(L-4,14,14);

    if( TECHPOWER )
      translate([L/2-42/2,0,2]) cube([42, 12, 14]);
    
    // gaten voor de cylinders
    translate([-5,W/2-30,H-16]) rotate([90,0,90]) cylinder(L+10,2,2);
    translate([-5,W/2+30,H-16]) rotate([90,0,90]) cylinder(L+10,2,2);
}

  // versmalling in de breedte
  translate([0,0,H-2]) cube([L,10+POFFSET,2]);
  translate([0,W-10-POFFSET,H-2]) cube([L,10+POFFSET,2]);

    
  // boven stuk
  difference() {
    translate([0,8+POFFSET,H]) cube([L,W-2*8-2*POFFSET,8]);
    translate([2,8+2+POFFSET,H]) cube([L-4,W-2*8-4-2*POFFSET,8]);
    // bevestigings gaten
    translate([10,W,H+4]) rotate([90,0,0]) cylinder(W+10,1.6,1.6);
    translate([L-10,W,H+4]) rotate([90,0,0]) cylinder(W+10,1.6,1.6);
  }



}

if( CYLINDER ) translate([L+40,0,0]) {
  R=10;
  difference() {
    translate([0,R,R]) rotate([270,0,0]) cylinder(W-2*R,R,R);
    translate([0,R,R]) rotate([270,0,0]) cylinder(W-2*R,R-1,R-1);
  }
  translate([0,W/2-30,R+8]) cylinder(8,1.9,1.9);
  translate([0,W/2+30,R+8]) cylinder(8,1.9,1.9);
  
  difference() {
    translate([0,R,R]) sphere(R);
    translate([0,R,R]) sphere(R-1);
    translate([-R,R,0]) cube([2*R,2*R,2*R]);
  }
  difference() {
    translate([0,W-R,R]) sphere(R);
    translate([0,W-R,R]) sphere(R-1);
    translate([-R,W-R-2*R,0]) cube([2*R,2*R,2*R]);
  }
}



module pressCylinder(R,W) {
  difference() {
    translate([0,R,R]) rotate([270,0,0]) cylinder(W-2*R,R,R);
    translate([0,R,R]) rotate([270,0,0]) cylinder(W-2*R,R-1,R-1);
  }

  difference() {
    translate([0,R,R]) sphere(R);
    translate([0,R,R]) sphere(R-1);
    translate([-R,R,0]) cube([2*R,2*R,2*R]);
  }
  difference() {
    translate([0,W-R,R]) sphere(R);
    translate([0,W-R,R]) sphere(R-1);
    translate([-R,50-R-2*R,0]) cube([2*R,2*R,2*R]);
  }
}

if( CYLINDER2 ) translate([L+60,0,0]) {
  translate([0,0,0]) pressCylinder(4,50);
  translate([0,60,0]) pressCylinder(4,30);

  translate([0,50-1,4]) rotate([270,0,0]) cylinder(12,1.5,1.5);

  difference() {
    translate([-3.75,50+5,-3]) rotate([50,0,90]) cylinder(12,2.2,2.2);
    translate([-10,50-5,-5]) cube([20,20,5]);
  }
}

if( TANKDOP ) translate([L+80,0,0]) {
  difference() {
    translate([0,0,0]) cylinder(2,3,3);
    translate([0,0,.8]) cylinder(2,2.3,2.3);
  }
}

if( ZANDDOP ) translate([L+80,0,0]) {
  difference() {
    translate([0,0,0]) cylinder(1.8,2.5,2.5);
    translate([0,0,.6]) cylinder(2,2.1,2.1);
  }
}
