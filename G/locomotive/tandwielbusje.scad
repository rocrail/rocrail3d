/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../lib/shapes.scad>

difference() {
  translate([0,0,0]) cylinder(1, 5.0, 5.0);
  translate([0,0,0]) cylinder(1, 1.65, 1.65);
}
difference() {
  translate([0,0,0]) cylinder(17, 2.55, 2.55);
  translate([0,0,0]) cylinder(17, 1.65, 1.65);
}
