/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../../lib/shapes.scad>


L=80; // head=180 middle=200 middle2=108
H=.4;

LOGO=0;
TOUR=0;
NSLOGO=1;
CABNR=0;

logo="NS-Logo.png";
SCALEF=.015;

if( NSLOGO ) {
translate([0,0,-.5]) scale([SCALEF,SCALEF,.008]) surface(file=logo);
}

if( LOGO ) {
difference() {
  translate([0,0,0]) cylinder(H, 8, 8);
  translate([0,0,0]) cylinder(H, 6, 6);
}


difference() {
  translate([-(L/2),3,0]) roundedcube(L, 2, H, 1);
  translate([0,0,0]) cylinder(H, 6, 6);
}
difference() {
translate([-((L-10)/2),-1,0]) roundedcube(L-10, 2, H, 1);
  translate([0,0,0]) cylinder(H, 6, 6);
}
difference() {
translate([-((L-20)/2),-5,0]) roundedcube(L-20, 2, H, 1);
  translate([0,0,0]) cylinder(H, 6, 6);
}


translate([0,0,0]) {
  translate([-4.3,-5.5,0]) linear_extrude(H) text("R",9);
}

}

if( TOUR ) {

translate([0,20,0]) {
  translate([-4.3,-5.5,0]) linear_extrude(.6) text("Rocrail on Tour",16, font="Helvetica");
  translate([-3.5,-5.9,0]) cube([142, .6, .6]);
  translate([55.75,4,0]) cube([.4, 5, .6]);
}
}


if( CABNR ) {

translate([0,0,0]) {
  translate([0,0,0]) linear_extrude(.6) text("1  2  2  1",8, font="Helvetica");
}
}
