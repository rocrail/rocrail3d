/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../../lib/shapes.scad>

BW=90;
W=118;
HoH=90;
L=HoH+20;
H=35;
D=10.0;

WP=20;
LP=HoH+2*18;
LPB=140;

CELLAR=0;
PROFILE=0;
BOTTOM=0;
WIOMOUNT=0;
HEAD=1;
HEADSTEP=0;
BUFFER=0;
WIOPICAP=0;
DOORSTEP=0;
DOORSTEPTOP=0;

if( DOORSTEPTOP ) {
    // rondingen
    translate([2,10-2,0]) cylinder(.6,2,2);
    translate([36-2,10-2,0]) cylinder(.6,2,2);
    translate([0,0,0]) cube([2, 10-2, .6]);
    translate([36-2,0,0]) cube([2, 10-2, .6]);

    // step
    translate([2,0,0]) cube([36-4, 10, .6]);
}


if( DOORSTEP ) {
  translate([0,0,0]) {
    difference() {
      translate([0,0,0]) cube([W-4, 16, .6]);
      translate([(W-4)/2-68/2,0,0]) cylinder(1.2,6.1,6.1);
      translate([(W-4)/2+68/2,0,0]) cylinder(1.2,6.1,6.1);
    }
    translate([0,-8,0]) cube([16.9, 8, .6]);
    translate([16.9+12.2,-8,0]) cube([55.8, 8, .6]);
    translate([W-4-16.9,-8,0]) cube([16.9, 8, .6]);
  }  
  translate([(W-4)/2-36/2,16,0]) rotate([90,0,0]) {
    // rondingen
    translate([2,10-2,0]) cylinder(1.2,2,2);
    translate([36-2,10-2,0]) cylinder(1.2,2,2);
    translate([0,0,0]) cube([2, 10-2, 1.2]);
    translate([36-2,0,0]) cube([2, 10-2, 1.2]);

    // step
    translate([2,0,0]) cube([36-4, 10, 1.2]);
    translate([0,0,0]) cube([36, 1.2, 6]);
    
    translate([1,8,0]) mirror([0,1,0]) prism(2, 8, 6);
    translate([36-3,8,0]) mirror([0,1,0]) prism(2, 8, 6);
  }
}



if( HEADSTEP ) {
  translate([120,0,0]) {
    difference() {
      translate([0,0,0]) cube([18, 17, 10]);
      translate([0,2,8]) cube([18, 17, 10]);
      translate([2,2,0]) cube([18-4, 6, 8]);
      translate([2,2+8,0]) cube([18-4, 6, 8]);
    }
  }
}


module CAPScrew(x,y) {
  difference() {
    translate([x,y,0]) cylinder(35, 4, 4);
    translate([x,y,0]) cylinder(33, 3, 3);
    translate([x,y,0]) cylinder(35, 1.6, 1.6);
  }
}

if( WIOPICAP ) {
  translate([120,0,0]) {
    CAPScrew(90/2-20,-2);
    CAPScrew(90/2+20,-2);
    CAPScrew(90/2-20,70+2);
    CAPScrew(90/2+20,70+2);

    difference() {
      translate([0,0,0]) cube([90, 70, 35]);
      translate([90/2-8/2,10,0]) cube([8, 8, 2]);
      translate([2,2,2]) cube([90-4, 70-4, 35]);
      translate([0,2,28]) cube([90, 70-4, 35]);
      translate([90/2-14/2,0,35-7]) cube([14, 70, 7]);
      translate([0,70/2-30/2,2]) cube([10, 30, 35]);
    }
  }
}
  
// Buffer
if( BUFFER ) {
translate([-30,0,0]) {
  // Plate
  difference() {
    roundedcube(20,14,2,2);
    //translate([20/2,14/2,0]) cylinder(.4, 4, 4);
  }
  // Spring cylinder
  difference() {
    translate([20/2,14/2,2]) cylinder(14, 4, 4);
    translate([20/2,14/2,4]) cylinder(12, 3.1, 3.1);
  }
}
}


if( HEAD ) {
  translate([0,0,0]) {


    difference() {
      translate([W/2+68/2,20+12,18]) rotate([90,0,0]) cylinder(12, 5, 6);
      translate([W/2+68/2,20+12,18]) rotate([90,0,0]) cylinder(12, 4, 4);
    }
    
    difference() {
      translate([W/2-68/2,20+12,18]) rotate([90,0,0]) cylinder(12, 5, 6);
      translate([W/2-68/2,20+12,18]) rotate([90,0,0]) cylinder(12, 4, 4);
    }

    difference() {
      translate([2,-40,0]) cube([12, 58, 10]);
      translate([2+2,-40,0]) cube([12-4, 58, 10-2]);
      // screw holes left
      translate([8,-20,0]) cylinder(10, 1.6, 1.6);
      translate([8,-20,9]) cylinder(1, 1.6, 3.2);
      translate([8,-30,0]) cylinder(10, 1.6, 1.6);
      translate([8,-30,9]) cylinder(1, 1.6, 3.2);
    }

    difference() {
      translate([W-12-2,-40,0]) cube([12, 58, 10]);
      translate([W-12-2+2,-40,0]) cube([12-4, 58, 10-2]);
      // screw holes right
      translate([W-8,-20,0]) cylinder(10, 1.6, 1.6);
      translate([W-8,-20,9]) cylinder(1, 1.6, 3.2);
      translate([W-8,-30,0]) cylinder(10, 1.6, 1.6);
      translate([W-8,-30,9]) cylinder(1, 1.6, 3.2);
    }
    
    difference() {
      translate([0,0,0]) cube([W, 20, 25+8]);
      translate([12,0,2]) cube([W-2*12, 20-2, 25+8-2]);
      translate([2+2,-40,0]) cube([12-4, 53-8, 10-2]);
      translate([W-12-2+2,-40,0]) cube([12-4, 53-8, 10-2]);
    }



    difference() {
      translate([0,-18,0]) mirror([0,0,0]) prism(12, 18, 25+8);
      translate([0,-24.5,0]) cube([W, 12, 10]);
      translate([2+2,-40,0]) cube([12-4, 58, 10-2]);
    }


    difference() {
      translate([W-12,-18,0]) mirror([0,0,0]) prism(12, 18, 25+8);
      translate([0,-24.5,0]) cube([W, 12, 10]);
      translate([W-12-2+2,-40,0]) cube([12-4, 58, 10-2]);
    }

  }
}




module Flip(x,y,z) {
  translate([x,y,z]) {
    translate([2,0,0]) rotate([0,0,90]) {
      // flip switch
      rotate([90,0,0]) translate([18,4+6,-2]) cylinder(2, 3.1, 3.1);

      // XT60 chassis
      translate([29.8,0,6]) cube([12.2, 2, 8.2]);
      rotate([90,0,0]) translate([42.2,4+6,-2]) cylinder(2, 4.1, 4.1);

      }    
    }
}
    

module RFID(x,y,z) {
  translate([x,y,z]) rotate([0,0,90]) {
    // ID12LA socket
      difference() {
        translate([(40-28.2)/2, (31.5-27.2)/2,0]) cube([28.2, 27.2, 5]);
        translate([(40-28.2)/2+1, (31.5-27.2)/2+1,0]) cube([26.2, 25.2, 5]);
        
      translate([33,18,2]) cube([2, 5, 5]);
      }
      
    // RFID print bevestiging  
    translate([-1,0,0]) {
      difference() {
        translate([3,(31.5-20)/2,0]) cylinder(10, 2.5, 2.5);
        translate([3,(31.5-20)/2,2]) cylinder(12, 1, 1);
      }  
      translate([2.5,3.5,0])  cube([5, 3, 5]);
      
      difference() {
        translate([3,(31.5-20)/2+20,0]) cylinder(10, 2.5, 2.5);
        translate([3,(31.5-20)/2+20,2]) cylinder(8, 1, 1);
      }  
      translate([2.5,25,0])  cube([5, 3, 5]);

      difference() {
        translate([3+34,(31.5-20)/2,0]) cylinder(10, 2.5, 2.5);
        translate([3+34,(31.5-20)/2,2]) cylinder(8, 1, 1);
      }  
      //translate([1.5+34,4,0])  cube([3, 20, 6]);

      difference() {
        translate([3+34,(31.5-20)/2+20,0]) cylinder(10, 2.5, 2.5);
        translate([3+34,(31.5-20)/2+20,2]) cylinder(12, 1, 1);
      }  
    }
  }   
}


module Holes() {
      // zijkant schroefgaten
  translate([0,0,H-2.5]) rotate([0,90,0]) cylinder(W, 1.2, 1.2);
  translate([0,HoH,H-2.5]) rotate([0,90,0]) cylinder(W, 1.2, 1.2);
  
  
}

if( CELLAR ) {

RFID(W/2+27.2/2+4.3,HoH/2-18,0);


    // In between walls
    translate([0,-2,2*D]) cube([W, 2, H-2*D-5]);
    translate([0,HoH,2*D]) cube([W, 2, H-2*D-5]);

    // wall
    difference() {
      translate([0,0,0]) cube([2, HoH, H]);
      Flip(0,0,0);
      Holes();
    }
    difference() {
    translate([0,-D,D]) cube([2, HoH+2*D, H-D]);
      Flip(0,0,0);
      Holes();
    }
    
  difference() {
    translate([0,-26.5,35]) mirror([0,0,1]) prism(2, 18, 30);
    translate([0,-18-12,35-8]) cube([W, 8, 8]);
    translate([0,HoH+18+4,35-8]) cube([W, 8, 8]);
  }
  difference() {
    translate([2,HoH+26.5,35]) mirror([0,0,1]) rotate([0,0,180]) prism(2, 18, 30);
    translate([0,-18-12,35-8]) cube([W, 8, 8]);
    translate([0,HoH+18+4,35-8]) cube([W, 8, 8]);
  }

    // bottom
    translate([0,0,0]) cube([W, HoH, 2]);
    // wall
    difference() {
    translate([W-2,0,0]) cube([2, HoH, H]);
      Holes();
    }
    difference() {
    translate([W-2,-D,D]) cube([2, HoH+2*D, H-D]);
      Holes();
    }
    
    
  difference() {
    translate([W-2,-26.5,35]) mirror([0,0,1]) prism(2, 18, 30);
    translate([0,-18-12,35-8]) cube([W, 8, 8]);
    translate([0,HoH+18+4,35-8]) cube([W, 8, 8]);
  }
  difference() {
    translate([W,HoH+26.5,35]) mirror([0,0,1]) rotate([0,0,180]) prism(2, 18, 30);

    translate([0,-18-12,35-8]) cube([W, 8, 8]);
    translate([0,HoH+18+4,35-8]) cube([W, 8, 8]);
  }



  difference() {
   translate([0,0,10]) rotate([0,90,0]) cylinder(W, 10.0, 10.0);
   translate([2,0,10]) rotate([0,90,0]) cylinder(W-4, 10.0-2, 10.0-2);
    translate([2,0,0]) cube([W-4, 2*D, 2*D]);
  }


  
  difference() {
   translate([0,HoH,10]) rotate([0,90,0]) cylinder(W, 10.0, 10.0);
   translate([2,HoH,10]) rotate([0,90,0]) cylinder(W-4, 10.0-2, 10.0-2);
    translate([2,HoH-2*D,0]) cube([W-4, 2*D, 2*D]);
  }
  
}



  
if( PROFILE ) {
  translate([0,-40,0]) {
    
    // Zijkant
    difference() {
      translate([0,0,0]) cube([LP, WP, 2]);

      translate([LP/2+HoH/2,WP-3,0]) cylinder(2, 1.1, 1.1);
      translate([LP/2-HoH/2,WP-3,0]) cylinder(2, 1.1, 1.1);

      
    }

    // Bodem
    difference() {
      translate([0,4,0]) cube([LP, 2, 18]);

      for( b =[0:2] ) {
        translate([15+b*((LP-30)/2),WP-3,6]) rotate([90,0,0]) cylinder(20, 1.6, 1.6);
        translate([15+b*((LP-30)/2),WP-3,15]) rotate([90,0,0]) cylinder(20, 1.6, 1.6);
      }
    }
    
    // H-Profiel binnen zijde
    translate([0,4,10]) cube([LP, 3, 2]);
    
  }
}

 
if( BOTTOM ) {
  translate([0,2*W,0]) {
    difference() {
      translate([-(LPB-LP)/2,0,0]) cube([LPB, BW, 2]);
      //translate([L/3*1.75,BW/3,0]) cube([L/3, BW/3, 2]);

      // bevestigings gaten profiel HoH 84
      for( b =[0:2] ) {
        translate([15+b*((LP-30)/2),(BW-84)/2,0]) cylinder(2, 1.6, 1.6);
        translate([15+b*((LP-30)/2),BW-(BW-84)/2,0]) cylinder(2, 1.6, 1.6);
      }
      
      // RFID/Power kabel doorvoer
      translate([5,BW/4,0]) cylinder(12, 9, 9);
      translate([LP-5,3*(BW/4),0]) cylinder(12, 9, 9);
      
    }
    
    // Zijdelingse versteviging
    translate([-(LPB-LP)/2,10,0]) cube([LPB, 2, 8]);
    translate([-(LPB-LP)/2,BW-10-2,0]) cube([LPB, 2, 8]);

    
    // Kopse versteviging
    difference() {
      translate([-(LPB-LP)/2,10,0]) cube([2, BW-20, 8]);
      translate([-(LPB-LP)/2,BW/4,4]) rotate([0,90,0]) cylinder(LPB, 1.5, 1.5);
      translate([-(LPB-LP)/2,3*(BW/4),4]) rotate([0,90,0]) cylinder(LPB, 1.5, 1.5);
    }
    difference() {
      translate([LP-2+(LPB-LP)/2,10,0]) cube([2, BW-20, 8]);
      translate([-(LPB-LP)/2,BW/4,4]) rotate([0,90,0]) cylinder(LPB, 1.5, 1.5);
      translate([-(LPB-LP)/2,3*(BW/4),4]) rotate([0,90,0]) cylinder(LPB, 1.5, 1.5);
    }
    difference() {
      translate([LP/2-2,10,0]) cube([2, BW-20, 8]);
    }
    
  }
}


 
if( WIOMOUNT ) {
  translate([0,2*W,0]) {
    difference() {
      translate([0,0,0]) cube([LPB+2*2, 45, 60]);
      translate([2,0,0]) cube([LPB, 45-2, 60]);
      translate([(LPB+2*2)/2,60-6,60/2]) rotate([90,0,0]) cylinder(12, 9, 9);
    
    offset=(60-2*(BW/4))/2;
      translate([-10,4,offset]) rotate([0,90,0]) cylinder(LPB+20, 1.5, 1.5);
      translate([-10,4,offset+2*(BW/4)]) rotate([0,90,0]) cylinder(LPB+20, 1.5, 1.5);
    
    }
  }
}



