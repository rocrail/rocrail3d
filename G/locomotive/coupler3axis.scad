/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../../lib/shapes.scad>



CENTER=0;
PLATE=1;
RFID=0;
POWER=1;

if( PLATE ) {
  translate([80,0,0]) { 
    difference() {
      translate([0,0,0]) cube([11, 70, 2]);
      translate([4,8,0]) cylinder(6, 1.7, 1.7);
      translate([4,70-8,0]) cylinder(6, 1.7, 1.7);
    }
    
    difference() {
      translate([0,35/2-5,0]) cube([2, 45, POWER?24:4]);
      if( POWER )
        translate([0,70/2-6,15]) rotate([0,90,0]) cylinder(6, 3, 3);
        translate([0,70/2+6,12]) rotate([0,90,0]) cylinder(6, 2, 2);
      translate([0,70/2+6-1,12]) cube([2, 2, 30]);
    }
    
    translate([9,35/2-10,0]) cube([2, 55, 4]);
    
    translate([6,70/2-35/2,0]) cube([19.5, 2, 8]);
    translate([6,70/2-35/2+35-2,0]) cube([19.5, 2, 8]);

    if( POWER ) {
      translate([8,17.5,8]) rotate([0,0,90]) prism(2,7,16);
      translate([8,50.5,8]) rotate([0,0,90]) prism(2,7,16);
      translate([0,70/2-35/2,0]) cube([19.5, 2, 8]);
      translate([0,70/2-35/2+35-2,0]) cube([19.5, 2, 8]);
    }
    
    if( RFID ) {
      difference() {
        translate([6+7,70/2-35/2,0]) cube([5, 35, 28]);
        translate([6+7,70/2-35/2+4,0]) cube([3, 35-8, 28]);
      }
      translate([25,17.5,8]) rotate([0,0,90]) prism(2,7,16);
      translate([25,50.5,8]) rotate([0,0,90]) prism(2,7,16);
    }
    
    difference() {
      translate([0,70/2-35/2,0]) roundedcube(32, 35, 2, 5);
      //translate([13,35,0]) cylinder(6, 5, 5);

      translate([24-5,35-11,0]) cylinder(6, 1.7, 1.7);
      translate([24-5,35+11,0]) cylinder(6, 1.7, 1.7);


      translate([24+3,35-11,0]) cylinder(6, 1.7, 1.7);
      translate([24+3,35+11,0]) cylinder(6, 1.7, 1.7);

      translate([24-5,35-11-1.7,0]) cube([8, 1.7*2, 6]);
      translate([24-5,35+11-1.7,0]) cube([8, 1.7*2, 6]);
    }
    
  }
}


CLEN=13; // NS1200=10, NS2400=2, NS6400=25
if( CENTER ) {
// Coupler connection
translate([0,35,0]) {
  difference() {
    translate([0,-15,0]) cube([2, 30, 11]);
    translate([0,30/2-4,5]) rotate([0,90,0]) cylinder(6, 1.7, 1.7);
    translate([0,-30/2+4,5]) rotate([0,90,0]) cylinder(6, 1.7, 1.7);
  }
  
  difference() {
    translate([0,-6,0]) cube([CLEN, 12, 11]);
    translate([2,-4,2]) cube([CLEN-4, 8, 11]);
  }
  
  difference() {
    translate([CLEN-2,-6,0]) cube([33, 12, 6]);
    translate([CLEN,-6,2]) cube([31, 12, 4]);
    translate([CLEN+9,0,0]) cylinder(6, 1, 1);
    translate([CLEN+16,0,0]) cylinder(6, 1.25, 1.25);
    translate([CLEN+16,0,0]) cylinder(1, 2.5, 1.25);
  }

  difference() {
    translate([CLEN+25,0,0]) cylinder(6, 3.8, 3.8);
    translate([CLEN+25,0,0]) cylinder(6, 2.5, 2.5);
  }
  difference() {
    translate([CLEN-2,-3.8,2]) cube([26, 7.6, 2]);
    translate([CLEN+24,0,0]) cylinder(6, 2.5, 2.5);
    translate([CLEN+9,0,0]) cylinder(6, 1, 1);
    translate([CLEN+16,0,2]) cylinder(2, 1.25, 1.25);
  }

}

}
