/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;
//$fn=64;
include <../../lib/shapes.scad>

DOOR=0;
SIDE=0;
SIDENR=1;
BOX=0;
BOXCOVER=0;
HALLWHEEL=0;
HALLMOUNT=0;

ROUNDEDTRAPEZE_1=0;
ROUNDEDTRAPEZE_2=0;

NSLOGO=0;

H=.4;
DW=21+4;

L=120;
LNR=80;

BOXLEN=50; // 60 3assen, 50 2assen

LOGOW=20;
if( NSLOGO ) {
  difference() {
    translate([0,0,0]) cube([LOGOW,6,.6]);
    translate([3,0,1]) rotate([270,90,0]) prism(1,3,4);
    translate([LOGOW-3,0,1]) mirror([1,0,0]) rotate([270,90,0]) prism(1,3,4);
  }
  
  translate([LOGOW/2,0,0]) cylinder(.6,4,4);
  difference() {
    translate([LOGOW/2,0,0]) cylinder(1,4,4);
    translate([LOGOW/2,0,0]) cylinder(1,3.5,3.5);
  }
  difference() {
    translate([LOGOW/2,0,0]) cylinder(1,2,2);
    translate([LOGOW/2,0,0]) cylinder(1,1.5,1.5);
  }
  difference() {
    translate([LOGOW/2-3,6,0]) cube([6,4,1]);
    translate([LOGOW/2-2.5,6,.6]) cube([5,3.5,1]);
  }
  translate([LOGOW/2-1.5,3.5,0]) rotate([0,0,30]) cube([.5,3,1]);
  translate([LOGOW/2+1,3.5,0]) rotate([0,0,-30]) cube([.5,3,1]);
}

module roundedRect(x,y,xx,yy,r,h) {
  translate([x,y,0]) cylinder(h,r,r);
  
  translate([xx,yy,0]) cylinder(h,r,r);
  
  g = atan((xx-x)/(yy-y));
  l = sqrt( (xx-x)^2 + (yy-y)^2 );
  
  translate([x,y,0]) rotate([0,0,-g]) cube([r,l,h]); 
  translate([x,y,0]) mirror([1,0,0]) rotate([0,0,g]) cube([r,l,h]); 
}

module roundedTrapeze(x1,y1,x2,y2,x3,y3,x4,y4,r,h) {
  roundedRect(x1,y1,x2,y2,r,h);
  roundedRect(x2,y2,x3,y3,r,h);
  roundedRect(x1,y1,x4,y4,r,h);
  roundedRect(x4,y4,x3,y3,r,h);
}

if( ROUNDEDTRAPEZE_1 ) {
  h=21-2*2.5;
  w1=31.5-2*2.5;
  w2=(31.5-26.5);
  difference() {
    roundedTrapeze(0,0, w2,h, w1,h, w1,0, 2.5,1);
    roundedTrapeze(2,1, w2+1,h-1, w1-1,h-1, w1-1, 1, 1.25,1);
  }
}

if( ROUNDEDTRAPEZE_2 ) {
  h=19-2*2.5;
  w1=48.5-2*2.5;
  w2=(48.5-45.5);
  difference() {
    roundedTrapeze(0,0, w2,h, w1,h, w1,0, 2.5,1);
    roundedTrapeze(2,1, w2+1,h-1, w1-1,h-1, w1-1, 1, 1.25,1);
  }
}

module triangle(base,top,h) {
  linear_extrude(h) polygon(points=[[0,0],[base,0],[base/2,top]]);
}

if( SIDE ) {
  translate([0,30,0]) {
    translate([0,0,0]) cube([L, 4, .4]);
  }
}

if( BOX ) {
  translate([0,60,0]) {
    difference() {
      translate([0,0,0]) cube([BOXLEN, 36, 30]);
      translate([2,2,2]) cube([BOXLEN-4, 36-4, 30-2]);
    }
  }
}
if( BOXCOVER ) {
  translate([0,100,0]) {
    translate([0,0,0]) cube([BOXLEN, 36, 2]);
    difference() {
      translate([2.2,2.2,2]) cube([BOXLEN-4.4, 36-4.4, 2]);
      translate([2.2+2,2.2+2,2]) cube([BOXLEN-4.4-4, 36-4.4-4, 2]);
    }
  }
}


if( SIDENR ) {
  translate([0,40,0]) {
    difference() {
      translate([0,0,0]) ellipse(.4, 20, 10);
      translate([0,0,0]) ellipse(.4, 19, 9);
    }
    translate([19,-2,0]) cube([LNR, 4, .4]);
    translate([-19-LNR,-2,0]) cube([LNR, 4, .4]);
    translate([-14,-4,0]) linear_extrude(1.6) text("1212",9);
    translate([-18,-4,0]) cube([36, .9, .4]);
  }
}


if( DOOR ) {
  translate([0,0,0]) {
        
   translate([-DW/2+2,0,0]) {
     difference() {
       translate([0,0,0]) cylinder(.4, DW/2, DW/2);
       translate([0,0,0]) cylinder(.4, DW/2-4, DW/2-4);
       // bottom side cut
       translate([-DW/2,-DW/2-2.5,0]) cube([DW, DW/2+4, .4]);
       // left side cut
       translate([-DW/2,0,0]) cube([DW/2, DW/2, .4]);
     }
   }
   
   translate([DW/2-2,0,0]) {
     difference() {
       translate([0,0,0]) cylinder(.4, DW/2, DW/2);
       translate([0,0,0]) cylinder(.4, DW/2-4, DW/2-4);
       // bottom side cut
       translate([-DW/2,-DW/2-2.5,0]) cube([DW, DW/2+4, .4]);
       // right side cut
       translate([0,0,0]) cube([DW/2, DW/2, .4]);
      }
    }
 
    //translate([0,0,0]) cylinder(.4, 4/2, 4/2);
    
    translate([-2.4,2.6,0]) mirror([0,1,0]) triangle(4.8,8,.4);

   
  }
}





module magneetgat()
{
  rotate([0, 90, 0]) cylinder (3.5, 1.1, 1.1);
}
module luchtgat()
{
  rotate([0, 90, 0]) cylinder (12.0, .6, .6);
}
if( HALLWHEEL ) {
  difference() {
    translate ([0,0,0]) cylinder(7, 3.05, 3.05);
    translate ([0,0,0]) cylinder(7, 1.5, 1.5);
  }
  difference()
  {
    CNT=12;  
    translate ([0,0,0]) cylinder(5, 14, 14);
    translate ([0,0,0]) cylinder(3.2, 9, 9);
    
    for(x = [0 : CNT])
    {
      rotate([0, 0, x*(360/CNT)]) 
      {
        translate ([11.0, 0, 2.5])  magneetgat();
        translate ([0, 0, 2.5])  luchtgat();
      }
    }
  }
}
  

if( HALLMOUNT ) {
  translate ([50,0,0]) {
    difference() {
      translate ([0,0,0]) cube([2, 40, 34]);
      translate ([0,40/2-20/2,16]) cube([2, 20, 34-16]);

      translate ([0,40/2,16]) rotate([0,90,0]) cylinder(10, 20/2, 20/2);
      
      translate ([0,40/2-31/2,20]) rotate([0,90,0]) cylinder(10, 1.6, 1.6);
      translate ([0,40/2+31/2,20]) rotate([0,90,0]) cylinder(10, 1.6, 1.6);
      translate ([0,40/2-31/2,28]) rotate([0,90,0]) cylinder(10, 1.6, 1.6);
      translate ([0,40/2+31/2,28]) rotate([0,90,0]) cylinder(10, 1.6, 1.6);
      translate ([0,40/2+31/2-1.6,20]) cube([2, 3.2, 8]);
      translate ([0,40/2-31/2-1.6,20]) cube([2, 3.2, 8]);
    }

    difference() {
      translate ([0,40/2-10/2,0]) cube([30, 10, 2]);
      translate ([6,40/2,0]) cylinder(10, 3, 3);
    }

    translate ([0,40/2-10/2-10,2]) rotate([0,90,0]) prism(2,10,30);
    translate ([0,40/2+10/2+10,2]) mirror([0,1,0]) rotate([0,90,0]) prism(2,10,30);


    translate ([10,40/2-4.4/2-1.2,2]) cube([20, 1.2, 2]);
    translate ([10,40/2+4.4/2,2]) cube([20, 1.2, 2]);

  }
}




