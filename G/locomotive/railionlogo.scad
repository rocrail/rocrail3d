/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;


linear_extrude(height = .6) {
  scale(0.15)
  import("RailionLogo.svg");
}
translate([10,4,0]) cube([50,.6,.6]);
translate([28.3,10,0]) cube([.6,5,.6]);    
translate([38.3,10,0]) cube([.6,5,.6]);    
