/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
//$fn=64;
include <../../lib/shapes.scad>

W=110; // breedte over de U-Profielen + 2 x 2mm.
L=110;
H=25;

TANK=1;
TECHPOWER=1;

module RFID(x,y,z) {
  translate([x,y,z]) rotate([0,0,90]) {
    // ID12LA socket
      difference() {
        translate([(40-28.2)/2, (31.5-27.2)/2,0]) cube([28.2, 27.2, 5]);
        translate([(40-28.2)/2+1, (31.5-27.2)/2+1,0]) cube([26.2, 25.2, 5]);
        
      translate([33,18,2]) cube([2, 5, 5]);
      }
      
    // RFID print bevestiging  
    translate([-1,0,0]) {
      difference() {
        translate([3,(31.5-20)/2,0]) cylinder(10, 2.5, 2.5);
        translate([3,(31.5-20)/2,2]) cylinder(12, 1, 1);
      }  
      translate([2.5,3.5,0])  cube([5, 3, 5]);
      
      difference() {
        translate([3,(31.5-20)/2+20,0]) cylinder(10, 2.5, 2.5);
        translate([3,(31.5-20)/2+20,2]) cylinder(8, 1, 1);
      }  
      translate([2.5,25,0])  cube([5, 3, 5]);

      difference() {
        translate([3+34,(31.5-20)/2,0]) cylinder(10, 2.5, 2.5);
        translate([3+34,(31.5-20)/2,2]) cylinder(8, 1, 1);
      }  
      //translate([1.5+34,4,0])  cube([3, 20, 6]);

      difference() {
        translate([3+34,(31.5-20)/2+20,0]) cylinder(10, 2.5, 2.5);
        translate([3+34,(31.5-20)/2+20,2]) cylinder(12, 1, 1);
      }  
    }
  }   
}




if( TANK ) {
  RFID(L/2+27.2/2+2.3,W/2-20,0);
  
  if( TECHPOWER ) {
    translate([L/2-46/2-8,12,0]) {
      difference() {
        translate([8,-10,0]) cube([46, 12, 20]);
        translate([10,-10,0]) cube([42, 10, 18]);
        translate([0,-15+2,15]) rotate([270,0,0]) prism(L, 15, 15);

        // flip switch
        rotate([90,0,0]) translate([18,4+6,-2]) cylinder(2, 3.1, 3.1);

        // XT60 chassis
        translate([29.8,0,6]) cube([12.2, 2, 8.2]);
        rotate([90,0,0]) translate([42.2,4+6,-2]) cylinder(2, 4.1, 4.1);
      }
    }
  }
  
  
  difference() {
    translate([0,0,0]) cube([L,W,H]);
    translate([2,15,2]) cube([L-4,W-2*15,H]);
    translate([2,2,15]) cube([L-4,W-4,H]);

    translate([0,0,15]) rotate([270,0,0]) prism(L,15,15);
    translate([2,15+0,2]) rotate([90,0,0]) prism(L-4,14,14);

    translate([0,W-15,0]) rotate([0,0,0]) prism(L,15,15);
    translate([2,W-1,14+2]) rotate([180,0,0]) prism(L-4,14,14);

    if( TECHPOWER )
      translate([L/2-42/2,0,2]) cube([42, 12, 14]);
    
    
    // afronding links
    translate([0,0,H-1]) cube([L+10,2,3]);
    // afronding rechts
    translate([0,W-2,H-1]) cube([L+10,2,3]);

  }

  // versmalling in de breedte
  translate([0,3,H]) cube([L,7,2]);
  translate([0,W-10,H]) cube([L,7,2]);

  // afronding links
  translate([0,3,H-1]) { difference() {
    translate([0,0,0]) rotate([0,90,0]) cylinder(L,3,3);
    translate([0,0,0]) rotate([0,90,0]) cylinder(L,1,1);
    translate([0,0,0]) cube([L+10,5,5]);
    translate([0,-5,-5]) cube([L+10,10,5]);
  }
  }
  
  // afronding rechts
  translate([0,W-3,H-1]) { difference() {
    translate([0,0,0]) rotate([0,90,0]) cylinder(L,3,3);
    translate([0,0,0]) rotate([0,90,0]) cylinder(L,1,1);
    translate([0,-5,0]) cube([L+10,5,5]);
    translate([0,-5,-5]) cube([L+10,10,5]);
  }
  }
  
  // boven stuk
  difference() {
    translate([0,8,H]) cube([L,W-2*8,10+8]);
    translate([2,8+2,H]) cube([L-4,W-2*8-4,10+8]);
    // bevestigings gaten
    translate([10,W,H+10+4]) rotate([90,0,0]) cylinder(W+10,1.6,1.6);
    translate([L-10,W,H+10+4]) rotate([90,0,0]) cylinder(W+10,1.6,1.6);
  }


  // ophanging links
  translate([8,-2,15]) cube([5,2,H-15+10]);
  translate([8,-2+15,0]) rotate([45,0,0]) cube([5,2,21]);
  translate([8,13,0]) cube([5,W-26.1,2]);
  // extra versteviging om afbreken te voorkomen
  difference() {
    translate([8+2,0,H+1]) cube([1,W,5]);
    translate([8+2,10,H+1]) cube([1,W-20,5]);
  }
  
  translate([29,-2,15]) cube([5,2,H-15+10]);
  translate([29,-2+15,0]) rotate([45,0,0]) cube([5,2,21]);
  translate([29,13,0]) cube([5,W-26.1,2]);
  difference() {
    translate([29+2,0,H+1]) cube([1,W,5]);
    translate([29+2,10,H+1]) cube([1,W-20,5]);
  }

  translate([L-8-5,-2,15]) cube([5,2,H-15+10]);
  translate([L-8-5,-2+15,0]) rotate([45,0,0]) cube([5,2,21]);
  translate([L-8-5,13,0]) cube([5,W-26.1,2]);
  difference() {
    translate([L-8-5+2,0,H+1]) cube([1,W,5]);
    translate([L-8-5+2,10,H+1]) cube([1,W-20,5]);
  }
  
  translate([L-29-5,-2,15]) cube([5,2,H-15+10]);
  translate([L-29-5,-2+15,0]) rotate([45,0,0]) cube([5,2,21]);
  translate([L-29-5,13,0]) cube([5,W-26.1,2]);
  difference() {
    translate([L-29-5+2,0,H+1]) cube([1,W,5]);
    translate([L-29-5+2,10,H+1]) cube([1,W-20,5]);
  }


  // ophanging rechts
  translate([8,W,15]) cube([5,2,H-15+10]);
  translate([8,W-14.5,1.5]) rotate([-45,0,0]) cube([5,2,21]);
  
  translate([29,W,15]) cube([5,2,H-15+10]);
  translate([29,W-14.5,1.5]) rotate([-45,0,0]) cube([5,2,21]);

  translate([L-8-5,W,15]) cube([5,2,H-15+10]);
  translate([L-8-5,W-14.5,1.5]) rotate([-45,0,0]) cube([5,2,21]);
  
  translate([L-29-5,W,15]) cube([5,2,H-15+10]);
  translate([L-29-5,W-14.5,1.5]) rotate([-45,0,0]) cube([5,2,21]);


}

