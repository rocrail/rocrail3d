/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=180;
include <../../lib/shapes.scad>


L=180;
W=20;
BW=90;
BBW=118; // overall width + 2 x 2mm wall
BBR=116; // head radius

HEADTOP=0;
HEADROOF=1;
HEADROOFMID=0;
DIVIDER=1.5;
//SINDIV=8.4;
//LAYER=0.4;

SINDIV=34;
LAYER=0.2;


ROOFSINDIV=34;
ROOFDIVIDER=1.5;
ROOFLAYER=0.02;
ROOFSTEP=5;

ROOFMIDLEN=342;
if( HEADROOFMID ) {
  translate([150,0,0]) {
    translate([-BBR/2 + 4,0,0]) cube([2, ROOFMIDLEN/2, 3]);
    translate([BBR/2 - 4 - 2,0,0]) cube([2, ROOFMIDLEN/2, 3]);
    translate([-BBR/2+1,0,3]) cube([BBR-2, ROOFMIDLEN/2, 1]);
    
    difference() {
      translate([0,ROOFMIDLEN/2,4]) rotate([90,0,0]) ellipse(ROOFMIDLEN/2, 55.5 , 10);    
      translate([-100,0,-20+4]) cube([200, 200, 20]);
    }
  }
}


if( HEADROOF ) {
  translate([150,0,0]) {
   
  // dak
  translate([0, 0, 4]) difference() {
  ROOFSTEPS=114;
    
  for(i = [0:ROOFSTEPS]) {
    translate([0,-i*.4,0]) rotate([90,0,0]) 
        ellipse(.4, 
        55.5 - sin(i*.4)*(i*.4)*(0.9+((0.3*(i*2/ROOFSTEPS))/ROOFSTEPS)*(i^2*.004)), 
        10-(sin(i*.4) * (13*(i/ROOFSTEPS))) );    
      //translate([0,-i,0]) rotate([90,0,0]) ellipse(1,55.5,5);    
      //translate([0,-i,0]) rotate([90,0,0]) ellipse(1,55.5-2,5-2);    
     
  }

    translate([-100,-100,-190]) cube([200, 200, 190]);
  }    
    
  // bodem
  translate([0, 0, 0]) difference() {
    ellipse(3, (BBR/2) - ((400-320)*.1)/2, 82 - (sin(400/ROOFSINDIV)/ROOFDIVIDER * 400/ROOFDIVIDER));
    
    ellipse(3, ((BBR/2) - ((400-320)*.1)/2) - 2, (82 - (sin(400/ROOFSINDIV)/ROOFDIVIDER * 400/ROOFDIVIDER) - 2));
    
    translate([-BBR/2,0,0]) cube([BBR, 82, 100]);
  }
  
  // flens
  translate([0, 0, 3]) difference() {
    ellipse(1, ((BBR/2) - ((400-320)*.1)/2) + 3, (82 - (sin(400/ROOFSINDIV)/ROOFDIVIDER * 400/ROOFDIVIDER))+3);
    
    //ellipse(2, ((BBR/2) - ((400-320)*.1)/2) - 2, (82 - (sin(400/ROOFSINDIV)/ROOFDIVIDER * 400/ROOFDIVIDER) - 2));
    
    translate([-BBR/2,0,0]) cube([BBR, 82, 100]);
  }

// rest straight
  
    translate([-BBR/2 + 4,0,0]) cube([2, 160, 3]);
    translate([BBR/2 - 4 - 2,0,0]) cube([2, 160, 3]);
  difference() {
    translate([-BBR/2+1,0,3]) cube([BBR-2, 160, 1]);
    translate([40,50,0]) cylinder(16,3,3);
  }
  difference() {
    translate([0,160,4]) rotate([90,0,0]) ellipse(160, 55.5 , 10);    
    translate([-100,0,-20+4]) cube([200, 200, 20]);
    translate([40,50,0]) cylinder(16,3,3);
  }
  difference() {
    translate([40,50,0]) cylinder(16,5,4);
    translate([40,50,0]) cylinder(16,3,3);
  }
  
  }
}


if( HEADTOP ) {
  translate([0,0,0]) {

    translate([BBR/2-2-2,-5,5]) cube([2, 10, 60]);
    translate([-BBR/2+2,-5,5]) cube([2, 10, 60]);
    
    // Basement
    difference() {
      translate([0,0,0]) ellipse(6.2, BBR/2, 82);
      translate([0,0,0]) ellipse(5, BBR/2-2, 82-2);
      translate([0,0,0]) ellipse(6.2, BBR/2-3, 82-3);
      
      translate([-BBR/2,0,0]) cube([BBW, 82, 12]);
      translate([-BBR/2,-7.5,5]) cube([BBW, 82, 12]);
      
      translate([-60,-10,2.5]) rotate([0,90,0]) cylinder(120, 1.2, 1.2);
      translate([0,-10,2.5]) rotate([90,90,0]) cylinder(120, 1.2, 1.2);
    }    
      
    // Top
    difference() {
      for(i = [0:400]) {
        translate([0, 0, 5+ i*LAYER])
        if( i > 320 ) {
          difference() {
            ellipse(LAYER, (BBR/2) - ((i-320)*.1)/2, 82 - (sin(i/SINDIV)/DIVIDER * i/DIVIDER));
            ellipse(LAYER, (BBR/2) - ((i-320)*.1)/2 - 2, 80 - (sin(i/SINDIV)/DIVIDER * i/DIVIDER));
          }
        }
        else {
          difference() {
            ellipse(LAYER, (BBR/2), 82 - (sin(i/SINDIV)/DIVIDER * i/DIVIDER));
            ellipse(LAYER, (BBR/2-2), 80 - (sin(i/SINDIV)/DIVIDER * i/DIVIDER));
          }
        }
      }

      translate([-BBR/2,0,0]) cube([BBR, 82, 100]);
        

      translate([10,30,35]) rotate([0,270,45]) windowCut( 30, 20, 100, 2.5);

      translate([15,20,35]) rotate([0,270,77.5]) windowCut( 30, 30, 100, 2.5);
            
      translate([15,20,35]) rotate([0,270,102.5]) windowCut( 30, 30, 100, 2.5);

      translate([10,30,35]) rotate([0,270,135]) windowCut( 30, 20, 100, 2.5);
        
        
       translate([-35,-60,12]) rotate([90,90,0]) cylinder(20,2.5,2.5);         
       translate([35,-60,12]) rotate([90,90,0]) cylinder(20,2.5,2.5);         
       translate([-18,-45,78]) rotate([90,90,0]) cylinder(20,2.5,2.5);         
       translate([18,-45,78]) rotate([90,90,0]) cylinder(20,2.5,2.5);         

    }

    // lamp links
    difference() {
       translate([-35,-58,12]) rotate([90,90,0]) cylinder(11,6,4);         
       translate([-35,-68,12]) rotate([90,90,0]) cylinder(1,3.5,3.5);         
       translate([-35,-58,12]) rotate([90,90,0]) cylinder(20,2.5,2.5);         
    }
    // lamp rechts
    difference() {
       translate([35,-58,12]) rotate([90,90,0]) cylinder(11,6,4);         
       translate([35,-68,12]) rotate([90,90,0]) cylinder(1,3.5,3.5);         
       translate([35,-58,12]) rotate([90,90,0]) cylinder(20,2.5,2.5);         
    }
    // lamp links midden
    difference() {
       translate([-18,-45,78]) rotate([90,90,0]) cylinder(7,4.5,3.5);         
       translate([-18,-51,78]) rotate([90,90,0]) cylinder(1,3,3);         
       translate([-18,-45,78]) rotate([90,90,0]) cylinder(20,2.5,2.5);         
    }
    // lamp rechts midden
    difference() {
       translate([18,-45,78]) rotate([90,90,0]) cylinder(7,4.5,3.5);         
       translate([18,-51,78]) rotate([90,90,0]) cylinder(1,3,3);         
       translate([18,-45,78]) rotate([90,90,0]) cylinder(20,2.5,2.5);         
    }
    
  }
}


