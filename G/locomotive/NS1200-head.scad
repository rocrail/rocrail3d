/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;
//$fn=16;
include <../../lib/shapes.scad>

W=118;
H1=70;
D1=25;
L1=90; // was 70

TEST=0;

HEAD=1;
HEADNUT=0;
HEADTOP=0;
HEADPROFILE=0;
HEADLIGHT=0;
LENSBOTTOM=0;
LENSTOP=0;
HP=4;

LW=70;
LH=40;

if( LENSBOTTOM ) {
  translate([0,0,0]) {
   difference() {
     translate([0,0,0]) cylinder(1.6, 4.9, 4.9);
     translate([0,0,.6]) cylinder(2.0, 2.45, 2.45);
   }
  }
}

if( LENSTOP ) {
  translate([20,0,0]) {
   difference() {
     translate([0,0,0]) cylinder(1.6, 2.9, 2.9);
     translate([0,0,.6]) cylinder(2.0, 2.45, 2.45);
   }
  }
}



module LEDHOLES(x,y) {
  translate([x,y-2.5/2,0]) cylinder(4,.75,.75);
  translate([x,y+2.5/2,0]) cylinder(4,.75,.75);
}

module LEDCUP(x,y,cut) {
  difference() {
    translate([x,y,0]) cylinder(5,4,4);
    if( !cut ) {
      translate([x,y,2]) cylinder(5,3,3);
      translate([x,y-2.5/2,0]) cylinder(4,.75,.75);
      translate([x,y+2.5/2,0]) cylinder(4,.75,.75);
    }
  }
}

if( HEADLIGHT ) {
  LEDCUP(-LW/2,-LH/2,false);
  LEDCUP(LW/2,-LH/2,false);
  LEDCUP(-LW/2,LH/2,false);
  LEDCUP(LW/2,LH/2,false);

  difference() {
    translate([-10/2,-LH/2,0]) cube([10, LH, 2]);
    translate([0,0,0]) cylinder(4,2.5,2.5);
    translate([0,12,0]) cylinder(4,2.5,2.5);
    translate([0,-12,0]) cylinder(4,2.5,2.5);
  }

  difference() {
    translate([-LW/2,LH/2-8/2,0]) cube([LW, 8, 2]);
    LEDCUP(-LW/2,LH/2,true);
    LEDCUP(LW/2,LH/2,true);
    LEDHOLES(LW/2-10,LH/2);
    LEDHOLES(-LW/2+10,LH/2);
  }
  difference() {
    translate([-LW/2,-LH/2-8/2,0]) cube([LW, 8, 2]);
    LEDCUP(-LW/2,-LH/2,true);
    LEDCUP(LW/2,-LH/2,true);
    LEDHOLES(LW/2-10,-LH/2);
    LEDHOLES(-LW/2+10,-LH/2);
  }
}



if( HEADPROFILE ) {
  difference() {
    translate([0,0,0]) ellipse(HP, D1-2, W/2-2);
    translate([0,0,2]) ellipse(HP, D1-4, W/2-4);
    translate([0,0,0]) ellipse(HP, D1-10, W/2-10);
    translate([0,-W/2,0]) cube([D1, W, HP]);
  }
  translate([0,-W/2+2,0]) cube([6, 2, HP]);
  translate([0,W/2-4,0]) cube([6, 2, HP]);

  translate([0,-W/2+2,0]) cube([6, 8, 2]);
  translate([0,W/2-2-8,0]) cube([6, 8, 2]);

}


module Lamp1(x,y,z,d,cut) {
  difference() {
    translate([x,y,z]) rotate([0,90,0]) cylinder(7,d,d);
    if( !cut ) {
      translate([x,y,z]) rotate([0,90,0]) cylinder(7,2.6,2.6);
      translate([x,y,z]) rotate([0,90,0]) cylinder(2,d-1,d-1);
    }
  }  
}

if( HEAD ) {
  if( HEADNUT ) {
    // Kop naar dak overgang
    for(i = [0:28]) {
      difference() {
        translate([0,0,H1+i*.2]) ellipse(.2, D1-i/4, W/2-i/4);
        if( i < 18 )
          translate([0,0,H1+i*.2]) ellipse(.2, D1-i/4-2, W/2-2-i/4);
        translate([0,-W/2,0]) cube([D1, W, H1+20]);
       }
     }
   }
   
  // kop
  difference() {
    translate([0,0,0]) ellipse(H1, D1, W/2);
    translate([0,0,0]) ellipse(H1, D1-2, W/2-2);
    translate([0,-W/2,0]) cube([D1, W, H1]);
    translate([-D1,-W/2,0]) cube([30, W, 10]);
    // deur
    translate([-D1,-24/2,14]) cube([1, 24, H1-20]);

    // lampen onder
    Lamp1(-23,-70/2,20, 6, true);
    Lamp1(-23,70/2,20, 6, true);
    // lampen boven
    Lamp1(-21.5,-70/2,60, 4, true);
    Lamp1(-21.5,70/2,60, 4, true);

    // deur railing gaten
    translate([-30,24/2+3,16]) rotate([0,90,0]) cylinder(20,1,1);
    translate([-30,24/2+3,16+45]) rotate([0,90,0]) cylinder(20,1,1);
    translate([-30,-24/2-3,16]) rotate([0,90,0]) cylinder(20,1,1);
    translate([-30,-24/2-3,16+45]) rotate([0,90,0]) cylinder(20,1,1);

    // Bevestigingsgat voorkant
    translate([-50,0,12]) rotate([0,90,0]) cylinder(150,.75,.75);
  }
  
  // zijwanden
  difference() {
    translate([0,-W/2,0]) cube([L1, 2, H1]);
    translate([10,-W/2,55]) cube([6, 1, 8]);
    translate([0,-W/2,0]) cube([6, 2, 10]);
    translate([L1-28,75,6]) rotate([90,0,0]) cylinder(150,1,1);
    translate([L1-8,75,6]) rotate([90,0,0]) cylinder(150,1,1);
  }
  difference() {
    translate([0,W/2-2,0]) cube([L1, 2, H1]);
    translate([10,W/2-1,55]) cube([6, 1, 8]);
    translate([0,W/2-2,0]) cube([6, 2, 10]);
    translate([L1-28,75,6]) rotate([90,0,0]) cylinder(150,1,1);
    translate([L1-8,75,6]) rotate([90,0,0]) cylinder(150,1,1);
  }
  
  // deur
  difference() {
    translate([-D1,-24/2,14]) cube([2, 24, H1-20]);
    translate([-D1,-24/2+1,14+1]) cube([2, 24-2, H1-20-2]);
  }
  
  // lampen onder
  Lamp1(-22.5,-70/2,20, 6, false);
  Lamp1(-22.5,70/2,20, 6, false);

  // lampen boven
  Lamp1(-21.5,-70/2,60, 4, false);
  Lamp1(-21.5,70/2,60, 4, false);

  // LED houder bevestiging
  difference() {
    translate([-23,0,H1-30]) rotate([0,90,0]) cylinder(10.5, 5, 5);
    translate([-23,0,H1-30]) rotate([0,90,0]) cylinder(10.5, 1.4, 1.4);
  }   
   
  // Lijmstrips
  translate([L1-5,-W/2+2,10]) cube([10, 2, H1-20]);
  translate([L1-5,W/2-4,10]) cube([10, 2, H1-20]);
  
  
}



module frontWindow(w, h) {
  difference() {
    cube([h, w, 3]);
    translate([4,4,0]) windowCut(h-8, w-8, 3, 2.5);
  }
}

module roof(w, h, w2, s) {
    linear_extrude(2) polygon(points=[[0,0],[w,0],[w,h],[w2,h]]);
  // lijm strip
  translate([w-3, 2, s]) cube([6,h-4,2]);
}

module sideWindow(w, h, w2, s) {
  difference() {
    linear_extrude(3) polygon(points=[[0,0],[w,0],[w,h],[w2,h]]);
    //translate([6,4,0]) linear_extrude(2) polygon(points=[[0,0],[w-9,0],[w-9,h-8],[w2-3,h-8]]);

    translate([8.2, 6, 0]) cylinder(10, 2.5, 2.5);
    translate([w-6, 6, 0]) cylinder(10, 2.5, 2.5);
    translate([w-6, h-6, 0]) cylinder(10, 2.5, 2.5);
    translate([w2+4, h-6, 0]) cylinder(10, 2.5, 2.5);

    translate([7.25,3.5,0]) linear_extrude(3) polygon(points=[[0,0],[w-12.5,0],[w-12.5,h-7],[w2-3,h-7]]);

    translate([7.25-2.5+1,3.5+2.5,0]) linear_extrude(4) polygon(points=[[0,0],[w-12.5+3.3,0],[w-12.5+3.3,h-7-5],[w2-4.2,h-7-5]]);
  }
  // lijm strip
  translate([w-3, 2, s]) cube([6,h-4,2]);
}

module frontWindowR(w, h, w2, s) {
  translate([-w,0,0]) difference() {
    linear_extrude(3) polygon(points=[[0,0],[w,0],[w,h],[w2,h]]);
    //translate([6,4,0]) linear_extrude(2) polygon(points=[[0,0],[w-9,0],[w-9,h-8],[w2-3,h-8]]);

    translate([7.6, 6, 0]) cylinder(10, 2.5, 2.5);
    translate([w-6, 6, 0]) cylinder(10, 2.5, 2.5);
    translate([w-6, h-6, 0]) cylinder(10, 2.5, 2.5);
    translate([w2+6, h-6, 0]) cylinder(10, 2.5, 2.5);

    translate([7.25,3.5,0]) linear_extrude(3) polygon(points=[[0,0],[w-12.5,0],[w-12.5,h-7],[w2,h-7]]);

    translate([7.25-2.5+1,3.5+2.5,0]) linear_extrude(4) polygon(points=[[-0.6,0],[w-12.5+3.3,0],[w-12.5+3.3,h-7-5],[w2-2.2,h-7-5]]);
  }
}

if( TEST ) {
  translate([100,120,0]) {
    frontWindowR(W/2-1, 27, 4.2, 0);
  }
}

if( HEADTOP ) {
  translate([0,120,0]) {
    
    // Kop naar dak overgang
    translate([0,W/2,0]) {

    // Bodem kop
    difference() {
      translate([0,0,0]) ellipse(2, D1-2, W/2-2);
      translate([0,0,0]) ellipse(2, D1-2-2, W/2-2-2);
      translate([0,-W/2,0]) cube([D1, W, H1+20]);
    }
      // Kop naar dak overgang
      // b = sqrt(c^2 -a^2);
      for(i = [0:19]) {
        difference() {
          translate([0,0,2+i*.2]) ellipse(.2, sqrt(D1^2-i^2), sqrt((W/2)^2-i^2) );
          if( i < 10 )
            translate([0,0,2+i*.2]) ellipse(.2, sqrt(D1^2-i^2)-2, sqrt((W/2)^2-i^2)-2-i/4);
          translate([0,-W/2,0]) cube([D1, W, H1+20]);
         }
       }
    }

      // top left
      layers=36;
      for(i = [0:layers]) {
        offset=-D1+10.4+i*.4;
        length=layers - sqrt( i )*6.0;
        translate([offset,3.8+length,6]) rotate([0,0,0]) prism(.4, (W/2-3.8)-length, (3.98/layers)*sqrt(i)*6);
      }    
      // top right
      for(i = [0:layers]) {
        offset=-D1+10.8+i*.4;
        length=layers - sqrt( i )*6.0;
        translate([offset,W-3.8-length,6]) rotate([0,0,180]) prism(.4, (W/2-3.8)-length, (3.98/layers)*sqrt(i)*6);
      }    
    
    
    // Bottom  
    difference() {
      translate([0,2,0]) cube([L1+2, W-4, 3]);
      translate([4,2+4,0]) cube([L1-6-45, W-4-8, 10]);
      translate([L1-40-5,2+4,0]) cube([42, W-4-8, 10]);
    }    
    //translate([0,0,2]) cube([72, W, 1]);

    // rounding left and right
    for(i = [0:19]) {
      difference() {
        WW = sqrt((W/2)^2-i^2) * 2;
        translate([0,(W-WW)/2,2+i*.2]) cube([34+10, WW, .2]);
      }
    }  

    // rounding mid
    difference() {
      translate([0,W/2,5]) rotate([0,90,0]) cylinder(34+10,5,5);
      translate([2,W/2,5]) rotate([0,90,0]) cylinder(34+10-4,4,4);
      translate([0,0,0]) cube([34+20, W, 3]);
    }

      // top left
      translate([0,3.2,6]) rotate([0,0,0]) prism(34+10, W/2-3.8, 3.98);
    
      // top right
      translate([72-28,W-3.2,6]) rotate([0,0,180]) prism(34+10, W/2-3.8, 3.98);

    // filling
    /*
    difference() {
      translate([0,3.2,3]) cube([72, W-3.2*2, 3]);
      translate([2,3.2+2,3]) cube([72-4, W-3.2*2-4, 2]);
    }
    */
    
    
    // Venster links
    translate([33+10-15.4, W/2-1.6, 9.5]) rotate([90+20.5,-4.3,90+13]) {
      //frontWindow( W/2-1, 26);
      frontWindowR( W/2-1, 27, 4.2, 0);

    }  
    
    // midden spijl
    translate([18.8+10,W/2,8.6]) rotate([0,21.5,0]) cylinder(27.6, 2.2, 2.2);
    
    // Venster rechts
    translate([19.2+10+1.0,W/2+1.0,8.46]) rotate([69,-4.3,-103.5]) {
      frontWindowR( W/2-1, 27, 4.2, 0);
    }
    
    // Venster zijkant links
    translate([30+10,2.8,1.8]) rotate([80,0,0]) sideWindow( 56-4, 28.5, 10, -1);
    
    // Venster zijkant rechts
    translate([30+10,W,2.2]) rotate([100,0,0]) sideWindow( 56-4, 28.5, 10, 2);

    //translate([26,2.7,28.7]) rotate([3.8,0,0]) cube([46, W/2-2.4, 2]);
    
    // dak links
    translate([41+10, 3.1+3.5, 28.7+1.42]) rotate([4.3,0,0]) roof(41, W/2-6.5, -13, -2);
      // ronding zijkant
    translate([40+10,6.9,30.25]) rotate([0,90,0]) cylinder(42, 2, 2);
      // ronding voorkant
    translate([41.8+10-0.4,3.0+3,30.1]) rotate([0,86.0,103.5]) cylinder(W/2-3.5, 2, 2);

    // dak rechts
    translate([41+10, W-2.6-3.5, 28.7+3.4]) rotate([-4.3,180,180]) roof(41, W/2-6.5, -13, 2);
      // ronding zijkant
    translate([40+10, W-6.9, 30.25]) rotate([0,90,0]) cylinder(42, 2, 2);
      // ronding voorkant
    translate([41.8+10-0.4, W-3.0-3, 30.1]) rotate([0,86.0,-103.5]) cylinder(W/2-3.5, 2, 2);



    // midden spijl
    translate([27.9+10,W/2,34.2]) rotate([0,90,0]) cylinder(54.1, 2, 2);
  
  }
}


