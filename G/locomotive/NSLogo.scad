/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;

linear_extrude(height = .6) {
  scale(0.10)
  import("NSLogo.svg");
}
    