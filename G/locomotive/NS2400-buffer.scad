/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
//$fn=256;
//$fn=128;
$fn=64;
include <../../lib/shapes.scad>

W=110+4; // breedte over de U-Profielen + 2 x 2mm.

HEAD=1;
HEADSTEP=0;
BUFFER=0;

HEADRED=0;
LINERED=0;
HOOK=0;

if( LINERED ) {
  translate([W+40,0,0]) {
    translate([0,0,0]) cube([57, 4, .6]);
  }
}


if( HEADSTEP ) {
  translate([0,0,0]) {
    translate([0,0,0]) rcube(.6, 10, 4, .5);
    //translate([0,0,0]) cube([10, 4, .6]);
    translate([0,1,0]) cube([10, .6, 2.6]);
  }
}


if( HOOK ) {
  translate([W+20,-20,0]) {
    translate([W/2,0,0]) cylinder(2, 1.5, 1.5);
    translate([W/2-3,-3,2]) cube([6, 6, 2]);
    difference() {
      translate([W/2-1.5,-2,8]) rotate([90,0,90]) cylinder(3, 6, 6);
      translate([W/2-1.5,-2,8]) rotate([90,0,90]) cylinder(3, 4, 4);
      translate([W/2-5,0,4]) cube([10, 10, 10]);
      translate([W/2-3,-3,4]) cube([6, 6, 2]);
    }
  }
}

if( HEADRED ) {
  translate([W+20,0,0]) {
    // front plaat
    difference() {
      translate([1.5,-.5,0]) cube([W-3, 23, .8]);
      // buffer openingen
      translate([W/2+68/2,7,0]) cylinder(2, 6.1, 6.1);
      translate([W/2-68/2,7,0]) cylinder(2, 6.1, 6.1);
      // haak opening
      translate([W/2,7,0]) cylinder(2, 1.6, 1.6);
      // hydro openingen
      translate([W/2-33/2,14,0]) cylinder(2, 1.1, 1.1);
      translate([W/2+33/2,14,0]) cylinder(2, 1.1, 1.1);
      // slang openingen
      translate([W-6,19,0]) cylinder(2, 1.1, 1.1);
      // slang openingen
      translate([W/2-10,19,0]) cylinder(2, 1.1, 1.1);
    }
    
    // haak bevestiging
    difference() {
      translate([W/2-4,7-4,.8]) rcube(1.2, 8, 8, .5);
      translate([W/2,7,0]) cylinder(2, 1.6, 1.6);
    }    
    
    // hydro bevestigingen
    difference() {
      translate([W/2-33/2,14,.8]) cylinder(1, 2.1, 2.1);
      translate([W/2-33/2,14,0]) cylinder(2, 1.1, 1.1);
    }
    difference() {
      translate([W/2+33/2,14,.8]) cylinder(1, 2.1, 2.1);
      translate([W/2+33/2,14,0]) cylinder(2, 1.1, 1.1);
    }
    
    // slang bevestiging
    difference() {
      translate([W-6,19,.8]) cylinder(1, 2, 2);
      translate([W-6,19,0]) cylinder(2, 1.1, 1.1);
    }
    difference() {
      translate([W/2-10,19,.8]) cylinder(1, 2, 2);
      translate([W/2-10,19,0]) cylinder(2, 1.1, 1.1);
    }
    
    // Deco blokjes
    translate([3,13,.8]) cube([6, 2, 1]);
    translate([W-3-6,13,.8]) cube([6, 2, 1]);
    
    // buffer bevestiging links
    difference() {
      translate([W/2-68/2-20/2,0,.8]) rcube(1, 20, 14, 1);
      translate([W/2-68/2,7,0]) cylinder(4, 6.0, 6.0);
    }
    // buffer moeren links
    translate([W/2-68/2-8,7-5,0]) cylinder(2.6, 1, 1);
    translate([W/2-68/2+8,7-5,0]) cylinder(2.6, 1, 1);
    translate([W/2-68/2-8,7+5,0]) cylinder(2.6, 1, 1);
    translate([W/2-68/2+8,7+5,0]) cylinder(2.6, 1, 1);
    
    // buffer bevestiging rechts
    difference() {
      translate([W/2+68/2-20/2,0,.8]) rcube(1, 20, 14, 1);
      translate([W/2+68/2,7,0]) cylinder(4, 6.0, 6.0);
    }
    // buffer moeren rechts
    translate([W/2+68/2-8,7-5,0]) cylinder(2.6, 1, 1);
    translate([W/2+68/2+8,7-5,0]) cylinder(2.6, 1, 1);
    translate([W/2+68/2-8,7+5,0]) cylinder(2.6, 1, 1);
    translate([W/2+68/2+8,7+5,0]) cylinder(2.6, 1, 1);

  }
}

if( BUFFER ) {
  translate([-30,0,0]) {
    // Plate
    translate([20/2,14/2,0]) cylinder(2, 8.5, 8.5);
    translate([20/2,14/2,2]) cylinder(2, 5.5, 5.5);
    // Spring cylinder
    difference() {
      translate([20/2,14/2,2]) cylinder(14, 4, 4);
      translate([20/2,14/2,4]) cylinder(12, 3.1, 3.1);
    }
  }
}


if( HEAD ) {
  translate([0,0,0]) {

    // Buffer basis rechts
    difference() {
      translate([W/2+68/2,20+12,18]) rotate([90,0,0]) cylinder(12, 5, 6);
      translate([W/2+68/2,20+12,18]) rotate([90,0,0]) cylinder(12, 4, 4);
    }
    
    // Buffer basis links
    difference() {
      translate([W/2-68/2,20+12,18]) rotate([90,0,0]) cylinder(12, 5, 6);
      translate([W/2-68/2,20+12,18]) rotate([90,0,0]) cylinder(12, 4, 4);
    }

    // Profiel bevestiging links
    difference() {
      translate([0,-21+3.6,2]) cube([12, 39, 10]);
      translate([2,-40,2]) cube([12-4, 58, 10-2]);
      // screw holes left
      translate([6,-10,2]) cylinder(10, 1.6, 1.6);
      //translate([6,-10,11]) cylinder(1, 1.6, 3.2);
      translate([6,-30,2]) cylinder(10, 1.6, 1.6);
      //translate([6,-30,11]) cylinder(1, 1.6, 3.2);
      // stair cut
      translate([0,4,10]) cube([W, 14, 6]);
    }

    // Profiel bevestiging rechts
    difference() {
      translate([W-10-2,-21+3.6,2]) cube([12, 39, 10]);
      translate([W-10-2+2,-40,2]) cube([12-4, 58, 10-2]);
      // screw holes right
      translate([W-6,-10,2]) cylinder(10, 1.6, 1.6);
      //translate([W-6,-10,11]) cylinder(1, 1.6, 3.2);
      translate([W-6,-30,2]) cylinder(10, 1.6, 1.6);
      //translate([W-6,-30,11]) cylinder(1, 1.6, 3.2);
      // stair cut
      translate([0,4,10]) cube([W, 14, 6]);
    }
    
    // front
    difference() {
      translate([0,-2-34,0]) cube([W, 24+34, 25+8+2+5]);
      translate([0,21,27]) cube([W, 1, 20]);

      // profile cut left
      translate([2,-2-34,2]) cube([8, 24+30, 8]);
      // profile cut right
      translate([W-8-2,-2-34,2]) cube([8, 24+30, 8]);

      translate([3,-4-34,2]) cube([W-6, 24+34-2, 25+8+8]);
      translate([2,-40,0]) cube([12-4, 53-8, 10]);
      translate([W-10-2+2,-40,0]) cube([12-4, 53-8, 10]);
      
      // Zijkant rondingen
      translate([0,-29,40+5]) rotate([0,90,0]) cylinder(W, 35, 35);
      translate([0,-36,10]) cube([W, 7, 5]);
      
      
      // top cut
      translate([24-2,-40,0]) cube([W+4-2*24, 64-24+8, 2]);
      // stair cut
      translate([0,6,32]) rotate([0,90,0]) rcube(W,5,12,1);
      translate([0,6,15]) rotate([0,90,0]) rcube(W,5,12,1);
      
      // coupler cut
      translate([W/2-50/2,18,32]) cube([50,4,10]);
      // railing
      translate([1,16,0]) cylinder(2, 1.6, 1.6);
      translate([1,-4,0]) cylinder(2, 1.6, 1.6);
      translate([W-1,16,0]) cylinder(2, 1.6, 1.6);
      translate([W-1,-4,0]) cylinder(2, 1.6, 1.6);
      
      // deksel
      translate([14,6,0]) difference() {
        cylinder(.4, 4,4);
        cylinder(.4, 3,3);
      }
      translate([W-14,6,0]) difference() {
        cylinder(.4, 4,4);
        cylinder(.4, 3,3);
      }
      
      translate([0,-28,20]) cube([.8,50,20]);
      translate([W-.8,-28,20]) cube([.8,50,20]);
    }


    // top
    difference() {
      translate([-2,-36,0]) cube([W+4, 60, 2]);
      translate([24-2,-40,0]) cube([W+4-2*24, 64-24+8, 2]);

      // railing
      translate([1,16,0]) cylinder(4, 1.6, 1.6);
      translate([1,-4,0]) cylinder(4, 1.6, 1.6);
      translate([W-1,16,0]) cylinder(4, 1.6, 1.6);
      translate([W-1,-4,0]) cylinder(4, 1.6, 1.6);
      
      // deksel
      translate([14,6,0]) difference() {
        cylinder(.4, 4,4);
        cylinder(.4, 3,3);
      }
      translate([W-14,6,0]) difference() {
        cylinder(.4, 4,4);
        cylinder(.4, 3,3);
      }
      
    }
    translate([W/2,28,2]) rotate([0,90,180]) prism(2,4,W/2+2); 
    translate([W/2,28,0]) rotate([180,270,0]) prism(2,4,W/2+2); 


  }
}

