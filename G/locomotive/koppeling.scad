/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../../lib/shapes.scad>


KOPPELING=1;
DOPJE=0;
LENS1=0;
LENS2=0;
LEDTOP=0;
LEDLEPEL=0;
PLAFONDSTRIP=0;
PLAFONDLIP=0;
SCHERM=0;

LEDW=70; // 36 top, 70 bottom
LEDH=3.0; // 6.5 top, 3.0 bottom

if( SCHERM ) {
  translate([50,50,0]) {
    translate([0,0,0]) cube([108, 50, 1.4]);
  }
}

if( PLAFONDSTRIP ) {
  translate([0,50,0]) {
    difference() { 
      translate([0,0,0]) cube([220, 12, 4]);
      translate([0,2,2]) cube([220, 8, 2]);
    }
  }
}
if( PLAFONDLIP ) {
  translate([0,70,0]) {
    difference() { 
      translate([0,0,0]) cube([40, 7.8, 2]);
    }
  }
}



if( LEDLEPEL ) {
  translate([0,40,0]) {
    difference() { 
      translate([0,0,0]) cube([62, 2, 8]);
      translate([5,5,4]) rotate([90,0,0]) cylinder(10, 1.75, 1.75);
    }
    translate([62,0,0]) cube([2, 10, 8]);
    difference() { 
      translate([62,10,0]) cube([15, 2, 8]);
      translate([72,15,4]) rotate([90,0,0]) cylinder(10, 1.75, 1.75);
    }

  }
  
}

if( LEDTOP ) {
  translate([0,40,0]) {
    difference() { 
      translate([0,0,0]) cylinder(LEDH, 3.75, 3.75);
      translate([0,0,1]) cylinder(LEDH-1, 3.2, 3.2);
   
      translate([-1.25,0,0]) cylinder(1, 0.75, 0.75);
      translate([1.25,0,0]) cylinder(1, 0.75, 0.75);
    }
  }

  translate([0,40,0]) {
    difference() { 
      translate([-3.75,0,0]) cube([3.75*2, LEDW, 3]);
      translate([-3.2,0,1]) cube([3.2*2, LEDW, 3]);
      translate([0,0,0]) cylinder(4, 3.2, 3.2);
      translate([0,LEDW,0]) cylinder(4, 3.2, 3.2);

      translate([0,LEDW/2,0]) cylinder(4, 1.5, 1.5);

      translate([-1.25,5,0]) cylinder(5, 0.75, 0.75);
      translate([1.25,5,0]) cylinder(5, 0.75, 0.75);
      translate([-1.25,LEDW-5,0]) cylinder(5, 0.75, 0.75);
      translate([1.25,LEDW-5,0]) cylinder(5, 0.75, 0.75);
    }
  }
  
  translate([0,40+LEDW,0]) {
    difference() { 
      translate([0,0,0]) cylinder(LEDH, 3.75, 3.75);
      translate([0,0,1]) cylinder(LEDH-1, 3.2, 3.2);
   
      translate([-1.25,0,0]) cylinder(1, 0.75, 0.75);
      translate([1.25,0,0]) cylinder(1, 0.75, 0.75);
    }
  }
}



if( KOPPELING ) {
 difference() { 
  translate([-4,0,0]) cube([8, 30, 4]);
  translate([0,30,0]) cylinder(9, 1.75, 1.75);
 }

  //translate([-12,-2,0]) cube([24, 4, 14]);
  translate([-12,-2,0]) roundedcube(24, 4, 14, 2);
  translate([0,0,7]) rotate([90,0,0]) cylinder(7, 5, 3);


 difference() { 
  translate([0,30,0]) cylinder(9, 4, 4);
  translate([0,30,0]) cylinder(9, 1.75, 1.75);
 }
 
 }
 
 if( DOPJE ) {
   translate([20,0,0]) difference() { 
   translate([0,0,0]) rotate([0,0,0]) cylinder(7, 3.5, 5.5);
   translate([0,0,1]) rotate([0,0,0]) cylinder(7, 3, 5);
   translate([-10,-10,6]) cube([20, 20, 4]);
 }
 }
 
 
if( LENS1 ) {
  translate([40,0,0]) {
   translate([0,0,0]) cylinder(.6, 3.25, 3.25);
   translate([0,0,0]) cylinder(2.5, 2.45, 2.45);
  }
}


if( LENS2 ) {
  translate([40,0,0]) {
   translate([0,10,0]) cylinder(.6, 2.75, 2.75);
   translate([0,10,0]) cylinder(2.5, 2.45, 2.45);
  }
}

