/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
//$fn=64;
include <../../lib/shapes.scad>

W =70; // binnen breedte
WW=74; // buiten breedte
H =65; // bovenkant motor kap

LSF=30;   // front length
LSM=105;  // motor cut length
LS=105+30+4; // strip lengte + 4mm overhang


HEAD=0;
LEDHOUDER=1;
LEDHOUDERVERBINDER=1;
HEADRAILING=0;
HEADSIDE=0;
HEADSIDEMIRROR=0;
HEADSTRIP=0;
HEADGLUESTRIP1=0; // 40mm zijkant -->> Eerst aan de zijkant lijmen.
HEADGLUESTRIP2=0; // 60mm bovenkant -->> Moet eerst aan de top gelijmd worden!!! Anders past het niet goed omdat het front niet helemaal 2mm dick is.

HEADLIGHT=0;
HEADLIGHTLENS=0;

LONGSIDE=0;
SHORTSIDE=0;
CABINSIDE=0;
CABINFRONT=0;
CABINROOF=0;
CABINSIDETOP=0;
CABINDOOR=0;
CABINDOORSTEP=0;
MIRRORSIDE=0;
ROOSTER=0;
CABINROOSTER=0;
LONGTOP=0;
SHORTTOP=0;
CHIMNEY=0;
CHIMNEYTOP=0;
VENTILATOR=0;
ROTOR=0;

CABINSIDEWINDOW=0;
CABINFRONTWINDOW=0;
FRONTLOGO=0;
SIDENUMBER=0;


KR=5; // komplam radius
WR=2.5; // waarschuwingslamp radius
WARNLIGHTS=0;

module handgreep() {
  translate([0,0,30]) rotate([0,0,0]) tube(1, 4);
  translate([-3,0,4]) cylinder(26, 1,1);
  translate([0,0,4]) rotate([0,180,180]) tube(1, 4);
}

if( HEADSIDE ) mirror([0,HEADSIDEMIRROR?1:0,0]) translate([-10,0,0]) {
  translate([-WW/2-1,7,6]) handgreep();
  translate([-WW/2,7,6]) rotate([90,0,90]) cylinder(5, 1,1);
  translate([-WW/2,7,40]) rotate([90,0,90]) cylinder(5, 1,1);

  translate([-WW/2,2,0]) cube([1.2,10,H-8]);
  translate([-WW/2+1.2,4,4]) cube([1,6,H-8-8]);
  difference() {
    translate([-W/2+3,2,0]) cylinder(H-8,5,5);
    translate([-W/2+3,2,0]) cylinder(H-8,3.8,3.8);
    translate([-WW/2,2,0]) cube([10,10,H-8]);
    translate([-WW/2+5,-3,0]) cube([10,10,H-8]);
  }
}


if( HEADLIGHT ) translate([90,0,0]) {
  LR=5/2; // LED radius
  difference() {
    translate([0,0,0]) cylinder(4,KR-1,KR);
    translate([0,0,0]) cylinder(6,LR,LR);
  }
  difference() {
    translate([0,0,4]) cylinder(2,KR,KR);
    translate([0,0,4]) cylinder(2,LR,LR+1.5);
  }
  
  difference() {
    translate([0,0,6]) cylinder(2,KR+1,KR+1);
    translate([0,0,7]) cylinder(6,KR+.25,KR+.25);
    translate([0,0,0]) cylinder(8,LR,LR);

    translate([0,0,6]) cylinder(1,LR+1.5,KR-.5);
  }
  
}
if( HEADLIGHTLENS ) translate([90,20,0]) {
  d=10.25;
  difference() {
    translate([0,0,0]) cylinder(.8,d/2-.5,d/2);
    translate([0,0,.4]) cylinder(1.8,(d-2)/2,(d-2)/2);
  }
  difference() {
    translate([0,0,.8]) cylinder(1,d/2,d/2);
    translate([0,0,.4]) cylinder(1.8,(d-2)/2,(d-2)/2);
  }
}


if( LEDHOUDER ) translate([100,0,0]) {

  // koplampen rij 1
  translate([-W/2+8,0,0]) LEDCUP(0,0,false);
  translate([W/2-8,0,0]) LEDCUP(0,0,false);
  difference() {
    translate([-W/2+8,-4,0]) cube([W-2*8, 8,2]);
    translate([-W/2+8,0,0]) LEDHOLES(0,0);
    translate([W/2-8,0,0]) LEDHOLES(0,0);
    translate([0,0,0]) cylinder(10,2,2);
    translate([-W/2+20,0,0]) cylinder(10,2.1,2.1);
    translate([W/2-20,0,0]) cylinder(10,2.1,2.1);
  }


  // koplampen rij 2
  translate([-W/2+8,14,0]) LEDCUP(0,0,false);
  translate([W/2-8,14,0]) LEDCUP(0,0,false);
  difference() {
    translate([-W/2+8,14-4,0]) cube([W-2*8, 8,2]);
    translate([-W/2+8,14,0]) LEDHOLES(0,0);
    translate([W/2-8,14,0]) LEDHOLES(0,0);
    translate([0,14,0]) cylinder(10,2,2);
    translate([-W/2+20,14,0]) cylinder(10,2.1,2.1);
    translate([W/2-20,14,0]) cylinder(10,2.1,2.1);
  }

  // koplamp boven
  translate([0,45,0]) LEDCUP(0,0,false);
  difference() {
    translate([-4,14-4,0]) cube([8,45-10,2]);
    translate([0,45,0]) LEDHOLES(0,0);
    translate([0,14,0]) cylinder(10,2,2);
    translate([0,32,0]) cylinder(10,1.5,1.5);
  }
  
  // versteviging
  difference() {
    translate([0,14,0]) cylinder(2,16,16);
    translate([-W/2+8,0,0]) cube([W,16,2]);
    translate([0,0,0]) cylinder(10,2,2);
    translate([0,14,0]) cylinder(10,2,2);
    translate([-W/2+20,14,0]) cylinder(10,2.1,2.1);
    translate([W/2-20,14,0]) cylinder(10,2.1,2.1);
  }

}
if( LEDHOUDERVERBINDER ) translate([140,0,0]) {
  // verbinding
  // bodem
  difference() {
    translate([0,-4,4]) cube([W-24,8,2]);
    translate([(W-24)/2,0,4]) cylinder(10,2,2);
    translate([(W-24)/2+15,0,4]) cylinder(10,1.5,1.5);
    translate([(W-24)/2-15,0,4]) cylinder(10,1.5,1.5);
  }
  difference() {
    translate([(W-24)/2+15,0,6]) cylinder(2,2,2);
    translate([(W-24)/2+15,0,6]) cylinder(2,1.5,1.5);
  }
  difference() {
    translate([(W-24)/2-15,0,6]) cylinder(2,2,2);
    translate([(W-24)/2-15,0,6]) cylinder(2,1.5,1.5);
  }

  difference() {
    translate([0,14-4,4]) cube([W-24,8,2]);
    translate([(W-24)/2,14,4]) cylinder(10,2,2);
    translate([(W-24)/2+15,14,4]) cylinder(10,1.5,1.5);
    translate([(W-24)/2-15,14,4]) cylinder(10,1.5,1.5);
  }
  difference() {
    translate([(W-24)/2+15,14,6]) cylinder(2,2,2);
    translate([(W-24)/2+15,14,6]) cylinder(2,1.5,1.5);
  }
  difference() {
    translate([(W-24)/2-15,14,6]) cylinder(2,2,2);
    translate([(W-24)/2-15,14,6]) cylinder(2,1.5,1.5);
  }
  // wanden
  translate([0,4-2,0]) cube([W-24,2,4]);
  translate([0,14-4,0]) cube([W-24,2,4]);
  // top
  translate([0,4-2,0]) cube([W-24,10,2]);
}


if( CABINDOORSTEP ) translate([0,-10,0]) {
  w=12;
  h=6;
  translate([0,0,0]) cube([w,.8,4]);
  translate([1,h,0]) rotate([0,0,180]) prism(1,h,4);
  translate([w,h,0]) rotate([0,0,180]) prism(1,h,4);

  difference() {
    translate([0,0,0]) cube([w,h,1.2]);
    translate([1,1,0]) cube([w-2,h-2,2]);
  } 
  // verticale lamellen
  for(x = [0 : 6]) {
    translate([2+x*1.47,0,0]) cube([.4,h,1.2]);
  }
  // horizontale lamellen
  for(y = [0 : 2]) {
    translate([0,2+y*1.52,0]) cube([w,.4,1.2]);
  }
}

if( CABINDOOR ) translate([0,0,0]) {
  translate([0,0,0])  cube([20,53,.6]);
  // scharnieren
  translate([0,6,.8]) rotate([0,90,90]) cylinder(4,.5,.5);
  translate([0,53-6-4,.8]) rotate([0,90,90]) cylinder(4,.5,.5);
  translate([0,6,.6])  cube([3,4,.4]);
  translate([0,53-6-4,.6])  cube([3,4,.4]);
  // deurklink
  translate([17,40,.6])  cube([2,6,.4]);
  translate([18,43,1.2]) rotate([0,270,0]) cylinder(4,.6,.6);
  translate([18,43,1.2])  sphere(.6);
  
    for(y = [0 : 6]) {
    translate([3,12+y*1.2,.4]) rotate([30,0,0]) cube([6,.4,.8]);
    translate([11.5,12+y*1.2,.4]) rotate([30,0,0]) cube([6,.4,.8]);
  }

}


if( HEAD ) translate([0,0,0]) {
  // voorkant
  difference() {
    translate([-W/2,0,0]) cube([W,2,H-2]);
    // koplamp openingen onder rij 1
    translate([-W/2+8,4,10]) rotate([90,90,0]) cylinder(10,KR,KR);
    translate([W/2-8,4,10]) rotate([90,90,0]) cylinder(10,KR,KR);

    // koplamp openingen onder rij 2
    translate([-W/2+8,4,24]) rotate([90,90,0]) cylinder(10,KR,KR);
    translate([W/2-8,4,24]) rotate([90,90,0]) cylinder(10,KR,KR);

    // koplamp openingen boven
    translate([0,4,H-10]) rotate([90,90,0]) cylinder(10,KR,KR);

    // waarschuwlamp openingen boven
    if( WARNLIGHTS ) {
      translate([-WW/2+7,4,H-7]) rotate([90,90,0]) cylinder(10,WR,WR);
      translate([WW/2-7,4,H-7]) rotate([90,90,0]) cylinder(10,WR,WR);
    }
    // schroefgat
    translate([0,-10,8-2]) rotate([270,0,0]) cylinder(20, .7,.7);
  }
  
  if( HEADRAILING ) {
  // handgrepen
    translate([-WW/2-1,7,6]) handgreep();
    translate([WW/2+1,7,6]) mirror([1,0,0]) handgreep();
  }

  // deuren
  translate([.5,-.6,8]) cube([9,.6,H-27]);
  translate([.5+9+1,-.6,8]) cube([9,.6,H-27]);
  translate([-9-.5,-.6,8]) cube([9,.6,H-27]);
  translate([-18-.5-1,-.6,8]) cube([9,.6,H-27]);
  // scharnieren links
  translate([-9-.5,0,12]) cylinder(4,.8,.8);
  translate([-9-.5-9-1,0,12]) cylinder(4,.8,.8);
  translate([-9-.5,0,H-23-4]) cylinder(4,.8,.8);
  translate([-9-.5-9-1,0,H-23-4]) cylinder(4,.8,.8);
  // scharnieren rechts
  translate([9+.5,0,12]) cylinder(4,.8,.8);
  translate([9+.5+9+1,0,12]) cylinder(4,.8,.8);
  translate([9+.5,0,H-23-4]) cylinder(4,.8,.8);
  translate([9+.5+9+1,0,H-23-4]) cylinder(4,.8,.8);

  // zijkant links
  difference() {
    translate([-WW/2,2,0]) cube([2,10,H-2]);
    // waarschuwlamp openingen boven
    if( WARNLIGHTS ) {
      translate([-WW/2-5,7,H-7-WR*2]) rotate([90,0,90]) cylinder(WW+10,WR,WR);
    }
    if( !HEADRAILING ) {
      translate([-WW/2-5,7,6]) rotate([90,0,90]) cylinder(WW+10, 1.1,1.1);
      translate([-WW/2-5,7,40]) rotate([90,0,90]) cylinder(WW+10, 1.1,1.1);
    }
  }
  
  // zijkant rechts
  difference() {
    translate([WW/2-2,2,0]) cube([2,10,H-2]);
    // waarschuwlamp openingen boven
    if( WARNLIGHTS ) {
      translate([-WW/2-5,7,H-7-WR*2]) rotate([90,0,90]) cylinder(WW+10,WR,WR);
    }
    if( !HEADRAILING ) {
      translate([-WW/2-5,7,6]) rotate([90,0,90]) cylinder(WW+10, 1.1,1.1);
      translate([-WW/2-5,7,40]) rotate([90,0,90]) cylinder(WW+10, 1.1,1.1);
    }
  }
  
  // bovenkant
  difference() {
    translate([-W/2,2,H-2]) cube([W,10,2]);
  }
  // versteviging
  difference() {
    translate([-W/2,2,16]) cube([W,10,2]);
  }
  
  // ronding links 
  difference() {
    translate([-W/2,2,0]) cylinder(H-2, 2,2);
    translate([-W/2,2,0]) cube([W,2,16]);
  }
  // ronding rechts 
  difference() {
    translate([+W/2,2,0]) cylinder(H-2, 2,2);
    translate([-W/2,2,0]) cube([W,2,16]);
  }
  // rondingen boven en hoeken
  translate([-W/2,2,H-2]) rotate([0,90,0]) cylinder(W,2,2);
  translate([-W/2,2,H-2]) sphere(2);
  translate([W/2,2,H-2]) sphere(2);

  // rondingen links boven
  translate([-W/2,10+2,H-2]) rotate([90,90,0]) cylinder(10,2,2);
  // rondingen rechst boven
  translate([W/2,10+2,H-2]) rotate([90,90,0]) cylinder(10,2,2);

}


if( HEADSTRIP ) translate([80,0,0]) {
  difference() {
    translate([-W/2,0,0]) cube([W,LS,12]);
    translate([-W/2+2,2,0]) cube([W-4,LS-2,12]);

    // motorsteun sparing
    translate([-W/2,LS-12-16,0]) cube([W,16,2]);
    
    // overhang
    translate([-W/2,LS-4,0]) cube([W,4,2]);
    
    // schroefgaten
    translate([0,-10,8]) rotate([270,0,0]) cylinder(20, .7,.7);
    translate([-W/2,40,8]) rotate([0,90,0]) cylinder(W, .7,.7);
    translate([-W/2,120,8]) rotate([0,90,0]) cylinder(W, .7,.7);

    translate([-W/2,0,0]) cube([W,30,2]);
    
    // koplamp openingen onder rij 1
    translate([-W/2+8,4,10+2]) rotate([90,90,0]) cylinder(10,KR+1,KR+1);
    translate([W/2-8,4,10+2]) rotate([90,90,0]) cylinder(10,KR+1,KR+1);
    
  }

  difference() {
    translate([-W/2,0,2]) cube([W,30,2]);
    translate([-W/2+6,2+4,2]) cube([W-12,30-10,2]);
  }

  translate([-W/2,LS-2-4,4]) cube([W,2,8]);
}


if( HEADGLUESTRIP1 ) {
  translate([0,0,0]) cube([40,16,1.8]);
}

if( HEADGLUESTRIP2 ) {
  difference() { 
    translate([0,20,0]) cube([60,16,1.8]);
    // ventilator cut
    translate([30,50,0]) cylinder(10,25,25);
  }
}

// use 2.5mm nozzle
module roosterA(w,h) {
  difference() {
    translate([0,0,0]) cube([w,h,1.2]);
    translate([1,1,.3]) cube([w-2,h-2,2]);
  } 
  // verticale lamellen
  for(x = [0 : w-12]) {
    translate([2+x*1.47,0,0]) cube([.4,h,.9]);
  }
  // horizontale lamellen
  for(y = [0 : h-21]) {
    translate([0,2+y*1.52,0]) cube([w,.4,.9]);
  }
}

module deurScharnieren(w,h,links) {
  if( links ) {
    // links
    translate([0,10,0]) rotate([90,0,0]) cylinder(4,.8,.8);
    translate([0,h-4,0]) rotate([90,0,0]) cylinder(4,.8,.8);
  }
  else {
    // rechts
    translate([w,10,0]) rotate([90,0,0]) cylinder(4,.8,.8);
    translate([w,h-4,0]) rotate([90,0,0]) cylinder(4,.8,.8);
  }
}


module deurA(w,h,beluchting) {
  translate([0,0,0]) cube([w,h,.8]);
  if( beluchting ) {
    for(y = [0 : 3]) {
      translate([(w-w/3)/2,4+y*1.2,-.2+.8]) rotate([30,0,0]) cube([w/3,.4,.8]);
      translate([(w-w/3)/2,h-10+y*1.2,-.2+.8]) rotate([30,0,0]) cube([w/3,.4,.8]);
    }
  }
}


// rooster onder
module deurB(w,h) {
  difference() {
    translate([0,0,0]) cube([w,h,.8]);
    translate([1,2,0]) cube([w-2,h/2,.8]);
  }
  for(y = [0 : 25]) {
    translate([1,2+y*1.2,-.2]) rotate([30,0,0]) cube([w-2,.4,.8]);
  }
}


// rooster boven
module deurC(w,h,onder) {
  difference() {
    translate([0,0,0]) cube([w,h,.8]);
    translate([1,h/2+14,0]) cube([w-2,h/2-14-1,.8]);
    if( onder )
      translate([1,2+10,0]) cube([w-2,h/2-10,.8]);
  }
  for(y = [0 : 12]) {
    translate([1,h/2+14+y*1.2,-.2]) rotate([30,0,0]) cube([w-2,.4,.8]);
  }
  if( onder ) {
    for(y = [0 : 16]) {
      translate([1,2+10+y*1.2,-.2]) rotate([30,0,0]) cube([w-2,.4,.8]);
    }
  }
}


if( ROOSTER ) {
  translate([0,-70,0]) roosterA(28,H-12);
}


if( LONGSIDE ) mirror([MIRRORSIDE?1:0,0,0]) translate([0,0,0]) {
  LSL=240; // +2mm voor cabin plakstrip
  
  difference() {
    translate([0,0,0]) cube([LSL,H-2,2]);
    //translate([85+2,10,0]) cube([85-4,H-2-12,2]);

    // bevestigingsgaten
    translate([33,6,0]) cylinder(14,.7,.7);
    translate([110,6,0]) cylinder(14,.7,.7);
    translate([229,6,0]) cylinder(14,.7,.7);
    // rooster sparing
    translate([2.8,2.8,1.6]) cube([28.4,H-11.6,1]);
  }
  // roorster rand
  difference() {
    translate([2,2,2]) cube([30,H-10,1]);
    translate([2.8,2.8,2]) cube([28.4,H-11.6,1]);
  }  
  
  // cabin plakstrip
  translate([LSL,0,0]) cube([2,H-2,8]);
  // midden segment
  difference() {
    translate([85,0,2]) cube([85,H-2,1.2]);
    translate([110,6,0]) cylinder(14,.7,.7);
  }  
  // front rooster -> 0.25mm nozzle !!!
  //translate([2,2,1.8]) roosterA(30,H-6);
  
  // segment 1 deuren
  for(x = [0 : 2]) {
    DW1=16;
    if( x == 1 ) {
      translate([2+30+2+x*(DW1+1),2,2]) deurB(DW1,H-6);
    }
    else {
      translate([2+30+2+x*(DW1+1),2,2]) deurA(DW1,H-6,true);
      translate([2+30+2+x*(DW1+1),2,2]) deurScharnieren(DW1,H-6,x%2);
    }
  }
  
  // segment 2 deuren
  difference() {
    for(x = [0 : 4]) {
      DW1=15.45;
      translate([85+2+x*(DW1+1),2,2+1.2]) deurA(DW1,H-6);
      if( x == 0 )
        translate([85+2+x*(DW1+1),2,2+1.2]) deurScharnieren(DW1,H-6,1);
      else if( x > 2 )
        translate([85+2+x*(DW1+1),2,2+1.2]) deurScharnieren(DW1,H-6,0);
      else
        translate([85+2+x*(DW1+1),2,2+1.2]) deurScharnieren(DW1,H-6,x%2);
    }
    translate([110,6,0]) cylinder(14,.7,.7);
  }
  
  // segment 3 deuren
  difference() {
    for(x = [0 : 3]) {
      DW1=15.75;
      if( x > 1 ) {
        translate([85+85+2+x*(DW1+.6),2,2]) deurC(DW1,H-6,true);
      }
      else {
        translate([85+85+2+x*(DW1+.6),2,2]) deurC(DW1,H-6,false);
        translate([85+85+2+x*(DW1+.6),2,2]) deurScharnieren(DW1,H-6,!(x%2));
      }
    }
    translate([229,6,0]) cylinder(14,.7,.7);
  }
}


if( SHORTSIDE ) mirror([MIRRORSIDE?1:0,0,0]) translate([0,H+10,0]) {
  SSL=104; // + 2mm voor cabin plakstrip
  
  difference() {
    translate([0,0,0]) cube([SSL,H-2,2]);
    // bevestigingsgaten
    translate([30,6,0]) cylinder(14,.7,.7);
    translate([92,6,0]) cylinder(14,.7,.7);
  }
  
  translate([SSL,0,0]) cube([2,H-2,8]);


  difference() {
    for(x = [0 : 2]) {
      DW1=12;
      translate([2+x*(DW1+1),2,2]) deurA(DW1,H-6,x==1?false:true);
      translate([2+x*(DW1+1),2,2]) deurScharnieren(DW1,H-6,1);
    }
    // bevestigingsgaten
    translate([30,6,0]) cylinder(14,.7,.7);
  }

  for(x = [0 : 1]) {
    DW1=11;
    translate([2+42+x*(DW1+1),2,2]) deurA(DW1,H-6,false);
    translate([2+42+x*(DW1+1),2,2]) deurScharnieren(DW1,H-6,1);
  }

  difference() {
    for(x = [0 : 1]) {
      DW1=15;
      translate([2+42+26+x*(DW1+1),2,2]) deurC(DW1,H-6,false);
    }
    // bevestigingsgaten
    translate([92,6,0]) cylinder(14,.7,.7);
  }
}

CH=H-2; // height
CW=84;  // width
CD=22;  // depth
if( CABINSIDE ) mirror([MIRRORSIDE?1:0,0,0]) translate([0,H+10,0]) {
  difference() {
    translate([0,0,0]) cube([CW,CD,CH]);
    translate([2,2,1]) cube([CW-4,CD-2,CH]);
    translate([2,CD-8,0]) cube([CW-4,CD-2,CH]);

    // klep sparing
    translate([CW/2-(CW-16)/2,0,0]) cube([CW-16,.4,20]);
    
    // deur sparing
    translate([0,1,10]) cube([.4,CD-2,CH]);

    // luchtrooster sparing
    translate([CW-.4,2,40]) cube([.4,CD-4,10]);
  }
  // klep boven rand
  translate([CW/2-(CW-14)/2,-.4,20]) cube([CW-14,.4,1]);


}


if( CABINROOSTER ) translate([90,H+10,0]) {
  
  difference() {
    translate([.2,.2,0]) cube([CD-4-.4,10-.4,.6]);
  }  
  for(y = [0 : 6]) {
    translate([1.5,1.4+y*1.2,.4]) rotate([30,0,0]) cube([6,.4,.8]);
    translate([10.5,1.4+y*1.2,.4]) rotate([30,0,0]) cube([6,.4,.8]);
  }
  

  // cabin klep
  translate([CD,0,0]) {
    difference() {
      translate([.4,.4,0]) cube([CW-16-.8,20-.8,.8]);
      translate([(CW-16)/2-3,0,.4]) cube([6,6,1]);
    }
    translate([(CW-16)/2-2.5,4,0]) cube([5,1,1.4]);
    
    for(y = [0 : 6]) {
      translate([1,4+y*1.2,.6]) rotate([30,0,0]) cube([7,.4,.8]);
      translate([10,4+y*1.2,.6]) rotate([30,0,0]) cube([7,.4,.8]);
      translate([CW-16-18,4+y*1.2,.6]) rotate([30,0,0]) cube([7,.4,.8]);
      translate([CW-16-18+10,4+y*1.2,.6]) rotate([30,0,0]) cube([7,.4,.8]);
    }
  }

}


if( SHORTTOP ) translate([0,H+10+WW+10,0]) {
  SSL=104-2; //  -2mm dikte cabin

  // lijm rand
  difference() {
    translate([10,2,0]) cube([SSL-10,WW-4,2]);
    translate([10+2,2+2,0]) cube([SSL-10-4,WW-4-4,2]);
  }
  // verstevigingen
  translate([10,WW/2-1,0]) cube([SSL-10,2,2]);
  for(x = [1 : 2]) {
    translate([10+x*30,2,0]) cube([2,WW-4,2]);
  }

  // top
  difference() {
    translate([0,2,2]) cube([SSL,WW-4,2]);
  }

  // afronding links
  difference() {
    translate([0,2,2]) rotate([90,0,90]) cylinder(SSL,2,2);
    translate([0,0,0]) cube([SSL,WW,2]);
  }
  // afronding rechts
  difference() {
    translate([0,WW-2,2]) rotate([90,0,90]) cylinder(SSL,2,2);
    translate([0,0,0]) cube([SSL,WW,2]);
  }
  
  MR=1;
  // dekplaat
  translate([10,5,4.6]) cube([40,WW-10,1.2]);
  translate([10+.4,5+.4,4]) cube([40-.4*2,WW-10-.4*2,1.2]);
  // moeren
  translate([10+2,5+2,5.6]) cylinder(.8,MR,MR);
  translate([10+40-2,5+2,5.6]) cylinder(.8,MR,MR);
  translate([10+2,5+WW-10-2,5.6]) cylinder(.8,MR,MR);
  translate([10+40-2,5+WW-10-2,5.6]) cylinder(.8,MR,MR);
  translate([10+40/2,5+2,5.6]) cylinder(.8,MR,MR);
  translate([10+40/2,5+WW-10-2,5.6]) cylinder(.8,MR,MR);
  translate([10+2,5+(WW-10)/2,5.6]) cylinder(.8,MR,MR);
  translate([10+40-2,5+(WW-10)/2,5.6]) cylinder(.8,MR,MR);
  

}


if( LONGTOP ) translate([0,H+10,0]) {
  LSL=240-2; // -2mm dikte cabin
  
  // lijm rand
  difference() {
    translate([10,2,0]) cube([LSL-10,WW-4,2]);
    translate([10+2,2+2,0]) cube([LSL-10-4,WW-4-4,2]);
    // ventilator cut
    translate([30,WW/2,0]) cylinder(10,24.2,24.2);
  }

  // verstevigingen
  difference() {
    translate([10,WW/2-1,0]) cube([LSL-10,2,2]);
    // ventilator cut
    translate([30,WW/2,0]) cylinder(10,24.2,24.2);
  }

  difference() {
    for(x = [1 : 6]) {
      translate([10+x*32.5,2,0]) cube([2,WW-4,2]);
    }
    // ventilator cut
    translate([30,WW/2,0]) cylinder(10,24.2,24.2);
  }
  
  // top
  difference() {
    translate([0,2,2]) cube([LSL,WW-4,2]);
    // ventilator cut
    translate([30,WW/2,0]) cylinder(10,24.2,24.2);
    // uitlaat cut
    translate([LSL-14,WW/2-12,0]) cylinder(10,10.2,10.2);
    translate([85-.5,2,3.6]) cube([1,WW-4,2]);
    translate([85+85-.5,2,3.6]) cube([1,WW-4,2]);
  }

  // ventilator flens
  difference() {
    translate([30,WW/2,4]) cylinder(.4,24.2+2,24.2+2);
    translate([30,WW/2,4]) cylinder(.4,24.2,24.2);
  }
  
  // top 2de segment
  difference() {
    translate([85,.8,2]) cube([85,WW-4+2*1.2,2]);
  }
  
  MR=1; // moer radius
  // 1ste segment
  translate([64,40,4.6]) cube([12,28,2]);
  translate([64+.4,40+.4,4]) cube([12-.4*2,28-.4*2,1]);
  // moeren
  translate([64+2,40+2,6.6]) cylinder(.8,MR,MR);
  translate([64+12-2,40+2,6.6]) cylinder(.8,MR,MR);
  translate([64+2,40+28-2,6.6]) cylinder(.8,MR,MR);
  translate([64+12-2,40+28-2,6.6]) cylinder(.8,MR,MR);

  // 2de segment
  translate([90,5,4.6]) cube([60,WW-10,1.2]);
  translate([90+.4,5+.4,4]) cube([60-.4*2,WW-10-.4*2,1.2]);
  // moeren
  translate([90+2,5+2,5.6]) cylinder(.8,MR,MR);
  translate([90+60-2,5+2,5.6]) cylinder(.8,MR,MR);
  translate([90+2,5+WW-10-2,5.6]) cylinder(.8,MR,MR);
  translate([90+60-2,5+WW-10-2,5.6]) cylinder(.8,MR,MR);
  translate([90+60/2,5+2,5.6]) cylinder(.8,MR,MR);
  translate([90+60/2,5+WW-10-2,5.6]) cylinder(.8,MR,MR);
  translate([90+2,5+(WW-10)/2,5.6]) cylinder(.8,MR,MR);
  translate([90+60-2,5+(WW-10)/2,5.6]) cylinder(.8,MR,MR);


  // 3de segment
  difference() {
    // uitlaat flens
    translate([LSL-14,WW/2-12,4]) cylinder(.6,10.2+2,10.2+2);
    translate([LSL-14,WW/2-12,0]) cylinder(10,10.2,10.2);
  }

  
  // afronding links
  difference() {
    translate([0,2,2]) rotate([90,0,90]) cylinder(85,2,2);
    translate([0,0,0]) cube([LSL,WW,2]);
  }
  difference() {
    translate([85,.8,2]) sphere(2);
    translate([0,-2,0]) cube([LSL,WW+4,2]);
  }
  difference() {
    translate([85,2,2]) rotate([90,0,0]) cylinder(1.2,2,2);
    translate([0,-2,0]) cube([LSL,WW+4,2]);
  }
  difference() {
    translate([85,.8,2]) rotate([90,0,90]) cylinder(85,2,2);
    translate([0,-2,0]) cube([LSL,WW+4,2]);
  }
  difference() {
    translate([85+85,.8,2]) sphere(2);
    translate([0,-2,0]) cube([LSL,WW+4,2]);
  }
  difference() {
    translate([85+85,2,2]) rotate([90,0,0]) cylinder(1.2,2,2);
    translate([0,-2,0]) cube([LSL,WW+4,2]);
  }
  difference() {
    translate([85+85,2,2]) rotate([90,0,90]) cylinder(LSL-2*85,2,2);
    translate([0,0,0]) cube([LSL,WW,2]);
  }
 
  // afronding rechts
  difference() {
    translate([0,WW-2,2]) rotate([90,0,90]) cylinder(85,2,2);
    translate([0,0,0]) cube([LSL,WW,2]);
  }
  difference() {
    translate([85,WW-.8,2]) sphere(2);
    translate([0,-2,0]) cube([LSL,WW+4,2]);
  }
  difference() {
    translate([85,WW-2,2]) rotate([270,0,0]) cylinder(1.2,2,2);
    translate([0,-2,0]) cube([LSL,WW+4,2]);
  }
  difference() {
    translate([85,WW-.8,2]) rotate([90,0,90]) cylinder(85,2,2);
    translate([0,-2,0]) cube([LSL,WW+4,2]);
  }
  difference() {
    translate([85+85,WW-.8,2]) sphere(2);
    translate([0,-2,0]) cube([LSL,WW+4,2]);
  }
  difference() {
    translate([85+85,WW-2,2]) rotate([270,0,0]) cylinder(1.2,2,2);
    translate([0,-2,0]) cube([LSL,WW+4,2]);
  }
  difference() {
    translate([85+85,WW-2,2]) rotate([90,0,90]) cylinder(LSL-2*85,2,2);
    translate([0,0,0]) cube([LSL,WW,2]);
  }

}


module roostermal(r) {
  difference() {
    translate([0,0,0]) cylinder(10,r*2,r*2);
    translate([0,0,0]) cylinder(10,r,r);
  }
}

if( CHIMNEYTOP ) translate([-40,H+10,0])  {
  R=10;
  // verlengstuk
  translate([-26,0,0]) {
    VH=3;
    difference() {
      translate([0,0,0]) cylinder(VH,R,R);
      translate([0,0,0]) cylinder(VH,R-1.6,R-1.6);
    }
    difference() {
      translate([0,0,VH]) cylinder(2,R-1,R-1);
      translate([0,0,VH]) cylinder(2,R-2,R-2);
    }
  }
}

if( CHIMNEY ) translate([-40,H+10,0])  {
  R=10;
  difference() {
    translate([0,0,0]) cylinder(30+3,R,R);
    translate([0,0,0]) cylinder(30+3,R-1.6,R-1.6);
    translate([0,0,28]) cylinder(5,R-1.1,R-1.1);
  }
  difference() {
    translate([0,0,0]) cylinder(1,R+2,R+2);
    translate([0,0,0]) cylinder(1,R,R);
  }

  translate([26,0,0]) {
    difference() {
      translate([0,0,0]) cylinder(1,R-1,R-1);
      translate([0,0,0]) cylinder(1,R-2,R-2);
    }
    
    difference() {
      // verticale lamellen
      for(x = [0 : 10]) {
        translate([-10+.75+x*2,-10,0]) cube([.4,20,.8]);
      }
      roostermal(R-1);
    }
    difference() {
      // horizontale lamellen
      for(y = [0 : 10]) {
        translate([-10,-10+.75+y*2,0]) cube([20,.4,.8]);
      }
      roostermal(R-1);
    }    
   
  }

}



if( VENTILATOR ) translate([+40,H+10,0])  {
  R=24.1;
  // ring
  difference() {
    translate([0,0,0]) cylinder(6,R,R);
    translate([0,0,0]) cylinder(6,R-1.4,R-1.4);
  }
  // flens
  difference() {
    translate([0,0,0]) cylinder(1,R+1,R+1);
    translate([0,0,0]) cylinder(1,R-1.4,R-1.4);
  }

  // rooster ring
  translate([R*2+5,0,0]) {
    difference() {
      translate([0,0,0]) cylinder(2,R-1.6,R-1.6);
      translate([0,0,0]) cylinder(2,R-2.6,R-2.6);
    }
    // rooster flens
    difference() {
      translate([0,0,0]) cylinder(.6,R,R);
      translate([0,0,0]) cylinder(.6,R-2.6,R-2.6);
    }
    
    difference() {
      // verticale lamellen
      for(x = [0 : R/1.5]) {
        translate([-R+0+x*3,-R,0]) cube([.4,R*2,.8]);
      }
      roostermal(R-2.6);
    }
    difference() {
      // horizontale lamellen
      for(y = [0 : R/1.5]) {
        translate([-R,-R+0+y*3,0]) cube([R*2,.4,.8]);
      }
      roostermal(R-2.6);
    }    
    
    
  }

}

module rotorblad(r,h) {
  translate([0,0,0]) rotate([20,0,0]) {
    difference() {
      rcube(.8,r,h,1.5);
      translate([0,1,0]) rotate([0,0,10]) cube([r,h,.8]);
    }
  }
  
}

if( ROTOR )  translate([+40,H+10,0]) {
  R=24;  
  difference() {
    translate([0,0,0]) cylinder(2.6,4,4);
    translate([0,0,0]) cylinder(4,1.6,1.6);
  }
  
  c=12;
  difference() {
    for(n = [0 : c]) {
      translate([0,0,0]) rotate([0,0,n*(360/c)]) rotorblad(R-2,4);
    }
    translate([0,0,0]) cylinder(4,1.6,1.6);
  }
  
  
  
  translate([R*2+5,0,0]) {
    difference() {
      translate([0,0,0]) cylinder(2,R-1.6,R-1.6);
      translate([0,0,0]) cylinder(2,R-2.6,R-2.6);
    }
    // rooster flens
    difference() {
      translate([0,0,0]) cylinder(.6,R,R);
      translate([0,0,0]) cylinder(.6,R-2.6,R-2.6);
    }
    translate([0,0,0]) cylinder(4,1.5,1.5);
    difference() {
      translate([-R,-2,0]) cube([R*2,4,1]);
      roostermal(R-2.6);
    }
    difference() {
      translate([-2,-R,0]) cube([4,R*2,1]);
      roostermal(R-2.6);
    }
  }  

}

module window2(w,h) {
  
linear_extrude(height=2)
    RoundedPolygon([[0,0],[0,18],[5.5,0]],2,64);
}

if( CABINFRONT ) translate([0,0,0]) {  
  CFW=118;
  CFH=30;
  difference() {
    translate([0,0,0]) cube([CFW,CFH-4,4]);
    translate([0,2,2]) cube([CFW,CFH-4,4]);
    translate([0,0,2]) cube([2,CFH-4,4]);
    translate([CFW-2,0,2]) cube([2,CFH-4,4]);
    // schuine kant links
    translate([0,0,4]) rotate([0,90,0]) prism(4,CFH-4,8);
    translate([2,0,6]) rotate([0,90,0]) prism(4,CFH-4,8);
    // schuine kant rechts
    translate([CFW,0,4]) mirror([1,0,0]) rotate([0,90,0]) prism(4,CFH-4,8);
    translate([CFW-2,0,6]) mirror([1,0,0]) rotate([0,90,0]) prism(4,CFH-4,8);

    
    translate([12.4,4,0]) mirror([1,0,0]) window2(8);
    translate([11,2,0]) rcube(2,8,22,2);
    
    translate([CFW-12.4,4,0]) window2(8);
    translate([23,2,0]) rcube(2,84,22,2);

  }

  // lijmstrips cabin basis
  translate([2,-6,2]) cube([10,6,2]);
  translate([CFW-10-2,-6,2]) cube([10,6,2]);
  translate([(CFW-60)/2,-2,0]) cube([60,2,2]);

  // linkse lijmstrip
  difference() {
    translate([8+2,CFH-4,4]) rotate([0,90,180]) prism(2,CFH-4,8);
    translate([8+2+2,CFH-4,4]) rotate([0,90,180]) prism(2,CFH-4,8);
  }
  // rechtse lijmstrip
  difference() {
    translate([CFW-8-2,CFH-4,4]) mirror([1,0,0]) rotate([0,90,180]) prism(2,CFH-4,8);
    translate([CFW-8-2-2,CFH-4,4]) mirror([1,0,0]) rotate([0,90,180]) prism(2,CFH-4,8);
  }

  // boven stuk met lijmstrip
  difference() {
    translate([8,CFH-4,0]) cube([CFW-2*8,6,4]);
    translate([8,CFH-4,2]) cube([CFW-2*8,4,4]);
    translate([8,CFH-4,4]) rotate([0,90,0]) prism(4,4,3);
    translate([CFW-8,CFH-4,4]) mirror([1,0,0]) rotate([0,90,0]) prism(4,4,3);


    translate([CFW-19,CFH+2,4]) rotate([90,90,0]) prism(4,8,2);
    translate([CFW-11,CFH,0]) cube([4,2,4]);

    translate([19,CFH+2,4]) mirror([1,0,0]) rotate([90,90,0]) prism(4,8,2);
    translate([7,CFH,0]) cube([4,2,4]);
  }


}


module cabinroof(h) {
  difference() {
    translate([19,32,0]) cube([40,2,h]);
  }
  translate([30,26,(h-76)/2]) cube([2,6,76]);
  translate([58,26,(h-76)/2]) cube([2,6,76]);

  translate([30,26,(h-76)/2]) cube([28,6,2]);
  translate([30,26,(h-76)/2+76-2]) cube([28,6,2]);

  difference() {
    translate([9.8,29.7,0]) rotate([0,0,14]) cube([10,2,h]);
    translate([5,24.8,0]) rotate([0,0,53]) cube([10,2,h]);
  }
  difference() {
    translate([7,24.8,0]) rotate([0,0,53]) cube([7,2,h]);
    translate([5,24.8,0]) rotate([0,0,53]) cube([10,2,h]);
  }
}

if( CABINROOF ) translate([0,0,0]) {  
  h=112;

  difference() {
    translate([0,0,0]) cabinroof(h);
    translate([120,32,0]) rotate([0,0,180]) prism(120,8,14);
    translate([120,32,h]) mirror([0,0,1]) rotate([0,0,180]) prism(120,8,14);
    translate([60-13.5,42,0]) rotate([90,90,0]) cylinder(20,11,11);
  }

  difference() {
    translate([118,0,0]) mirror([1,0,0]) cabinroof(h);
    translate([120,32,0]) rotate([0,0,180]) prism(120,8,14);
    translate([120,32,h]) mirror([0,0,1]) rotate([0,0,180]) prism(120,8,14);
  }
  

}


if( CABINSIDETOP ) translate([0,40,0]) {  
  CFH=27;
  cw=CW-4;
  difference() {
    translate([0,0,0]) cube([cw,CFH,2]);
    translate([4,2.5,0]) rcube(2,(cw-4*4)/3,22,2);
    translate([4+(cw-4*4)/3+4,2.5,0]) rcube(2,(cw-4*4)/3,22,2);
    translate([4+((cw-4*4)/3)*2+4*2,2.5,0]) rcube(2,(cw-4*4)/3,22,2);
    translate([0,0,2]) rotate([270,0,0]) prism(cw,30-4,8);
 }
}


module window2frame() {
  difference() {
    linear_extrude(height=1) 
      RoundedPolygon([[0,0],[0,18],[5.5,0]],2-.1,64);
    linear_extrude(height=2) 
      RoundedPolygon([[0,0],[0,18],[5.5,0]],.5,64);
  }
}

if( CABINFRONTWINDOW ) translate([0,80,0]) {  
  CFW=118;
  CFH=30;
    difference() {
      translate([1.4,2,0]) mirror([1,0,0]) window2frame();
      translate([1.4,0,0]) cube([4,22,2]);
    }
    difference() {
      translate([-2+.1,.1,0]) rcube(1,8+2-.2,22-.2,2);
      translate([1.5-2,1.5,0]) rcube(2,8-3+2,22-3,1);
      translate([-2,0,0]) cube([3,22,2]);
    }
    difference() {
      translate([CFW-12.4,2,0]) window2frame();
      translate([CFW-12.4-4,0,0]) cube([4,22,4]);
    }
    difference() {
      translate([23+.1,.1,0]) rcube(1,84+2-.2,22-.2,2);
      translate([23+1.5,1.5,0]) rcube(2,84+2-3,22-3,1);
      translate([CFW-12.4,0,0]) cube([4,22,4]);
    }
    translate([23+84/2,.1,0]) cube([1.5,22-.2,1]);
}

if( CABINSIDEWINDOW ) translate([0,80,0]) {  
  cw=CW-4;
  difference() {
    translate([.1,.1,0]) rcube(1,(cw-4*4)/3-.1,22-.1,2);
    translate([1.5,1.5,0]) rcube(1,(cw-4*4)/3-3,22-3,1);
  }
}

if( FRONTLOGO ) translate([40,80,0]) {  
    translate([0,0,0]) rotate([0,0,-10]) cube([30,30,.4]);
    translate([7.3,13,.4]) linear_extrude(.6) text("6405",7);
}

if( SIDENUMBER ) translate([0,0,0]) {  
    translate([0,0,0]) linear_extrude(.6) text("6405",20);
}
