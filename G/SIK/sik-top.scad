/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
//$fn=360;
$fn=128;
//$fn=64;
include <../../lib/shapes.scad>
include <../lib/modules.scad>




FRONT=0;
FRONTROOSTER=0;
UITLAAT=0;
TOPROOSTER=0;
ZIJROOSTER=0;

LIJMSTRIPFRONT=0; // 45x10x1.4

CABFRONT=0;
CABBACK=0;
CABROOF=0;
CABSIDE=0;
CABSIDEWINDOW=0;
CABBACKWINDOW=0;
CABFRONTWINDOW=0;
CABSIDEMIRROR=0;
LIJMSTRIPSIDE=0;
HANDLE1=0;
HANDLE2=0;
KOPLAMP=0;
HAAK=0;
ZWAAILICHT=0;
ZWAAILICHTBODEM=0;
RAILING=0;

KOFFER=0;
KOFFERDEKSEL=0;
ROADNUMBER=0; // 232
ROADNAME=0;   // Janna
ROADNAME2=0;  // Erik
ROADNAME3=1;  // Filia
HANDGREEP=0;

FW=60; // binnen maat
FH=60;
FWT=42;
FD=8; // 8=front 96=midden

if( RAILING ) {
  translate([0,0,1]) rotate([90,0,90]) cylinder(90,1,1);
  translate([0,0,0]) cube([3,3,2]);
  translate([90/2-3,0,0]) cube([3,3,2]);
  translate([90-3,0,0]) cube([3,3,2]);

  translate([0,3-.8,0]) cube([3,.8,6]);
  translate([90/2-3,3-.8,0]) cube([3,.8,6]);
  translate([90-3,3-.8,0]) cube([3,.8,6]);
}

if( ZWAAILICHT ) {
  difference() {
    translate([0,0,3.4]) sphere(5);
    translate([0,0,3.4]) sphere(4);
    translate([-5,-5,-2]) cube([10,10,9]);
  }
  difference() {
    translate([0,0,0]) cylinder(7,4,3.5);
    translate([0,0,0]) cylinder(7,3,2.5);
  }
}
if( ZWAAILICHTBODEM ) {
    translate([20,0,1]) cylinder(1,4,4);
    translate([20,0,0]) cylinder(1,3,3);
    translate([20,0,0]) cylinder(4,1,.9);
}


if( HAAK ) {
  difference() {
    translate([0,0,0]) cylinder(3.8, 4, 4);
    translate([-2,.5,0]) cylinder(5, 3,3);
    translate([-5,-4,0]) rotate([0,0,0]) prism(10,10,1.5);
    translate([5,-4,3.8]) rotate([0,180,0]) prism(10,10,1.5);
  }  
  
  translate([-9,-2.5,1.9]) rotate([90,0,90]) cylinder(5, 1.9,1.9);
  difference() {
    translate([-4,-2.5,1.9]) rotate([90,0,90]) cylinder(5, 1.9,1.5);
    translate([-2,.5,0]) cylinder(5, 3,3);
    translate([-5,-4,0]) rotate([0,0,0]) prism(10,10,1.5);
    translate([5,-4,3.8]) rotate([0,180,0]) prism(10,10,1.5);
  }  
}


if( KOPLAMP ) {
  // afdekplaat achterkant
  translate([20,0,0]) cylinder(.8, 14/2,14/2);
  difference() {
    translate([20,0,.8]) cylinder(1.2, 12/2,12/2);
    translate([20,0,.8]) cylinder(2, 10/2,10/2);
  }  

  // behuizing
  difference() {
    translate([0,0,0]) cylinder(9.6, 14/2,14/2);
    translate([0,0,1.4]) cylinder(12, 12/2,12/2);
    translate([0,0,0]) cylinder(12, 10/2,10/2);
    translate([0,-5,5]) rotate([90,0,0]) cylinder(10, 1.4,1.2);
  }
  
  // standaard
  difference() {
    translate([0,-6,5]) rotate([90,0,0]) cylinder(8, 2, 2);
    translate([0,-5,5]) rotate([90,0,0]) cylinder(10, 1.4, 1.2);
  }
  // standaard versteviging
  difference() {
    translate([0,-6,5]) rotate([90,0,0]) cylinder(5, 2.5,2.5);
    translate([0,-5,5]) rotate([90,0,0]) cylinder(10, 1.5,1.2);
  }
}

if( HANDLE1 ) {
  difference() {
    translate([0,0,0]) cylinder(2, 2.5,2.5);
    translate([0,0,0]) cylinder(2, 1.5,1.5);
  }
  translate([30,0,0]) cylinder(3, 3.5,3.5);
  translate([32,0,3]) cylinder(3.5, 1,1);

  difference() {
    translate([0,-1.5,0]) cube([30,3,1.4]);
    translate([0,0,0]) cylinder(2, 1.5,1.5);
  }

  translate([0,10,0]) cylinder(1.2, 2.5,2.5);
  translate([0,10,0]) cylinder(8, 1.4,1.4);

  difference() {
    translate([-10,0,0]) cylinder(1, 2.5,2.5);
    translate([-10,0,0]) cylinder(2, 1.5,1.5);
  }
}

if( HANDLE2 ) {
  difference() {
    translate([0,0,0]) cylinder(2, 2.5,2.5);
    translate([0,0,0]) cylinder(2, 1.5,1.5);
  }

  difference() {
    translate([30,0,2]) rotate([0,90,90]) prism(2,30,2.5);
    translate([0,0,0]) cylinder(2, 1.5,1.5);
  }
  difference() {
    translate([30,0,0]) rotate([0,270,90]) prism(2,30,2.5);
    translate([0,0,0]) cylinder(2, 1.5,1.5);
  }

  translate([30-6,-.5,0]) cube([6,1,4]);

  translate([0,10,0]) cylinder(1.2, 2.5,2.5);
  translate([0,10,0]) cylinder(6, 1.4,1.4);

}

module handgreep(l,d) {
  translate([0,0,l-4]) rotate([0,0,0]) tube(d, 4);
  translate([-(4-d),0,4]) cylinder(l-4-4, d,d);
  translate([0,0,4]) rotate([0,180,180]) tube(d, 4);

  translate([0,0,0]) rotate([0,90,0]) cylinder(3, .9,.9);
  translate([0,0,l]) rotate([0,90,0]) cylinder(3, .9,.9);
}

if( HANDGREEP ) {
  translate([-1,0,0]) rotate([90,0,0]) handgreep(22,1.4);
  translate([-1+10,0,0]) rotate([90,0,0]) handgreep(55, 1.4);
}



if( ROADNUMBER ) translate([0,0,0]) {  
  translate([-9,7,8]) linear_extrude(.6) text("232",8);
  translate([-8,7,8]) cube([16,.8,.4]);
}

if( ROADNAME ) translate([0,0,0]) {  
  translate([0,0,0]) linear_extrude(.6) text("Janna",6);
  translate([2,0,0]) cube([18,.4,.4]);
}

if( ROADNAME2 ) translate([0,0,0]) {  
  translate([0,0,0]) linear_extrude(.6) text("Erik",6);
  translate([2,0,0]) cube([9,.4,.4]);
  translate([9.05,0,0]) cube([.4,6,.4]);
}

if( ROADNAME3 ) translate([0,0,0]) {  
  translate([0,0,0]) linear_extrude(.6) text("Filia",6);
  translate([1,0,0]) cube([11,.4,.4]);
  translate([5.8,0,0]) cube([.4,6,.4]);
  translate([9.5,0,0]) cube([.4,6,.4]);
}

if( LIJMSTRIPFRONT ) {
  translate([0,0,0]) cube([45,10,1.4]);
  translate([-4,5,0]) cylinder(8, 1.9, 1.9);
}


if( UITLAAT ) {
  translate([0,0,0]) cylinder(1, 8, 8);
  difference() {
    translate([0,0,0]) cylinder(25, 6, 6);
    translate([0,0,0]) cylinder(25, 5, 5);
  }
}

if( FRONTROOSTER ) {
  translate([0,0,0]) cube([30,30,.6]);
  for(y = [0 : 6]) {
    translate([0,.2+y*4.1,0]) rotate([15,0,0]) cube([30,5,.6]);
  }
}


if( ZIJROOSTER ) {
  difference() {
    translate([0,0,0]) cube([20,30,2]);
    translate([2,2,0]) cube([20-4,30-4,1.4]);
  }
  translate([-1,-1,2]) cube([22,32,.6]);
  for(y = [0 : 11]) {
    translate([0,y*2.5,1.6]) rotate([25,0,0]) cube([20,3.2,.6]);
  }
}


if( TOPROOSTER ) {
  difference() {
    translate([0,0,0]) cube([30,30,2]);
    translate([2,2,0]) cube([30-4,30-4,2]);
  }
  difference() {
    translate([-1,-1,2]) cube([32,32,2]);
    translate([2,2,2]) cube([30-4,30-4,2]);
    for(y = [0 : 6]) {
      translate([-1,-1+3+y*4,2.4]) cube([32,2,2]);
    }
    for(x = [0 : 6]) {
      translate([-1+3+x*4,-1,2.4]) cube([2,32,2]);
    }
  }
  translate([-3,-3,4]) cube([36,36,1]);
}


module FrontL(h,front,step) {
  // links onder
  translate([-FW/2-2,0,0]) cube([2,8,h]);
  difference() {
    translate([-FW/2-2,8,0]) rotate([0,0,-10.1]) cube([2,52,h]);
    if( !front ) {
      // roosters zijkant
      translate([-FW/2-2,18,10]) rotate([0,0,-10.1]) cube([12,30,20]);
      translate([-FW/2-2,18,10+30]) rotate([0,0,-10.1]) cube([12,30,20]);
      translate([-FW/2-2,18,10+60]) rotate([0,0,-10.1]) cube([12,30,20]);
    }
  }


  // top
  difference() {
    translate([-((FWT-2)/2),FH,0]) cube([(FWT-2)/2,2,h]);
    if( front ) {
      translate([0,FH+10,h/2]) rotate([90,90,0]) cylinder(20, 1, 1);
    }
    else {
      // uitlaat
      translate([0,FH+5,h-10]) rotate([90,90,0]) cylinder(10, 6, 6);
      // rooster boven
      translate([-15,FH-4,h-50]) cube([30,8,30]);
    }
  }

  // top afronding
  difference() {
    translate([-(FWT/2)+2,FH-2,0]) cylinder(h, 4, 4);
    translate([-(FWT/2)+2,FH-2,0]) cylinder(h, 2, 2);
  }
  
  if( front ) {
    // front
    difference() {
      translate([-FW/2-2,0,h-2]) cube([FW/2+2,8,2]);
      translate([0,3,0]) cylinder(20, .75, .75);
    }
    translate([-FWT/2-1.5,8,h-2]) cube([FWT/2+2,FH-8,2]);
    translate([-FW/2+7.5,FH,h]) rotate([180,90,0]) prism(2,FH-8,8);
   
    difference() {
      translate([-32/2,20,h]) cube([32/2,32, 1]);
      translate([-32/2+1,20+1,h]) cube([32/2-1,32-2, 1]);
    }
    // lamp plaat
    difference() {
      translate([-5,FH+.6,h]) cube([5,1.2, 6]);
      translate([0,FH+10,h+3]) rotate([90,90,0]) cylinder(20, 1, 1);
    }
    translate([-4.5,FH-2,h]) rotate([0,0,0]) prism(1.2,3,4);
    
    // klink nagels
    for(x = [0 : 5]) {
      translate([-2.5-x*5,2,h]) cylinder(.6, .7, .7);
    }
    for(x = [0 : 3]) {
      translate([-2.5-x*5,FH,h]) cylinder(.6, .7, .7);
    }
    for(y = [0 : 9]) {
      translate([-FW/2+y*.9,10+y*5,h]) cylinder(.6, .7, .7);
    }
    if( step ) {
      translate([-FW/2+1,18,h]) cube([20,1.2, 8]);
      translate([-FW/2+2,18-4,h]) rotate([0,0,0]) prism(1.2,4,6);
      translate([-FW/2+20-.4-1,18-4,h]) rotate([0,0,0]) prism(1.2,4,6);
      translate([-FW/2+5,38,h]) cube([8,1.2, 6]);
      translate([-FW/2+5,38-2,h]) cube([8,2,.6]);
    }
  }
  
  // midden deel
  else {
    // uitlaat
    difference() {
      translate([0,FH+2.6,h-10]) rotate([90,90,0]) cylinder(.6, 8, 8);
      translate([0,FH+2.6,h-10]) rotate([90,90,0]) cylinder(.6, 6, 6);
    }
  }

}


if( FRONT ) {
  FrontL(FD,1,1);
  mirror([1,0,0]) FrontL(FD,1,0);
}


CW=84;
CWW=118;
CH=76+2;
CHH=23;
RC=-57+2; // roof center

module CabBaseBottomL(h,front,step) {
  // plakstrip zijkant
  translate([-((CW)/2)+3,-3,-6]) cube([2,CH-6,6]);
  // Dak plakstrip
  difference() {
    translate([0,RC,-6]) cylinder(6, 130, 130);
    translate([0,RC,-6]) cylinder(6, 130-2, 130-2);
    translate([-130,-130+RC,-6]) cube([260,243, 10]);
    translate([0,0,-6]) cube([130,130, 10]);
    translate([-CWW/2-10,0,-6]) cube([30,CWW, 10]);
  }
    
  // midden plaat
  difference() {
    translate([-CW/2,-3,0]) cube([CW/2,CH, 3]);
    // dak buiging
    difference() {
      translate([0,RC,0]) cylinder(3, 150, 150);
      translate([0,RC,0]) cylinder(3, 130, 130);
    }
  }
}
module CabBaseTopL(h,front,step) {
  // bovenplaat
  difference() {
    translate([-CWW/2,-3+CH-CHH,0]) cube([CWW/2,CHH, 3]);
    translate([-CW/2-7,CH-CHH-7/2,0]) cylinder(6, 7, 7);
    translate([-CWW/2,-3+CH-CHH,0]) cube([CWW/2-CW/2-7,6.5, 5]);
    // dak buiging
    difference() {
      translate([0,RC,0]) cylinder(3, 150, 150);
      translate([0,RC,0]) cylinder(3, 130, 130);
    }
  }  
}
module CabFrontL(h,front,step) {
  // plak strips en stekker
  translate([-(FWT/2)+2,FH-2,0]) cylinder(6, 1.9, 1.9);
  translate([-(FWT/2)+2,FH-2,0]) cylinder(3, 4, 4);
  translate([-FW/2,8,-6]) rotate([0,0,-10.1]) cube([2,46,12]);
  translate([-((FWT-2)/2)+6,FH-2,-6]) cube([(FWT-2)/2-6,2,12]);

  // midden plaat
  difference() {
    CabBaseBottomL(h,front,step);
    
    // motorruimte sparing
    translate([-FW/2-2+4,-3,0]) cube([FW/2+2-4,8+3,4]);
    translate([-FWT/2-1.5+2,8,0]) cube([FWT/2+2-2,FH-8-2,4]);
    translate([-FW/2+7.5+2,FH,4]) rotate([180,90,0]) prism(4,FH-8,8);
    
    // zijvenster
    translate([-(CW/2)+11,55,4]) rotate([0,180,180]) linear_extrude(height=4)
    RoundedPolygon([[0,0],[0,20],[3.5,0]],2,64);
    translate([-(CW/2)+5,33,0]) rcube(4,7,24,2);
  }
  


  
  // bovenplaat
  difference() {
    CabBaseTopL(h,front,step);
    // motorruimte sparing
    translate([-FWT/2-1.5+2,8,0]) cube([FWT/2+2-2,FH-8-2,4]);
    // zijvenster
    translate([-(CW/2)+11,55,4]) rotate([0,180,180]) linear_extrude(height=4)
    RoundedPolygon([[0,0],[0,20],[3.5,0]],2,64);
    translate([-(CW/2)+5,33,0]) rcube(4,7,24,2);
  }
}

module CabBackL() {
  // midden plaat
  difference() {
    CabBaseBottomL(8,0,0);
    translate([-30,-3,0]) cube([30,26,4]);
    // venster
    translate([-30,34,0]) rcube(4,16,26,2);
  }
  translate([-30,-3+10,0]) cube([1.4,14,7]);

  // bovenplaat
  difference() {
    CabBaseTopL(8,0,0);
    translate([-30,34,0]) rcube(4,16,26,2);
  }
  // lamp plaat
  difference() {
    translate([-5,FH+.6,3]) cube([5,1.2, 10]);
    translate([0,FH+10,0+10]) rotate([90,90,0]) cylinder(20, 1, 1);
  }
  translate([-4.5,FH-2,3]) rotate([0,0,0]) prism(1.2,3,8);

  // klink nagels
  for(x = [0 : 7]) {
    translate([2.3-x*4.75,26,3]) cylinder(.6, .7, .7);
  }
  for(x = [0 : 9]) {
    translate([2.3-x*4.75,63,3]) cylinder(.6, .7, .7);
  }
  for(y = [0 : 11]) {
    translate([CW/2-2,2+y*4.75,3]) cylinder(.6, .7, .7);
  }

}


if( CABBACK ) {
  CabBackL();
  mirror([1,0,0]) CabBackL();
}


if( CABFRONT ) {
  CabFrontL(8,0,0);
  mirror([1,0,0]) CabFrontL(8,0,0);

  //FrontL(4,0,0);
  //mirror([1,0,0]) FrontL(4,0,0);

}

CSW=81;
CSH=70;

if( CABSIDEWINDOW ) {
  translate([12,32,0]) rcube(.4,37,30,2);
  difference() {
    translate([12,32,0]) rcube(2,37,30,2);
    translate([13,33,0]) rcube(4,37-2,30-2,1);
  }
  translate([12+17,32,0]) cube([1,30,2]);
  translate([12+17+1.4,32,0]) cube([1,30,2]);
}

if( CABBACKWINDOW ) {
  translate([-30,34,0]) rcube(.4,16,26,2);
  difference() {
    translate([-30,34,0]) rcube(2,16,26,2);
    translate([-29,35,0]) rcube(4,16-2,26-2,1);
  }
}

if( CABFRONTWINDOW ) mirror([1,0,0]) {
  // zijvenster
  translate([-(CW/2)+11,55,0]) rotate([0,180,180]) linear_extrude(height=.4)
  RoundedPolygon([[0,0],[0,20],[3.5,0]],2,64);
  translate([-(CW/2)+5,33,-0.4]) rcube(.4,7,24,2);

  difference() {
    translate([-(CW/2)+11,55,2]) rotate([0,180,180]) linear_extrude(height=2) RoundedPolygon([[0,0],[0,20],[3.5,0]],2,64);

    translate([-(CW/2)+11,55,2]) rotate([0,180,180]) linear_extrude(height=2) RoundedPolygon([[0,0],[0,20],[3.5,0]],1,64);
    translate([-(CW/2)+5+2,33,0]) cube([4,24,4]);
  }

  difference() {
    translate([-(CW/2)+5,33,0]) rcube(2,8,24,2);
    translate([-(CW/2)+5+1,33+1,-0.4]) rcube(3,8-2,24-2,1);
    translate([-(CW/2)+5+6,33,0]) cube([4,24,4]);
  }

}

if( CABSIDE ) mirror([CABSIDEMIRROR?1:0,0,0]) {
  difference() {
    translate([0,0,0]) cube([CSW,CSH, 3]);
    // langs sparing
    //translate([0,0,2]) cube([12,CSH, 2]);
    //deur sparing
    translate([12,0,1]) cube([18,30, 3]);
    
    // venster
    translate([12,32,0]) rcube(4,37,30,2);
    //translate([12+18+2,32,0]) rcube(4,18,30,2);

    // gaten voor deur railing
    translate([32,5,0]) cylinder(4, 1, 1);
    translate([32,27,0]) cylinder(4, 1, 1);

    // gaten voor achter railing
    translate([CSW-4,5,0]) cylinder(4, 1, 1);
    translate([CSW-4,CSH-10,0]) cylinder(4, 1, 1);
  }
  // klink nagels
  for(y = [0 : 12]) {
    translate([2,5+y*5,3]) cylinder(.6, .7, .7);
  }
  for(y = [0 : 12]) {
    translate([10,5+y*5,3]) cylinder(.6, .7, .7);
  }
  for(x = [0 : 9]) {
    translate([32+x*4.75,2,3]) cylinder(.6, .7, .7);
  }
  for(x = [0 : 9]) {
    translate([32+x*4.75,30,3]) cylinder(.6, .7, .7);
  }
  for(y = [0 : 6]) {
    translate([52,35+y*5,3]) cylinder(.6, .7, .7);
  }
  for(y = [0 : 12]) {
    translate([CSW-2,5+y*5,3]) cylinder(.6, .7, .7);
  }
}

module CabRoof() {
  difference() {
    translate([0,-57,0]) cylinder(96, 132, 132);
    translate([0,-57,0]) cylinder(96, 130, 130);
    translate([-132,-132-57,0]) cube([264,243, 100]);
    translate([0,0,0]) cube([132,132, 100]);
    translate([-CWW/2-32,0,0]) cube([30,CWW, 100]);
  }
  // lijm strip
  translate([-78/2,60,18]) cube([2,8,96-2*18]);
}


if( CABROOF ) {
  CabRoof();
  mirror([1,0,0]) CabRoof();
}

if( LIJMSTRIPSIDE ) {
  translate([0,0,0]) cube([10,CSH,1.2]);
  translate([2.5,0,1.2]) cube([7.5,CSH,2]);
}

if( KOFFER ) {
  difference() {
    translate([0,0,0]) cube([2+60+2,25+2,23]);
    translate([2,2,0]) cube([60,25,25]);
    translate([0,25+2,23]) rotate([0,180,180]) prism(2+60+2,25+2,4);
    translate([32,5,4]) rotate([90,90,0]) cylinder(10, .75, .75);
  }
}

if( KOFFERDEKSEL ) {
  translate([0,0,0]) cube([4+60+4,25+6,1.6]);
  difference() {
    translate([4,5.4,1.6]) cube([60,15,4]);
    //translate([6,8,1.6]) cube([60-4,15,5]);
  }
  translate([0,30,0]) rotate([90,0,90]) cylinder(68,1.2,1.2);
}

