/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
//$fn=64;
include <../../lib/shapes.scad>
include <../lib/modules.scad>

W=76;
D=35+12;

H=26;

HoH=120.5;  // (280 - 39) / 2 = 120.5 (NS200/NS300)

FRONT=0;
BASEFRONT=1;
BASETOPFRONT=0;
BASETOPFRONT_RED=0;
BUFFER=0;

FRAME=0;
FRAME2=0;
SIDEBOTTOM=0;
MIRROR=0;
BATTERY=0;
DOORBOTTOM=0;

STEP=0;
STEPW=20; //20 80

if( STEP ) {
  difference() {
    translate([0,0,0]) rcube(1.6, STEPW+10, 12, 2);
    for(y = [0 : 9]) {
      translate([0,.85+y*1.1,1.4]) cube([STEPW+10,.4,.6]);
    }
    /*
    for(x = [0 : STEPW*1.3]) {
      translate([1+x*1.1,0,1.4]) cube([.4,12,.6]);
    }
    */
  }
  
  difference() {
    translate([2.5,12-1.6,0]) cube([5,1.6,6.5]);
    translate([5,15,4]) rotate([90,0,0]) cylinder(20, 1, 1);
  }
  difference() {
    translate([STEPW+2.5,12-1.6,0]) cube([5,1.6,6.5]);
    translate([5+STEPW,15,4]) rotate([90,0,0]) cylinder(20, 1, 1);
  }

//  translate([5+2.5,12-1.6,0]) cube([STEPW,1.6,2.6]);

  difference() {
    translate([2.5,6.5,0]) rotate([0,0,0]) prism(5,4,6.5);
    translate([5,15,4]) rotate([90,0,0]) cylinder(20, 1.5, 1.5);
  }
  difference() {
    translate([STEPW+2.5,6.5,0]) rotate([0,0,0]) prism(5,4,6.5);
    translate([5+STEPW,15,4]) rotate([90,0,0]) cylinder(20, 1.5, 1.5);
  }
}



if( BATTERY ) {
  //translate([0,0,0]) WIOpiBase(79,52);
  BW=66;
  BL=56;
  BH=30;
  difference() {
    translate([0,0,BH]) cube([BW+4, BL+4, 15]);
    translate([2,2,BH+2]) cube([BW, BL, 15]);
    translate([2+5,2+5,BH]) cube([BW-10, BL-10, 15]);
  }

  difference() {
    translate([0,0,0]) cube([2, BL+4, BH]);
    translate([0,10,5]) cube([2, BL+4-20, BH-10]);
  }
  difference() {
    translate([BW+2,0,0]) cube([2, BL+4, BH]);
    translate([BW+2,10,5]) cube([2, BL+4-20, BH-10]);
  }

  difference() {
    translate([0,-6,0]) cube([6, BL+4+12
  , 2]);
    translate([3,-3,0]) cylinder(4, 1.6, 1.6);
    translate([3,BL+4+3,0]) cylinder(4, 1.6, 1.6);
  }
  difference() {
    translate([BW-2,-6,0]) cube([6, BL+4+12
  , 2]);
    translate([BW+1,-3,0]) cylinder(4, 1.6, 1.6);
    translate([BW+1,BL+4+3,0]) cylinder(4, 1.6, 1.6);
  }
}


module AxGear200(x, y, z) {
  //H,W,L,r
  S=10;
  difference() {
    translate([x-S/2,y-S/2,z]) rcube(9, S, S, 1);
    translate([x-S/2,y+S/2,z+6]) rotate([90,0,0]) prism(10,4,10);
  }

  translate([x-S/2-3,y-3-.5,z]) cube([S+6, 1, 2]);
  translate([x-S/2-3,y-.5,z]) cube([S+6, 1, 2]);
  translate([x-S/2-3,y+3-.5,z]) cube([S+6, 1, 2]);

  translate([x-S/2-3,y-S/2,z]) cube([2, S, 1]);
  translate([x+S/2+1,y-S/2,z]) cube([2, S, 1]);

  translate([x,y+8,z+2]) rotate([90,0,0]) cylinder(4, 2, 2);

  translate([x-30/2,y+10,z]) cube([30, .8, 4.8]);
  translate([x-24/2,y+9.2,z]) cube([24, .8, 4.6]);
  translate([x-18/2,y+8.4,z]) cube([18, .8, 4.4]);
  translate([x-12/2,y+7.6,z]) cube([12, .8, 4.2]);
  translate([x-6/2,y+6.8,z]) cube([6, .8, 4]);

  translate([x-2,y+S/2+1,z]) cube([4, 5, 5.6]);
  
  translate([x-2,y+S/2+5.6,z]) cube([4, 2, 7]);
  
  translate([x-15,y+S/2+5.6,z]) cube([4, 2, 6]);
  translate([x-15,y+S/2+5.6,z]) cube([4, 6, 1]);

  translate([x+15-4,y+S/2+5.6,z]) cube([4, 2, 6]);
  translate([x+15-4,y+S/2+5.6,z]) cube([4, 6, 1]);

  translate([x-15+1.4,y+S/2+5.6,z]) cube([1.2, 6, 4]);
  translate([x+15-4+1.4,y+S/2+5.6,z]) cube([1.2, 6, 4]);
}

//230 x 35
// 11+14=25 as hoogte
SL=230;
SH=35;
if( SIDEBOTTOM ) mirror([MIRROR?1:0,0,0]) {
  AxGear200((SL-HoH)/2,SH-25,3);
  AxGear200(SL-(SL-HoH)/2,SH-25,3);

  // Handle boven (rijden)
  difference() {
    translate([(SL-HoH)/2+25+10,SH/2+8,0]) cylinder(4, 3, 3);
    translate([(SL-HoH)/2+25+10,SH/2+8,0]) cylinder(5, 1.5, 1.5);
  }
  // Handle onder (rem)
  difference() {
    translate([(SL-HoH)/2+25+12.5,SH/2-8,0]) cylinder(4, 3, 3);
    translate([(SL-HoH)/2+25+12.5,SH/2-8,0]) cylinder(5, 1.5, 1.5);
  }

  // Olie
  difference() {
    translate([25,6,0]) cube([10, 10, 12]);
    translate([35,6,6]) rotate([90,0,180]) prism(10,10,10);
  }
  
  
  // schroefgaten voor het draaistel
  difference() {
    translate([(SL-HoH)/2,SH-11,0]) cylinder(4, 3, 3);
    translate([(SL-HoH)/2,SH-11,0]) cylinder(7, 1.75, 1.75);
  }
  difference() {
    translate([SL-(SL-HoH)/2,SH-11,0]) cylinder(4, 3, 3);
    translate([SL-(SL-HoH)/2,SH-11,0]) cylinder(7, 1.75, 1.75);
  }
  
  // Basis
  difference() {
    translate([0,0,0]) cube([SL, SH+1, 3]);
    // schroefgaten voor het draaistel
    translate([(SL-HoH)/2,SH-11,0]) cylinder(10, 1.0, 1.0);
    translate([SL-(SL-HoH)/2,SH-11,0]) cylinder(10, 1.0, 1.0);

    // links opening
    translate([SH/2,SH/2,0]) cylinder(10, 8, 8);
    // rechts opening
    translate([SL-SH/2,SH/2,0]) cylinder(10, 8, 8);

    // links lamp kabel opening
    translate([SH/2-10,SH/2+10,0]) cylinder(10, 2.5/2, 2.5/2);
    // rechts lamp kabel opening
    translate([SL-SH/2+10,SH/2+10,0]) cylinder(10, 2.5/2, 2.5/2);

    // midden opening
    translate([(SL-HoH)/2+25,SH/2,0]) cylinder(10, 8, 8);
    translate([(SL-HoH)/2+25+20,SH/2,0]) cylinder(10, 8, 8);
    translate([(SL-HoH)/2+25,SH/2-8,0]) cube([20, 16, 4]);
    // Handle boven
    translate([(SL-HoH)/2+25+10,SH/2+8,0]) cylinder(5, 1.5, 1.5);
    // Handle onder
    translate([(SL-HoH)/2+25+12.5,SH/2-8,0]) cylinder(5, 1.5, 1.5);


    // afronding links
    difference() {
      translate([10,10,0]) cylinder(10, 14, 14);
      translate([10,10,0]) cylinder(10, 10, 10);
      translate([0,10,0]) cube([28, 14, 3]);
      translate([10,0,0]) cube([14, 28, 3]);
    }
    // afronding rechts
    difference() {
      translate([SL-10,10,0]) cylinder(10, 14, 14);
      translate([SL-10,10,0]) cylinder(10, 10, 10);
      translate([SL-28,10,0]) cube([28, 14, 3]);
      translate([SL-24,0,0]) cube([14, 28, 3]);
    }

    // Deur opening 
    difference() {
      translate([125,SH/2,1]) cube([18, SH, 4]);
      // afronding rechts
      difference() {
        translate([125+18-8,SH/2+8,0]) cylinder(10, 14, 14);
        translate([125+18-8,SH/2+8,0]) cylinder(10, 8, 8);
        translate([125+18-28,SH/2+8,0]) cube([28, 14, 5]);
        translate([125+18-22,SH/2,0]) cube([14, 28, 5]);
      }
    }  
    translate([113,2,2]) cube([12, SH, 4]);

    // schroef gaten voor de opstap platen
    // links
    translate([14,2,0]) cylinder(10, 1, 1);
    translate([34,2,0]) cylinder(10, 1, 1);
    // midden
    translate([SL/2-40,2,0]) cylinder(10, 1, 1);
    translate([SL/2+40,2,0]) cylinder(10, 1, 1);
    // rechts
    translate([SL-14,2,0]) cylinder(10, 1, 1);
    translate([SL-34,2,0]) cylinder(10, 1, 1);
  }
  
  // klink nagels
  for(x = [0 : 18]) {
    translate([2+x*6,SH-2,3]) cylinder(.8, 1, 1);
  }
  for(x = [0 : 13]) {
    translate([SL-2-x*6,SH-2,3]) cylinder(.8, 1, 1);
  }
  for(x = [0 : 3]) {
    translate([146-x*6,SH-20,3]) cylinder(.8, 1, 1);
  }
  
}

if( DOORBOTTOM ) mirror([MIRROR?1:0,0,0]) {
  // Deur opening 
  difference() {
    translate([.4,SH/2,1]) cube([17.6, SH/2+1, 1]);
    // afronding rechts
    difference() {
      translate([18-8,SH/2+8,0]) cylinder(10, 14, 14);
      translate([18-8,SH/2+8,0]) cylinder(10, 8, 8);
      translate([18-28,SH/2+8,0]) cube([28, 14, 5]);
      translate([18-22,SH/2,0]) cube([14, 28, 5]);
    }
  }  
}




if( BUFFER ) {
  translate([-30,0,0]) {
    // Plate
    translate([20/2,14/2,0]) cylinder(2, 8.5, 8.5);
    translate([20/2,14/2,2]) cylinder(2, 5.5, 5.5);
    // Spring cylinder
    difference() {
      translate([20/2,14/2,2]) cylinder(19, 3.95, 3.95);
      translate([20/2,14/2,4]) cylinder(17, 3.1, 3.1);
    }
  }
}




TW=100;
TD=35;


if( FRAME ) {
  difference() {
    translate([(TW-60)/2,10,0]) cube([60, TD+(FRONT?45:0), 11]);
    translate([(TW-60)/2+2,10+2,0]) cube([60-4, TD+(FRONT?(65-4):-2), 11]);
    translate([TW/2,50,7]) rotate([90,0,0]) cylinder(100, .75, .75);
    translate([0,30,7]) rotate([90,0,90]) cylinder(100, .75, .75);
    translate([0,70,7]) rotate([90,0,90]) cylinder(100, .75, .75);
  }
  difference() {
    translate([(TW-60)/2-6,10,0]) cube([60+12, TD+(FRONT?45:0), 1]);
    translate([(TW-60)/2,10,0]) cube([60, TD+(FRONT?45:0), 1]);
  }
  translate([(TW-45)/2,10-6,0]) cube([45, 6, 1]);
}


if( FRAME2 ) {
  difference() {
    translate([(TW-60)/2,10,0]) cube([60, 2, 6]);
    translate([TW/2,50,3]) rotate([90,0,0]) cylinder(100, .75, .75);
  }
}


module handgreep() {
  translate([0,0,20]) rotate([0,0,0]) tube(1, 4);
  translate([-3,0,4]) cylinder(16, 1,1);
  translate([0,0,4]) rotate([0,180,180]) tube(1, 4);
}

if( BASETOPFRONT_RED ) {
  
  translate([4,-5,20]) rotate([90,0,90]) handgreep();
  translate([4+68,-5,20]) rotate([90,0,90]) handgreep();
  
  // sier
  difference() {
    //translate([0,-4,1]) cube([TW, 1, H+2]); // new
    translate([0,-4,3]) cube([TW, 1, H]); // old
    // koppeling sleuf
    translate([(TW-40)/2,-4,H-6+3]) cube([40, 3, 6]);
    // haak opening
    translate([TW/2,50,10]) rotate([90,0,0]) cylinder(100, 2.0, 2.0);
    // buffer opening: 6.1=new 5.1=old
    translate([TW/2+68/2,0,10]) rotate([90,0,0]) cylinder(12, 5.1, 5.1);
    translate([TW/2-68/2,0,10]) rotate([90,0,0]) cylinder(12, 5.1, 5.1);
  }
  difference() {
    // haak opening
    translate([TW/2,-4,10]) rotate([90,0,0]) cylinder(2, 3.0, 3.0);
    translate([TW/2,50,10]) rotate([90,0,0]) cylinder(100, 2.0, 2.0);
  }  
  
  // bufferplaat links
  difference() {
    translate([TW/2+68/2-14/2,-4,10-14/2]) rotate([90,0,0]) rcube(1, 14, 14, 1);
    translate([TW/2+68/2,0,10]) rotate([90,0,0]) cylinder(12, 5.1, 5.1);
  }
  // bufferplaat rechts
  difference() {
    translate([TW/2-68/2-14/2,-4,10-14/2]) rotate([90,0,0]) rcube(1, 14, 14, 1);
    translate([TW/2-68/2,0,10]) rotate([90,0,0]) cylinder(12, 5.1, 5.1);
  }
  // bouten links
  translate([TW/2-68/2-5,-4,10-5]) rotate([90,0,0]) cylinder(2, 1, 1);
  translate([TW/2-68/2+5,-4,10-5]) rotate([90,0,0]) cylinder(2, 1, 1);
  translate([TW/2-68/2+5,-4,10+5]) rotate([90,0,0]) cylinder(2, 1, 1);
  translate([TW/2-68/2-5,-4,10+5]) rotate([90,0,0]) cylinder(2, 1, 1);

  // bouten rechts
  translate([TW/2+68/2-5,-4,10-5]) rotate([90,0,0]) cylinder(2, 1, 1);
  translate([TW/2+68/2+5,-4,10-5]) rotate([90,0,0]) cylinder(2, 1, 1);
  translate([TW/2+68/2+5,-4,10+5]) rotate([90,0,0]) cylinder(2, 1, 1);
  translate([TW/2+68/2-5,-4,10+5]) rotate([90,0,0]) cylinder(2, 1, 1);
}


if( BASETOPFRONT ) {
  difference() {
    translate([0,0,3]) cube([TW, 2, H]);
    // koppeling sleuf
    translate([(TW-40)/2,0,H-6+3]) cube([40, 3, 6]);
    // buffer schroefgaten
    translate([TW/2+68/2,50,10]) rotate([90,0,0]) cylinder(100, 1.0, 1.0);
    translate([TW/2-68/2,50,10]) rotate([90,0,0]) cylinder(100, 1.0, 1.0);
    // haak opening
    translate([TW/2,50,10]) rotate([90,0,0]) cylinder(100, 2.0, 2.0);
  }
    
  difference() {
    translate([0,-2,0]) cube([TW, TD+2+(FRONT?80:0), 3]);
    translate([0,-2,1]) cube([TW, 2, 2]);
    translate([(TW-60)/2,10,0]) cube([60, TD+(FRONT?75:0), 3]);
  
    translate([0,10,3]) rotate([0,90,0]) prism(3,TD-10,(TW-(W+2*4))/2);
    translate([TW,10,3]) mirror([1,0,0]) rotate([0,90,0]) prism(3,TD-10,(TW-(W+2*4))/2);

    if( FRONT ) {
      translate([0,TD,0]) cube([(TW-(W+2*4))/2, 100, 4]);
      translate([TW-(TW-(W+2*4))/2,TD,0]) cube([(TW-(W+2*4))/2, 100, 4]);
    }
      // Lamp openingen
      translate([8,6,0]) cylinder(100, 2.0, 2.0);
      translate([TW-8,6,0]) cylinder(100, 2.0, 2.0);
  }


  if( FRONT ) {
    translate([(TW-(W+2*4))/2,90,0]) cube([TW-(TW-(W+2*4)), 6, 3]);
  }
  
  difference() {
    translate([(TW-70-.6)/2,2+3.4,0]) cube([70-.6, 4, 8]);
    translate([(TW-50)/2,2+3.4,0]) cube([50, 4, 8]);
  }

  translate([0,2,H]) rotate([270,0,0]) prism(2,H,5);
  translate([TW-2,2,H]) rotate([270,0,0]) prism(2,H,5);
  
  
    // Buffer basis rechts
    difference() {
      translate([TW/2+68/2,0,10]) rotate([90,0,0]) cylinder(12, 6, 5);
      translate([TW/2+68/2,0,10]) rotate([90,0,0]) cylinder(12, 4, 4);
    }
  
    // Buffer basis links
    difference() {
      translate([TW/2-68/2,0,10]) rotate([90,0,0]) cylinder(12, 6, 5);
      translate([TW/2-68/2,0,10]) rotate([90,0,0]) cylinder(12, 4, 4);
    }
}



if( BASEFRONT ) {

  difference() {
    translate([3,D-15,0]) cube([W-6, 3, 8]);
    translate([(W-54)/2,D,4]) rotate([90,0,0]) cylinder(100, 1.4, 1.4);
    translate([W-(W-54)/2,D,4]) rotate([90,0,0]) cylinder(100, 1.4, 1.4);
  }
  
  difference() {
    translate([0,0,0]) cube([W, D, H]);
    translate([3,3,0]) cube([W-6, 35-6, H+1]);
    translate([3,D-12,0]) cube([W-6, 12, H+1]);
    translate([(W-40)/2,0,H-6]) cube([40, 3, 6]);
    
    translate([(76-56)/2,D-15,0]) cube([56, 12, H+1]);
    
    // rond opening
    translate([-5,SH/2,SH/2]) rotate([0,90,0]) cylinder(W+10, 8, 8);
    
    // lamp kabel opening
    translate([-5, SH/2-10,SH/2-10]) rotate([0,90,0]) cylinder(W+10, 2.5/2, 2.5/2);
    

    // Zijkant bevestiging
    translate([-4,D-6,6]) rotate([90,0,90]) cylinder(W+8, 1.4, 1.4);
    translate([-4,D-6,16]) rotate([90,0,90]) cylinder(W+8, 1.4, 1.4);
    // Buffer gaten
    translate([(W-68)/2,4,5]) rotate([90,0,0]) cylinder(10, 1.4, 1.4);
    translate([W-(W-68)/2,4,5]) rotate([90,0,0]) cylinder(10, 1.4, 1.4);
  }
}
  
  