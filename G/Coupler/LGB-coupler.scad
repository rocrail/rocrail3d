/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;
//$fn=64;


include <../../lib/shapes.scad>

BASE=0;
SPRING=0;
PIN=0; // obsolete; replaced with M3
PINRING=0;
HOOKTOP=1;
HOOKBOTTOM=0;
DECOUPLER=0;


if( DECOUPLER ) translate([60,80,0]) {

  difference() {
    translate([0,0,0]) cylinder(1,12/2,12/2);
    translate([0,0,0]) cylinder(1/2,8/2,6.6/2);
    translate([0,0,1/2]) cylinder(1/2,6.6/2,8/2);
  }
  difference() {
    translate([0,-12/2,0]) cube([9,12,1]);
    translate([0,0,0]) cylinder(1/2,8/2,6.6/2);
    translate([0,0,1/2]) cylinder(1/2,6.6/2,8/2);
  }
  
  translate([9,-12/2,0]) cube([2,12,8]);
  translate([9,-30/2,8]) cube([7.5,30,2]);
  translate([11,-6,6.5]) rotate([90,0,90]) prism(12,1.5,1.5);
  
  difference() {
    translate([.5,0,8]) ellipse(2,17,41);
    translate([-17-8,-41*2/2,8]) cube([17*2,41*2,2]);
    translate([9,-36,8]) cube([8,22,2]);
    translate([9,14,8]) cube([8,22,2]);
  }

  translate([10.5,-15,0]) cube([5,3,10]);
  translate([13,-18.5,0]) cylinder(1.6,7,7);

  translate([10.5,15-3,0]) cube([5,3,10]);
  translate([13,18.5,0]) cylinder(1.6,7,7);
}


module springSide(x,y,t,H) {
  spring(x,y,t,H,true);
  spring(x+6,y,t,H,true);
  spring(x+12,y,t,H,false);
}

if( HOOKBOTTOM ) translate([60,70,0]) {
  difference() {
    translate([-10,0,0]) cube([5,7,7.5]);
    translate([-11.5,3.5,.5]) rotate([0,45,0]) cylinder(12,1.5,1.5);
    translate([-11,0,1.3]) rotate([0,-45,0]) cube([2.8,7,1.4]);
  }
    //translate([-10,0,8]) cube([10,7,1.4]);
  translate([-6.5,3.5,-2]) cylinder(2,1.5,1.5);

  difference() {
    translate([-10,7/2,7.5]) rotate([0,90,0]) ellipse(5, 7/4, 7/2 );
    translate([-10,0,0]) cube([5,7,7.5]);
    translate([-11.5,3.5,.5]) rotate([0,45,0]) cylinder(12,1.5,1.5);
  }
}


if( HOOKTOP ) translate([60,60,0]) {
  translate([0,0,0]) {
    difference() {
      translate([0,0,0]) cylinder(2,11.6/2,11.6/2);
      translate([0,0,0]) cylinder(2/2,8/2,6.6/2);
      translate([0,0,2/2]) cylinder(2/2,6.6/2,8/2);
      translate([-6.5,0,0]) cylinder(2,1.6,1.6);
    }
    difference() {
      translate([4,-1,0]) cube([33.5,2,3]);
      translate([-10,-7,2]) cube([22,14,3]);
    }
    
    difference() {
    translate([34,1,3]) rotate([0,90,90]) prism(3,30,1.5);
      translate([-10,-7,2]) cube([22,14,3]);
    }
    difference() {
    translate([34,-1,0]) rotate([0,270,90]) prism(3,30,1.5);
      translate([-10,-7,2]) cube([22,14,3]);
    }

    difference() {
      translate([-10,-3.5,0]) cube([6,7,3]);
      translate([-10,-7,2]) cube([21,14,3]);
      translate([-6.5,0,0]) cylinder(2,1.6,1.6);
    }

    difference() {
      translate([12,1,23]) rotate([90,180,90]) prism(2,20,6);
      translate([11,-8,10]) cube([16,16,16]);
    }

    // hook
    difference() {
      translate([30,1,2]) rotate([90,90,0]) cylinder(2,8,8);
      translate([26,1,5]) rotate([90,90,0]) cylinder(2,6,6);
      translate([22,-8,-7]) cube([16,16,7]);
      translate([22.5,-4,8]) cube([8.2,8,4]);
    }
      translate([30.75,1,9.35]) rotate([90,90,0]) cylinder(2,.6,.6);
    
  }
  
}


if( PINRING ) translate([60,40,0]) {
    difference() {
      translate([0,0,0]) cylinder(2,6/2,6/2);
      translate([0,0,0]) cylinder(2,3.4/2,3.4/2);
    }
    difference() {
      translate([0,0,0]) cylinder(2,9/2,9/2);
      translate([0,0,0]) cylinder(2,3.4/2,3.4/2);
      translate([-4.5,1,0]) cube([9,9,2]);
    }
}

if( PIN ) {
  translate([60,40,0]) {
    difference() {
      translate([0,0,0]) cylinder(2,9/2,9/2);
      translate([-4.5,1,0]) cube([9,9,2]);
    }
    difference() {
      translate([0,0,0]) cylinder(7.3,3.2/2,3.2/2);
      //translate([-3.2/2,-.3,6]) cube([3.2,.6,9.4-6]);
    }
    
    translate([0,0,7.3]) cylinder(9.5-7.3, 3.2/2, 3.2/2-.4);
    
    difference() {
      translate([0,0,7.3]) cylinder(9.5-7.3, 2, 3.2/2-.4);
      translate([-2,-4,7.3]) cube([4,4,9.4-7.2]);
    }
    
  }
}


if( SPRING ) {
  H=1.2;
  H2=1.4;
  H3=2.4;
  translate([60,0,0]) {
    translate([0,2.5,0]) springSide(0,0,7.5,H);
    translate([0,-2.5,0]) mirror([0,1,0]) springSide(0,0,7.5,H);

    translate([-.5,-6,0]) cube([2.3,12,H]);
    translate([15,-8,0]) cube([1,16,H3]);

    difference() {
      translate([5.5,-3,0]) cube([10.5,6,2*H2+.6]);
      translate([5.5,-3,0]) cube([8.5,6,H2+.4]);
      translate([8.0,0,0]) cylinder(10,2.6/2,2.6/2);
    }

    translate([1,0,H3/2]) rotate([0,270,0]) cylinder(6,H3/2,H3/2);
    
    //translate([1,0,H3/2]) rotate([0,270,0]) ellipse(6,H3/2,H3/2);
  }
}


if( BASE ) {
  translate([0,0,0]) {
    difference() {
      translate([0,0,0]) cube([31,12,7.5]);
      
      // bevestiging
      translate([8,12/2,0]) cylinder(7.5, 2.5/2, 2.5/2);
      
      translate([22,12/2,0]) cylinder(7.5, 3.6/2, 3.6/2);

      translate([22,12/2,5]) cylinder(1, 6/2, 6/2);
      translate([22,12/2,6]) cylinder(1, 6/2, 3.4/2);
      
      translate([0,0,5]) cube([13,12,7.5]);
      translate([13,0,5]) rotate([90,0,90]) prism(12,7.5-5,9);
      translate([0,(12-7)/2,3.6]) cube([11,7,7.5]);
      translate([0,0,3.6]) cube([11,1,7.5]);
      translate([0,12-1,3.6]) cube([11,1,7.5]);

      translate([0,(12-8.2)/2,0]) cube([25,8.2,2.5]);
      translate([25,12/2,0]) cylinder(5,8.2/2,8.2/2);
      translate([25-(10-8.2),12/2,0]) cylinder(5,8.2/2,8.2/2);
      translate([25-(10-8.2)/2-1,(12-8.2)/2,0]) cube([10-8.2,8.2,5]);
    
      translate([0,0,3.6]) cube([1.5,12,7.5]);

      // extra bevestiging
      translate([15,12/2,0]) cylinder(15,1, 1);
    }

    translate([29,-1.5,0]) cylinder(11.75,1.5,1.5);
    translate([29,13.5,0]) cylinder(11.75,1.5,1.5);


    difference() {
      translate([22,12/2,5]) cylinder(11.75-5,6/2,6/2);
      translate([22,12/2,5]) cylinder(11.75-5, 3.4/2, 3.4/2);
      translate([22,12/2,5]) cylinder(1, 6/2, 6/2);
      translate([22,12/2,6]) cylinder(1, 6/2, 3.4/2);
    }
      //translate([22,12/2,5]) cylinder(1, 6/2, 6/2);
      //translate([22,12/2,6]) cylinder(1.4, 6/2, 3.4/2);
    
    translate([31-3.56,-(43-12)/2,0]) difference() {
      translate([0,0,0]) cube([14,43,7.5]);
      translate([31-4-25,43/2,0]) cylinder(5,8.2/2,8.2/2);
      translate([(16-9)/2,(43-32)/2,0]) cube([11,32,7.5]);

      translate([3.5,1.5,0]) prism(24, 4, 7.5);
      translate([27.5,41.5,0]) rotate([0,0,180]) prism(24, 4, 7.5);
      translate([23.5,40,7.5]) rotate([0,180,0]) prism(24, 4, 7.5);
      translate([0,3,7.5]) rotate([180,0,0]) prism(24, 4, 7.5);
      
    }
    
    translate([-7,0,0]) difference() {
      translate([31.5,6,0]) ellipse(6.5,20,43);

      difference() {
        //translate([31.5,6,6.5]) ellipse(7.5,20,43);
        //translate([31.5,6,6.5]) ellipse(7.5,19.5,42);
      }
      difference() {
        //translate([31.5,6,7]) ellipse(7.5,19.5,42);
        //translate([31.5,6,7]) ellipse(7.5,19,41.5);
      }

      translate([31.5,6,0]) ellipse(6.5,18,41);
      translate([31.5,6,0]) ellipse(7.5,17,41);
      
      translate([31.5-21,-44,0]) cube([20,100,7.5]);
      translate([31.5-6,-39.5,0]) cube([24,24,7.5]);
      translate([31.5-6,27.5,0]) cube([24,24,7.5]);
      translate([28,-12.5,7.5]) rotate([180,0,0])  prism(24, 4, 7.5);
      translate([52,24.5,7.5]) rotate([180,0,180]) prism(24, 4, 7.5);
    }
    
    
    translate([-7,0,0]) {
      difference() {
        for(i = [0:8]) {
          translate([31.5,6,6.5+i*.1]) ellipse(.2,20-i/8,43-i/8);
        }
        translate([31.5,6,6.5]) ellipse(1,17,40);
        translate([31.5-21,-44,0]) cube([20,100,7.5]);
        translate([31.5-6,-39.5,0]) cube([24,24,7.5]);
        translate([31.5-6,27.5,0]) cube([24,24,7.5]);
        translate([28,-12.5,7.5]) rotate([180,0,0])  prism(24, 4, 7.5);
        translate([52,24.5,7.5]) rotate([180,0,180]) prism(24, 4, 7.5);
      }
    }
    
  }
}
