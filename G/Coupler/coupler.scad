/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

include <../../lib/shapes.scad>

COUPLER=true;
CHASSIS=true;
SPRING=false;
LEVER=false;
NANO=false;
LGBHOLE=false;
SHORTCOUPLER=true;

// Koppeling.
if( COUPLER ) {
translate([14,0,0]) {
  // Schuine aankoppel punt:
  translate([SHORTCOUPLER?-1:0,0,0]) {
    difference() {
      translate([8.6,2,0]) cube([6.4, 18, 8]);

      // Edges
      translate([9,0,0]){
        rotate(90) mirror([0,1,0]) prism(20, 6, 4);
      }
      translate([9,20,8]){
        rotate(270) mirror([0,0,1]) prism(20, 6, 4);
      }
      translate([8.6,6,8]){
        if( !SHORTCOUPLER )
          rotate([0,90,0]) mirror([0,1,0]) prism(8, 4, 4);
      }
      translate([11,20,8]){
        rotate([90,90,0]) mirror([0,0,0]) prism(8, 4, 5);
      }
      
      // Compatible hole
      if( LGBHOLE ) {
        translate([10,-13,4]) rotate([270,0,0]) cylinder(22, .75, .75);
      }
      
    }
  }

  // Extra 'klever' om te voorkomen dat de punt opkrult 
  //translate([9,2,0]) cylinder(.2, 2, 2);
    
  difference() {
    translate([-4,-10,0]) cube([4, 30, 4]);
    translate([-10,1.8,0]) cube([6.5, 12.4, 6]);
  }
  
  // Achterwand
  translate([-1,-10,4]) cube([1, 30, 6]);

  // Y- zijde slide-in
  translate([0,-10,0]) cube([8, 2, 10]);
  
  difference() {
    translate([7,-9.8,0]) rotate(-22.5) cube([8, 2, 10]);
    if( LGBHOLE ) {
      translate([x10,-13,4]) rotate([270,0,0]) cylinder(22, 1, 1);
    }
  }
  translate([-4,-10,4]){
    rotate(90) mirror([0,1,0]) prism(2, 4, 6);
  }

  // Y+ zijde slide-in
  difference() {
    translate([-7,16,0]) cube([16, 4, 8]);
    translate([-7,-2,3]) rotate([270,0,0]) cylinder(20, 1.5, 1.5);
  }
  translate([13,20,0]) rotate([90,-90,0]) mirror([0,0,0]) prism(8, 4, 4);

  
  // Montage aan het chassis deel:
  difference() {
    translate([-7,-2,3]) rotate([270,0,0]) cylinder(22, 3, 3);
    translate([-7,-2,3]) rotate([270,0,0]) cylinder(20, 1.5, 1.5);
    translate([-10,1.8,0]) cube([6, 12.4, 6]);
  }
  difference() {
    translate([-7,-2,0]) cube([3, 20, 4]);
    translate([-7,-2,3]) rotate([270,0,0]) cylinder(22, 1.5, 1.5);
    translate([-8,1.8,0]) cube([4, 12.4, 4]);
  }
  
  // Lever
  difference() {
    translate([-8,16,0]) cube([5, 4, 16]);
    translate([-5.5,16,16]) rotate([270,0,0]) cylinder(4, 1.0, 1.0);
    translate([-7,-2,3]) rotate([270,0,0]) cylinder(22, 1.5, 1.5);
  }
  translate([0,16,8]) rotate([0,0,90]) mirror([0,0,0]) prism(4, 3, 6);
  
  difference() {
    translate([-5.5,16,16]) rotate([270,0,0]) cylinder(4, 2.5, 2.5);
    translate([-5.5,16,16]) rotate([270,0,0]) cylinder(4, 1.0, 1.0);
  }  
  
  //translate([11,-13,4]) rotate([270,0,0]) cylinder(22, 1, 1);
  
}
}

// Chassis deel.
/*
 Print this part separately, and rotate it in Cura 
 with the servo mountings on the build plate to 
 get a stable result.
*/
if( CHASSIS ) {
translate([-34,0,0]) {
  difference() {
    translate([0,2,0]) cube([34.5, 12, 4]);
    translate([34,2,3]) cube([2, 12, 2]);
    translate([0,4,0]) cube([29, 8, 2]);
    
    // Originele LGB haak bevestiging:
    translate([25,8,0]) cylinder(4, 4, 4);
    translate([22,8,0]) cylinder(4, 4, 4);
    translate([22,4,0]) cube([3, 8, 4]);

    // Schroef opening met soeverein:
    translate([8,8,0]) cylinder(4, 1.5, 1.5);
    translate([8,8,3]) cylinder(1, 1.5, 2.5);

    // Extra schroef opening met soeverein:
    translate([15,8,0]) cylinder(4, 1.5, 1.5);
    translate([15,8,3]) cylinder(1, 1.5, 2.5);

    // Draaipunt opening:
    translate([32,2,3]) rotate([270,0,0]) cylinder(12, 1.5, 1.5);
  }

  // Draaipunt koppeling:
  difference() {
    translate([32,2,3]) rotate([270,0,0]) cylinder(12, 3, 3);
    // Draaipunt opening:
    translate([32,2,3]) rotate([270,0,0]) cylinder(12, 1.5, 1.5);
    if( !NANO )
      translate([28,9,4]) cube([5, 3, 10]);
  }

  // Servo bevestiging:
  if( NANO ) {
    difference() {
      translate([1.75,12,7.2]) rotate([270,0,0]) cylinder(2, 1.75, 1.75);
      translate([1.75,12,7.2]) rotate([270,0,0]) cylinder(2, 0.8, 0.8);
    }
    difference() {
      translate([0,12,4]) cube([3.5, 2, 3.2]);
      translate([1.75,12,7.2]) rotate([270,0,0]) cylinder(2, 0.8, 0.8);
    }

    difference() {
      translate([1.75+17,12,7.2]) rotate([270,0,0]) cylinder(2, 1.75, 1.75);
      translate([1.75+17,12,7.2]) rotate([270,0,0]) cylinder(2, 0.8, 0.8);
    }
    difference() {
      translate([0+17,12,4]) cube([3.5, 2, 3.2]);
      translate([1.75+17,12,7.2]) rotate([270,0,0]) cylinder(2, 0.8, 0.8);
    }
  }

  // Micro mounting
  difference() {
    translate([2.5,12,10]) rotate([270,0,0]) cylinder(2, 2.5, 2.5);
    translate([2.5,12,10]) rotate([270,0,0]) cylinder(2, 1.0, 1.0);
    if( NANO )
      translate([1.75,12,7.2]) rotate([270,0,0]) cylinder(2, 0.8, 0.8);
  }
  difference() {
    translate([0,12,4]) cube([5, 2, 6]);
    translate([2.5,12,10]) rotate([270,0,0]) cylinder(2, 1.0, 1.0);
    if( NANO )
      translate([1.75,12,7.2]) rotate([270,0,0]) cylinder(2, 0.8, 0.8);
  }
  
  difference() {
    translate([30.5,12,10]) rotate([270,0,0]) cylinder(2, 2.5, 2.5);
    translate([30.5,12,10]) rotate([270,0,0]) cylinder(2, 1.0, 1.0);
  }
  difference() {
    translate([28,12,4]) cube([6.5, 2, 6]);
    translate([30.5,12,10]) rotate([270,0,0]) cylinder(2, 1.0, 1.0);
    // Draaipunt opening:
    translate([32,2,3]) rotate([270,0,0]) cylinder(12, 1.5, 1.5);
  }

}
}


// Drijfstang V1
if( SPRING ) {
  translate([0,30,0]) {
    LENGTH=31; // 24 voor servo, 31 voor passief. 
    H=1.6;
    yoff=0;
    difference() {
      translate([0,yoff+0,0]) cube([LENGTH, 2, H]);
      translate([0,yoff+1,0]) cylinder(H, 1.2, 1.2);
      translate([LENGTH,yoff+1,0]) cylinder(H, 1.2, 1.2);
      // Veer knip:
      translate([0,yoff+0,0]) cube([17, 2, H]);
    }

    // X- Ronde aansluitingen met gat:
    conoff=4;
    difference() {
      translate([0,conoff+yoff-1,0]) cube([3, 4, H]);
      translate([0,conoff+yoff+1,0]) cylinder(H, 1.2, 1.2);
    }
    difference() {
      translate([0,conoff+yoff+1,0]) cylinder(H, 2, 2);
      translate([0,conoff+yoff+1,0]) cylinder(H, 1.2, 1.2);
    }
    // X+ Ronde aansluitingen met gat:
    difference() {
      translate([LENGTH,yoff+1,0]) cylinder(H, 2, 2);
      translate([LENGTH,yoff+1,0]) cylinder(H, 1.2, 1.2);
    }

    // Veer
    spring(2,0,10,H,true);
    spring(8,0,10,H,true);
    spring(14,0,10,H,false);

    // Versteviging met rechtse aansluiting
    translate([17,0,0]) {
      difference() {
        translate([0,0,0]) cube([3, 3, H]);
      }
    }


  }
}



// Servo hevel
if( LEVER ) {
translate([40,0,0]) {
  LEVERLENGTH=5.5;
  difference() {
    cylinder(5, 3.5, 3.5);
    translate([0,0,0]) cylinder(5, 2.45, 2.45);
  }
  difference() {
    translate([0,0,0]) cylinder(1.5, 2.45, 2.45);
    translate([0,0,0]) cylinder(2.5, 1.3, 1.3);
  }
  difference() {
    translate([0,-2.5,0]) cube([LEVERLENGTH, 5, 2.5]);
    translate([0,0,0]) cylinder(5, 2.45, 2.45);
    translate([LEVERLENGTH,0,0]) cylinder(2.5, 1, 1);
  }
  difference() {
    translate([LEVERLENGTH,0,0]) cylinder(2.5, 2.5, 2.5);
    translate([LEVERLENGTH,0,0]) cylinder(2.5, 1, 1);
  }
}
}
