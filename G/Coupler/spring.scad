
/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=96;
//$fn=24;

include <../../lib/shapes.scad>

translate([0,0,0]) {
  LENGTH=31; // 24 voor servo, 31 voor passief. 
  H=1.6;
  yoff=0;
  difference() {
    translate([2,yoff+0,0]) cube([LENGTH-2, 2, H]);
    translate([0,yoff+1,0]) cylinder(H, 1.2, 1.2);
    translate([LENGTH,yoff+1,0]) cylinder(H, 1.2, 1.2);
    // Veer knip:
    translate([2,yoff+0,0]) cube([15, 2, H]);
  }
  // Ronde aansluitingen met gat:
  conoff=4; // 0 voor servo, 4 voor passief
  difference() {
    translate([0,conoff+yoff-1,0]) cube([3, 4, H]);
    translate([0,conoff+yoff+1,0]) cylinder(H, 1.2, 1.2);
  }
  difference() {
    translate([0,conoff+yoff+1,0]) cylinder(H, 2, 2);
    translate([0,conoff+yoff+1,0]) cylinder(H, 1.2, 1.2);
  }
  difference() {
    translate([LENGTH,yoff+1,0]) cylinder(H, 2, 2);
    translate([LENGTH,yoff+1,0]) cylinder(H, 1.2, 1.2);
  }

  // Veer
  spring(2,0,10,H,true);
  spring(8,0,10,H,true);
  spring(14,0,10,H,false);

  // Versteviging met rechtse aansluiting
  translate([17,0,0]) {
    difference() {
      translate([0,0,0]) cube([3, 3, H]);
    }
  }


}

