/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=96;
//$fn=48;
// Servo hevel

translate([0,0,0]) {
  LEVERLENGTH=5.5;
  difference() {
    cylinder(5, 3.5, 3.5);
    translate([0,0,0]) cylinder(5, 2.45, 2.45);
  }
  difference() {
    translate([0,0,0]) cylinder(1.5, 2.45, 2.45);
    translate([0,0,0]) cylinder(2.5, 1.3, 1.3);
  }
  difference() {
    translate([0,-2.5,0]) cube([LEVERLENGTH, 5, 2.5]);
    translate([0,0,0]) cylinder(5, 2.45, 2.45);
    translate([LEVERLENGTH,0,0]) cylinder(2.5, 1, 1);
  }
  difference() {
    translate([LEVERLENGTH,0,0]) cylinder(2.5, 2.5, 2.5);
    translate([LEVERLENGTH,0,0]) cylinder(2.5, 1, 1);
  }
}

