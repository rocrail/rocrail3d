/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
//include <../../lib/shapes.scad>


R=60/2;
D=1.8;

difference() {
  translate([0,0,0]) cylinder(D, R, R);
  translate([0,0,0]) cylinder(D, 2.5, 2.5);
 
}
