/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
//include <../../lib/shapes.scad>


L=20;
Win=14.5;
Hin=5;
D=1.4;
X=.8;

difference() {
  translate([0,0,0]) cube([L, Win+2*D, Hin+2*D]);
  translate([0,D,D]) cube([L, Win, Hin]);
  translate([0,D+X,D]) cube([L, Win-2*X, Hin+D]);
  
  translate([L/2,D+Win/2,0]) cylinder(D, 1.5, 4.5);
  
}
 
  
  