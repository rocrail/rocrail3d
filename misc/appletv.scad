/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
//include <../../lib/shapes.scad>

BASE=1;


if( BASE ) translate([0,0,0]) {
  difference() {
    translate([-8,0,0]) cube([95+4+16, 95+4, 2]);
    translate([-3,15,0]) cylinder(10,3,3);
    translate([95+4+3,15,0]) cylinder(10,3,3);
    translate([-3,95+4-15,0]) cylinder(10,3,3);
    translate([95+4+3,95+4-15,0]) cylinder(10,3,3);
    translate([(95+4)/2,(95+4)/2,0]) cylinder(10,40,40);
  }
  difference() {
    translate([0,0,0]) cube([95+4, 95+4, 20]);
    translate([2,2,2]) cube([95, 95, 20]);
    translate([0,(95+4)/2,10]) rotate([0,90,0]) cylinder(100,4,4);
    translate([(95+4)/2,0,10]) rotate([270,90,0]) cylinder(100,4,4);
    translate([(95+4)/2,(95+4)/2,0]) cylinder(10,40,40);
  }
}