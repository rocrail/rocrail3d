/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
//$fn=256;
$fn=64;
include <../lib/shapes.scad>

RING=0;
LIJST1=0;
LIJST2=0;

STAAF=1;

if( RING ) {
  difference() {
    translate([0,0,0]) cylinder(4, 10,10);
    translate([0,0,0]) cylinder(5, 7,7);
    translate([-.2,0,0]) cube([.4,12,6]);
  }
}

if( STAAF ) {
  l=100;
  difference() {
    translate([0,0,4]) rotate([90,0,90]) cylinder(l, 4,4);
    translate([l/2-80/2,0,2]) cylinder(10,2.1,2.1);
    translate([l/2+80/2,0,2]) cylinder(10,2.1,2.1);
  }
  translate([0,0,4]) sphere(4);
  translate([l,0,4]) sphere(4);
}


if( LIJST2 ) translate([20,0,0]) {
  l=100;
  w=30;
  difference() {
    translate([0,0,0]) cube([l,w,2]);
    translate([l/5,w/2,0]) cylinder(2, 1.5,2.5);
    translate([l-l/5,w/2,0]) cylinder(2, 1.5,2.5);
  }

  translate([0,w/2,0]) cylinder(2,w/2,w/2);
  translate([l,w/2,0]) cylinder(2,w/2,w/2);

  translate([l/2-80/2,w/2,1]) rotate([-10,0,0]) cylinder(10,2,2);
  translate([l/2+80/2,w/2,1]) rotate([-10,0,0]) cylinder(10,2,2);

  translate([l/2-80/2,w/2,1]) cylinder(2,3,3);
  translate([l/2+80/2,w/2,1]) cylinder(2,3,3);

  translate([l/2-80/2,w/2+1.7,10.5]) sphere(2);
  translate([l/2+80/2,w/2+1.7,10.5]) sphere(2);

  translate([l/2-220/2,w,0]) cube([220,2,10]);
  translate([l/2-220/2,w+2,5]) rotate([90,0,0]) cylinder(2,5,5);
  translate([l/2+220/2,w+2,5]) rotate([90,0,0]) cylinder(2,5,5);
  
  translate([-60,w,2]) rotate([0,90,270]) prism(2,60,w/2);
  translate([160,w,0]) rotate([0,270,90]) prism(2,60,w/2);

}



if( LIJST1 ) translate([20,0,0]) {
  l=100;
  difference() {
    translate([0,0,0]) cube([l,10,2]);
    translate([l/5,10/2,0]) cylinder(5, 2,2);
    translate([l-l/5,10/2,0]) cylinder(5, 2,2);
  }
  translate([0,-2,0]) {
    difference() {
      translate([0,0,0]) cube([l,2,10]);
      translate([l/2-80/2-4/2,-5,5]) cube([4,10,3]);
      translate([l/2+80/2-4/2,-5,5]) cube([4,10,3]);
    }
    translate([0,2/2-.8/2,0]) cube([l,.8,10]);
  }
}

