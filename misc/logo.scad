/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=64;


logo="/Users/robversluis/Projects/Rocrail/doc/rocrail-logo.net.png"; 


difference() {
  translate([0,0,1.8]) scale([.18, .18, .01]) surface(file=logo,invert=true,convexity=1); 
  translate([3,3,0]) cylinder(10,1.25,1.25);
  translate([3,60-3,0]) cylinder(10,1.25,1.25);
  translate([200-3,3,0]) cylinder(10,1.25,1.25);
  translate([200-3,60-3,0]) cylinder(10,1.25,1.25);
}

difference() {
  translate([0,0,0]) cube([200,60,1]);
  translate([3,3,0]) cylinder(10,1.25,1.25);
  translate([3,60-3,0]) cylinder(10,1.25,1.25);
  translate([200-3,3,0]) cylinder(10,1.25,1.25);
  translate([200-3,60-3,0]) cylinder(10,1.25,1.25);
}
