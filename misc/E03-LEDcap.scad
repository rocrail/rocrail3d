/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;


translate([0,0,0]) {
  difference() {
    translate([0,0,0]) cube([30, 10, 1]);
    translate([(30-6)/2,9,0]) cube([6, 1, 1]);
    translate([0,0,0]) cube([4, 5, 1]);
    translate([30-4,0,0]) cube([4, 5, 1]);
  }
}
