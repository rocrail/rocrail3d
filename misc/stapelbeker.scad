/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;
//include <../../lib/shapes.scad>
R=20; // step 5.5
H=30; // step 5.5
D=1.6;
V=4;
DD=2;
inc=4; // -1 ...

INC=(inc*4);

BEKER=0;
TOP=1;
TEST=0;

current_color = "ALL";
//current_color = "black";
//current_color = "white";
//current_color = "red";
//current_color = "green";
//current_color = "yellow";

module cc(color) {
  if (current_color == "ALL" || current_color == color) { 
    color(color)
    children();
  }        
}

if( TEST ) {
  cc("green") { 
    cylinder(2,10,10);
    cylinder(1,15,15);
  }
  cc("red") cylinder(3,5,5);
  cc("yellow") cylinder(5,2,2);
  cc("yellow") translate([10,0,0]) cylinder(5,2,2);
}

if( TOP ) {
  r=inc*4;
  h=DD*(inc+2);
  difference() {
    translate([0,0,0]) cylinder(h, R+r, R+r);
    translate([0,0,DD]) cylinder(DD+h, R+r-D*2, R+r-D*2);
    translate([0,0,DD*(inc+2)]) cylinder(DD+h, R+r-D, R+r-D);
    translate([0,0,0]) cylinder(DD+2*h, 10, 10);
  }
  difference() {
    translate([0,0,0]) cylinder(h+V, R+r-D, R+r-D);
    translate([0,0,0]) cylinder(h+V, R+r-D-D, R+r-D-D);
  }
  
}

if( BEKER ) {
  difference() {
    translate([0,0,V]) cylinder(H+INC,R+INC,R+INC);
    translate([0,0,V+D]) cylinder(H+INC,R+INC-D,R+INC-D);
  }
    translate([0,0,0]) cylinder(V,R+INC-inc-DD,R+INC-inc-DD);

  echo( "bottom R", R+INC-inc-DD);
  echo( "top    R", R+INC-D);


  m=.25;  
  for(y = [0 : 4]) {
    difference() {
      translate([0,0,H+INC+V+y*.2]) cylinder(.2, R+INC-y*m, R+INC-y*m);
      translate([0,0,H+INC+y*.2]) cylinder(10, R+INC-D+y*m, R+INC-D+y*m);
    }

    translate([0,0,V-y*.2]) cylinder(.2, R+INC-y*m, R+INC-y*m);
  }
}