/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

include <../lib/shapes.scad>

difference() {
  translate([0,0,0]) cube([12, 20, 1]);
  translate([2.5,2.8,0]) cube([7, 14.4, 1]);
  translate([6,0,0]) cylinder(2, 2.5, 2.5);
  translate([6,20,0]) cylinder(2, 2.5, 2.5);
}

translate([0,0,0]) {
  difference() {
    translate([0,0,1]) cube([12, 20, 24]);
    translate([1.8,2,1]) cube([8.4, 16, 24]);
    translate([6,0,0]) cylinder(2, 2.5, 2.5);
    translate([6,20,0]) cylinder(2, 2.5, 2.5);
  }
  difference() {
    translate([-.5,-.5,23]) cube([13, 21, 2]);
    translate([1.8,2,23]) cube([8.4, 16, 2]);
  }
}
