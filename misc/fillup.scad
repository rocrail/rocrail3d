/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;


for( a =[0:3] ) {
  translate([a*23,0,0]) cube([20, 60, a*.5+1]);
}
