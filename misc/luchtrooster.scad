/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;
//$fn=64;
include <../lib/shapes.scad>

ROND=0;
VIERKANT=0;
STOP=0;
STOPDOP=0;
HAARFILTER1=0;
HAARFILTER2=0;
HAARFILTERTOP=1;

if( HAARFILTERTOP ) {
  d0=37;
  d1=15.5;
  h1=19;
  l=1.3;
 
  translate([0,0,0]) cylinder(1, d1/2,d1/2);
  difference() {
    translate([0,0,0]) cylinder(1, d0/2+1,d0/2+1);
    translate([0,0,0]) cylinder(1, d0/2-1,d0/2-1);
  }

  difference() {
    translate([0,0,0]) cylinder(2, d0/2+1-2,d0/2+1-2);
    translate([0,0,0]) cylinder(2, d0/2-2,d0/2-2);
  }
  
  // rooster spijlen
  CNT3=36;      
  for(x = [0 : CNT3]) {
    rotate([0, 0, x*(360/CNT3)]) {
      translate ([d1/2, -.5, 0]) cube([d0/2-d1/2,.8,1]);
    }
  }
    
  
}

if( HAARFILTER2 ) {
  d0=37;
  d1=15.5;
  h1=19;
  l=1.3;
  
  // bodem
  difference() {
    translate([0,0,0]) cylinder(1.2, d0/2,d0/2);
    translate([0,0,0]) cylinder(1.2, d0/2-1.5,d0/2-1.5);
    translate([0,0,0]) cylinder(h1-1, d1/2,d1/2);
    translate([-5/2,-(d1/2+2),0]) cube([5,4,h1+2]);
  }
  // buiten rand
  difference() {
    translate([0,0,0]) cylinder(h1-2, d0/2,d0/2);
    translate([0,0,0]) cylinder(h1-2, d0/2-1,d0/2-1);
  }
  // rooster spijlen
  CNT0=36;      
  for(x = [0 : CNT0]) {
    rotate([0, 0, x*(360/CNT0)]) {
      translate ([d0/2-.8, -.5, h1-2]) cube([1.8,1,10]);
    }
  }
  difference() {
    translate([0,0,h1+7]) cylinder(1, d0/2+1,d0/2+1);
    translate([0,0,h1+7]) cylinder(1, d0/2-1,d0/2-1);
  }
  difference() {
    translate([0,0,h1-3]) cylinder(1, d0/2,d0/2+1);
    translate([0,0,h1-3]) cylinder(1, d0/2-1,d0/2-1);
  }
  
  // rooster midden
  difference() {
    //translate([0,0,0]) cylinder(1, d0/3+2.5,d0/3+2.5);
    //translate([0,0,0]) cylinder(1, d0/3+2.5-.8,d0/3+2.5-.8);
  }
  
  // rooster spijlen
  CNT3=8;      
  for(x = [0 : CNT3]) {
    difference() {
      rotate([0, 0, x*(360/CNT3)]) {
        translate ([d1/2, -.5, 0]) cube([d0/2-d1/2,1,1.2]);
      }
      translate([-5/2,-(d1/2+2),0]) cube([5,4,h1+2]);
    }
  }
    
  // midden,as    
  difference() {
    translate([0,0,0]) cylinder(h1, (d1+3)/2,(d1+3)/2);
    translate([0,0,0]) cylinder(h1+1, d1/2,d1/2);
    translate([-5/2,-(d1/2+2),0]) cube([5,4,h1+2]);
  }
  
  // rooster binnen ring
  difference() {
    translate([0,0,0]) cylinder(1.2, (d1+6)/2,(d1+6)/2);
    translate([0,0,0]) cylinder(h1+1, d1/2,d1/2);
    translate([-5/2,-(d1/2+2),0]) cube([5,4,h1+2]);
  }

  //translate([-5/2,-(d1/2+2),0]) cube([5,4,h1+2]);
}






if( HAARFILTER1 ) {
  d0=37;
  d1=15.5;
  h1=19;
  l=1.3;
  
  // bodem
  difference() {
    translate([0,0,0]) cylinder(1.2, d0/2,d0/2);
    translate([0,0,0]) cylinder(1.2, d0/2-1.5,d0/2-1.5);
    translate([0,0,0]) cylinder(h1-1, d1/2,d1/2);
    rotate([0,0,67]) translate([-5/2,-(d1/2+2),0]) cube([5,4,h1+2]);
  }
  // buiten rand
  difference() {
    translate([0,0,0]) cylinder(4, d0/2,d0/2);
    translate([0,0,0]) cylinder(4, d0/2-1,d0/2-1);
  }
  
  // rooster midden
  difference() {
    translate([0,0,0]) cylinder(1, d0/3+2.5,d0/3+2.5);
    translate([0,0,0]) cylinder(1, d0/3+2.5-.8,d0/3+2.5-.8);
  }
  
  // rooster spijlen
  CNT3=36;      
  for(x = [0 : CNT3]) {
    difference() {
      rotate([0, 0, x*(360/CNT3)]) {
        translate ([d1/2, 0, 0]) cube([d0/2-d1/2,.8,1]);
      }
      rotate([0,0,67]) translate([-5/2,-(d1/2+2),0]) cube([5,4,h1+2]);
    }
  }
    
  // midden,as    
  difference() {
    translate([0,0,0]) cylinder(h1, (d1+2)/2,(d1+2)/2);
    translate([0,0,0]) cylinder(h1+1, d1/2,d1/2);
    rotate([0,0,67]) translate([-5/2,-(d1/2+2),0]) cube([5,4,h1+2]);
  }
  
  // rooster binnen ring
  difference() {
    translate([0,0,0]) cylinder(1.2, (d1+5)/2,(d1+5)/2);
    translate([0,0,0]) cylinder(h1+1, d1/2,d1/2);
    rotate([0,0,67]) translate([-5/2,-(d1/2+2),0]) cube([5,4,h1+2]);
  }

  //translate([-5/2,-(d1/2+2),0]) cube([5,4,h1+2]);
}



if( STOPDOP ) {
  translate([0,0,0]) cylinder(5, 8/2,8/2);
  translate([0,0,0]) cylinder(1.6, 12/2,12/2);
}

if( STOP ) {
  d=195;
  s=3;
  h=20;
  difference() {
    translate([0,0,0]) cylinder(h, d/2,d/2);
    translate([0,0,s]) cylinder(h, d/2-s,d/2-s);
    translate([20,20,0]) cylinder(20, 8/2,8/2);
    translate([-20,-20,0]) cylinder(20, 8/2,8/2);
    translate([20,-20,0]) cylinder(20, 8/2,8/2);
    translate([-20,20,0]) cylinder(20, 8/2,8/2);
  }
  
  translate([0,-s/2,0]) rotate([0,0,0]) cube([d/2,s,15]);
  translate([s/2,0,0]) rotate([0,0,90]) cube([d/2,s,15]);
  translate([0,+s/2,0]) rotate([0,0,180]) cube([d/2,s,15]);
  translate([-s/2,0,0]) rotate([0,0,270]) cube([d/2,s,15]);
}


if( VIERKANT ) {
  w=90;
  l=120;
  
  difference() {
    translate([0,0,0]) cube([w,l,30]);
    translate([10,10,0]) cube([w-2*10,l-2*10,34]);
    translate([2,2,0]) cube([w-2*2,l-2*2,34]);
    translate([-2,-2,20]) cube([w,l,30]);
  
    translate([0,20,25]) rotate([90,0,90]) cylinder(100, 1.5,1.5);
    translate([0,l-20,25]) rotate([90,0,90]) cylinder(100, 1.5,1.5);
    translate([20,20,25]) rotate([270,0,0]) cylinder(l, 1.5,1.5);
    translate([w-20,20,25]) rotate([270,0,0]) cylinder(l, 1.5,1.5);
  }


  
  for(y = [0 : 10]) {
    difference() {
      translate([0,2+y*10.5-2,0]) rotate([-40,0,0]) cube([w, 2, 20]);
      translate([0,0,-10]) cube([w, l, 10]);
    }
  }

}


module mask() {
  translate([0,0,0]) difference() {
    translate([0,0,0]) cylinder(20, 100,100);
    translate([0,0,-1]) cylinder(30, r-2,r-2);
  }
}

if( ROND ) {
  r=68/2;
  rr=10;

  translate([0,0,0]) difference() {
    translate([0,0,0]) cylinder(2, r+rr,r+rr);
    translate([0,0,-1]) cylinder(4, r,r);
    translate([0,-r-rr/2,0]) cylinder(2, 2.5,1.5);
    translate([0,r+rr/2,0]) cylinder(2, 2.5,1.5);
  }

  translate([0,0,0]) difference() {
    translate([0,0,0]) cylinder(20, r,r);
    translate([0,0,-1]) cylinder(30, r-2,r-2);
  }


  translate([-1,-r,0]) cube([2, r*2, 12]);


  for(y = [0 : 7]) {
    difference() {
      translate([-r,-r+y*10-2,0]) rotate([-40,0,0]) cube([r*2, 2, 20]);
      mask();
      translate([-60,-60,-10]) cube([60*2, 60*2, 10]);
      translate([-60,-60,10]) cube([60*2, 60*2, 10]);
    }
  }
}
