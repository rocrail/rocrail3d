/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;

difference() {
  translate([0,0,0]) cylinder(20, 25/2, 25/2);
  translate([0,0,0]) cylinder(20, 23/2+.25, 23/2+.25);
}
