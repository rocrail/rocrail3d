/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../lib/shapes.scad>

/*
print maat x = 86,36 y = 76,50

gaten:  
L.B.     x = 1,9       y = 1,59
R.B.     x = 84,46     y = 1,59 
L.O.     x = 1,9       y = 74,61
R.O.     x = 84,46     y = 74,61

*/


DUMMY=false;
BOTTOM=false;
COVER=true;

PCB_THICKNESS=1.6;

if( DUMMY ) {
  translate([120,2.82,0]) {
    difference() {
      // print maat x = 86,36 y = 76,50
      translate([0,0,0]) cube([86.36, 76.50, PCB_THICKNESS]);
      translate([1.9,1.59,0]) cylinder(PCB_THICKNESS,1,1);
      translate([1.9,74.61,0]) cylinder(PCB_THICKNESS,1,1);
      translate([84.46,1.59,0]) cylinder(PCB_THICKNESS,1,1);
      translate([84.46,74.61,0]) cylinder(PCB_THICKNESS,1,1);
    }
  }
}

// Bottom
if( BOTTOM ) {
  translate([0,0,0]) {
    difference() {
      // print maat x = 86,36 y = 76,50
      translate([0,0,0]) cube([92, 82, 8]);
      translate([2,2,2]) cube([88, 78, 6]);

      translate([0,82/2,5])rotate([90,0,90]) cylinder(92, 1.0, 1.0);
      
      
      // PCB mounting points:
      translate([1.9+2.82,1.59+2.75,0]) 
        translate([0,0,0]) cylinder(6.4,1.2,1.0);
      
      translate([84.46+2.82,1.59+2.75,0]) 
        translate([0,0,0]) cylinder(6.4,1.2,1.0);
      
      translate([1.9+2.82,74.61+2.75,0]) 
        translate([0,0,0]) cylinder(6.4,1.2,1.0);
      
      translate([84.46+2.82,74.61+2.75,0]) 
        translate([0,0,0]) cylinder(6.4,1.2,1.0);
      
    }
    
    translate([92+10,(82/2)-10,2]){
      rotate(90) prism(2, 8, 6);
    }
    translate([92+10,(82/2)+8,2]){
      rotate(90) prism(2, 8, 6);
    }
    translate([-10,(82/2)-8,2]){
      rotate(270) prism(2, 8, 6);
    }
    translate([-10,(82/2)+10,2]){
      rotate(270) prism(2, 8, 6);
    }

    
    // Clips:
    translate([-2,(82/2)-10,0]) {
      difference() {
        translate([0,0,0]) cube([2, 20, 8]);
        translate([0,2,0]) cube([2, 16, 8]);
      }
    }    
    translate([92,(82/2)-10,0]) {
      difference() {
        translate([0,0,0]) cube([2, 20, 8]);
        translate([0,2,0]) cube([2, 16, 8]);
      }
    }    

    translate([-10,(82/2)-10,0]) {
      difference() {
        translate([0,0,0]) cube([10, 20, 2]);
        translate([4,10,0]) cylinder(2,2,2);
      }
    }    
    translate([92,(82/2)-10,0]) {
      difference() {
        translate([0,0,0]) cube([10, 20, 2]);
        translate([6,10,0]) cylinder(2,2,2);
      }
    }    

    
    /*
    gaten:  
    L.B.     x = 1,9       y = 1,59
    R.B.     x = 84,46     y = 1,59 
    L.O.     x = 1,9       y = 74,61
    R.O.     x = 84,46     y = 74,61
    */
    // PCB mounting points:
    translate([1.9+2.82,1.59+2.75,0]) {
      difference() {
        translate([0,0,0]) cylinder(6.4,3.5,3.5);
        translate([0,0,0]) cylinder(6.4,1.2,1.0);
      }
    }
    translate([84.46+2.82,1.59+2.75,0]) {
      difference() {
        translate([0,0,0]) cylinder(6.4,3.5,3.5);
        translate([0,0,0]) cylinder(6.4,1.2,1.0);
      }
    }
    translate([1.9+2.82,74.61+2.75,0]) {
      difference() {
        translate([0,0,0]) cylinder(6.4,3.5,3.5);
        translate([0,0,0]) cylinder(6.4,1.2,1.0);
      }
    }
    translate([84.46+2.82,74.61+2.75,0]) {
      difference() {
        translate([0,0,0]) cylinder(6.4,3.5,3.5);
        translate([0,0,0]) cylinder(6.4,1.2,1.0);
      }
    }
    
  }
}


// Cover
COVERHEIGHT=20;
if( COVER ) {
  translate([-2,90,0]) {
    difference() {
      translate([0,0,0]) cube([96, 48, COVERHEIGHT]);
      translate([2,2,2]) cube([92, 44, COVERHEIGHT-2]);
      translate([2,0,COVERHEIGHT-8]) cube([92, 48, 8]);
      
      translate([0,(48/2)-10,COVERHEIGHT-2])cube([96, 20, 2]);
      
      translate([0,48/2,COVERHEIGHT-5])rotate([90,0,90]) cylinder(96, 1.2, 1.2);
      
      // Logos:
      translate([84,30,0]){
        rotate(0) mirror([1,0,0]) linear_extrude(0.4) text("GCAbox",8);
      }
      
      // Clips:
      translate([0,(48/2)-10,COVERHEIGHT-8]) {
        difference() {
          translate([0,0,0]) cube([2, 20, 8]);
          translate([0,2,0]) cube([2, 16, 8]);
        }
      }    
      translate([94,(48/2)-10,COVERHEIGHT-8]) {
        difference() {
          translate([0,0,0]) cube([2, 20, 8]);
          translate([0,2,0]) cube([2, 16, 8]);
        }
      }    
    


      // Roundings
      translate([96,2,0]){
        rotate(180) prism(96, 10, 10);
      }
      translate([0,46,0]){
        rotate(0) prism(96, 10, 10);
      }
      translate([2,0,0]){
        rotate(90) prism(48, 10, 10);
      }
      translate([94,48,0]){
        rotate(270) prism(48, 10, 10);
      }

    }
    
    
  }
}
