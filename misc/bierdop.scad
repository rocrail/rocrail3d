/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;
//$fn=64;
include <../lib/shapes.scad>

BOTTOM=1;
TOP=1;

if( BOTTOM ) {
  difference() {
    translate([0,0,0]) cylinder(2,34/2,34/2);
    translate([0,0,0]) cylinder(12,.75,.75);
  }
  difference() {
    translate([0,0,0]) cylinder(14,32/2,30/2);
    translate([0,0,0]) cylinder(14,.75,.75);
    translate([0,0,2]) cylinder(12,28/2,28.5/2);
  }
  difference() {
    translate([0,0,2]) cylinder(8,16.5/2,16.5/2);
    translate([0,0,2]) cylinder(8,5/2,14/2);
  }
}

if( TOP ) {
  difference() {
    translate([40,0,-5]) sphere(30/2);
    translate([40,0,-5]) sphere(28/2);
    translate([40-32/2,-32/2,-32]) cube([32,32,32]);
    translate([40,0,0]) cylinder(32,.75,.75);
  }
  difference() {
    translate([40,0,0]) cylinder(1,34/2,34/2);
    translate([40,0,0]) cylinder(1,24/2,24/2);
  }

  difference() {
    //translate([40,0,12]) sphere(10/2);
    translate([40,0,8]) cylinder(10,4,5);
    translate([40,0,0]) cylinder(32,.75,.75);
  }
}

