/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../lib/shapes.scad>
MIRROR=0;
BEAM=1;

if( BEAM ) {
  translate([0,0,0]) cube([222, 2, 10]);
  difference() {
    translate([0,0,0]) cube([222, 30, 10]);
    translate([4,0,4]) cube([222-2*4, 30, 10]);
    for(x = [0:7]) {
      translate([23+x*25,30/2,0]) cylinder(8, 10, 10);
    }
  }
}
else {
  mirror([MIRROR,0,0]) {
    difference() {
      translate([0,0,4]) rotate([0,90,0]) prism(4, 210, 50);
      translate([5,45,4]) rotate([0,90,0]) prism(4, 150, 35.7);
      translate([0,0,0]) cube([10, 10, 4]);
      translate([6,53,0]) cylinder(8, 2.5, 2.5);

      translate([6,210-8,0]) cylinder(8, 2.5, 2.5);
      translate([50-12,210-8,0]) cylinder(8, 2.5, 2.5);
    }


    translate([0,10,0]) rotate([0,0,-15]) cube([14, 6, 4]);

    translate([0,130,0]) cube([30, 6, 4]);

    difference() {
      translate([0,45,0]) cube([10, 16, 4]);
      translate([6,53,0]) cylinder(8, 2.5, 2.5);
    }
    
    difference() {
      h=51.5;
      translate([-40,10,0]) cube([40, 200, 4]);
      translate([-40+8,14+10,0]) cube([40-8-4, h, 4]);
      translate([-40+8,14+10+h+8,0]) cube([40-8-4, h, 4]);
      translate([-40+8,14+10+2*h+2*8,0]) cube([40-8-4, h, 4]);
      translate([-40+8,210-8,0]) cylinder(8, 2.5, 2.5);
      translate([-40+8,18,0]) cylinder(8, 2.5, 2.5);
    }  
    translate([-40,10,0]) cube([4, 200, 8]);
  }
}