/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;
//$fn=64;
include <../lib/shapes.scad>



W=21; // binnen maat
L=40; // 33/40 binnen maat
D=2;
H=20;


translate([0,0,5]) difference() {
  translate([0,0,0]) cube([W+2*D, L+2*D, H]);
  //translate([0,0,0]) rcube(H, W+2*D, L+2*D, 2);
  translate([D,D,2*D]) cube([W, L, H]);
  translate([-D,L/2+D,13]) rotate([90,0,90]) cylinder(W+D*4,1.5,1.5);
}

r=50; // 37/50
difference() {
  translate([0,L/2+D,r]) rotate([90,0,90]) cylinder(W+D*2,r,r);
  translate([0,-40,5]) cube([W*2, 120, 120]);
}
  //translate([-10,-40,5]) cube([W*2, 120, 120]);

