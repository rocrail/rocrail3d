/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;
//$fn=64;
include <../lib/shapes.scad>

/*
  Trennwand für Stanly Kleinteilemagazine.
*/

TW30=0;
TW09=1;

if( TW30 ) {
  rcube(1.6,51,30,2);
}

if( TW09 ) {
  rcube(1.6,107,45,2);
}
