/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;

stok=0;
bal=0;
DS214=1;



if( DS214 ) {
  difference() {
    translate([0,0,0]) cube([27+2, 16.5+2, 5]);
    translate([1,1,1.5]) cube([27, 16.5, 5]);
  }
}


if( stok ) {
  translate([0,0,10]) sphere(10);
  translate([0,0,10]) rotate([0,90,0]) cylinder(150,3.5,3.5);
  translate([150,0,10]) sphere(10);
}

if( bal ) {
  difference() {
    translate([0,0,20]) sphere(20);
    translate([0,0,20]) sphere(18);
    translate([0,0,0]) cylinder(150,10,10);
  }
}