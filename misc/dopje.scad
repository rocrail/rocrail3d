

$fn=96;


translate([0,0,0]) cylinder(1.5, 5, 5);

difference() {
  translate([0,0,0]) cylinder(5.0, 2.5, 2.5);
  translate([-.2,-5,0]) cube([.6, 10, 5]);
}

difference() {
  translate([0,0,3.5]) cylinder(1.0, 2.9, 2.9);
  translate([-.2,-5,0]) cube([.6, 10, 5]);
}
