/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../lib/shapes.scad>

difference() {
  translate([0,0,0]) rcube(1.4, 115, 16, 5);
  translate([4,16/2,0]) cylinder(10,1.5,1.5);
  translate([115-4,16/2,0]) cylinder(10,1.5,1.5);
}

translate([8,5,1.4]){
  linear_extrude(0.6) text("Bitte keine Werbung",8);
}
