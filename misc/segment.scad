
/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
/*
https://www.thingiverse.com/thing:1604369
https://github.com/justinotherguy/OpenSCAD-Getriebebibliothek
*/
include <../lib/shapes.scad>

$fn=128;

// height, radius, degrees right, degrees total
translate([0,0,0]) sector(20, 200, 0, 180);
//translate([22,0,0]) sector(30, 20, 300, 30);
//translate([0,22,0]) sector(30, 20, 30, 300);
//translate([22,22,0]) sector(30, 20, 10, 190);

