/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../lib/shapes.scad>

// CAM V2
// 25mm x 24mm
CAM_WIDTH=25;
CAM_HEIGHT=30;

CAMBOX=true;
CONNPOLE=true;
CONNFOOT=true;

// CAM Box
if( CAMBOX ) {
  difference() {
    translate([0,0,0]) cube([CAM_WIDTH+2, CAM_HEIGHT+2, 10]);
    translate([1,1,2]) cube([CAM_WIDTH, CAM_HEIGHT, 8]);
    translate([2,1,0]) cube([CAM_WIDTH-2, 1, 2]);

    // Edges
    translate([CAM_WIDTH+2,1,0]){
      rotate(180) prism(CAM_WIDTH+2, 10, 10);
    }
    translate([0,CAM_HEIGHT+1,0]){
      rotate(0) prism(CAM_WIDTH+2, 10, 10);
    }
    translate([1,0,0]){
      rotate(90) prism(CAM_HEIGHT+2, 10, 10);
    }
    translate([CAM_WIDTH+1,CAM_HEIGHT+2,0]){
      rotate(270) prism(CAM_HEIGHT+2, 10, 10);
    }
    
    rotate([90,0,0]) translate([0,4,(CAM_WIDTH+2-10)/2]) cylinder(10, 1.5, 1.5);
    
    rotate([90,0,0]) translate([(CAM_WIDTH+2)/2,5,-4]) cylinder(10, 1.1, 1.1);
  }

  // CAM mount
  difference() {
    translate([2+1,CAM_HEIGHT-1,0.5]) cylinder(4.5, 2.4, 2.4);
    translate([2+1,CAM_HEIGHT-1,0.5]) cylinder(4.5, .5, .5);
  }
  difference() {
    translate([2+1+21,CAM_HEIGHT-1,0.5]) cylinder(4.5, 2.4, 2.4);
    translate([2+1+21,CAM_HEIGHT-1,0.5]) cylinder(4.5, .5, .5);
  }

}

if( CONNPOLE ) {
  // Connector pole
  translate([0,-10,0]) {
    difference() {
      translate([(CAM_WIDTH+2-10)/2,-60,0]) cube([10, 61, 8]);
      translate([(CAM_WIDTH+2-10)/2+1,-56,1]) cube([8, 55, 7]);
      rotate([90,0,90]) translate([-60,4,(CAM_WIDTH+2-10)/2]) cylinder(10, 1.7, 1.7);
    
      rotate([90,0,0]) translate([(CAM_WIDTH+2)/2,4,-4]) cylinder(10, 1.1, 1.1);
    }

    // Connector mount
    difference() {
      rotate([90,0,90]) translate([-60,4,(CAM_WIDTH+2-10)/2]) cylinder(10, 4, 4);
      rotate([90,0,90]) translate([-60,4,(CAM_WIDTH+2-10)/2]) cylinder(10, 1.7, 1.7);
    }
  }
}


if( CONNFOOT ) {
  translate([0,-85,4]) {
    
    difference() {
      translate([1,-8,-4]) cube([CAM_WIDTH, 16, 1.6]);
      translate([4,0,-5]) cylinder(14, 1, 1);
      translate([4+19.5,0,-5]) cylinder(14, 1, 1);
    }
    
    difference() {
      translate([6.5,-4,-4]) cube([2, 8, 8]);
      rotate([90,0,90]) translate([0,4,(CAM_WIDTH+2-14)/2]) cylinder(14, 1.7, 1.7);
    }
    difference() {
      translate([6.5+12,-4,-4]) cube([2, 8, 8]);
      rotate([90,0,90]) translate([0,4,(CAM_WIDTH+2-14)/2]) cylinder(14, 1.7, 1.7);
    }
    
    difference() {
      rotate([90,0,90]) translate([0,4,(CAM_WIDTH+2-14)/2]) cylinder(14, 4, 4);
      
      rotate([90,0,90]) translate([0,4,(CAM_WIDTH+2-10)/2]) cylinder(10, 4, 4);
      rotate([90,0,90]) translate([0,4,(CAM_WIDTH+2-14)/2]) cylinder(14, 1.7, 1.7);

    }
  }
}

