/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;


difference() {
  translate([60,60,0]) cylinder(2, 65, 65);
  translate([60,60,0]) cylinder(2, 25, 25);

  translate([60,-35,0]) cylinder(2, 60, 60);
  translate([60,35+120,0]) cylinder(2, 60, 60);
  translate([-35,60,0]) cylinder(2, 60, 60);
  translate([35+120,60,0]) cylinder(2, 60, 60);
  /*
  translate([60,15,0]) cylinder(2, 10, 10);
  translate([60,120-15,0]) cylinder(2, 10, 10);
  translate([15,60,0]) cylinder(2, 10, 10);
  translate([120-15,60,0]) cylinder(2, 10, 10);
  */
}

// Vesa connectors
difference() {
  translate([10,10,0]) cylinder(2, 10, 10);
  translate([10,10,0]) cylinder(2, 2.5, 2.5);
}
difference() {
  translate([10,110,0]) cylinder(2, 10, 10);
  translate([10,110,0]) cylinder(2, 2.5, 2.5);
}
difference() {
  translate([110,10,0]) cylinder(2, 10, 10);
  translate([110,10,0]) cylinder(2, 2.5, 2.5);
}
difference() {
  translate([110,110,0]) cylinder(2, 10, 10);
  translate([110,110,0]) cylinder(2, 2.5, 2.5);
}

// RPi 4 mounting
difference() {
  translate([31,35.5,0]) cylinder(10, 5, 3);
  translate([31,35.5,0]) cylinder(10, 1, 1);
}

difference() {
  translate([31+58,35.5,0]) cylinder(10, 5, 3);
  translate([31+58,35.5,0]) cylinder(10, 1, 1);
}

difference() {
  translate([31,35.5+49,0]) cylinder(10, 5, 3);
  translate([31,35.5+49,0]) cylinder(10, 1, 1);
}

difference() {
  translate([31+58,35.5+49,0]) cylinder(10, 5, 3);
  translate([31+58,35.5+49,0]) cylinder(10, 1, 1);
}

