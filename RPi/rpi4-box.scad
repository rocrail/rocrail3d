/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../lib/shapes.scad>

BOTTOM=0;
TOP=1;
SIDEMOUNT=1;
FAN=1;
TOPH=26;
HEADER=1;

W=85;
D=56;

if( TOP ) {
  translate([0,D+20,0]) {
    difference() {
      // Top
      translate([-4,-4,0]) cube([W+8, D+8, TOPH]);
      translate([-2.1,-2.1,2]) cube([W+4.2, D+4.2, TOPH-2]);
            
      // Edges
      translate([W+4,-2,0]){
        rotate(180) prism(W+8, 10, 10);
      }
      translate([-4,D+2,0]){
        rotate(0) prism(W+8, 10, 10);
      }
      translate([-2,-4,0]){
        rotate(90) prism(D+8, 10, 10);
      }
      translate([W+2,D+4,0]){
        rotate(270) prism(D+8, 10, 10);
      }
      
      // Header
      if( HEADER ) {
        translate([5,-4,0]) cube([53, 12, TOPH-14]);
      }
      
      // SD
      translate([-4,(D/2)-6,TOPH-5]) cube([4, 12, 5]);
      
      // LAN + USB
      translate([W,(D/2)-53/2,TOPH-21]) cube([4, 53, 17]);

      // USB + HDMI
      translate([5,D,TOPH-12]) cube([53, 4, 12]);

      // Top structure
      for( b =[1:41] ) {
        translate([.5+b*2,2,0]) cube([.4, D-4, .2]);
      }
      
      // Air
      for( c =[1:8] ) {
        translate([W/6,D/6+c*4,0]) cube([W/2, 2, 2]);
      }
      
      // Fan: 30x30x7
      if( FAN ) {
        translate([ (W-38)/2, (D-24)/2, 0 ]) cylinder(2, 1.5, 1.5);
        translate([ (W-38)/2+24, (D-24)/2, 0 ]) cylinder(2, 1.5, 1.5);
        translate([ (W-38)/2, (D-24)/2+24, 0 ]) cylinder(2, 1.5, 1.5);
        translate([ (W-38)/2+24, (D-24)/2+24, 0 ]) cylinder(2, 1.5, 1.5);
      }
      
    }

    // Bottom mounting
    difference() {
      translate([3.5+58,3.5,1]) cylinder(TOPH-12, 3.5, 3);
      translate([3.5+58,3.5,1]) cylinder(TOPH-12, 1.0, 1.0);
    }
    difference() {
      translate([3.5+58,3.5+49,1]) cylinder(TOPH-12, 3.5, 3);
      translate([3.5+58,3.5+49,1]) cylinder(TOPH-12, 1.0, 1.0);
    }


    // Flatten Air 
    difference() {
      translate([(W/6)-1,(D/6)+.5,0]) cube([W/2+2.4, D/1.5, 1]);
      // Air
      for( c =[1:8] ) {
        difference() {
          translate([W/6,D/6+c*4,0]) cube([W/2, 2, 2]);
        }
      }

      if( FAN ) {
        translate([ (W-38)/2, (D-24)/2, 0 ]) cylinder(2, 1.5, 1.5);
        translate([ (W-38)/2+24, (D-24)/2, 0 ]) cylinder(2, 1.5, 1.5);
        translate([ (W-38)/2, (D-24)/2+24, 0 ]) cylinder(2, 1.5, 1.5);
        translate([ (W-38)/2+24, (D-24)/2+24, 0 ]) cylinder(2, 1.5, 1.5);
      }
    }
    
    // Fan
    if( FAN ) {
      difference() {
        translate([ (W-38)/2, (D-24)/2, 0 ]) cylinder(2, 4, 4);
        translate([ (W-38)/2, (D-24)/2, 0 ]) cylinder(2, 1.5, 1.5);
      }
      difference() {
        translate([ (W-38)/2+24, (D-24)/2, 0 ]) cylinder(2, 4, 4);
        translate([ (W-38)/2+24, (D-24)/2, 0 ]) cylinder(2, 1.5, 1.5);
      }
      difference() {
        translate([ (W-38)/2, (D-24)/2+24, 0 ]) cylinder(2, 4, 4);
        translate([ (W-38)/2, (D-24)/2+24, 0 ]) cylinder(2, 1.5, 1.5);
      }
      difference() {
        translate([ (W-38)/2+24, (D-24)/2+24, 0 ]) cylinder(2, 4, 4);
        translate([ (W-38)/2+24, (D-24)/2+24, 0 ]) cylinder(2, 1.5, 1.5);
      }
    }
    
  }


}


if( BOTTOM ) {
  translate([0,0,0]) {
    // Base plate
    difference() {
      translate([-4,-4,0]) cube([W+8, D+8, 2]);
      if( !SIDEMOUNT ) {
        translate([10,D/2,0]) cylinder(2, 1.5, 3);
        translate([W-10,D/2,0]) cylinder(2, 1.5, 3);
      }      
      // Holes for top
      translate([3.5+58,3.5,0]) cylinder(2, 1.5, 1.5);
      translate([3.5+58,3.5+49,0]) cylinder(2, 1.5, 1.5);
      translate([3.5+58,3.5,0]) cylinder(1, 3, 1.5);
      translate([3.5+58,3.5+49,0]) cylinder(1, 3, 1.5);
    }
    
    // Bottom box
    difference() {
      translate([-2,-2,0]) cube([W+4, D+4, 6]);
      translate([-.2,-.2,2]) cube([W+.4, D+.4, 4]);
      if( !SIDEMOUNT ) {
        translate([10,D/2,0]) cylinder(2, 1.5, 3);
        translate([W-10,D/2,0]) cylinder(2, 1.5, 3);
      }
      // SD
      translate([-2,(D/2)-6,2]) cube([2, 12, 4]);

      // Holes for top
      translate([3.5+58,3.5,0]) cylinder(2, 1.5, 1.5);
      translate([3.5+58,3.5+49,0]) cylinder(2, 1.5, 1.5);
      translate([3.5+58,3.5,0]) cylinder(1, 3, 1.5);
      translate([3.5+58,3.5+49,0]) cylinder(1, 3, 1.5);
    }
    
    // RPi 4 mounting
    translate([3.5,3.5,0]) {
      difference() {
        cylinder(5, 5, 3); cylinder(5, 1, 1);
      }
    }
    translate([3.5+58,3.5,0]) {
      difference() {
        cylinder(5, 5, 3); cylinder(5, 1.5, 1.5); cylinder(2, 3, 1.5);
      }
    }
    translate([3.5,3.5+49,0]) {
      difference() {
        cylinder(5, 5, 3); cylinder(5, 1, 1);
      }
    }
    translate([3.5+58,3.5+49,0]) {
      difference() {
        cylinder(5, 5, 3); cylinder(5, 1.5, 1.5); cylinder(2, 3, 1.5);
      }
    }

  if( SIDEMOUNT ) {
    // left
    difference() {
      translate([-8,(D)/2-6,0]) cube([4, 12, 2]);
      translate([-8,(D)/2,0]) cylinder(2, 1.5, 3.0);
    }  
    difference() {
      translate([-8,(D)/2,0]) cylinder(2, 6, 6);
      translate([-8,(D)/2,0]) cylinder(2, 1.5, 3.0);
    }
    // right
    difference() {
      translate([W+4,(D)/2-6,0]) cube([4, 12, 2]);
      translate([W+8,(D)/2,0]) cylinder(2, 1.5, 3.0);
    }  
    difference() {
      translate([W+8,(D)/2,0]) cylinder(2, 6, 6);
      translate([W+8,(D)/2,0]) cylinder(2, 1.5, 3.0);
    }
  }
    
    
  }
}
