/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../lib/shapes.scad>



  difference() { 
    translate([0,0,0]) cube([10, 6+2*.8, 2]);
    translate([.8,.8,.4]) cube([10-.8, 6, 2]);
  }