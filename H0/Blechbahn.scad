/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../lib/shapes.scad>

HoH = 77.4;
RAILMAAT = 1;
H=4;

module railcut() {
  translate ([-10,0,0]) cube([2,2,H]);
  translate ([10-2,0,0]) cube([2,2,H]);
}

if( RAILMAAT ) {
  difference() {
    translate ([0,0,0]) cube([15+2*HoH+15,10,H]);
    translate ([30,3,0]) cube([HoH-2*15,5,H]);
    translate ([30+HoH,3,0]) cube([HoH-2*15,5,H]);
    translate ([15+0*HoH,0,0]) railcut();
    translate ([15+1*HoH,0,0]) railcut();
    translate ([15+2*HoH,0,0]) railcut();
  }

  difference() {
    translate ([(15+2*HoH+15)/2,0,0]) cylinder(4,20,20);
    translate ([(15+2*HoH+15)/2-20,-30,0]) cube([40,40,H]);
  }
}

