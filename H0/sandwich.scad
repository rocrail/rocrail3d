/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=256;
include <../lib/shapes.scad>

pi02  = 0;
wio01 = 0;

gca79 = 0;
gca93 = 1;

top    = 0;
bottom = 0;


pi02W = 69.5;
pi02D = 57;
pi02Wg = 60;
pi02Dg = 50;

wio01W = 78;
wio01D = 44;
wio01Wg = 73;
wio01Dg = 39.5;

gca79W  = 94.5;
gca79D  = 50.5;
gca79Wg = 84;
gca79Dg = 40;

gca93W  = 93.5;
gca93D  = 53;
gca93Wg = 77;
gca93Dg = 33.5;
gca93Og = 5;    // Gat offset: Gaten zitten niet symetrisch...



if( gca93 ) {
  difference() {
    translate([-(gca93W+4)/2,-(gca93D+4)/2,0]) cube([gca93W+4, gca93D+4, 5]);
    translate([-gca93W/2,-gca93D/2,1.5]) cube([gca93W, gca93D, 10]);

    translate([-(gca93W-24)/2,-(gca93D-18)/2,0]) cube([gca93W-24, gca93D-18, 10]);


    translate([-gca93Wg/2, -gca93D/2 + gca93Og,0]) cylinder(10.5,1.5,1.5);
    translate([gca93Wg/2, -gca93D/2 + gca93Og,0]) cylinder(10.5,1.5,1.5);
    translate([-gca93Wg/2, -gca93D/2 + gca93Og + gca93Dg,0]) cylinder(10.5,1.5,1.5);
    translate([gca93Wg/2, -gca93D/2 + gca93Og + gca93Dg,0]) cylinder(10.5,1.5,1.5);
  }


  difference() {
    translate([-gca93Wg/2, -gca93D/2 + gca93Og,0]) cylinder(3.5,3,3);
    translate([-gca93Wg/2, -gca93D/2 + gca93Og,0]) cylinder(10.5,1.5,1.5);
  }

  difference() {
    translate([gca93Wg/2, -gca93D/2 + gca93Og,0]) cylinder(3.5,3,3);
    translate([gca93Wg/2, -gca93D/2 + gca93Og,0]) cylinder(10.5,1.5,1.5);
  }

  difference() {
    translate([-gca93Wg/2, -gca93D/2 + gca93Og + gca93Dg,0]) cylinder(3.5,3,3);
    translate([-gca93Wg/2, -gca93D/2 + gca93Og + gca93Dg,0]) cylinder(10.5,1.5,1.5);
  }
  difference() {
    translate([gca93Wg/2, -gca93D/2 + gca93Og + gca93Dg,0]) cylinder(3.5,3,3);
    translate([gca93Wg/2, -gca93D/2 + gca93Og + gca93Dg,0]) cylinder(10.5,1.5,1.5);
  }

  
}


if( gca79 ) {
  difference() {
    translate([-(gca79W+4)/2,-(gca79D+4)/2,0]) cube([gca79W+4, gca79D+4, 5]);
    translate([-gca79W/2,-gca79D/2,1.5]) cube([gca79W, gca79D, 10]);

    translate([-(gca79W-18)/2,-(gca79D-18)/2,0]) cube([gca79W-18, gca79D-18, 10]);
    
    translate([0,gca79Dg/2,-1]) cylinder(10,1.5,1.5);
    translate([0,-gca79Dg/2,-1]) cylinder(10,1.5,1.5);
    
    translate([-gca79Wg/2,-gca79Dg/2,-1]) cylinder(10,1.5,1.5);
    translate([-gca79Wg/2,gca79Dg/2,0]) cylinder(10,1.5,1.5);
    translate([gca79Wg/2,-gca79Dg/2,0]) cylinder(10,1.5,1.5);
    translate([gca79Wg/2,gca79Dg/2,0]) cylinder(10,1.5,1.5);
  }

  difference() {
    translate([-gca79Wg/2,-gca79Dg/2,0]) cylinder(3.5,3,3);
    translate([-gca79Wg/2,-gca79Dg/2,-1]) cylinder(10,1.5,1.5);
  }
  difference() {
    translate([-gca79Wg/2,gca79Dg/2,0]) cylinder(3.5,3,3);
    translate([-gca79Wg/2,gca79Dg/2,0]) cylinder(10,1.5,1.5);
  }
  difference() {
    translate([gca79Wg/2,-gca79Dg/2,0]) cylinder(3.5,3,3);
    translate([gca79Wg/2,-gca79Dg/2,0]) cylinder(10,1.5,1.5);
  }
  difference() {
    translate([gca79Wg/2,gca79Dg/2,0]) cylinder(3.5,3,3);
    translate([gca79Wg/2,gca79Dg/2,0]) cylinder(10,1.5,1.5);
  }
}




if( bottom ) {
  diff = (pi02D-wio01D)/2;
  difference() {
    translate([-(pi02W+4)/2,-(pi02D+4+(pi02D-wio01D))/2,0]) cube([pi02W+4, pi02D+4, 1.5]);
    translate([-(wio01W-14)/2,-(wio01D-14)/2-diff,0]) cube([wio01W-14, wio01D-14, 36]);

    translate([0,27-diff,0]) cylinder(5,1.5,1.5);
    translate([0,-27-diff,0]) cylinder(5,1.5,1.5);
    
    translate([-pi02Wg/2,-pi02Dg/2-diff,0]) cylinder(6,1.5,1.5);
    translate([-pi02Wg/2,pi02Dg/2-diff,0]) cylinder(6,1.5,1.5);
    translate([pi02Wg/2,-pi02Dg/2-diff,0]) cylinder(6,1.5,1.5);
    translate([pi02Wg/2,pi02Dg/2-diff,0]) cylinder(6,1.5,1.5);
  }
  difference() {
    translate([-pi02Wg/2,-pi02Dg/2-diff,0]) cylinder(4,4,4);
    translate([-pi02Wg/2,-pi02Dg/2-diff,0]) cylinder(5,1.5,1.5);
  }
  difference() {
    translate([-pi02Wg/2,pi02Dg/2-diff,0]) cylinder(4,4,4);
    translate([-pi02Wg/2,pi02Dg/2-diff,0]) cylinder(5,1.5,1.5);
  }
  difference() {
    translate([pi02Wg/2,-pi02Dg/2-diff,0]) cylinder(4,4,4);
    translate([pi02Wg/2,-pi02Dg/2-diff,0]) cylinder(5,1.5,1.5);
  }
  difference() {
    translate([pi02Wg/2,pi02Dg/2-diff,0]) cylinder(4,4,4);
    translate([pi02Wg/2,pi02Dg/2-diff,0]) cylinder(5,1.5,1.5);
  }


  translate([-(pi02W)/2,-(wio01D+4)/2,24]) cube([pi02W, 2, 4]);
  translate([-(pi02W)/2,(wio01D+4)/2-2,24]) cube([pi02W, 2, 4]);
  
  difference() {
    translate([-(pi02W+4)/2,-(wio01D+4)/2,0]) cube([2, wio01D+4, 28.0]);
    translate([-(wio01W)/2,-(wio01D-14)/2,6]) cube([wio01W, wio01D-14, 20]);
  }
  
  difference() {
    translate([(pi02W+4)/2-2,-(wio01D+4)/2,0]) cube([2, wio01D+4, 28.0]);
    translate([-(wio01W)/2,-(wio01D-14)/2,6]) cube([wio01W, wio01D-14, 20]);
  }

  translate([(pi02W+4)/2-2, -(pi02D+4)/2+6.5, 21]) rotate([90,0,90]) prism(wio01D+4, 7, 6.2);
  mirror([1,0,0]) translate([(pi02W+4)/2-2, -(pi02D+4)/2+6.5, 21]) rotate([90,0,90]) prism(wio01D+4, 7, 6.2);
}



if( top ) {
  difference() {
    translate([-(wio01W+4)/2,-(wio01D+4)/2,28]) cube([wio01W+4, wio01D+4, 1.5]);
    translate([-(wio01W-14)/2,-(wio01D-14)/2,0]) cube([wio01W-14, wio01D-14, 36]);

    translate([-wio01Wg/2,-wio01Dg/2,28]) cylinder(5,1,1);
    translate([-wio01Wg/2,wio01Dg/2,28]) cylinder(5,1,1);
    translate([wio01Wg/2,-wio01Dg/2,28]) cylinder(5,1,1);
    translate([wio01Wg/2,wio01Dg/2,28]) cylinder(5,1,1);
  }
  
  difference() {
    translate([-wio01Wg/2,-wio01Dg/2,28]) cylinder(3,3,3);
    translate([-wio01Wg/2,-wio01Dg/2,28]) cylinder(5,1,1);
  }
  difference() {
    translate([-wio01Wg/2,wio01Dg/2,28]) cylinder(3,3,3);
    translate([-wio01Wg/2,wio01Dg/2,28]) cylinder(5,1,1);
  }
  difference() {
    translate([wio01Wg/2,-wio01Dg/2,28]) cylinder(3,3,3);
    translate([wio01Wg/2,-wio01Dg/2,28]) cylinder(5,1,1);
  }
  difference() {
    translate([wio01Wg/2,wio01Dg/2,28]) cylinder(3,3,3);
    translate([wio01Wg/2,wio01Dg/2,28]) cylinder(5,1,1);
  }
}

if( pi02 ) {
  difference() {
    translate([-pi02W/2,-pi02D/2,3]) cube([pi02W, pi02D, 1.5]);

    translate([-pi02Wg/2,-pi02Dg/2,0]) cylinder(10,2.5,2.5);
    translate([-pi02Wg/2,pi02Dg/2,0]) cylinder(10,2.5,2.5);
    translate([pi02Wg/2,-pi02Dg/2,0]) cylinder(10,2.5,2.5);
    translate([pi02Wg/2,pi02Dg/2,0]) cylinder(10,2.5,2.5);
  }
}


if( wio01 ) {
  difference() {
    translate([-wio01W/2,-wio01D/2,31]) cube([wio01W, wio01D, 1.5]);
    
    translate([-wio01Wg/2,-wio01Dg/2,30]) cylinder(50,1.5,1.5);
    translate([-wio01Wg/2,wio01Dg/2,30]) cylinder(50,1.5,1.5);
    translate([wio01Wg/2,-wio01Dg/2,30]) cylinder(50,1.5,1.5);
    translate([wio01Wg/2,wio01Dg/2,30]) cylinder(50,1.5,1.5);
  }
}
