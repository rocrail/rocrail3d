/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../lib/shapes.scad>

/*
Afstand busjes voor SPAX 2 x 16 schroeven.
*/
SV=0; // stroom verzorging
SV_W=19;

KD=1; // kabel doorvoer

M4=0;
M3=0;
H=6;

if( KD ) {
  W=160;
  difference() { 
    translate([0,0,0]) rcube(2,W, 14, 5);
    translate([20,14/2+4,0]) cylinder(6, 2.5/2, 2.5/2);
    translate([20,14/2-4,0]) cylinder(6, 2.5/2, 2.5/2);
    translate([W-20,14/2+4,0]) cylinder(6, 2.5/2, 2.5/2);
    translate([W-20,14/2-4,0]) cylinder(6, 2.5/2, 2.5/2);
  }

    translate([0,14/2+1,0]) rotate([0,0,270]) prism(2,4, 10);
    translate([W,14/2-1,0]) rotate([0,0,90]) prism(2,4, 10);
  
  WD=(W-20)/5;
  difference() { 
    translate([4,14/2-1,0]) cube([W-8, 2, 10]);
    translate([10,14/2+2,5.5]) rotate([90,90,0]) cylinder(6, 3, 3);
    translate([10+WD*1,14/2+2,5.5]) rotate([90,90,0]) cylinder(6, 3, 3);
    translate([10+WD*2,14/2+2,5.5]) rotate([90,90,0]) cylinder(6, 3, 3);
    translate([10+WD*3,14/2+2,5.5]) rotate([90,90,0]) cylinder(6, 3, 3);
    translate([10+WD*4,14/2+2,5.5]) rotate([90,90,0]) cylinder(6, 3, 3);
    translate([W-10,14/2+2,5.5]) rotate([90,90,0]) cylinder(6, 3, 3);
  }
}

if( SV ) {
  difference() { 
    translate([0,0,0]) cube([SV_W+2*2, 10, 6]);
    translate([2,0,4]) cube([SV_W, 8, 2]);
    translate([2+SV_W/2,5,0]) cylinder(10, 3/2, 3/2);
  }
}


if( M3 ) {
  difference() { 
    translate([0,0,0]) rotate([0,0,0]) cylinder(6, 7/2, 5/2);
    translate([0,0,0]) rotate([0,0,0]) cylinder(6, 2.5/2, 2.5/2);
  }
  difference() { 
    translate([10,0,0]) rotate([0,0,0]) cylinder(2, 5.5/2, 5.5/2);
    translate([10,0,0]) rotate([0,0,0]) cylinder(2, 2.5/2, 2.5/2);
    translate([10,0,1]) rotate([0,0,0]) cylinder(1, 2.5/2, 4/2);
  }
}


if( M4 ) translate([0,10,0]) {
  difference() { 
    translate([0,0,0]) rotate([0,0,0]) cylinder(6, 7/2, 7/2);
    translate([0,0,0]) rotate([0,0,0]) cylinder(6, 2.5/2, 2.5/2);
  }
  difference() { 
    translate([0,0,0]) rotate([0,0,0]) cylinder(7, 4/2, 4/2);
    translate([0,0,0]) rotate([0,0,0]) cylinder(7, 2.5/2, 2.5/2);
  }
  difference() { 
    translate([10,0,0]) rotate([0,0,0]) cylinder(2, 7/2, 7/2);
    translate([10,0,0]) rotate([0,0,0]) cylinder(2, 2.5/2, 2.5/2);
    translate([10,0,1]) rotate([0,0,0]) cylinder(1, 2.5/2, 4/2);
  }
}