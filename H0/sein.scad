/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
//$fn=64;
include <../lib/shapes.scad>


LEDR=3.2/2;
KAPH=10;

FRONT=0;
BACK=0;
MAST=0;
SOCKET=0;

LEDRING=1;

module LEDkap(x,y,z) {
  translate([x,y,z]) {
    difference() {
      translate([0,0,0]) cylinder(KAPH, LEDR+.4, LEDR+.4);
      translate([0,0,0]) cylinder(KAPH, LEDR, LEDR);
      translate([-10,-16,KAPH/2]) rotate([-45,0,0]) cube([20,12,30]);
    }
  }
}


if( FRONT ) {
  difference() {
    translate([-4,-10,0]) rcube(.8,8,20,2);
    translate([0,0,0]) cylinder(KAPH, LEDR, LEDR);
    translate([0,LEDR*4,0]) cylinder(KAPH, LEDR, LEDR);
    translate([0,-LEDR*4,0]) cylinder(KAPH, LEDR, LEDR);
  }
  translate([0,0,.8]) LEDkap(0,0,0);
  translate([0,LEDR*4,.8]) LEDkap(0,0,0);
  translate([0,-LEDR*4,.8]) LEDkap(0,0,0);
}

if( BACK ) translate([20,0,0]) {
  H=5.5;
  difference() {
    translate([-4-.8,-10-.8,0]) rcube(H,8+2*.8,20+2*.8,2.4);
    translate([-4.1,-10.1,H-.8]) rcube(H,8+.2,20+.2,2);
    translate([-4+.6,-10+.6,.8]) rcube(6,8-2*.6,20-2*.6,2);
    translate([0,-4,1.5+.8]) rotate([90,90,0]) cylinder(10, 1.5, 1.5);
  }

  difference() {
    translate([0,-9.5,1.5+.8]) rotate([90,90,0]) cylinder(4, 1.5, 1.5);
    translate([0,-4,1.5+.8]) rotate([90,90,0]) cylinder(10, 1.25, 1.25);
  }
}

if( MAST ) translate([40,0,0]) {
  H=40;
  difference() {
    translate([0,-9.5,1.5]) rotate([90,90,0]) cylinder(H, 2.2, 2.2);
    translate([0,-9.5,1.5]) rotate([90,90,0]) cylinder(H, 1.6, 1.6);
  }
}

if( SOCKET ) translate([60,0,0]) {
  H=4;
  B=12;
  difference() {
    translate([0,0,0]) rcube(H,B,20,2.4);
    translate([.8,.8,1]) rcube(H,B-2*.8,20-2*.8,2);
    translate([B/2,20/2,0]) cylinder(H, 2.3, 2.3);
    
    translate([B/2,4,0]) cylinder(1, 2, 1.3);
    translate([B/2,20-4,0]) cylinder(1, 2, 1.3);
  }

  difference() {
    translate([B/2,20/2,0]) cylinder(H, 3, 3);
    translate([B/2,20/2,0]) cylinder(H, 2.3, 2.3);
  }

  difference() {
    translate([B/2,4,1]) cylinder(H-1, 2, 2);
    translate([B/2,4,1]) cylinder(H-1, 1.3, 1.3);
  }
  difference() {
    translate([B/2,20-4,1]) cylinder(H-1, 2, 2);
    translate([B/2,20-4,1]) cylinder(H-1, 1.3, 1.3);
  }
}


if( LEDRING ) translate([80,0,0]) {
  H=4.5;
  difference() {
    translate([0,0,0]) cylinder(H, 4.6/2, 4.6/2);
    translate([0,0,.8]) cylinder(H, 1.7, 1.7);
    translate([1.2,0,0]) cylinder(H, .5, .5);
    translate([-1.2,0,0]) cylinder(H, .5, .5);
  }
}



