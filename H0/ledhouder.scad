/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
include <../lib/shapes.scad>

HOH=110;
HOUDER=0;
BASIS=1;


if( BASIS ) translate([0,20,0]) {
  difference() {
    translate([-1,-1,0]) cube([HOH+10+2, 10+2, 8]);
    translate([0,0,6]) cube([HOH+10, 10, 8]);
    translate([1,1,1]) cube([HOH+10-2, 10-2, 8]);

    translate([(HOH+10)/2,10/2,0]) cylinder(4,2,2);

    translate([5+HOH/4,10/2,0]) cylinder(4,1.5,1.5);
    translate([5+HOH-HOH/4,10/2,0]) cylinder(4,1.5,1.5);
  }
}


if( HOUDER ) translate([0,0,0]) {
  difference() {
    translate([0,0,0]) cube([HOH+10, 10, 2]);
    translate([0,0,0]) LEDHOLES(5,5);
    translate([0,0,0]) LEDHOLES(5+HOH,5); 
    translate([0,0,0]) LEDHOLES(10,5);
    translate([0,0,0]) LEDHOLES(HOH,5); 
    
    translate([(HOH+10)/2,10/2,0]) cylinder(4,2,2);

    translate([5+HOH/4,10/2,0]) cylinder(4,1.5,1.5);
    translate([5+HOH-HOH/4,10/2,0]) cylinder(4,1.5,1.5);
  }
  
  translate([0,0,12]) LEDCUP(5,5,false);
  translate([0,0,12]) LEDCUP(5+HOH,5,false);
  
  difference() {
    translate([5,10/2,2]) cylinder(10,4,4);
    translate([0,0,2]) LEDHOLES(5,5);
  }
  
  difference() {
    translate([HOH+5,10/2,2]) cylinder(10,4,4);
    translate([HOH,0,2]) LEDHOLES(5,5);
  }
}