/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2024 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
$fn=128;
//$fn=64;
include <../lib/shapes.scad>


TYPE1=0;
TYPE2=1;

LED=3.2/2;
COMP=1.5/2;

// Speissewagen
/*
L=220;
LED1=20;
LED2=45;
LED3=70;
LED4=95;
*/

// D-Wagen
/*
L=220;
LED1=25;
LED2=50;
LED3=75;
LED4=95;
LED4B=100; // Gepäckwagen
CUT=100;
*/

// NL-Post
L=220;
LED1=20;
LED2=40;
LED3=60;
LED4=95;
LED4B=95;
CUT=110;


// D-Wagen Kurz
/*
L=200;
LED1=25;
LED2=45;
LED3=65;
LED4=85;
LED4B=85;
CUT=100;
*/

if( TYPE2 ) {
  W=25;

  if( CUT > 0 ) {
    difference() {
      translate([CUT-5,1,1.6]) cube([10, W-2, 1.2]);
      translate([LED4,W/2,0]) cylinder(5, LED+1, LED+1);
    }
  }
  
  difference() {
    translate([0,0,0]) cube([L, W, 1.6]);
    if( CUT > 0 )
      translate([CUT-.5,0,0]) cube([1, W, 1.6]);
    
    // Bevestigingsgaten
    translate([14,W/2,0]) cylinder(2, 1.0, 1.0);
    translate([L-14,W/2,0]) cylinder(2, 1.0, 1.0);

    // LEDs
    translate([LED1,W/2,0]) cylinder(2, LED, LED);
    translate([LED2,W/2,0]) cylinder(2, LED, LED);
    translate([LED3,W/2,0]) cylinder(2, LED, LED);
    translate([LED4,W/2,0]) cylinder(2, LED, LED);

    translate([L-LED4B,W/2,0]) cylinder(2, LED, LED);
    translate([L-LED3,W/2,0]) cylinder(2, LED, LED);
    translate([L-LED2,W/2,0]) cylinder(2, LED, LED);
    translate([L-LED1,W/2,0]) cylinder(2, LED, LED);

    // Elco
    translate([L-4,W/2,0]) cylinder(2, COMP, COMP);
    translate([L-4,W/2-6,0]) cylinder(2, COMP, COMP);
    
    // Diode
    translate([L-35,W/2+4,0]) cylinder(2, COMP, COMP);
    translate([L-35,W/2-4,0]) cylinder(2, COMP, COMP);
    
    // Weerstanden
    translate([5,W/2+4,0]) cylinder(2, COMP, COMP);
    translate([5,W/2-4,0]) cylinder(2, COMP, COMP);
    
    // Kabel opening
    translate([L-30,W/4,0]) cylinder(2, 2, 2);
    // Kabel opening
    translate([30,W/4,0]) cylinder(2, 2, 2);
  }
}



if( TYPE1 ) {
  L=200;
  W=25;

  difference() {
    translate([0,0,0]) cube([L, W, 2]);
    
    // Bevestigingsgaten
    translate([72,W/2,0]) cylinder(2, 1.0, 1.0);
    translate([128,W/2,0]) cylinder(2, 1.0, 1.0);
    
    // LEDs
    translate([20,W/2,0]) cylinder(2, LED, LED);
    translate([45,W/2,0]) cylinder(2, LED, LED);
    translate([65,W/2,0]) cylinder(2, LED, LED);
    translate([45,W/2,0]) cylinder(2, LED, LED);
    translate([90,W/2,0]) cylinder(2, LED, LED);
    translate([110,W/2,0]) cylinder(2, LED, LED);
    translate([135,W/2,0]) cylinder(2, LED, LED);
    translate([155,W/2,0]) cylinder(2, LED, LED);
    translate([175,W/2,0]) cylinder(2, LED, LED);
    //translate([190,W/2,0]) cylinder(2, LED, LED);

    // Elco
    translate([190,W/2+2,0]) cylinder(2, COMP, COMP);
    translate([190,W/2-2,0]) cylinder(2, COMP, COMP);
    // Diode
    translate([195,W/2+4,0]) cylinder(2, COMP, COMP);
    translate([195,W/2-4,0]) cylinder(2, COMP, COMP);
    // Weerstande
    translate([15,W/2+4,0]) cylinder(2, COMP, COMP);
    translate([15,W/2-4,0]) cylinder(2, COMP, COMP);
    
    // Kabel opening
    translate([190,W/4,0]) cylinder(2, LED, LED);
    // Kabel opening
    translate([5,W/2,0]) cylinder(2, 3, 3);
  }
}

